
function select_tscreensyncsubject(num) {
	if ( num == 'user' ){
		$("#TScreensyncUserId").removeAttr("disabled");
		$("#TScreensyncUserId").attr("required", "required");
		$("#TScreensyncUserId").css("background-color","#ffffff");
		$("#TScreensyncGroupId").attr("disabled", "disabled");
		$("#TScreensyncGroupId").css("background-color","#d2d1c6");
	} else if ( num == 'group' ) {
		$("#TScreensyncUserId").attr("disabled", "disabled");
		$("#TScreensyncUserId").removeAttr("required");
		$("#TScreensyncUserId").css("background-color","#d2d1c6");
		$("#TScreensyncGroupId").removeAttr("disabled");
		$("#TScreensyncGroupId").css("background-color","#ffffff");

	}
}
function select_tsubmitsubject(num) {
	if ( num == 'user' ){
		$("#TSubmitUserId").removeAttr("disabled");
		$("#TSubmitUserId").attr("required", "required");
		$("#TSubmitUserId").css("background-color","#ffffff");
		$("#TSubmitGroupId").attr("disabled", "disabled");
		$("#TSubmitGroupId").css("background-color","#d2d1c6");
	} else if ( num == 'group' ) {
		$("#TSubmitUserId").attr("disabled", "disabled");
		$("#TSubmitUserId").removeAttr("required");
		$("#TSubmitUserId").css("background-color","#d2d1c6");
		$("#TSubmitGroupId").removeAttr("disabled");
		$("#TSubmitGroupId").css("background-color","#ffffff");

	}
}
function select_mschedulesubject(num) {
	if ( num == 'user' ){
		$("#MScheduleUserId").removeAttr("disabled");
		$("#MScheduleUserId").attr("required", "required");
		$("#MScheduleUserId").css("background-color","#ffffff");
		$("#MScheduleGroupId").attr("disabled", "disabled");
		$("#MScheduleGroupId").css("background-color","#d2d1c6");
	} else if ( num == 'group' ) {
		$("#MScheduleUserId").attr("disabled", "disabled");
		$("#MScheduleUserId").removeAttr("required");
		$("#MScheduleUserId").css("background-color","#d2d1c6");
		$("#MScheduleGroupId").removeAttr("disabled");
		$("#MScheduleGroupId").css("background-color","#ffffff");

	}
}

function toggleEndDatetime(type) {
	$("dt#check_user,dd#check_user").hide(); // 採点者を非表示
	if (type == '0') {
		$("select[id^='TSubmitEndDatetime']").attr('disabled', true);
		$("select[id^='TSubmitEndDatetime']").css("background-color","#d2d1c6");
	} else {
		$("select[id^='TSubmitEndDatetime']").removeAttr('disabled');
		$("select[id^='TSubmitEndDatetime").css("background-color","#ffffff");
		$("dt#check_user,dd#check_user").toggle(); // 採点者を表示
	}
}

function existImage(sImageUrl){
	var img = new Image();
	img.src = sImageUrl;
	return img.height > 0; // 読み込みに失敗すれば 0 になる。
}

$(function(){
	var $date_input = '<input type="hidden" class="datepicker" value="" />';
	$('.form div.input.date').append($date_input);// 日付のみの場合(date)
	$('.form div.input.datetime').find('select:eq(2)').after($date_input);// 日時も含まれる場合(dateTime)
	var icon_img = "../img/calendar.gif";
	if(!existImage(icon_img)) {
		icon_img = "../../img/calendar.gif";
	}
	var nowD = new Date(); // selectの表示に合わせて、今年・来年のみ表示
	$('input.datepicker').datepicker({
		showOn:'button',
		buttonImage: icon_img,
		buttonImageOnly: true,
		changeMonth: true,
		changeYear: true,
		showMonthAfterYear: true,
		minDate: new Date(nowD.getFullYear(), 0 , 1),
		maxDate: new Date(nowD.getFullYear()+1, 11, 31),
		beforeShow: function(){
				var date = new Date(
					$(this).prevAll("select[id$='Year']:first").val(),
					$(this).prevAll("select[id$='Month']:first").val()-1,
					$(this).prevAll("select[id$='Day']:first").val()
				);
				$(this).datepicker('setDate', date);
			},
		onClose: function(){
				var date = $(this).datepicker('getDate');
				var day = ('0' + (date.getDate())).slice(-2);
				var month = ('0' + (date.getMonth()+1)).slice(-2);
				$(this)
					.prevAll("select[id$='Year']:first").val(date.getFullYear()).end()
					.prevAll("select[id$='Month']:first").val(month).end()
					.prevAll("select[id$='Day']:first").val(day);
			}
	});

	var set_type=$("select[id='TSubmitResourceId'] option:selected").attr('class');
	toggleEndDatetime(set_type);
	$("select[id='TSubmitResourceId']").change(function(){
		var type = '';
		$("select[id='TSubmitResourceId'] option:selected").each(function () {
			type = $(this).attr('class');
		});
		toggleEndDatetime(type);
	});

	var def_num=$("input[id^='TScreensyncSubjectFlg']:checked").val();
	if ($("input[id^='TScreensyncSubjectFlg']:checked").attr("disabled") !== undefined) {
		def_num=$("input[id^='TScreensyncSubjectFlg']:checked").attr("disabled");
	}
	select_tscreensyncsubject(def_num);
	$("input[id^='TScreensyncSubjectFlg']").click(function(){
		var num = $(this).val();
		select_tscreensyncsubject(num);
	});
	var def_num=$("input[id^='TSubmitSubjectFlg']:checked").val();
	if ($("input[id^='TSubmitSubjectFlg']:checked").attr("disabled") !== undefined) {
		def_num=$("input[id^='TSubmitSubjectFlg']:checked").attr("disabled");
	}
	select_tsubmitsubject(def_num);
	$("input[id^='TSubmitSubjectFlg']").click(function(){
		var num = $(this).val();
		select_tsubmitsubject(num);
	});
	var def_num=$("input[id^='MScheduleSubjectFlg']:checked").val();
	if ($("input[id^='MScheduleSubjectFlg']:checked").attr("disabled") !== undefined) {
		def_num=$("input[id^='MScheduleSubjectFlg']:checked").attr("disabled");
	}
	select_mschedulesubject(def_num);
	$("input[id^='MScheduleSubjectFlg']").click(function(){
		var num = $(this).val();
		select_mschedulesubject(num);
	});
});
