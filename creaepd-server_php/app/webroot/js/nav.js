 /* ブラウザ判別 */
var ie=document.all ? 1 : 0;
var ns6=document.getElementById&&!document.all ? 1 : 0;
var opera=window.opera ? 1 : 0;

/* 子メニューの表示・非表示切替 */
function openchild(childObj, parentObj) {
	var parent="";
	var child="";
	var sw="/img/js/arrowinbox_on.gif"; /* 子メニューが開いてる時のアイコン */
	var hd="/img/js/arrowinbox.gif"; /* 子メニューが閉じている時のアイコン */
	if (ie || ns6 || opera) {
		child=ns6 ? document.getElementById(childObj).style : document.all(childObj).style;
		parent=ns6 ? document.getElementById(parentObj).style : document.all(parentObj).style;
		test = document.getElementById(parentObj);
		if (child.display=="none") {
			child.display="block";
			document.getElementById(parentObj).innerHtml = '-';
		} else {
			child.display="none";
			document.getElementById(parentObj).innerHtml = '+';
		}
	}
}
/* 繰り返し学習・採点設定の表示・非表示切替 */
function toggleResourceItem(value) {
	$("dt#repeat_item,dd#repeat_item").hide(); // 繰り返し学習を非表示
	$("dt#allotment_item,dd#allotment_item").hide(); // 採点設定を非表示
	if (value == '1') {
		$("dt#repeat_item,dd#repeat_item").toggle(); // 繰り返し学習を表示
		$("dt#allotment_item,dd#allotment_item").toggle(); // 採点設定を表示
	}
}
/* pdf・mp3ファイル選択の表示・非表示切替 */
function toggleResourceFile(value) {
	$("dt#resource_item0,dd#resource_item0").hide(); // pdfファイル1を非表示
	$("dt#resource_item1,dd#resource_item1").hide(); // pdfファイル2を非表示
	$("dt#resource_item2,dd#resource_item2").hide(); // mp3ファイルを非表示
	if (value == '1') {
		$("dt#resource_item0,dd#resource_item0").toggle();
	} else if (value == '2') {
		$("dt#resource_item0,dd#resource_item0").toggle();
		$("dt#resource_item1,dd#resource_item1").toggle();
	} else if (value == '3') {
		$("dt#resource_item0,dd#resource_item0").toggle();
		$("dt#resource_item2,dd#resource_item2").toggle();
	}
}
//親子チェックボックス連動処理
$(function(){
	$("dt input:checkbox").change(function(){
		// 子階層
		var child_dd = $(this).parent().next('dd');
		// 子チェックボックス一覧
		var child_checkbox = $(this).parent().next().find("dt input:checkbox");


		if ($(this).is(':checked')) {
			// 子階層をチェック
			child_checkbox.prop("checked", true);
			// 子階層を開く
			if (!$.isEmptyObject(child_dd.css) && child_dd.css('display', 'none')) {
				openchild(child_dd.attr('id'), $(this).attr('id'));
			}
		}
	    else {
	    	// 子階層のチェックを外す
	    	child_checkbox.removeAttr('checked');
	    	// 子階層を閉じる
			if (!$.isEmptyObject(child_dd.css) && !child_dd.css('display', 'none')) {
				openchild(child_dd.attr('id'), $(this).attr('id'));
			}
	    }
		// 同階層が全てチェックが外れた場合は閉じる
        var self_dl = $(this).parent().parent();
		var parent_dt = self_dl.parent().prev(); // 親階層
		if (!$.isEmptyObject(parent_dt)) {
			parent_checkbox = parent_dt.children("input:checkbox");
			if (!$.isEmptyObject(parent_checkbox)) {
				child_list = self_dl.find("dt input:checkbox");
				if (!$.isEmptyObject(child_list)) {
					var exists_check = false;
					for(var i = 0 ; i < child_list.length ; i++){
						if (child_list.eq(i).is(':checked')) { // 一つでもチェック済みならチェック終了
							exists_check = true;
							break;
						}
					}
					if (exists_check) {
						// 一つでもチェックされたら親も連動させる
						parent_checkbox.eq(0).prop("checked", true);
					} else {
//							//全てチェックが外れているので閉じる
//							openchild(self_dl.parent().attr('id'), parent_checkbox.eq(0).attr('id'));
						// ↑閉じてしまっても良いのですが、一つだけ残したいのに誤って全部外してしまった場合、1からやり直しになるのでコメントアウトしておきます
						//親のチェックも外す
						parent_checkbox.eq(0).removeAttr('checked');
					}
				}
			}
	    }
	});

	// メニュー▼切り替え
	$("div#menuA dt:not(dt.link)").on("click", function() {
		if($(this).has("span.home").length === 0){
			$(this).next().slideToggle();
		}
		$(this).toggleClass("active");
	});

	$('a[rel*=leanModal]').leanModal({ top : 200, closeButton: ".modal_close" });
	$('a#subjects_button').click(function(){
		var selectVal = $("select#MResourceSubjectId").val();
//		$("input[name='data[MResource][subject_id][]']").val([selectVal]);
		$("input[name='data[MResource][subject_id][]']").removeAttr ('checked');
		$.each(selectVal, function(i, value) {
			// jquery1.9でpropでチェックボックス選択
			$("input[name='data[MResource][subject_id][]'][value='"+value+"']").prop("checked", true);
		});
	});
	$('button#subjects_button').click(function(){
		var inputVal = [];
		$("input[name='data[MResource][subject_id][]']:checked").each(function(){
			inputVal.push($(this).val());
		});
		$("select[name='data[MResource][subject_id][]']").val(inputVal);
	});
	$('a#resources_button').click(function(){
		var selectVal = $("select#TSubmitResourceId").val();
		$("input[name='data[TSubmit][resource_id]']").val([selectVal]);
	});
	$('button#resources_button').click(function(){
		var inputVal = $("input[name='data[TSubmit][resource_id]']:checked").val();
		$("select[name='data[TSubmit][resource_id]']").val([inputVal]);
		$("select[name='data[TSubmit][resource_id]']").trigger("change");
	});
	$('a#resources_button').click(function(){
		var selectVal = $("select#TScreensyncResourceId").val();
		$("input[name='data[TScreensync][resource_id]']").val([selectVal]);
	});
	$('button#resources_button').click(function(){
		var inputVal = $("input[name='data[TScreensync][resource_id]']:checked").val();
		$("select[name='data[TScreensync][resource_id]']").val([inputVal]);
		$("select[name='data[TScreensync][resource_id]']").trigger("change");
	});
	$('a#resources_button').click(function(){
		var selectVal = $("select#TScreensyncFileResourceId").val();
		$("input[name='data[TScreensyncFile][resource_id]']").val([selectVal]);
	});
	$('button#resources_button').click(function(){
		var inputVal = $("input[name='data[TScreensyncFile][resource_id]']:checked").val();
		$("select[name='data[TScreensyncFile][resource_id]']").val([inputVal]);
		$("select[name='data[TScreensyncFile][resource_id]']").trigger("change");
	});

	$("a.resource_modal").click(function(){
		var view_url = $(this).attr('id');
		get_html(view_url);
	});

	$("#resources_modal").leanModal({ top : 200, closeButton: ".modal_close" });

	// 教材タイプによる項目切り替え
	var set_value=$("select[id='MResourceResourceType'] option:selected").val();
	toggleResourceItem(set_value);
	if(set_value == null || set_value == undefined) {
		var set_value=$("input:hidden[id='MResourceResourceType']").val();
		toggleResourceItem(set_value);
	}
	$("select[id='MResourceResourceType']").change(function(){
		var value = '0';
		$("select[id='MResourceResourceType'] option:selected").each(function () {
			value = $(this).val();
		});
		toggleResourceItem(value);
	});

	// 教材スタイルによる項目切り替え
	var set_value=$("select[id='MResourceStyleId'] option:selected").val();
	toggleResourceFile(set_value);
	if(set_value == null || set_value == undefined) {
		var set_value=$("input:hidden[id='MResourceStyleId']").val();
		toggleResourceFile(set_value);
	}
	$("select[id='MResourceStyleId']").change(function(){
		var value = '0';
		$("select[id='MResourceStyleId'] option:selected").each(function () {
			value = $(this).val();
		});
		toggleResourceFile(value);
	});
});
function get_html(view_url) {
	$.ajax({
		type: "GET",
		url: view_url,
		dataType: "html",
		success: function( html ) {
			var mainHtml = $("div#main", $(html)).html();
			mainHtml = mainHtml.replace(/onclick=\"history.back\(\);\"/g,'class="modal_close" onclick="$(\'button#resources_button\').trigger(\'click\');"')
			$('#modal_content').html( mainHtml );
			$('#resources_modal').trigger("click");
		}
	});
}