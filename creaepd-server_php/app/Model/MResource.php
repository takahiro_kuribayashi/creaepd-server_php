<?php
App::uses('AppModel', 'Model');
/**
 * MResource Model
 *
 */
class MResource extends AppModel {
	public $name = 'MResource';

/**
 * Search plugin
 *
 * @var array
 */
	public $actsAs = array(
		'Search.Searchable',
		'Upload.Upload' => array(
			'resource_file_name'
		)
	);

/**
 * Validation rules
 *
 * @var array
 */
	function beforeValidate()
	{
		$this->validate['resource_name']['notEmpty']['message'] = sprintf($GLOBALS['message']->getMessage(MESSAGE_LOGIN_ERROR_007), "20");
	}
	public $validate = array(
		'resource_ids' => array(
			'notEmpty' => array(
				'rule' => array('notEmpty'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'resource_name' => array(
			'notEmpty' => array(
				'rule'	  => array( 'maxLengthJP', "20"),
				'message' => '全角・半角20文字以内で入力して下さい。',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'resource_type' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'repeat_flg' => array(
			'boolean' => array(
				'rule' => array('boolean'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'version' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'create_user_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'subject' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'style' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'delete_flg' => array(
			'boolean' => array(
				'rule' => array('boolean'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);

	public function validates($options = array()) { // 親を呼び出す
		$errors = parent::validates($options);
			if ($errors) {
			if( isset($this->data['MResourceFile']['0']) ) {
				$resource_file_name1 = isset($this->data['MResourceFile']['0']['resource_file_name']['name']) ?
				$this->data['MResourceFile']['0']['resource_file_name']['name']: $this->data['MResourceFile']['0']['resource_file_name'];
				// 教材登録時にpdfファイル１の必須チェック
				if ($resource_file_name1 === '') {
					$errors = false;
					$this->MResourceFile->validationErrors['0']['resource_file_name'] =
					sprintf($GLOBALS['message']->getMessage(MESSAGE_LOGIN_ERROR_013), $GLOBALS['message']->getMessage(MESSAGE_RESOURCE_ITEM_006));
				} else {
					$info = new SplFileInfo($resource_file_name1);
					$ext = $info->getExtension();
				
					if( $ext !== "pdf" ) {
						$errors = false;
						$this->MResourceFile->validationErrors['0']['resource_file_name'] =
						sprintf($GLOBALS['message']->getMessage(MESSAGE_LOGIN_ERROR_005), $GLOBALS['message']->getMessage(MESSAGE_RESOURCE_ITEM_006));
					}
				}
			}

			if( isset($this->data['MResourceFile']['1']) ) {
				$resource_file_name2 = isset($this->data['MResourceFile']['1']['resource_file_name']['name']) ?
					$this->data['MResourceFile']['1']['resource_file_name']['name']: $this->data['MResourceFile']['1']['resource_file_name'];
				// 長文問題登録時にpdfファイル2の必須チェック
				if (($this->data['MResource']['style_id'] == 2 ) && $resource_file_name2 === '') {
					$errors = false;
					$this->MResourceFile->validationErrors['1']['resource_file_name'] =
					sprintf($GLOBALS['message']->getMessage(MESSAGE_LOGIN_ERROR_013), $GLOBALS['message']->getMessage(MESSAGE_RESOURCE_ITEM_007));
				} else {
					if (($this->data['MResource']['style_id'] == 2 )) {
						$info = new SplFileInfo($resource_file_name2);
						$ext = $info->getExtension();
	
						if( $ext !== "pdf" ) {
							$errors = false;
							$this->MResourceFile->validationErrors['1']['resource_file_name'] =
							sprintf($GLOBALS['message']->getMessage(MESSAGE_LOGIN_ERROR_005), $GLOBALS['message']->getMessage(MESSAGE_RESOURCE_ITEM_007));
						}
					}
				}
			}

			if( isset($this->data['mp3']) ) {
				// 教材登録時にmp3ファイルの拡張子チェック
				if (is_array($this->data['mp3'] )) {
					foreach($this->data['mp3'] as $idx=>$mp3File){
						if(!empty($mp3File['resource_file_name']['name'])) {
							$info = new SplFileInfo($mp3File['resource_file_name']['name']);
							$ext = $info->getExtension();
				
							if( $ext !== "mp3" ) {
								$errors = false;
								$this->MResourceFile->validationErrors['mp3'][$idx]['resource_file_name'] =
								sprintf($GLOBALS['message']->getMessage(MESSAGE_LOGIN_ERROR_005), $GLOBALS['message']->getMessage(MESSAGE_RESOURCE_ITEM_008));
							}
						}
					}
				}
			}
		}
		return $errors;
	}

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'ResourceSubject' => array(
			'className' => 'MResourceSubject',
			'foreignKey' => 'subject_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'ResourceStyle' => array(
			'className' => 'MResourceStyle',
			'foreignKey' => 'style_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);

	public $hasMany = array(
		'MResourceFile' => array(
			'className' => 'MResourceFile',
			'foreignKey' => 'resource_id',
			'conditions' => '',
			'order' => '',
			'dependent' => false,
			'exclusive' => true,
			'finderQuery' => ''
		),
		'MResourceSubtitle' => array(
				'className' => 'MResourceSubtitle',
				'foreignKey' => 'resource_id',
				'conditions' => '',
				'order' => '',
				'dependent' => false,
				'exclusive' => true,
				'finderQuery' => ''
		)
	);


	// 検索対象のフィルタ設定
	public $filterArgs = array(
			array('name' => 'resource_ids', 'type' => 'like', 'field' => 'MResource.resource_ids'),
			array('name' => 'resource_name', 'type' => 'like', 'field' => 'MResource.resource_name'),
			array('name' => 'resource_type', 'type' => 'value', 'field' => 'MResource.resource_type'),
			array('name' => 'subject_id', 'type' => 'value', 'field' => 'MResource.subject_id'),
			array('name' => 'style_id', 'type' => 'value', 'field' => 'MResource.style_id'),
	);

	// 検索対象のフィールド設定
	public $presetVars = array(
			array('field' => 'resource_ids', 'type' => 'value', 'empty' => true),
			array('field' => 'resource_name', 'type' => 'value', 'empty' => true),
			array('field' => 'resource_type', 'type' => 'value', 'empty' => true),
			array('field' => 'subject_id', 'type' => 'value', 'empty' => true),
			array('field' => 'style_id', 'type' => 'value', 'empty' => true),
	);
}
