<?php
App::uses('AppModel', 'Model');
/**
 * TSubmit Model
 *
 */
class TSubmit extends AppModel {
	public $name = 'TSubmit';
	public $recursive = 2;

	/**
	 * Search plugin
	 *
	 * @var array
	 */
	public $actsAs = array(
			'Search.Searchable'
	);

	public $id_list = array();

/**
 * After save
 *
 * @var boolean
 */
	function afterSave($created)
	{
		if($created)
		{
			$this->id_list[] = $this->getInsertID();
		}
		return true;
	}

/**
 * Validation rules
 *
 * @var array
 */
	function beforeValidate()
	{
		$this->validate['start_datetime']['datetime']['message'] = $GLOBALS['message']->getMessage(MESSAGE_LOGIN_ERROR_010);
		$this->validate['end_datetime']['datetime']['message'] = $GLOBALS['message']->getMessage(MESSAGE_LOGIN_ERROR_010);
	}

	public $validate = array(
		'submit_no' => array(
			'notEmpty' => array(
				'rule' => array('notEmpty'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'resource_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'user_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'group_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'check_user_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'submit_state' => array(
			'notEmpty' => array(
				'rule' => array('notEmpty'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'check_state' => array(
			'notEmpty' => array(
				'rule' => array('notEmpty'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'result' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'unsubscribe' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'start_datetime' => array(
			'datetime' => array(
				'rule' => array('datetime'),
				'message' => '有効な日付を入力してください',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'end_datetime' => array(
			'datetime' => array(
				'rule' => array('datetime'),
				'message' => '有効な日付を入力してください',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);

	public function validates($options = array()) { // 親を呼び出す
		$errors = parent::validates($options);
		if ($errors) {
			// 配信開始日時が配信終了日時より後になっているか
			if ($this->data['TSubmit']['start_datetime'] > $this->data['TSubmit']['end_datetime']) {
				$errors = false;
				$this->validationErrors['start_datetime'] =
				sprintf($GLOBALS['message']->getMessage(MESSAGE_LOGIN_ERROR_023), $GLOBALS['message']->getMessage(MESSAGE_DELIVER_ITEM_004));
			}
		}
		return $errors;
	}

	/**
	 * belongsTo associations
	 *
	 * @var array
	 */
	public $belongsTo = array(
		'mResource' => array(
			'className' => 'MResource',
			'foreignKey' => 'resource_id',
			'conditions' => '',
			'order' => '',
			'dependent' => false
		),
		'mUser' => array(
			'className' => 'MUser',
			'foreignKey' => 'user_id',
			'conditions' => '',
			'order' => '',
			'dependent' => false
		),
		'mCheckUser' => array(
			'className' => 'MUser',
			'foreignKey' => 'check_user_id',
			'conditions' => '',
			'order' => '',
			'dependent' => false
		),
		'mGroup' => array(
				'className' => 'MGroup',
				'foreignKey' => 'group_id',
				'conditions' => '',
				'order' => '',
				'dependent' => false
		),
	);

	public $hasMany = array(
		'TScore' => array(
			'className' => 'TScore',
			'foreignKey' => 'submit_id',
			'conditions' => '',
			'order' => '',
			'dependent' => false,
			'exclusive' => true,
			'finderQuery' => ''
		),
		'TScreensyncFile' => array(
			'className' => 'TScreensyncFile',
			'foreignKey' => 'submit_id',
			'conditions' => '',
			'order' => '',
			'dependent' => false,
			'exclusive' => true,
			'finderQuery' => ''
		)
	);

	// 検索対象のフィルタ設定
	public $filterArgs = array(
			array('name' => 'submit_no', 'type' => 'like', 'field' => 'TSubmit.submit_no'),
			array('name' => 'resource_id', 'type' => 'like', 'field' => 'TSubmit.resource_id'),
			array('name' => 'group_id', 'type' => 'like', 'field' => array('TSubmit.group_id','tSection.group_id')),
			array('name' => 'user_id', 'type' => 'like', 'field' => 'TSubmit.user_id'),
			array('name' => 'start_datetime', 'type' => 'query', 'method' => 'startConditions'),
			array('name' => 'start_datetime_to', 'type' => 'query', 'method' => 'startToConditions'),
			array('name' => 'end_datetime', 'type' => 'query', 'method' => 'endConditions'),
			array('name' => 'end_datetime_to', 'type' => 'query', 'method' => 'endToConditions'),
			array('name' => 'check_user_id', 'type' => 'like', 'field' => 'TSubmit.check_user_id'),
			array('name' => 'unsubscribe', 'type' => 'like', 'field' => 'TSubmit.unsubscribe'),
			array('name' => 'submit_state', 'type' => 'query', 'method' => 'submitStateConditions'),
			array('name' => 'user_ids', 'type' => 'query', 'method' => 'multiUserConditions'),
			array('name' => 'resource_name', 'type' => 'like', 'field' => 'mResource.resource_name'),
			array('name' => 'subject_id', 'type' => 'like', 'field' => 'mResource.subject_id'),
//			array('name' => 'group_id', 'type' => 'like', 'field' => 'tSection.group_id'),
			);
	//start
	public function startConditions($data = array()){
		$start = $data['start_datetime'];
		$conditions = array('start_datetime >= CAST(? AS DATETIME)'=>array($start.' 00:00:00'));
		return $conditions;
	}
	public function startToConditions($data = array()){
		$start_to = $data['start_datetime_to'];
		$conditions = array('start_datetime <= CAST(? AS DATETIME)'=>array($start_to.' 23:59:59'));
		return $conditions;
	}
	//end
	public function endConditions($data = array()){
		$end = $data['end_datetime'];
		$conditions = array('end_datetime >= CAST(? AS DATETIME)'=>array($end.' 00:00:00'));
		return $conditions;
	}
	public function endToConditions($data = array()){
		$end_to = $data['end_datetime_to'];
		$conditions = array('end_datetime <= CAST(? AS DATETIME)'=>array($end_to.' 23:59:59'));
		return $conditions;
	}
	//submit_state
	public function submitStateConditions($data = array()){
		$conditions = array();
		if (!empty($data['submit_state'])) {
			// 未着手、着手済みのみ抽出
			$conditions = array("TSubmit.submit_state <= ".$data['submit_state']);
		}
		return $conditions;
	}
	//user_ids
	public function multiUserConditions($data = array()){
		//$userids = implode(',', $data['user_ids']);
		$datas = $data['user_ids'];
		$userids = '';
		foreach ($data['user_ids'] as $data) {
			if(next($datas)){
				$userids = $userids . $data . "','";
			} else {
				$userids = $userids . $data ;
			}
		}
		$conditions = array("mUser.user_ids IN ('".$userids."')");
		return $conditions;
	}

	// 検索対象のフィールド設定
	public $presetVars = array(
			array('field' => 'submit_no', 'type' => 'value', 'empty' => true),
			array('field' => 'resource_id', 'type' => 'value', 'empty' => true),
			array('field' => 'group_id', 'type' => 'value', 'empty' => true),
			array('field' => 'user_id', 'type' => 'value', 'empty' => true),
			array('field' => 'start_datetime', 'type' => 'value', 'empty' => true),
			array('field' => 'start_datetime_to', 'type' => 'value', 'empty' => true),
			array('field' => 'end_datetime', 'type' => 'value', 'empty' => true),
			array('field' => 'end_datetime_to', 'type' => 'value', 'empty' => true),
			array('field' => 'check_user_id', 'type' => 'value', 'empty' => true),
			array('field' => 'unsubscribe', 'type' => 'value', 'empty' => true),
			array('field' => 'submit_state', 'type' => 'value', 'empty' => true),
			array('field' => 'user_ids', 'type' => 'value', 'empty' => true),
			array('field' => 'resource_name', 'type' => 'value', 'empty' => true),
			array('field' => 'subject_id', 'type' => 'value', 'empty' => true),
	);
}
