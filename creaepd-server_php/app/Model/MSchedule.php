<?php
App::uses('AppModel', 'Model');
/**
 * MSchedule Model
 *
 */
class MSchedule extends AppModel {
	public $name = 'MSchedule';

/**
 * Search plugin
 *
 * @var array
 */
	public $actsAs = array(
			'Search.Searchable'
	);

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'title';

/**
 * Validation rules
 *
 * @var array
 */
	function beforeValidate()
	{
		$this->validate['title']['notEmpty']['message'] = sprintf($GLOBALS['message']->getMessage(MESSAGE_LOGIN_ERROR_007), "20");
		$this->validate['description']['maxlength']['message'] = sprintf($GLOBALS['message']->getMessage(MESSAGE_LOGIN_ERROR_007), "200");
		$this->validate['start_datetime']['datetime']['message'] = $GLOBALS['message']->getMessage(MESSAGE_LOGIN_ERROR_010);
		$this->validate['end_datetime']['datetime']['message'] = $GLOBALS['message']->getMessage(MESSAGE_LOGIN_ERROR_010);
	}
	public $validate = array(
		'schedule_ids' => array(
			'notEmpty' => array(
				'rule' => array('notEmpty'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'title' => array(
			'notEmpty' => array(
				'rule'	  => array('maxLengthJP', "20"),
				'message' => '全角・半角20文字以内で入力して下さい。',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'detail' => array(
			'maxlength' => array(
				'rule'	  => array('maxLengthJP', "200"),
				'message' => '全角・半角200文字以内で入力して下さい。',
			),
		),
		'user_id' => array(
				'numeric' => array(
						'rule' => array('numeric'),
						//'message' => 'Your custom message here',
						//'allowEmpty' => false,
						//'required' => false,
						//'last' => false, // Stop validation after this rule
						//'on' => 'create', // Limit validation to 'create' or 'update' operations
				),
		),
		'group_id' => array(
				'numeric' => array(
						'rule' => array('numeric'),
						//'message' => 'Your custom message here',
						//'allowEmpty' => false,
						//'required' => false,
						//'last' => false, // Stop validation after this rule
						//'on' => 'create', // Limit validation to 'create' or 'update' operations
				),
		),
		'version' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'start_datetime' => array(
			'datetime' => array(
				'rule' => array('datetime'),
				'message' => '有効な日付を入力してください',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'end_datetime' => array(
			'datetime' => array(
				'rule' => array('datetime'),
				'message' => '有効な日付を入力してください',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);


	public function validates($options = array()) { // 親を呼び出す
		$errors = parent::validates($options);
		if ($errors) {
			// 開始日時が終了日時より後になっているか
			if ($this->data['MSchedule']['start_datetime'] > $this->data['MSchedule']['end_datetime']) {
				$errors = false;
				$this->validationErrors['start_datetime'] =
				sprintf($GLOBALS['message']->getMessage(MESSAGE_LOGIN_ERROR_023), $GLOBALS['message']->getMessage(MESSAGE_SCHEDULE_ITEM_004));
			}
		}
		return $errors;
	}

	// 検索対象のフィルタ設定
	public $filterArgs = array(
			array('name' => 'schedule_ids', 'type' => 'like', 'field' => 'MSchedule.schedule_ids'),
			array('name' => 'title', 'type' => 'like', 'field' => 'MSchedule.title'),
			array('name' => 'start_datetime', 'type' => 'query', 'method' => 'startConditions'),
			array('name' => 'start_datetime_to', 'type' => 'query', 'method' => 'startToConditions'),
			array('name' => 'end_datetime', 'type' => 'query', 'method' => 'endConditions'),
			array('name' => 'end_datetime_to', 'type' => 'query', 'method' => 'endToConditions'),
			array('name' => 'delete_flg', 'type' => 'like', 'field' => 'MSchedule.delete_flg'),
	);
	//start
	public function startConditions($data = array()){
		$start = $data['start_datetime'];
		$conditions = array('start_datetime >= CAST(? AS DATETIME)'=>array($start.' 00:00:00'));
		return $conditions;
	}
	public function startToConditions($data = array()){
		$start_to = $data['start_datetime_to'];
		$conditions = array('start_datetime <= CAST(? AS DATETIME)'=>array($start_to.' 23:59:59'));
		return $conditions;
	}
	//end
	public function endConditions($data = array()){
		$end = $data['end_datetime'];
		$conditions = array('end_datetime >= CAST(? AS DATETIME)'=>array($end.' 00:00:00'));
		return $conditions;
	}
	public function endToConditions($data = array()){
		$end_to = $data['end_datetime_to'];
		$conditions = array('end_datetime <= CAST(? AS DATETIME)'=>array($end_to.' 23:59:59'));
		return $conditions;
	}

	// 検索対象のフィールド設定
	public $presetVars = array(
			array('field' => 'schedule_ids', 'type' => 'value', 'empty' => true),
			array('field' => 'title', 'type' => 'value', 'empty' => true),
			array('field' => 'start_datetime', 'type' => 'value', 'empty' => true),
			array('field' => 'start_datetime_to', 'type' => 'value', 'empty' => true),
			array('field' => 'end_datetime', 'type' => 'value', 'empty' => true),
			array('field' => 'end_datetime_to', 'type' => 'value', 'empty' => true),
			array('field' => 'delete_flg', 'type' => 'value', 'empty' => true),
	);
}
