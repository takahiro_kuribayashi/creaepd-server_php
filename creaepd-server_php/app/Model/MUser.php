<?php
App::uses('AppModel', 'Model');
App::import('Message', 'Vendor');
/**
 * MUser Model
 *
 * @property UserType $UserType
 */
class MUser extends AppModel {
	public $name = 'MUser';

/**
 * Search plugin
 *
 * @var array
 */
	public $actsAs = array('Search.Searchable', 'CsvExport' => array('delimiter'  => ','), 'CsvImport' => array('delimiter'  => ','));

/**
 * Validation rules
 *
 * @var array
 */
	function beforeValidate()
	{
		$this->validate['user_ids']['alphaNumeric']['message'] = sprintf($GLOBALS['message']->getMessage(MESSAGE_LOGIN_ERROR_004), $GLOBALS['message']->getMessage(MESSAGE_USER_ITEM_001));
		$this->validate['user_ids']['isUnique']['message'] = sprintf($GLOBALS['message']->getMessage(MESSAGE_LOGIN_ERROR_006), $GLOBALS['message']->getMessage(MESSAGE_USER_ITEM_001));
		$this->validate['user_ids']['maxlength']['message'] = sprintf($GLOBALS['message']->getMessage(MESSAGE_LOGIN_ERROR_008), "20");
		$this->validate['user_name']['notEmpty']['message'] = sprintf($GLOBALS['message']->getMessage(MESSAGE_LOGIN_ERROR_003), $GLOBALS['message']->getMessage(MESSAGE_USER_ITEM_002));
		$this->validate['user_name']['maxlength']['message'] = sprintf($GLOBALS['message']->getMessage(MESSAGE_LOGIN_ERROR_007), "15");
		$this->validate['admin_address']['email']['message'] = sprintf($GLOBALS['message']->getMessage(MESSAGE_LOGIN_ERROR_005), $GLOBALS['message']->getMessage(MESSAGE_USER_ITEM_005));
		$this->validate['admin_address']['maxlength']['message'] = sprintf($GLOBALS['message']->getMessage(MESSAGE_LOGIN_ERROR_008), "128");
		$this->validate['parent_address1']['email']['message'] = sprintf($GLOBALS['message']->getMessage(MESSAGE_LOGIN_ERROR_005), $GLOBALS['message']->getMessage(MESSAGE_USER_ITEM_003));
		$this->validate['parent_address1']['maxlength']['message'] = sprintf($GLOBALS['message']->getMessage(MESSAGE_LOGIN_ERROR_008), "128");
		$this->validate['parent_address2']['email']['message'] = sprintf($GLOBALS['message']->getMessage(MESSAGE_LOGIN_ERROR_005), $GLOBALS['message']->getMessage(MESSAGE_USER_ITEM_003));
		$this->validate['parent_address2']['maxlength']['message'] = sprintf($GLOBALS['message']->getMessage(MESSAGE_LOGIN_ERROR_008), "128");
		$this->validate['auth_pass']['notEmpty']['message'] = sprintf($GLOBALS['message']->getMessage(MESSAGE_LOGIN_ERROR_003), $GLOBALS['message']->getMessage(MESSAGE_COMMON_ITEM_006));
	}

	public $validate = array(
		'user_ids' => array(
			'alphaNumeric' => array(
				'rule' => array('custom', '/^[a-zA-Z0-9]+$/'),
				//'message' => 'Your custom message here',
				'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
			'isUnique' => array(
				'rule' => array('isUnique'),
				//'message' => 'Your custom message here',
			),
			'maxlength' => array(
				'rule'	  => array('maxLengthJP', "20"),
				'message' => '半角20文字以内で入力して下さい。',
			),
		),
		'user_name' => array(
			'notEmpty' => array(
				'rule' => array('notEmpty'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
			'maxlength' => array(
				'rule'	  => array('maxLengthJP', "15"),
				'message' => '全角・半角15文字以内で入力して下さい。',
			),
		),
		'user_type_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'admin_address' => array(
			'email' => array(
				'rule' => array('email'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
			'maxlength' => array(
				'rule'	  => array('maxLengthJP', "128"),
				'message' => '半角128文字以内で入力して下さい。',
			),
		),
		'parent_address1' => array(
			'email' => array(
				'rule' => array('email'),
				//'message' => 'Your custom message here',
				'allowEmpty' => true,
				'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
			'maxlength' => array(
				'rule'	  => array('maxLengthJP', "128"),
				'message' => '半角128文字以内で入力して下さい。',
			),
		),
		'parent_address2' => array(
			'email' => array(
				'rule' => array('email'),
				//'message' => 'Your custom message here',
				'allowEmpty' => true,
				'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
			'maxlength' => array(
				'rule'	  => array('maxLengthJP', "128"),
				'message' => '半角128文字以内で入力して下さい。',
			),
		),
		'delete_flg' => array(
			'notEmpty' => array(
				'rule' => array('notEmpty'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'ins_date' => array(
			'datetime' => array(
				'rule' => array('datetime'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'ancestor_group_id' => array(
				'notEmpty' => array(
						'rule' => array('notEmpty'),
						//'message' => 'Your custom message here',
						//'allowEmpty' => false,
						//'required' => false,
						//'last' => false, // Stop validation after this rule
						//'on' => 'create', // Limit validation to 'create' or 'update' operations
				),
		),
		'auth_pass' => array(
			'notEmpty' => array(
				'rule' => array('notEmpty'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);

	public function validates($options = array()) { // 親を呼び出す
		$errors = parent::validates($options);
		if ($errors) {
			$group_id = (isset($this->data['MUser']['ancestor_group_id']) ? $this->data['MUser']['ancestor_group_id']: 0);
			$sql = "SELECT * FROM `m_groups` WHERE `m_groups`.`id` = :group_id;";
			$params = array(
					'group_id'=> $group_id
			);
			$group_data = $this->query($sql,$params);
			// ユーザ区分の上下チェック（権限の異なるグループには登録不可とする）
			if (!empty($group_data) && $this->data['MUser']['user_type_id'] !== $group_data[0]['m_groups']['group_type_id']) {
				if ($this->data['MUser']['user_type_id'] !== '9') {
					$errors = false;
					$this->validationErrors['ancestor_group_id'] =
						sprintf($GLOBALS['message']->getMessage(MESSAGE_LOGIN_ERROR_011), $GLOBALS['message']->getMessage(MESSAGE_USER_ITEM_004));
				}
			}
		}
		return $errors;
	}

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'UserType' => array(
			'className' => 'MUserType',
			'foreignKey' => 'user_type_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);

	 public $hasOne = array(
		'Auth'=>array(
			'className' => 'MAuth',
			'foreignKey'=> 'user_id',
			'type'=>'inner',
			'dependent'=> true
		)
	);

	// 検索対象のフィルタ設定
	public $filterArgs = array(
		array('name' => 'user_ids', 'type' => 'like', 'field' => 'MUser.user_ids'),
		array('name' => 'user_name', 'type' => 'like', 'field' => 'MUser.user_name'),
		array('name' => 'mail_address', 'type' => 'like', 'field' => 'MUser.parent_address'),
		array('name' => 'user_type_id', 'type' => 'value', 'field' => 'MUser.user_type_id'),
	);

	// 検索対象のフィールド設定
	public $presetVars = array(
		array('field' => 'user_ids', 'type' => 'value', 'empty' => true),
		array('field' => 'user_name', 'type' => 'value', 'empty' => true),
		array('field' => 'mail_address', 'type' => 'value', 'empty' => true),
		array('field' => 'user_type_id', 'type' => 'value', 'empty' => true),
	);
}
