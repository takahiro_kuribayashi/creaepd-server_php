<?php
App::uses('AppModel', 'Model');
/**
 * MResourceFile Model
 *
 * @property Resource $Resource
 */
class MResourceFile extends AppModel {

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'resource_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'resource_file_type' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'0' => array( // 教材登録時のpdf1を必須
				'resource_file_name' => array(
						'notEmpty' => array(
								'rule' => array('notEmpty'),
								//'message' => 'Your custom message here',
								//'allowEmpty' => false,
								//'required' => false,
								//'last' => false, // Stop validation after this rule
								//'on' => 'create', // Limit validation to 'create' or 'update' operations
						),
				),
		),
/*		'resource_file_name' => array(
				// (ファイルアップロード時のサンプル)
				'rule1' => array(
						// 拡張子の指定
						'rule' => array('extension',array('pdf','mp3')),
						'message' => 'PDFまたはMP3ファイルではありません。',
						'allowEmpty' => true,
				),
				'rule2' => array(
						// サイズの制限
						'rule' => array('filesize', 100000),
						'message' => 'ファイルサイズは50MB以下でお願いします',
				),
		), */
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'MResource' => array(
			'className' => 'MResource',
			'foreignKey' => 'resource_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
}
