<?php
App::uses('AppModel', 'Model');
/**
 * MUpdate Model
 *
 */
class MUpdate extends AppModel {
/***** 行番号の付加
	public $name = 'MUpdate';

	public $useTable = false;
	public $base_sql = "select @count:=@count+1 as count, MUpdate.* from (select @count:=0) as dummy, m_updates as MUpdate";

	public function paginate($conditions,$fields,$order,$limit,$page=1,$recursive=null,$extra=array()){
		if($page==0){$page = 1;}
		$recursive = -1;
		$offset = $page * $limit - $limit;
		$sql = $this->base_sql . ' limit ' . $limit . ' offset ' . $offset;
		return $this->query($sql);
	}

	public function paginateCount($conditions=null,$recursive=0,$extra=array()){
		$this->recursive = $recursive;
		$results = $this->query($this->base_sql);
		return count($results);
	}*/

/**
 * Validation rules
 *
 * @var array
 */
	function beforeValidate()
	{
		$this->validate['version']['notEmpty']['message'] = sprintf($GLOBALS['message']->getMessage(MESSAGE_LOGIN_ERROR_007), "10");
		$this->validate['apk_file']['rule1']['message'] = sprintf($GLOBALS['message']->getMessage(MESSAGE_SYSTEM_UPDATE_001));
		$this->validate['apk_file']['rule2']['message'] = sprintf($GLOBALS['message']->getMessage(MESSAGE_SYSTEM_UPDATE_002), "50");
		$this->validate['description']['message'] = sprintf($GLOBALS['message']->getMessage(MESSAGE_LOGIN_ERROR_007), "200");
	}
	public $validate = array(
		'version' => array(
			'notEmpty' => array(
				'rule'	  => array('maxLengthJP', "10"),
				'message' => '全角・半角10文字以内で入力して下さい。',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'apk_file' => array(
			// (ファイルアップロード時のサンプル)
			'rule1' => array(
					// 拡張子の指定
					'rule' => array('extension',array('apk')),
					'message' => 'APKファイルではありません。',
					'allowEmpty' => true,
			),
			'rule2' => array(
					// サイズの制限
					'rule' => array('fileSize', '<=', '50MB'),
					'message' => 'ファイルサイズは50MB以下でお願いします',
			),
		),
		'description' => array(
				'rule'	  => array('maxLengthJP', "200"),
				'message' => '全角・半角200文字以内で入力して下さい。',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
		),
	);
}
