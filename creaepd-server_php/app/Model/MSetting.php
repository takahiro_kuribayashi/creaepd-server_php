<?php
App::uses('AppModel', 'Model');
/**
 * MSetting Model
 *
 */
class MSetting extends AppModel {

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'language_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'logo_file' => array(
			// (ファイルアップロード時のサンプル)
			'extension' => array(
					// 拡張子の指定
					'rule' => array('extension',array('jpg','png', '')),
					'message' => 'JPEGまたはPNGファイルではありません。',
					'allowEmpty' => true,
			),
			'filesize' => array(
					// サイズの制限
					'rule' => array('fileSize', '<=', '1MB'),
					'message' => 'ファイルサイズは1MB以下でお願いします',
			),
		),
		'color_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'MLanguage' => array(
			'className' => 'MLanguage',
			'foreignKey' => 'language_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'MBaseColor' => array(
			'className' => 'MBaseColor',
			'foreignKey' => 'basecolor_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
	);
}
