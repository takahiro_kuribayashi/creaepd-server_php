<?php
App::uses('AppModel', 'Model');
/**
 * TSection Model
 *
 */
class TSection extends AppModel {
	public $name = 'TSection';
	public $actsAs = array('Tree');

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'parent_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'ancestor_group_id' => array(
			'notEmpty' => array(
				'rule' => array('notEmpty'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);

	public $belongsTo = array(
		'mGroup' => array(
			'className' => 'MGroup',
			'foreignKey' => 'group_id',
			'conditions' => '',
			'order' => '',
			'dependent' => false
		),
		'mUser' => array(
			'className' => 'MUser',
			'foreignKey' => 'user_id',
			'conditions' => '',
			'order' => '',
			'dependent' => false
		)
		);
}
