<?php
class PdfPost extends AppModel {
	public $name = 'PdfPost';
	public $hasMany = array(
		'Pdf' => array( // ここで指定した名称がファイル保存時のディレクトリ名になる
			'className' => 'Attachment',
			'foreignKey' => 'foreign_key',
			'conditions' => array(
				'Pdf.model' => 'PdfPost',
			),
			'order' => 'Pdf.id DESC'
		),
	);

	public function createWithAttachments($data) {
		$pdfs = array();
		if (is_array($data['Pdf'])) {
			foreach ($data['Pdf'] as $i => $pdf) {
				if (is_array($pdf)) {
					// Force setting the `model` field to this model
					$pdf['model'] = 'PdfPost';

					// Unset the foreign_key if the user tries to specify it
					if (isset($pdf['foreign_key'])) {
						unset($pdf['foreign_key']);
					}

					$pdfs[] = $pdf;
				}
			}
		}
		$data['Pdf'] = $pdfs;

		if (empty($data['PdfPost']) || empty($data['PdfPost']['ID'])) { // POSTテーブル情報が未指定のときは新規に作成
			// 最新のIDを取得
			$pdf_id_new = $this->getNewestId();
			// IDを変数にセット
			$data['PdfPost'] = array('id'=>$pdf_id_new+1);
		}

		// Try to save the data using Model::saveAll()
		$this->create();
		if ($this->saveAll($data)) {
			return true;
		}
		// Throw an exception for the controller
		throw new Exception(__("This pdf could not be saved. Please try again"));
	}
}
?>