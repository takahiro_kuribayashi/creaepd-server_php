<?php
App::uses('AppModel', 'Model');
/**
 * MResourceSubtitle Model
 *
 * @property Subject $Subject
 */
class MResourceSubtitle extends AppModel {

/**
 * Validation rules
 *
 * @var array
 */
	function beforeValidate()
	{
		$this->validate['subtitle_name']['notEmpty']['message'] = sprintf($GLOBALS['message']->getMessage(MESSAGE_LOGIN_ERROR_003), $GLOBALS['message']->getMessage(MESSAGE_RESOURCE_ITEM_015));
		$this->validate['subtitle_name']['maxlength']['message'] = sprintf($GLOBALS['message']->getMessage(MESSAGE_LOGIN_ERROR_007), "50");
		$this->validate['question_num']['numeric']['message'] = sprintf($GLOBALS['message']->getMessage(MESSAGE_LOGIN_ERROR_004), $GLOBALS['message']->getMessage(MESSAGE_RESOURCE_ITEM_016));
		$this->validate['question_num']['between']['message'] = sprintf($GLOBALS['message']->getMessage(MESSAGE_LOGIN_ERROR_021), "1", "100");
		$this->validate['allot']['numeric']['message'] = sprintf($GLOBALS['message']->getMessage(MESSAGE_LOGIN_ERROR_004), $GLOBALS['message']->getMessage(MESSAGE_RESOURCE_ITEM_017));
		$this->validate['allot']['between']['message'] = sprintf($GLOBALS['message']->getMessage(MESSAGE_LOGIN_ERROR_021), "1", "100");
	}
	public $validate = array(
		'resource_id' => array(
				'numeric' => array(
						'rule' => array('numeric'),
						//'message' => 'Your custom message here',
						//'allowEmpty' => false,
						//'required' => false,
						//'last' => false, // Stop validation after this rule
						//'on' => 'create', // Limit validation to 'create' or 'update' operations
				),
		),
		'subtitle_name' => array(
				'notEmpty' => array(
						'rule' => array('notEmpty'),
						//'message' => 'Your custom message here',
						//'allowEmpty' => false,
						//'required' => false,
						//'last' => false, // Stop validation after this rule
						//'on' => 'create', // Limit validation to 'create' or 'update' operations
				),
				'maxlength' => array(
						'rule'	  => array('maxLengthJP', "50"),
				),
		),
		'question_num' => array(
				'numeric' => array(
						'rule' => array('numeric'),
						//'message' => 'Your custom message here',
						//'allowEmpty' => false,
						//'required' => false,
						//'last' => false, // Stop validation after this rule
						//'on' => 'create', // Limit validation to 'create' or 'update' operations
				),
				'between' => array(
						'rule' => array('range', 0, 101),
				),
		),
		'allot' => array(
				'numeric' => array(
						'rule' => array('numeric'),
						//'message' => 'Your custom message here',
						//'allowEmpty' => false,
						//'required' => false,
						//'last' => false, // Stop validation after this rule
						//'on' => 'create', // Limit validation to 'create' or 'update' operations
				),
				'between' => array(
						'rule' => array('range', 0, 101),
				),
		),
		'result' => array(
				'numeric' => array(
						'rule' => array('numeric'),
						//'message' => 'Your custom message here',
						//'allowEmpty' => false,
						//'required' => false,
						//'last' => false, // Stop validation after this rule
						//'on' => 'create', // Limit validation to 'create' or 'update' operations
				),
		),
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'MResource' => array(
			'className' => 'MResource',
			'foreignKey' => 'resource_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
}
