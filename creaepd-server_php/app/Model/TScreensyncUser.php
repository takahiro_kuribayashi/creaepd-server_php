<?php
App::uses('AppModel', 'Model');
/**
 * TScreensyncUser Model
 *
 * @property Submit $Submit
 * @property Subtitle $Subtitle
 */
class TScreensyncUser extends AppModel {

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'sync_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'user_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'group_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'TScreensync' => array(
			'className' => 'TScreensync',
			'foreignKey' => 'sync_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
}
