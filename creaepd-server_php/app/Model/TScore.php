<?php
App::uses('AppModel', 'Model');
/**
 * TScore Model
 *
 * @property Submit $Submit
 * @property Subtitle $Subtitle
 */
class TScore extends AppModel {

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'submit_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'subtitle_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'allot' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'result' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'TSubmit' => array(
			'className' => 'TSubmit',
			'foreignKey' => 'submit_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'MResourceSubtitle' => array(
			'className' => 'MResourceSubtitle',
			'foreignKey' => 'subtitle_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
}
