<?php
class Post extends AppModel {
	public $name = 'Post';

	public $hasMany = array(
		'Image' => array( // ここで指定した名称がファイル保存時のディレクトリ名になる
			'className' => 'Attachment',
			'foreignKey' => 'foreign_key',
			'conditions' => array(
				'Image.model' => 'Post',
			),
			'order' => 'Image.id DESC'
		),
	);

	public function createWithAttachments($data) {
		// Sanitize your images before adding them
		$images = array();
		if (is_array($data['Image'])) {
			foreach ($data['Image'] as $i => $image) {
				if (is_array($image)) {
					// Force setting the `model` field to this model
					$image['model'] = 'Post';

					// Unset the foreign_key if the user tries to specify it
					if (isset($image['foreign_key'])) {
						unset($image['foreign_key']);
					}

					$images[] = $image;
				}
			}
		}
		$data['Image'] = $images;
		if (empty($data['Post']) || empty($data['Post']['ID'])) { // POSTテーブル情報が未指定のときは新規に作成
			// 最新のIDを取得
			$post_id_new = $this->getNewestId();
			// IDを変数にセット
			$data['Post'] = array('id'=>$post_id_new+1);
		}

		// Try to save the data using Model::saveAll()
		$this->create();
		if ($this->saveAll($data)) {
			return true;
		}
		// Throw an exception for the controller
		throw new Exception(__("This post could not be saved. Please try again"));
	}
}
?>