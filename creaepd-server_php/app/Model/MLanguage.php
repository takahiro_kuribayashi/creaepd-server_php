<?php
App::uses('AppModel', 'Model');
/**
 * MLanguage Model
 *
 */
class MLanguage extends AppModel {
	public $name = 'MLanguage';

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'language_name' => array(
			'notEmpty' => array(
				'rule' => array('notEmpty'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);
}
