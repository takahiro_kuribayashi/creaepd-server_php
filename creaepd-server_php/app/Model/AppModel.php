<?php
/**
 * Application model for CakePHP.
 *
 * This file is application-wide model file. You can put all
 * application-wide model-related methods here.
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Model
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

App::uses('Model', 'Model');

/**
 * Application model for Cake.
 *
 * Add your application-wide methods in the class below, your models
 * will inherit them.
 *
 * @package       app.Model
 */
class AppModel extends Model {

	/**
	 * 最新ID取得
	 * @param unknown $data
	 */
	public function getNewestId() {
		$id_max = 0;
		// 最新のIDを取得
		$record_max = $this->find('first', array('fields'=>array('MAX('. $this->name .'.id) as id')));
		if(!empty($record_max) && is_array($record_max)
		&& !empty($record_max[0]) && is_array($record_max[0])) {
			$id_max = intval($record_max[0]['id']);
		}

		return $id_max;
	}

	/**
	 * 最新レコード取得
	 * @param unknown $data
	 */
	public function getNewestRecord() {
		// 最新のレコードを取得
		$record_newest = $this->find('first', array('order'=>array(''. $this->name .'.id' => 'desc')));

		if (empty($record_newest)) {
			return null;
		}
		return $record_newest;
	}

	function maxLengthJP( $wordvalue, $length )
	{
		if ( is_array($wordvalue) ) {
			// 改行を削除し、純粋な文字数とする
			$wordvalue = str_replace(array("\r\n","\n","\r"), '', $wordvalue);

			// $wordvalueはキーがモデル名の連想配列のためforeachで対応
			foreach ( $wordvalue as $key => $value ) {
				return(mb_strlen($value, mb_detect_encoding($value)) <= $length);
			}
		} else {
			return( mb_strlen($wordvalue, mb_detect_encoding($wordvalue)) <= $length );
		}
	}
}
