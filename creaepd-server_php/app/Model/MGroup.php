<?php
App::uses('AppModel', 'Model');
/**
 * MGroup Model
 *
 * @property MGroupType $MGroupType
 */
class MGroup extends AppModel {
	public $name = 'MGroup';
	public $recursive = 2;

/**
 * Search plugin
 *
 * @var array
 */
	public $actsAs = array('Search.Searchable');

/**
 * Validation rules
 *
 * @var array
 */
	function beforeValidate()
	{
		$this->validate['group_name']['notEmpty']['message'] = sprintf($GLOBALS['message']->getMessage(MESSAGE_LOGIN_ERROR_003), $GLOBALS['message']->getMessage(MESSAGE_GROUP_ITEM_002));
		$this->validate['group_name']['maxlength']['message'] = sprintf($GLOBALS['message']->getMessage(MESSAGE_LOGIN_ERROR_007), "15");
	}
	public $validate = array(
		'group_id' => array(
			'notEmpty' => array(
				'rule' => array('notEmpty'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'group_name' => array(
			'notEmpty' => array(
				'rule' => array('notEmpty'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
			'maxlength' => array(
				'rule'	  => array('maxLengthJP', "15"),
				'message' => '全角・半角15文字以内で入力して下さい。',
			),
		),
		'group_type_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'delete_flg' => array(
			'notEmpty' => array(
				'rule' => array('notEmpty'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'ins_date' => array(
			'datetime' => array(
				'rule' => array('datetime'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);

	public function validates($options = array()) { // 親を呼び出す
		$errors = parent::validates($options);
		if ($errors) {
			$group_id = (isset($this->data['TSection']['ancestor_group_id']) ? $this->data['TSection']['ancestor_group_id']: 0);
			$sql = "SELECT * FROM `m_groups` WHERE `m_groups`.`id` = :group_id;";
			$params = array(
					'group_id'=> $group_id
			);
			$group_data = $this->query($sql,$params);
			// グループ種別の上下チェック
			if (!empty($group_data) && $this->data['MGroup']['group_type_id'] > $group_data[0]['m_groups']['group_type_id']) {
				$errors = false;
				$this->tSection->validationErrors['ancestor_group_id'] =
					sprintf($GLOBALS['message']->getMessage(MESSAGE_LOGIN_ERROR_012), $GLOBALS['message']->getMessage(MESSAGE_GROUP_ITEM_004));
			}
		}
		return $errors;
	}

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'GroupType' => array(
			'className' => 'MGroupType',
			'foreignKey' => 'group_type_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);

	public $hasMany = array(
		'tSection' => array(
			'className' => 'TSection',
			'foreignKey' => 'group_id',
			'conditions' => '',
			'order' => '',
			'dependent' => false,
			'exclusive' => true,
			'finderQuery' => ''
		)
	);

	// 検索対象のフィルタ設定
	public $filterArgs = array(
		array('name' => 'group_ids', 'type' => 'like', 'field' => 'MGroup.group_ids'),
		array('name' => 'group_name', 'type' => 'like', 'field' => 'MGroup.group_name'),
		array('name' => 'group_type_id', 'type' => 'value', 'field' => 'MGroup.group_type_id'),
		array('name' => 'ancestor_group_id', 'type' => 'like', 'field' => 'tSection0.group_id'),
		array('name' => 'user_name', 'type' => 'like', 'field' => 'MUser.user_name'),
	);

	// 検索対象のフィールド設定
	public $presetVars = array(
		array('field' => 'group_ids', 'type' => 'value', 'empty' => true),
		array('field' => 'group_name', 'type' => 'value', 'empty' => true),
		array('field' => 'group_type_id', 'type' => 'value', 'empty' => true),
		array('field' => 'ancestor_group_id', 'type' => 'value', 'empty' => true),
		array('field' => 'user_name', 'type' => 'value', 'empty' => true),
	);
}
