<?php
App::uses('AppModel', 'Model');
/**
 * MBaseColor Model
 *
 */
class MBaseColor extends AppModel {
	public $name = 'MBaseColor';

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'color_name' => array(
			'notEmpty' => array(
				'rule' => array('notEmpty'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);
}
