<?php
App::uses('AppModel', 'Model');
/**
 * TSession Model
 *
*/
class TSession extends AppModel {
	public $name = 'CakeSession';

	public function getData($id){
		$sql = "SELECT * FROM cake_sessions WHERE id = :id;";

		$params = array(
			'id'=> $id
		);

		$data = $this->query($sql,$params);
		return $data;
	}
}
