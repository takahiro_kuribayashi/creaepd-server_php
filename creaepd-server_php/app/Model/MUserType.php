<?php
App::uses('AppModel', 'Model');
/**
 * MUserType Model
 *
 */
class MUserType extends AppModel {
	public $name = 'MUserType';

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'user_type_name' => array(
			'notEmpty' => array(
				'rule' => array('notEmpty'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);
}
