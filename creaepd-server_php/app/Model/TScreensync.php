<?php
App::uses('AppModel', 'Model');
/**
 * TScreensync Model
 *
 * @property Submit $Submit
 * @property Subtitle $Subtitle
 */
class TScreensync extends AppModel {
	public $name = 'TScreensync';

	/**
	 * Search plugin
	 *
	 * @var array
	 */
	public $actsAs = array(
			'Search.Searchable'
	);

/**
 * Validation rules
 *
 * @var array
 */
	function beforeValidate()
	{
		$this->validate['start_datetime']['datetime']['message'] = $GLOBALS['message']->getMessage(MESSAGE_LOGIN_ERROR_010);
		$this->validate['sync_datetime']['datetime']['message'] = $GLOBALS['message']->getMessage(MESSAGE_LOGIN_ERROR_010);
		$this->validate['end_datetime']['datetime']['message'] = $GLOBALS['message']->getMessage(MESSAGE_LOGIN_ERROR_010);
	}

	public $validate = array(
		'document_id' => array(
			'notEmpty' => array(
				'rule' => array('notEmpty'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'sync_mode' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'sync_state' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'sync_user_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'start_datetime' => array(
			'datetime' => array(
				'rule' => array('datetime'),
				'message' => '有効な日付を入力してください',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'sync_datetime' => array(
			'datetime' => array(
				'rule' => array('datetime'),
				'message' => '有効な日付を入力してください',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'end_datetime' => array(
			'datetime' => array(
				'rule' => array('datetime'),
				'message' => '有効な日付を入力してください',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'user_id' => array(
				'numeric' => array(
						'rule' => array('numeric'),
						//'message' => 'Your custom message here',
						//'allowEmpty' => false,
						//'required' => false,
						//'last' => false, // Stop validation after this rule
						//'on' => 'create', // Limit validation to 'create' or 'update' operations
				),
		),
	);

	public function validates($options = array()) { // 親を呼び出す
		$errors = parent::validates($options);
		if ($errors) {
			// １対１の場合、複数ユーザ選択不可
			if ($this->data['TScreensync']['sync_mode'] != 1 && $this->data['TScreensync']['multi'] === true) {
				$errors = false;
				$this->validationErrors['user_id'] = $GLOBALS['message']->getMessage(MESSAGE_LOGIN_ERROR_018);
			}
			// １対１の場合、複数ユーザ選択不可
			if ($this->data['TScreensync']['sync_mode'] != 1 && $this->data['TScreensync']['subject_flg'] === 'group') {
				$errors = false;
				$this->validationErrors['group_id'] = $GLOBALS['message']->getMessage(MESSAGE_LOGIN_ERROR_018);
			}
			// 同期開始日時が配信開始日時より後になっているか
			if ($this->data['TScreensync']['sync_datetime'] < $this->data['TScreensync']['start_datetime']) {
				$errors = false;
				$this->validationErrors['sync_datetime'] =
					sprintf($GLOBALS['message']->getMessage(MESSAGE_LOGIN_ERROR_023), $GLOBALS['message']->getMessage(MESSAGE_SCREENSYNC_ITEM_005));
			}
			// 配信開始日時が配信終了日時より後になっているか
			if ($this->data['TScreensync']['start_datetime'] > $this->data['TScreensync']['end_datetime']) {
				$errors = false;
				$this->validationErrors['start_datetime'] =
					sprintf($GLOBALS['message']->getMessage(MESSAGE_LOGIN_ERROR_023), $GLOBALS['message']->getMessage(MESSAGE_SCREENSYNC_ITEM_006));
			}
		}
		return $errors;
	}

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'mSyncUser' => array(
			'className' => 'MUser',
			'foreignKey' => 'sync_user_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);

	public $hasMany = array(
		'TScreensyncUser' => array(
			'className' => 'TScreensyncUser',
			'foreignKey' => 'sync_id',
			'conditions' => '',
			'order' => '',
			'dependent' => false,
			'exclusive' => true,
			'finderQuery' => ''
		),
		'TScreensyncFile' => array(
			'className' => 'TScreensyncFile',
			'foreignKey' => 'submit_id',
			'conditions' => '',
			'order' => '',
			'dependent' => false,
			'exclusive' => true,
			'finderQuery' => ''
		)
	);


	// 検索対象のフィルタ設定
	public $filterArgs = array(
			array('name' => 'resource_id', 'type' => 'like', 'field' => 'TScreensyncFile.resource_id'),
			array('name' => 'sync_mode', 'type' => 'like', 'field' => 'TScreensync.sync_mode'),
			array('name' => 'sync_datetime', 'type' => 'query', 'method' => 'syncDatetimeConditions'),
			array('name' => 'sync_datetime_to', 'type' => 'query', 'method' => 'syncDatetimeToConditions'),
	);
	//start
	public function syncDatetimeConditions($data = array()){
		$sync_datetime = $data['sync_datetime'];
		$conditions = array('sync_datetime >= CAST(? AS DATETIME)'=>array($sync_datetime.' 00:00:00'));
		return $conditions;
	}
	public function syncDatetimeToConditions($data = array()){
		$sync_datetime_to = $data['sync_datetime_to'];
		$conditions = array('sync_datetime <= CAST(? AS DATETIME)'=>array($sync_datetime_to.' 23:59:59'));
		return $conditions;
	}

	// 検索対象のフィールド設定
	public $presetVars = array(
			array('field' => 'resource_id', 'type' => 'value', 'empty' => true),
			array('field' => 'sync_mode', 'type' => 'value', 'empty' => true),
			array('field' => 'sync_datetime', 'type' => 'value', 'empty' => true),
			array('field' => 'sync_datetime_to', 'type' => 'value', 'empty' => true),
	);
}
