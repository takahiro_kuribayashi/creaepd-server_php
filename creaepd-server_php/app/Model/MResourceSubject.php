<?php
App::uses('AppModel', 'Model');
/**
 * MResourceSubject Model
 *
 */
class MResourceSubject extends AppModel {
	public $name = 'MResourceSubject';

/**
 * Search plugin
 *
 * @var array
 */
	public $actsAs = array('Search.Searchable');

/**
 * Validation rules
 *
 * @var array
 */
	function beforeValidate()
	{
		$this->validate['subject_name']['notEmpty']['message'] = sprintf($GLOBALS['message']->getMessage(MESSAGE_LOGIN_ERROR_007), "10");
	}
	public $validate = array(
		'subject_name' => array(
			'notEmpty' => array(
				'rule'	  => array('maxLengthJP', "10"),
				'message' => '全角・半角10文字以内で入力して下さい。',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);


	// 検索対象のフィルタ設定
	public $filterArgs = array(
			array('name' => 'subject_name', 'type' => 'like', 'field' => 'MResourceSubject.subject_name'),
	);

	// 検索対象のフィールド設定
	public $presetVars = array(
			array('field' => 'subject_name', 'type' => 'value', 'empty' => true),
	);
}
