<?php
/** サイドメニュークラス
 *
 * @author takashi_hayashi
 *
 */
class SideMenu {
	// リンク装飾用記号
	const LINK_SYMBOL_1 = '　　';
	const LINK_SYMBOL_2 = '　　';
	const LINK_SYMBOL_3 = '　▽';
	const LINK_SYMBOL_4 = '　　-';

	/** メニュー用情報の配列を取得する
	 *
	 */
	public static function getMenuArray($muser) {
		App::import('Vendor', 'Menu/LinkMenuItem');

		/* ホーム */
		$active = '';
		if (preg_match('/\/top\//', $_SESSION['ActiveURL'])) {
			// URLと一致のメニューを開く
			$active = 'active';
		}
		$home = new LinkMenuItem('home', 'home', self::LINK_SYMBOL_1.$GLOBALS['message']->getMessage(MESSAGE_COMMON_ITEM_110), null, '/top/', $active);
		$home->setChild_list(
				array()
		);
		/* 教材 */
		$active = '';
		if (preg_match('/(\/mresources)|(\/deliver)|(\/screensync)/', $_SESSION['ActiveURL'])) {
			// URLと一致のメニューを開く
			$active = 'active';
		}
		$recture = new LinkMenuItem('recture', 'recture', self::LINK_SYMBOL_1.$GLOBALS['message']->getMessage(MESSAGE_COMMON_ITEM_100), null, '', $active); // 教材
		$recture_regist = new LinkMenuItem('recture_1', 'recture_1', self::LINK_SYMBOL_2.$GLOBALS['message']->getMessage(MESSAGE_COMMON_ITEM_001), null, '/mresources/add'); // 登録
		$recture_search = new LinkMenuItem('recture_2', 'recture_2', self::LINK_SYMBOL_2.$GLOBALS['message']->getMessage(MESSAGE_COMMON_ITEM_002), null, '/mresources/index'); // 検索
		$deliver_regist = new LinkMenuItem('recture_3', 'recture_3', self::LINK_SYMBOL_2.$GLOBALS['message']->getMessage(MESSAGE_COMMON_ITEM_056), null, '/deliver/edit'); // 配信管理
		$deliver_search = new LinkMenuItem('recture_4', 'recture_4', self::LINK_SYMBOL_2.$GLOBALS['message']->getMessage(MESSAGE_COMMON_ITEM_057), null, '/deliver/index'); // 配信検索
		// 画面同期
		$active = '';
		if (preg_match('/\/screensync/', $_SESSION['ActiveURL'])) {
			// URLと一致のメニューを開く
			$active = 'active';
		}
		$recture_screensync = new LinkMenuItem('recture_5', 'recture_5', self::LINK_SYMBOL_3.$GLOBALS['message']->getMessage(MESSAGE_COMMON_ITEM_092), null, '', $active); // 画面同期管理
		$recture_screensync->setChild_list(
				array(
						new LinkMenuItem('recture_5_1', 'recture_5_1', self::LINK_SYMBOL_4.$GLOBALS['message']->getMessage(MESSAGE_COMMON_ITEM_001), null, '/screensync/edit'), // 登録
						new LinkMenuItem('recture_5_2', 'recture_5_2', self::LINK_SYMBOL_4.$GLOBALS['message']->getMessage(MESSAGE_COMMON_ITEM_002), null, '/screensync/index') // 検索
				)
		);
		// 教材メニューを設定
		$recture->setChild_list(
			array(
				$recture_regist
				,$recture_search
				,$deliver_regist
				,$deliver_search
				,$recture_screensync
			)
		);

		// 進捗確認
		$active = '';
		if (preg_match('/\/status\//', $_SESSION['ActiveURL'])) {
			// URLと一致のメニューを開く
			$active = 'active';
		}
		$progress = new LinkMenuItem('status', 'status', self::LINK_SYMBOL_1.$GLOBALS['message']->getMessage(MESSAGE_COMMON_ITEM_101), null, '', $active); // 進捗確認
		$progress_submit = new LinkMenuItem('status_1', 'status_1', self::LINK_SYMBOL_2.$GLOBALS['message']->getMessage(MESSAGE_COMMON_ITEM_103), null, '/status/submit'); // 提出物
		$progress_check = new LinkMenuItem('status_2', 'status_2', self::LINK_SYMBOL_2.$GLOBALS['message']->getMessage(MESSAGE_COMMON_ITEM_104), null,'/status/check'); // 採点状況
		// 進捗確認メニューを設定
		$progress->setChild_list(
				array(
				$progress_submit
				,$progress_check
		)
		);
		/* スケジュール */
		$active = '';
		if (preg_match('/\/mschedules/', $_SESSION['ActiveURL'])) {
			// URLと一致のメニューを開く
			$active = 'active';
		}
		$schedule = new LinkMenuItem('schedule', 'schedule', self::LINK_SYMBOL_1.$GLOBALS['message']->getMessage(MESSAGE_COMMON_ITEM_105), null, '', $active); // スケジュール
		$schedule_regist = new LinkMenuItem('schedule_1', 'schedule_1', self::LINK_SYMBOL_2.$GLOBALS['message']->getMessage(MESSAGE_COMMON_ITEM_001), null, '/mschedules/add'); // 登録
		$schedule_search = new LinkMenuItem('schedule_2', 'schedule_2', self::LINK_SYMBOL_2.$GLOBALS['message']->getMessage(MESSAGE_COMMON_ITEM_002), null, '/mschedules/index'); // 検索
		// スケジュールメニューを設定
		$schedule->setChild_list(
			array(
				$schedule_regist
				,$schedule_search
			)
		);
		/* ユーザ */
		$active = '';
		if (preg_match('/(\/musers)|(\/mgroups)/', $_SESSION['ActiveURL'])) {
			// URLと一致のメニューを開く
			$active = 'active';
		}
		$message_user = $GLOBALS['message']->getMessage(MESSAGE_COMMON_ITEM_106);
		$user = new LinkMenuItem('user', 'user', self::LINK_SYMBOL_1.$message_user, null, '', $active); // ユーザー
		$user_regist = new LinkMenuItem('user_1', 'user_1', self::LINK_SYMBOL_2.$GLOBALS['message']->getMessage(MESSAGE_COMMON_ITEM_001), null, '/musers/add'); // ユーザ登録
		$user_search = new LinkMenuItem('user_2', 'user_2', self::LINK_SYMBOL_2.$GLOBALS['message']->getMessage(MESSAGE_COMMON_ITEM_002), null, '/musers/index'); // ユーザ検索
		$user_multi_regist = new LinkMenuItem('user_3', 'user_3', self::LINK_SYMBOL_2.$GLOBALS['message']->getMessage(MESSAGE_COMMON_ITEM_011), null, '/musers/multi_add'); // ユーザ一括登録
		/* グループ */
		$message_group = $GLOBALS['message']->getMessage(MESSAGE_COMMON_ITEM_107);
		$group_regist = new LinkMenuItem('group_1', 'group_1', self::LINK_SYMBOL_2.$message_group.$GLOBALS['message']->getMessage(MESSAGE_COMMON_ITEM_001), null, '/mgroups/add'); // グループ位登録
		$group_search = new LinkMenuItem('group_2', 'group_2', self::LINK_SYMBOL_2.$message_group.$GLOBALS['message']->getMessage(MESSAGE_COMMON_ITEM_002), null, '/mgroups/index'); // グループ検索
		// ユーザメニューを設定
		$user->setChild_list(
				array(
					$user_regist
					,$user_search
					,$user_multi_regist
					,$group_regist
					,$group_search
				)
		);

		/* データ分析 */
		$active = '';
		if (preg_match('/\/analysis\//', $_SESSION['ActiveURL'])) {
			// URLと一致のメニューを開く
			$active = 'active';
		}
		$analysis = new LinkMenuItem('analysis', 'analysis', self::LINK_SYMBOL_1.$GLOBALS['message']->getMessage(MESSAGE_COMMON_ITEM_111), null, '', $active); // データ分析
		$analysis_ranking = new LinkMenuItem('analysis_1', 'analysis_1', self::LINK_SYMBOL_2.$GLOBALS['message']->getMessage(MESSAGE_COMMON_ITEM_084), null, '/analysis/ranking'); // 成績ランキング
		$analysis_group_record = new LinkMenuItem('analysis_2', 'analysis_2', self::LINK_SYMBOL_2.$GLOBALS['message']->getMessage(MESSAGE_COMMON_ITEM_085), null, '/analysis/group_record'); // グループ成績一覧
		$analysis_submit_record = new LinkMenuItem('analysis_3', 'analysis_3', self::LINK_SYMBOL_2.$GLOBALS['message']->getMessage(MESSAGE_COMMON_ITEM_086), null, '/analysis/submit_record'); // 教材・配信成績一覧
		$analysis_student_record = new LinkMenuItem('analysis_4', 'analysis_4', self::LINK_SYMBOL_2.$GLOBALS['message']->getMessage(MESSAGE_COMMON_ITEM_093), null, '/analysis/student_record'); // 成績一覧
		// データ分析メニューを設定
		$analysis->setChild_list(
				array(
						$analysis_ranking
						,$analysis_group_record
						,$analysis_submit_record
						,$analysis_student_record
				)
		);

		/* システム */
		$active = '';
		if (preg_match('/\/system\//', $_SESSION['ActiveURL'])) {
			// URLと一致のメニューを開く
			$active = 'active';
		}
		$message_system = $GLOBALS['message']->getMessage(MESSAGE_COMMON_ITEM_108);
		$system = new LinkMenuItem('system', 'system', self::LINK_SYMBOL_1.$message_system.$GLOBALS['message']->getMessage(MESSAGE_COMMON_ITEM_003), null, '', $active); // システム設定
		$message_server = $GLOBALS['message']->getMessage(MESSAGE_COMMON_ITEM_109);
		$system_design = new LinkMenuItem('system_1', 'system_1', self::LINK_SYMBOL_2.$message_server.$GLOBALS['message']->getMessage(MESSAGE_COMMON_ITEM_079), null, '/system/design'); // サーバデザイン変更
		$system_language = new LinkMenuItem('system_2', 'system_2', self::LINK_SYMBOL_2.$GLOBALS['message']->getMessage(MESSAGE_COMMON_ITEM_080), null, '/system/language'); // 言語設定
		$system_subject = new LinkMenuItem('system_3', 'system_3', self::LINK_SYMBOL_2.$GLOBALS['message']->getMessage(MESSAGE_COMMON_ITEM_094), null, '/system/subject_index'); // ジャンル検索
		$system_subject_edt = new LinkMenuItem('system_5', 'system_5', self::LINK_SYMBOL_2.$GLOBALS['message']->getMessage(MESSAGE_COMMON_ITEM_081), null, '/system/subject_edit'); // ジャンル設定
		$system_update = new LinkMenuItem('system_4', 'system_4', self::LINK_SYMBOL_2.$GLOBALS['message']->getMessage(MESSAGE_COMMON_ITEM_083), null, '/system/update_index'); // アップデート管理
				// システムメニューを設定
		$system->setChild_list(
				array(
					$system_design
					,$system_language
					,$system_subject_edt
					,$system_subject
					,$system_update
				)
		);

		// 全体メニューを設定
		if ($muser['user_type_id'] == '9') {
			$menu_array = array(
					$home
					,$recture
					,$progress
					,$schedule
					,$user
					,$analysis
					,$system
			);
		} else if( $muser['user_type_id'] == '3' ) {
			$menu_array = array(
					$home
					,$recture
					,$progress
					,$schedule
					,$user
					,$analysis
			);
		} else {
			$menu_array = array(
					$home
					,$recture
					,$progress
					,$schedule
					,$analysis
			);
		}

		return $menu_array;

	}
}
