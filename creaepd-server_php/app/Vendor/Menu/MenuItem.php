<?php
/** メニュー用アイテムクラス
 *
 * @author takashi_hayashi
 *
 */
abstract class MenuItem {
	// キー
	protected $key;
	// パラメータ名
	protected $param_name;
	// 名称
	protected $name;
	// 子メニューリスト
	protected $child_list;
	// アクティブクラス
	protected $active;

	/**
	 * コンストラクタ
	 * @param string $key キー
	 * @param string $name 名称
	 * @param array $child_list 子メニューリスト
	 */
	public function __construct($key, $param_name, $name, $child_list = null, $active = '') {
		// キーを設定
		$this->setKey($key);
		// パラメータ名を設定
		$this->setParamName($param_name);
		// 名称を設定
		$this->setName($name);
		// 子メニューリストを設定
		$this->setChild_list($child_list);
		// setActiveを設定
		$this->setActive($active);
	}

	/**
	 * キーを設定する
	 * @param string $value 設定する値
	 */
	public function setKey( $value) {
		$this->key = $value;
	}

	/**
	 * キーを取得する
	 * @return string キー
	 */
	public function getKey() {
		return $this->key;
	}

	/**
	 * パラメータ名を設定する
	 * @param string $value 設定する値
	 */
	public function setParamName( $value) {
		$this->param_name = $value;
	}

	/**
	 * パラメータ名を取得する
	 * @return string パラメータ名
	 */
	public function getParamName() {
		return $this->param_name;
	}

	/**
	 * 名称を設定する
	 * @param string $value 設定する値
	 */
	public function setName( $value) {
		$this->name = $value;
	}

	/**
	 * 名称を取得する
	 * @return string 名称
	 */
	public function getName() {
		return $this->name;
	}

	/**
	 * 子メニューリストを設定する
	 * @param string $value 設定するリスト
	 */
	public function setChild_list( $value) {
		$this->child_list = $value;
	}

	/**
	 * 子メニューリストを取得する
	 * @return array 子メニューリスト
	 */
	public function getChild_list() {
		return $this->child_list;
	}

	/**
	 * アクティブクラスを設定する
	 * @param string $value 設定する値
	 */
	public function setActive( $value) {
		$this->active = $value;
	}

	/**
	 * アクティブクラスを取得する
	 * @return string 名称
	 */
	public function getActive() {
		return $this->active;
	}

	/**
	 * HTML部分を表示
	 * @param HtmlHelper $htmlHelper htmlヘルパー
	 * @param FormHelper $formHelper formヘルパー
	 */
	abstract public function getHtml( $htmlHelper, $formHelper);
}
