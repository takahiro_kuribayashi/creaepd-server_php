<?php

App::import('Vendor', 'Menu/MenuItem');

/** メニュー用アイテムクラス(リンク形式)
 *
 * @author takashi_hayashi
 *
 */
class LinkMenuItem extends MenuItem{
	// URL
	protected $url;

	/**
	 * コンストラクタ
	 * @param string $key キー
	 * @param string $param_name パラメータ名称
	 * @param string $name 名称
	 * @param array $child_list 子メニューリスト
	 * @param string $url URL
	 * @param string $active アクティブクラス
	 */
	public function __construct($key, $param_name, $name, $child_list = null, $url = '', $active = '') {
		parent::__construct($key, $param_name, $name, $child_list, $active);
		// URLを設定
		$this->setUrl($url);
	}

	/**
	 * URLを設定する
	 * @param string $value 設定する値
	 */
	public function setUrl( $value) {
		$this->url = $value;
	}

	/**
	 * URLを取得する
	 * @return string URL
	 */
	public function getUrl() {
		return $this->url;
	}

	/**
	 * HTML部分を取得する
	 * @param HtmlHelper $htmlHelper htmlヘルパー
	 * @param FormHelper $formHelper formヘルパー
	 */
	public function getHtml( $htmlHelper, $formHelper) {
		// リンクの設定
		$link = $this->getUrl();
		if (empty($link)) {
			// リンクが無ければ空スクリプト
			$link = 'javascript:void(0)';
		}
		return $htmlHelper->link($this->getName(), $link, array('id' => $this->getKey()));
	}
}
