<?php
/** サイドメニュークラス
 *
 * @author takashi_hayashi
 *
 */
class GroupTree {

	/** メニュー用情報の配列を取得する
	 * @param array $data_array 表示対象のデータ一覧
	 *
	 */
	public static function getMenuArray($data_array) {
		$menu_array = array();
		if (!empty($data_array) && is_array($data_array)) {
			App::import('Vendor', 'Menu/CheckMenuItem');

			foreach ($data_array as $data) {
				// グループ情報
				$group_data = $data['MGroup'];
				// ユーザー情報
				$user_list = $data['tSection'];
				if (empty($user_list) || !is_array($user_list)) { // ユーザーがいない場合はスキップ
					continue;
				}
				$group = new CheckMenuItem($group_data['id'], 'group['.$group_data['id'].']', $group_data['group_name']); // ユーザー
				$user_item_list = array();
				foreach ($user_list as $user_data) {
					// ユーザー情報を作成(パラメータ名はgroup[グループID][ユーザーID]の形式)
					$user = new CheckMenuItem($user_data['mUser']['id'], $user_data['mUser'], $user_data['mUser']['user_name']); // ユーザー
					// 作成したユーザーをリストに追加
					$user_item_list[] = $user;
				}

				// 作成したユーザーリストをグループに紐づけ
				$group->setChild_list($user_item_list);
				// グループを全体リストに追加
				$menu_array[] = $group;
			}
		}

		return $menu_array;

	}
}