<?php

App::import('Vendor', 'Menu/MenuItem');

/** メニュー用アイテムクラス(リンク形式)
 *
 * @author takashi_hayashi
 *
 */
class CheckMenuItem extends MenuItem{

	/**
	 * コンストラクタ
	 * @param string $key キー
	 * @param string $param_name パラメータ名称
	 * @param string $name 名称
	 * @param array $child_list 子メニューリスト
	 */
	public function __construct($key, $param_name, $name, $child_list = null) {
		parent::__construct($key, $param_name, $name, $child_list);
	}


	/**
	 * HTML部分を取得する
	 * @param HtmlHelper $htmlHelper htmlヘルパー
	 * @param FormHelper $formHelper formヘルパー
	 */
	public function getHtml( $htmlHelper, $formHelper) {
		return $formHelper->input( $this->getKey(), array('type' => 'checkbox','label' => $this->getName(), 'div' => false, 'name' => $this->getParamName()));
	}
}
