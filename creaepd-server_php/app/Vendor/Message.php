<?php
/** メッセージクラス
 *
 * @author takashi_hayashi
 *
 */
class Message {
	// メッセージリスト
	protected $message_list;

	/** コンストラクタ */
	function __construct() {
		// 言語設定の取得
		$language_id = $this->getLanguageSetting();
		// 言語モデルのインポート
		App::import('Model', 'MWord');
		$mWord = new MWord();
		$this->message_list = $mWord->find('list', array('conditions' => array('MWord.language_id' => $language_id), 'fields' => array('MWord.word_key', 'MWord.word')));
	}

	/** 言語設定の取得
	 * @return int 言語ID
	 */
	protected function getLanguageSetting() {
		// 設定モデルのインポート
		App::import('Model', 'MSetting');
		$mSetting = new MSetting();
		// 言語設定の取得
		$setting_data = $mSetting->find('first');
		if (!empty($setting_data) && is_array($setting_data)
			&& !empty($setting_data['MSetting']) && is_array($setting_data['MSetting'])) {
			$language_id = $setting_data['MSetting']['language_id'];
		}

		return $language_id;
	}

	/** 言語設定の取得
	 *	@param string $word_key 取得したい単語のキー
	 *	@return string 取得した単語
	 */
	public function getMessage( $word_key) {
		// データがない時は殻文字を返す
		if (empty($this->message_list)
			|| !is_array($this->message_list)
			|| !array_key_exists($word_key, $this->message_list)) {
			return '';
		}
		// データがあればそれを返す
		return $this->message_list[$word_key];
	}
}