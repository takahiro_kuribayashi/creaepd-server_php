<?php
// 各種ファイルのインクルード
echo $this->Html->script( 'nav.js');
$class = $GLOBALS['msetting']['MBaseColor']['class_name'];
echo $this->Html->css( $class.'.sideMenu.css');

echo $this->Html->tag('div', null, array('id' => 'menuA'));
$label = $GLOBALS['message']->getMessage(MESSAGE_COMMON_ITEM_112);
echo '<h4><strong>'.$label.'</strong></h4>';

// メニューツリーを表示
$this->Tree->printMenuTree($this->Html, $this->Form, $menu_array);
echo $this->Html->tag('/div');

?>