<?php echo $this->Form->create('MSchedule', array('url' => '/mschedules/index', 'novalidate' => true))?>

<fieldset>
	<dl class="inline">
		<dt><?php echo $GLOBALS['message']->getMessage(MESSAGE_SCHEDULE_ITEM_001); ?></dt>
		<dd>
			<?php echo $this->Form->input('schedule_ids', array('type' => 'text', 'label' => false )); ?>
		</dd>
		<dt><?php echo $GLOBALS['message']->getMessage(MESSAGE_SCHEDULE_ITEM_002); ?></dt>
		<dd>
			<?php echo $this->Form->input('title', array('type' => 'text', 'label' => false )); ?>
		</dd>
		<dt><div><?php echo $GLOBALS['message']->getMessage(MESSAGE_SCHEDULE_ITEM_003); ?></div></dt>
		<dd>
			<span class="inline_box">
				<?php echo $this->DatePicker->datepicker('start_datetime', array('type' => 'text', 'label' => false, 'size' => '12')); ?>
				～
				<?php echo $this->DatePicker->datepicker('start_datetime_to', array('type' => 'text', 'label' => false, 'size' => '12')); ?>
			</span>
		</dd>
		<dt><div><?php echo $GLOBALS['message']->getMessage(MESSAGE_SCHEDULE_ITEM_004); ?></div></dt>
		<dd>
			<span class="inline_box">
				<?php echo $this->DatePicker->datepicker('end_datetime', array('type' => 'text', 'label' => false, 'size' => '12')); ?>
				～
				<?php echo $this->DatePicker->datepicker('end_datetime_to', array('type' => 'text', 'label' => false, 'size' => '12')); ?>
			</span>
		</dd>
	</dl>

</fieldset>
<div class="submit">
	<?php
		$button = $GLOBALS['message']->getMessage(MESSAGE_COMMON_ITEM_002);
		echo $this->Form->submit($button, array('div' => false, 'escape' => false));
	?>
</div>

<?php echo $this->Form->end()?>
