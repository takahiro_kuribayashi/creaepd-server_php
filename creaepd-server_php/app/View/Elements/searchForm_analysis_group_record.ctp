<?php echo $this->Form->create('TSubmit', array('url' => '/analysis/group_record', 'novalidate' => true))?>

<fieldset>
	<dl class="inline">
		<dt><?php echo $GLOBALS['message']->getMessage(MESSAGE_ANALYSIS_ITEM_001); ?></dt>
		<dd>
			<?php echo $this->Form->input('group_id', array('type'=>'select','options'=>$groupObjs, 'label' => false )); ?>
		</dd>
		<dt><?php echo $GLOBALS['message']->getMessage(MESSAGE_ANALYSIS_ITEM_005); ?></dt>
		<dd>
			<?php echo $this->Form->input('record_type', array('type'=>'radio','options'=>$recordTypeObjs, 'default'=>$recordType,'legend' => false)); ?>
		</dd>
		</dl>

</fieldset>
<div class="submit">
	<?php
		$button = $GLOBALS['message']->getMessage(MESSAGE_COMMON_ITEM_002);
		echo $this->Form->submit($button, array('div' => false, 'escape' => false));
	?>
</div>

<?php echo $this->Form->end()?>
