<?php echo $this->Form->create('TSubmit', array('url' => '/deliver/index', 'novalidate' => true))?>

<fieldset>
	<dl class="inline">
		<dt><?php echo $GLOBALS['message']->getMessage(MESSAGE_DELIVER_ITEM_001); ?></dt>
		<dd>
			<?php echo $this->Form->input('submit_no', array('type' => 'text', 'label' => false)); ?>
		</dd>
		<dt><?php echo $GLOBALS['message']->getMessage(MESSAGE_DELIVER_ITEM_002); ?></dt>
		<dd>
			<?php echo $this->Form->input('resource_name', array('type' => 'text', 'label' => false)); ?>
		</dd>
		<dt><?php echo $GLOBALS['message']->getMessage(MESSAGE_DELIVER_ITEM_009); ?></dt>
		<dd>
			<span class="inline_box">
				<?php echo $this->DatePicker->datepicker('start_datetime', array('type' => 'text', 'label' => false, 'size' => '12')); ?>
				～
				<?php echo $this->DatePicker->datepicker('start_datetime_to', array('type' => 'text', 'label' => false, 'size' => '12')); ?>
			</span>
		</dd>
		<dt><?php echo $GLOBALS['message']->getMessage(MESSAGE_DELIVER_ITEM_010); ?></dt>
		<dd>
			<span class="inline_box">
				<?php echo $this->DatePicker->datepicker('end_datetime', array('type' => 'text', 'label' => false, 'size' => '12')); ?>
				～
				<?php echo $this->DatePicker->datepicker('end_datetime_to', array('type' => 'text', 'label' => false, 'size' => '12')); ?>
			</span>
		</dd>
		<dt><?php echo $GLOBALS['message']->getMessage(MESSAGE_DELIVER_ITEM_005); ?></dt>
		<dd>
			<?php echo $this->Form->input('check_user_id', array('type'=>'select', 'label' => false, 'required' => false, 'options'=>$mUsers)); ?>
		</dd>
	</dl>

</fieldset>
<div class="submit">
	<?php
		$button = $GLOBALS['message']->getMessage(MESSAGE_COMMON_ITEM_002);
		echo $this->Form->submit($button, array('div' => false, 'escape' => false));
	?>
</div>

<?php echo $this->Form->end()?>
