<?php echo $this->Form->create('MResource', array('url' => '/mresources/index', 'novalidate' => true))?>

<fieldset>
	<dl class="inline">
		<dt><?php echo $GLOBALS['message']->getMessage(MESSAGE_RESOURCE_ITEM_001); ?></dt>
		<dd>
			<?php echo $this->Form->input('resource_ids', array('type' => 'text', 'label' => false )); ?>
		</dd>
		<dt><?php echo $GLOBALS['message']->getMessage(MESSAGE_RESOURCE_ITEM_002); ?></dt>
		<dd>
			<?php echo $this->Form->input('resource_name', array('type' => 'text', 'label' => false )); ?>
		</dd>
		<dt><?php echo $GLOBALS['message']->getMessage(MESSAGE_RESOURCE_ITEM_003); ?></dt>
		<dd>
			<?php echo $this->Form->input('resource_type', array('type'=>'select','options'=>$resourceTypes, 'label' => false )); ?>
		</dd>
		<dt><?php echo $GLOBALS['message']->getMessage(MESSAGE_RESOURCE_ITEM_004); ?></dt>
		<dd>
			<span class="inline_box">
				<?php echo $this->Form->input('subject_id', array('type'=>'select', 'label' => false, 'multiple'=> true, 'size' => 5, 'options'=>$resourceSubjects )); ?>
				<a type="button" id="subjects_button" rel="leanModal" href="#test_popup"><?php echo $GLOBALS['message']->getMessage(MESSAGE_COMMON_ITEM_017); ?></a>
			</span>
		</dd>
		<dt><?php echo $GLOBALS['message']->getMessage(MESSAGE_RESOURCE_ITEM_005); ?></dt>
		<dd>
			<?php echo $this->Form->input('style_id', array('type'=>'select','options'=>$resourceStyles, 'label' => false )); ?>
		</dd>
	</dl>

</fieldset>
<div class="submit">
	<?php
		$button = $GLOBALS['message']->getMessage(MESSAGE_COMMON_ITEM_002);
		echo $this->Form->submit($button, array('div' => false, 'escape' => false));
	?>
</div>

<?php echo $this->Form->end()?>
