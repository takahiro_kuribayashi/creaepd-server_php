<?php echo $this->Form->create('MResourceSubject', array('url' => '/system/subject_index', 'novalidate' => true))?>

<fieldset>
	<?php echo $this->Form->label('subject_name', $GLOBALS['message']->getMessage(MESSAGE_SYSTEM_ITEM_004), array('class'=>'subtittle')); ?>
	<?php echo $this->Form->input('subject_name', array('type' => 'text', 'label' => false, 'div'=>false )); ?>

</fieldset>
<div class="submit">
	<?php
		$button = $GLOBALS['message']->getMessage(MESSAGE_COMMON_ITEM_002);
		echo $this->Form->submit($button, array('div' => false, 'escape' => false));
	?>
</div>

<?php echo $this->Form->end()?>
