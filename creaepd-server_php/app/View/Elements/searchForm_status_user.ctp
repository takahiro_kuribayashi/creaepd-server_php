<?php echo $this->Form->create('TSubmit', array('url' => '/status/user', 'novalidate' => true))?>

<fieldset>
	<dl class="inline">
		<dt><?php echo $GLOBALS['message']->getMessage(MESSAGE_STATUS_ITEM_001); ?></dt>
		<dd>
			<?php echo $this->Form->input('user_ids', array('type'=>'select', 'label' => false, 'multiple'=> true, 'size' => 5, 'options'=>$mUsers )); ?>
		</dd>
	</dl>

</fieldset>
<div class="submit">
	<?php
		$button = $GLOBALS['message']->getMessage(MESSAGE_COMMON_ITEM_002);
		echo $this->Form->submit($button, array('div' => false, 'escape' => false));
	?>
</div>

<?php echo $this->Form->end()?>
