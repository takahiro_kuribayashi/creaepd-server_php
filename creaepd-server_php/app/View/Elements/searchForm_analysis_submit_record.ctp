<?php echo $this->Form->create('TSubmit', array('url' => '/analysis/submit_record', 'novalidate' => true))?>

<fieldset>
	<dl class="inline">
		<dt><?php echo $GLOBALS['message']->getMessage(MESSAGE_ANALYSIS_ITEM_004); ?></dt>
		<dd>
			<?php echo $this->Form->input('subject_id', array('type'=>'select','options'=>$resourceSubjects, 'label' => false )); ?>
		</dd>
		<dt><?php echo $GLOBALS['message']->getMessage(MESSAGE_ANALYSIS_ITEM_005); ?></dt>
		<dd>
			<?php echo $this->Form->input('resource_id', array('type'=>'select','options'=>$resourceObjs, 'label' => false )); ?>
		</dd>
		<dt><?php echo $GLOBALS['message']->getMessage(MESSAGE_ANALYSIS_ITEM_003); ?></dt>
		<dd>
			<span class="inline_box">
				<?php echo $this->DatePicker->datepicker('start_datetime', array('type' => 'text', 'label' => false, 'size' => '12')); ?>
				～
				<?php echo $this->DatePicker->datepicker('end_datetime_to', array('type' => 'text', 'label' => false, 'size' => '12')); ?>
			</span>
		</dd>
		<dt><?php echo $GLOBALS['message']->getMessage(MESSAGE_ANALYSIS_ITEM_001); ?></dt>
		<dd>
			<?php echo $this->Form->input('group_id', array('type'=>'select','options'=>$groupObjs, 'label' => false )); ?>
		</dd>
	</dl>

</fieldset>
<div class="submit">
	<?php
		$button = $GLOBALS['message']->getMessage(MESSAGE_COMMON_ITEM_002);
		echo $this->Form->submit($button, array('div' => false, 'escape' => false));
	?>
</div>

<?php echo $this->Form->end()?>
