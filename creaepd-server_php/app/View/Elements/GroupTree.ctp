<?php
// 各種ファイルのインクルード
echo $this->Html->script( 'nav.js');
echo $this->Html->css( 'groupTree.css');

echo $this->Html->tag('div', null, array('id' => 'group_tree'));
// グループツリーを表示
$this->Tree->printMenuTree($this->Html, $this->Form, $group_array);
echo $this->Html->tag('/div');

?>