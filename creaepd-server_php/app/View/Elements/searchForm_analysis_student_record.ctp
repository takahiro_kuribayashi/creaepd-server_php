<?php echo $this->Form->create('TSubmit', array('url' => '/analysis/student_record', 'novalidate' => true))?>

<fieldset>
	<dl class="inline">
		<dt><?php echo $GLOBALS['message']->getMessage(MESSAGE_ANALYSIS_ITEM_007); ?></dt>
		<dd>
			<!-- 以下のselectのidは明示的にinvalidとしている。理由としては、以下を外すとグレー表示になるため -->
			<?php echo $this->Form->input('user_id', array('type'=>'select','options'=>$userData, 'label' => false, 'id' => 'invalid' )); ?>
		</dd>
	</dl>

</fieldset>
<div class="submit">
	<?php
		$button = $GLOBALS['message']->getMessage(MESSAGE_COMMON_ITEM_002);
		echo $this->Form->submit($button, array('div' => false, 'escape' => false));
	?>
</div>

<?php echo $this->Form->end()?>
