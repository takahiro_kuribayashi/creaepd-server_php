<?php echo $this->Form->create('TSubmit', array('url' => '/status/submit', 'novalidate' => true))?>

<fieldset>
	<dl class="inline">
		<dt><?php echo $GLOBALS['message']->getMessage(MESSAGE_STATUS_ITEM_005); ?></dt>
		<dd>
			<?php echo $this->Form->input('submit_no', array('type' => 'text', 'label' => false )); ?>
		</dd>
		<dt><?php echo $GLOBALS['message']->getMessage(MESSAGE_STATUS_ITEM_009); ?></dt>
		<dd>
			<?php echo $this->Form->input('user_id', array('type'=>'select', 'label' => false, 'required' => false, 'options'=>$allUsers)); ?>
		</dd>
		<dt><?php echo $GLOBALS['message']->getMessage(MESSAGE_STATUS_ITEM_010); ?></dt>
		<dd>
			<?php echo $this->Form->input('group_id', array('type'=>'select','options'=>$allGroups, 'label' => false )); ?>
		</dd>
		<dt><?php echo $GLOBALS['message']->getMessage(MESSAGE_STATUS_ITEM_011); ?></dt>
		<dd>
			<span class="inline_box">
				<?php echo $this->DatePicker->datepicker('end_datetime', array('type' => 'text', 'label' => false, 'size' => '12')); ?>
				～
				<?php echo $this->DatePicker->datepicker('end_datetime_to', array('type' => 'text', 'label' => false, 'size' => '12')); ?>
			</span>
		</dd>
		<dt><?php echo $GLOBALS['message']->getMessage(MESSAGE_STATUS_ITEM_012); ?></dt>
		<dd>
			<?php echo $this->Form->input('submit_state', array('type' => 'checkbox', 'label' => false, 'value' => 1, 'hiddenField' => false, 'required' => false)); ?>
		</dd>
	</dl>

</fieldset>
<div class="submit">
	<?php
		$button = $GLOBALS['message']->getMessage(MESSAGE_COMMON_ITEM_002);
		echo $this->Form->submit($button, array('div' => false, 'escape' => false));
	?>
</div>

<?php echo $this->Form->end()?>
