<dt>
<?php echo $this->Form->label('select_subject', $GLOBALS['message']->getMessage(MESSAGE_COMMON_ITEM_087)); ?>
</dt>
<dd>
<?php
echo $this->Html->tag('div', null, array('id' => 'select_subject'));
// ラジオボタンを表示
echo $this->Form->radio('subject_flg', array('user'=>$GLOBALS['message']->getMessage(MESSAGE_COMMON_ITEM_088)), array('hiddenField' => false, 'default' => 'user','legend' => false, 'disabled' => $disabled));
echo $this->Form->input('user_id', array('type'=>'select','multiple'=> true,'size' => 5,'options'=>$setMUsers,'value'=>$valueMUser, 'disabled' => $disabled));
if( is_array($valueMUser) ) {
	foreach( $valueMUser as $data ) {
		if( $disabled === true ) {
			echo $this->Form->input('user_id', array('name'=>'data[TScreensync][user_id][]','type'=>'hidden','value'=>$data, 'disabled' => false));
		} else {
			echo $this->Form->input('user_id', array('type'=>'hidden','value'=>$data, 'disabled' => true));
		}
	}
} else {
	if( $disabled === true ) {
		echo $this->Form->input('user_id', array('name'=>'data[TSubmit][user_id]', 'type'=>'hidden','value'=>$valueMUser, 'disabled' => false));
	} else {
		echo $this->Form->input('user_id', array('type'=>'hidden','value'=>$valueMUser, 'disabled' => true));
	}
}

echo $this->Html->tag('br/');
echo $this->Form->radio('subject_flg', array('group'=>$GLOBALS['message']->getMessage(MESSAGE_COMMON_ITEM_089)), array('hiddenField' => false, 'legend' => false, 'disabled' => $disabled));
echo $this->Form->input('group_id', array('type'=>'select','options'=>$groupObjs, 'label' => false, 'disabled' => $disabled ));
	if( $disabled === true ) {
		echo $this->Form->input('group_id', array('type'=>'hidden','options'=>$groupObjs, 'label' => false, 'disabled' => false ));
	} else {
		echo $this->Form->input('group_id', array('type'=>'hidden','options'=>$groupObjs, 'label' => false, 'disabled' => true ));
	}
echo $this->Html->tag('/div');

?>
</dd>
