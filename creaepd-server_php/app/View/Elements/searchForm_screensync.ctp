<?php echo $this->Form->create('TScreensync', array('url' => '/screensync/index', 'novalidate' => true))?>

<fieldset>
	<dl class="inline">
		<dt><?php echo $GLOBALS['message']->getMessage(MESSAGE_SCREENSYNC_ITEM_001); ?></dt>
		<dd>
			<span class="inline_box">
				<?php echo $this->Form->input('resource_id', array('type'=>'select', 'label' => false, 'options'=>$mResources)); ?>
				<a type="button" id="resources_button" rel="leanModal" href="#test_popup"><?php echo $GLOBALS['message']->getMessage(MESSAGE_COMMON_ITEM_017); ?></a>
			</span>
		</dd>
		<dt><div><?php echo $GLOBALS['message']->getMessage(MESSAGE_SCREENSYNC_ITEM_002); ?></div></dt>
		<dd>
			<?php echo $this->Form->input('sync_mode', array('label' => false, 'type'=>'select','options'=>$syncMode)); ?>
		</dd>
		<dt><div><?php echo $GLOBALS['message']->getMessage(MESSAGE_SCREENSYNC_ITEM_004); ?></div></dt>
		<dd>
			<span class="inline_box">
				<?php echo $this->DatePicker->datepicker('sync_datetime', array('type' => 'text', 'label' => false, 'size' => '12')); ?>
				～
				<?php echo $this->DatePicker->datepicker('sync_datetime_to', array('type' => 'text', 'label' => false, 'size' => '12')); ?>
			</span>
		</dd>
	</dl>

</fieldset>
<div class="submit">
	<?php
		$button = $GLOBALS['message']->getMessage(MESSAGE_COMMON_ITEM_002);
		echo $this->Form->submit($button, array('div' => false, 'escape' => false));
	?>
</div>

<?php echo $this->Form->end()?>
