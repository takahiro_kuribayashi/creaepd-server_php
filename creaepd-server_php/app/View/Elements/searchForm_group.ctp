<?php echo $this->Form->create('MGroup', array('url' => '/mgroups/index', 'novalidate' => true))?>

<fieldset>
	<dl class="inline">
		<dt><?php echo $GLOBALS['message']->getMessage(MESSAGE_GROUP_ITEM_001); ?></dt>
		<dd>
			<?php echo $this->Form->input('group_ids', array('type' => 'text', 'label' => false )); ?>
		</dd>
		<dt><?php echo $GLOBALS['message']->getMessage(MESSAGE_GROUP_ITEM_002); ?></dt>
		<dd>
			<?php echo $this->Form->input('group_name', array('type' => 'text', 'label' => false )); ?>
		</dd>
		<dt><div><?php echo $GLOBALS['message']->getMessage(MESSAGE_GROUP_ITEM_003); ?></div></dt>
		<dd>
			<?php echo $this->Form->input('group_type_id', array('type'=>'select','options'=>$groupTypes, 'label' => false )); ?>
		</dd>
		<dt><?php echo $GLOBALS['message']->getMessage(MESSAGE_GROUP_ITEM_004); ?></dt>
		<dd>
			<?php echo $this->Form->input('ancestor_group_id', array('type'=>'select','options'=>$allGroups, 'label' => false )); ?>
		</dd>
		<dt><div><?php echo $GLOBALS['message']->getMessage(MESSAGE_GROUP_ITEM_005); ?></div></dt>
		<dd>
			<?php echo $this->Form->input('user_name', array('type' => 'text', 'label' => false )); ?>
		</dd>
	</dl>

</fieldset>
<div class="submit">
	<?php
		$button = $GLOBALS['message']->getMessage(MESSAGE_COMMON_ITEM_002);
		echo $this->Form->submit($button, array('div' => false, 'escape' => false));
	?>
</div>

<?php echo $this->Form->end()?>
