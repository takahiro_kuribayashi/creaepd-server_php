

<?php // サイドメニュー
echo $this->element('SideMenu', array('menu_array' => $menu_array));  ?>

<?php echo $this->Html->tag('div', null, array('id' => 'main'));  ?>
	<h2><?php echo $GLOBALS['message']->getMessage(MESSAGE_TOP_ITEM_001); ?></h2>
	<table cellpadding="0" cellspacing="0">
	<tr>
			<th><?php echo $GLOBALS['message']->getMessage(MESSAGE_SCHEDULE_ITEM_002); ?></th>
			<th><?php echo $GLOBALS['message']->getMessage(MESSAGE_COMMON_ITEM_087); ?></th>
			<th><?php echo $GLOBALS['message']->getMessage(MESSAGE_SCHEDULE_ITEM_003); ?></th>
	</tr>
	<?php foreach ($vSchedules as $vSchedule): ?>
	<tr>
		<td><?php echo h($vSchedule['VSchedule']['title']); ?>&nbsp;</td>
<?php if(!empty($vSchedule['VSchedule']['group_id'])) { ?>
		<td><?php echo h($vSchedule['VSchedule']['group_name']); ?>&nbsp;</td>
<?php } else { ?>
		<td><?php echo h($vSchedule['VSchedule']['user_name']); ?>&nbsp;</td>
<?php } ?>
		<td><?php echo h($vSchedule['VSchedule']['start_datetime']); ?>&nbsp;</td>
	</tr>
<?php endforeach; ?>
<?php for($i=0; $i < (5-count($vSchedules)); $i++){ ?>
	<tr>
		<td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td>
	</tr>
<?php } ?>
	</table>

	<h2><?php echo $GLOBALS['message']->getMessage(MESSAGE_TOP_ITEM_002); ?></h2>
	<table cellpadding="0" cellspacing="0">
	<tr>
			<th><?php echo $GLOBALS['message']->getMessage(MESSAGE_DELIVER_ITEM_002); ?></th>
			<th><?php echo $GLOBALS['message']->getMessage(MESSAGE_DELIVER_ITEM_003); ?></th>
			<th><?php echo $GLOBALS['message']->getMessage(MESSAGE_DELIVER_ITEM_004); ?></th>
			<th><?php echo $GLOBALS['message']->getMessage(MESSAGE_TOP_ITEM_004); ?></th>
	</tr>
	<?php foreach ($vSubmits as $vSubmit): ?>
	<tr>
		<td><?php echo h($vSubmit['VSubmit']['resource_name'].'['.$vSubmit['VSubmit']['submit_no'].']'); ?>&nbsp;</td>
		<td><?php echo h($vSubmit['VSubmit']['start_datetime']); ?>&nbsp;</td>
		<td><?php echo h($vSubmit['VSubmit']['end_datetime']); ?>&nbsp;</td>
		<td><?php echo h($vSubmit['VSubmit']['submit_cnt']); ?>
			<?php echo __('/'); ?>
			<?php echo h($vSubmit['VSubmit']['total_cnt']); ?>&nbsp;</td>
	</tr>
<?php endforeach; ?>
<?php for($i=0; $i < (5-count($vSubmits)); $i++){ ?>
	<tr>
		<td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td>
	</tr>
<?php } ?>
	</table>

<?php if(!empty($vSubmitChecks)) { // 管理者のみ表示する ?>
	<h2><?php echo $GLOBALS['message']->getMessage(MESSAGE_TOP_ITEM_003); ?></h2>
	<table cellpadding="0" cellspacing="0">
	<tr>
			<th><?php echo $GLOBALS['message']->getMessage(MESSAGE_DELIVER_ITEM_002); ?></th>
			<th><?php echo $GLOBALS['message']->getMessage(MESSAGE_DELIVER_ITEM_003); ?></th>
			<th><?php echo $GLOBALS['message']->getMessage(MESSAGE_DELIVER_ITEM_004); ?></th>
			<th><?php echo $GLOBALS['message']->getMessage(MESSAGE_TOP_ITEM_005); ?></th>
	</tr>
	<?php foreach ($vSubmitChecks as $vSubmitCheck): ?>
	<tr>
		<td><?php echo h($vSubmitCheck['VSubmit']['resource_name'].'['.$vSubmit['VSubmit']['submit_no'].']'); ?>&nbsp;</td>
		<td><?php echo h($vSubmitCheck['VSubmit']['start_datetime']); ?>&nbsp;</td>
		<td><?php echo h($vSubmitCheck['VSubmit']['end_datetime']); ?>&nbsp;</td>
		<td><?php echo h($vSubmitCheck['VSubmit']['check_cnt']); ?>
			<?php echo __('/'); ?>
			<?php echo h($vSubmitCheck['VSubmit']['total_cnt']); ?>&nbsp;</td>
	</tr>
<?php endforeach; ?>
<?php for($i=0; $i < (5-count($vSubmitChecks)); $i++){ ?>
	<tr>
		<td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td>
	</tr>
<?php } ?>
	</table>
<?php } ?>

<!-- Controlテスト用
<?php  // 日付選択フォーム  ?>
<?php echo $this->form->create('submit_test', array("id" => 'date_test', 'type' => 'post', 'url' => '/top/date_test'));  // submitテスト用form ?>
<?php if ( !empty($selected_date)) { // 選択済み日付があれば表示 ?>
	<?php echo $selected_date.'が選択されました';  ?>
<?php } ?>
<?php echo $this->Datepicker->datepicker('日付', array('id' => 'test_date', 'name' => 'test_date', 'value' => $selected_date));  ?>
<?php echo $this->form->submit();  ?>
<?php echo $this->form->end();  ?>

<?php  // ファイル選択フォーム  ?>
<?php
	echo $this->Form->create('Post', array('type' => 'file','type' => 'file', 'url' => '/top/file_test'));

	if(!empty($selected_file_post)) {
	 	echo '登録済みのファイル:'. $selected_file_post;
	 	echo $this->Html->image(DIR_FILE_UPLOAD.'/attachment/'. $attachment_id_post .'/'. $selected_file_post, array('alt' => '選択済み画像', 'width'=>'20','height'=>'20'));
	}

	echo $this->Form->input('Image.0.attachment', array('type' => 'file', 'label' => 'Image'));
	echo $this->Form->input('Image.0.model', array('type' => 'hidden', 'value' => 'Post'));
	echo $this->Form->input('Post.id', array('type' => 'hidden', 'value' => $post_id));
	echo $this->Form->input('Pdf.0.attachment', array('type' => 'file', 'label' => 'Pdf'));
	echo $this->Form->input('Pdf.0.model', array('type' => 'hidden', 'value' => 'PdfPost'));
	echo $this->Form->input('PdfPost.id', array('type' => 'hidden', 'value' => $pdf_id));
	echo $this->Form->end(__('Add'));
?>
-->
<?php echo $this->Html->tag('/div');  ?>
