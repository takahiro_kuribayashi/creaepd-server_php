<?php
$this->PaginatorExt->options( array('url' => $this->passedArgs) );
// サイドメニュー
echo $this->element('SideMenu', array('menu_array' => $menu_array));  ?>

<div id="main" class="tScreensyncs index">
	<h2><?php echo $GLOBALS['message']->getMessage(MESSAGE_COMMON_ITEM_091); ?></h2>
	<?php echo $this->element('searchForm_screensync')?>

	<table cellpadding="0" cellspacing="0">
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('sync_file_name', $GLOBALS['message']->getMessage(MESSAGE_SCREENSYNC_ITEM_001)); ?></th>
			<th><?php echo $this->Paginator->sort('sync_mode', $GLOBALS['message']->getMessage(MESSAGE_SCREENSYNC_ITEM_002)); ?></th>
			<th><?php echo $this->Paginator->sort('sync_user_name', $GLOBALS['message']->getMessage(MESSAGE_SCREENSYNC_ITEM_007)); ?></th>
			<th><?php echo $this->Paginator->sort('sync_state', $GLOBALS['message']->getMessage(MESSAGE_SCREENSYNC_ITEM_003)); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($tScreensyncs as $tScreensync): ?>
	<tr>
		<td><?php echo h($tScreensync['TScreensync']['id']); ?>&nbsp;</td>
		<td><?php echo h($tScreensync['MResource']['resource_name'] . '[' . $tScreensync['MResource']['resource_ids'] . ']'); ?>&nbsp;</td>
		<td><?php echo h(Configure::read("sync_mode.{$tScreensync['TScreensync']['sync_mode']}")); ?>&nbsp;</td>
		<td><?php echo h($tScreensync['MUser']['user_name']); ?>&nbsp;</td>
		<td><?php echo h(Configure::read("sync_state.{$tScreensync['TScreensync']['sync_state']}")); ?>&nbsp;</td>
		<td class="actions">
		<?php
			$button = $GLOBALS['message']->getMessage(MESSAGE_COMMON_ITEM_009);
			echo $this->Html->link($button, array('action' => 'edit2', $tScreensync['TScreensync']['id']));
		?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	<table class="page_n">
		<tr>
		<td class="first"><?php echo $this->PaginatorExt->first(__('<<'), array('class' => 'first')); ?></td>
		<td class="prev"><?php echo $this->PaginatorExt->prev(__('<'), array(), null, array('class' => 'prev disabled')); ?></td>
		<?php echo $this->PaginatorExt->numbers(array('separator' => '', 'tag'=>'td', 'currentClass'=>'active', 'first' => false, 'last' => false)); ?>
		<td class="next"><?php echo $this->PaginatorExt->next(__('>'), array(), null, array('class' => 'next disabled')); ?></td>
		<td class="last"><?php echo $this->PaginatorExt->last(__('>>'), array('class' => 'last')); ?></td>
		</tr>
	</table>

<!-- ポップアップ表示する内容 -->
	<div id="test_popup">
<?php echo $this->Form->create(false, array('type'=>'post','action'=>'index')); ?>
		<div id="popup_header">
			<h3><?php echo $GLOBALS['message']->getMessage(MESSAGE_SCREENSYNC_ITEM_001); ?></h3>
		</div>
		<div id="popup_body">

<?php
$resources = $mBaseResources;
echo $this->Form->input('TScreensync.resource_id', array(
	'type'=>'radio',
	'options'=> $resources,
	'legend' => false,
	'separator' => '<br><br><br>',
	'label' => false ));
 ?>
		</div>
		<div id="popup_footer">
	<div class="submit">
<?php
	$button = $GLOBALS['message']->getMessage(MESSAGE_COMMON_ITEM_016);
	echo $this->Form->button($button, array('div'=>false, 'type'=>'button', 'id'=>'resources_button', 'name'=>'selected', 'class'=>'modal_close'));
?>
	</div>
<?php echo $this->Form->end(); ?>
		</div>
	</div>

<a href="#modal" id="resources_modal" style="display:none;"></a>
<div id="modal" style="display:none;">
  <div id="modal_content"></div>
</div>
<!-- /ポップアップ表示する内容 -->
</div>