<?php // 各種ファイルのインクルード
echo $this->Html->script('datetimeselect.js');
// サイドメニュー
echo $this->element('SideMenu', array('menu_array' => $menu_array));  ?>

<div id="main" class="tScreensyncs form">
<?php
	echo $this->Form->create('TScreensync', array('type' => 'file'));
	$this->Form->inputDefaults(array('label' => false));
?>
	<fieldset>
		<h2><?php echo $GLOBALS['message']->getMessage(MESSAGE_COMMON_ITEM_092); ?></h2>
	<dl class="inline">
		<?php
			echo $this->Form->input('TScreensync.id');
			echo $this->Form->hidden('TScreensync.sync_state');
			echo $this->Form->hidden('TScreensync.document_id');
		?>
		<dt><?php echo $this->Form->label('TScreensyncFile.sync_file_name', $GLOBALS['message']->getMessage(MESSAGE_SCREENSYNC_ITEM_001)); ?></dt>
		<dd>
			<span class="inline_box">
				<?php echo $this->Form->input('TScreensyncFile.resource_id', array('type'=>'select', 'options'=>$mResources, 'disabled'=>true)); ?>
				<?php echo $this->Form->hidden('TScreensyncFile.resource_id'); ?>
			</span>
		</dd>
		<dt><?php echo $this->Form->label('TScreensync.sync_mode', $GLOBALS['message']->getMessage(MESSAGE_SCREENSYNC_ITEM_002)); ?></dt>
		<dd>
			<?php echo $this->Form->input('TScreensync.sync_mode', array('type'=>'select','options'=>$syncMode, 'disabled'=>true)); ?>
		</dd>
		<dt><?php echo $this->Form->label('TScreensync.sync_user_id', $GLOBALS['message']->getMessage(MESSAGE_SCREENSYNC_ITEM_007)); ?></dt>
		<dd>
			<?php echo $this->Form->hidden('TScreensync.sync_user_id'); ?>
			<?php echo $this->Form->input('TScreensync.sync_user_id', array('type'=>'select','options'=>$mUsers, 'disabled'=>true)); ?>
		</dd>
	<?php
		$option = array(
			'class' => 'datepicker',
			'default' => date('Y-m-d H:i', strtotime($this->data['TScreensync']['sync_datetime'])),
			'timeFormat' => '24',
			'dateFormat' => 'YMD',
			'monthNames' => false,
			'empty' => false,
			'separator' => '/',
			'minYear' => date('Y')-1,
			'maxYear' => date('Y')+1,
		);
	 ?>
		<dt><?php echo $this->Form->label('TScreensync.sync_datetime', $GLOBALS['message']->getMessage(MESSAGE_SCREENSYNC_ITEM_004)); ?></dt>
		<dd>
			<?php echo $this->Form->input('TScreensync.sync_datetime', $option); ?>
		</dd>
	<?php
		$option2 = array(
			'class' => 'datepicker',
			'default' => date('Y-m-d H:i', strtotime($this->data['TScreensync']['start_datetime'])),
			'timeFormat' => '24',
			'dateFormat' => 'YMD',
			'monthNames' => false,
			'empty' => false,
			'separator' => '/',
			'minYear' => date('Y')-1,
			'maxYear' => date('Y')+1,
		);
	 ?>
		<dt><?php echo $this->Form->label('TScreensync.start_datetime', $GLOBALS['message']->getMessage(MESSAGE_SCREENSYNC_ITEM_005)); ?></dt>
		<dd>
			<?php echo $this->Form->input('TScreensync.start_datetime', $option2); ?>
		</dd>
	<?php
		$option3 = array(
			'class' => 'datepicker',
			'default' => date('Y-m-d H:i', strtotime($this->data['TScreensync']['end_datetime'])),
			'timeFormat' => '24',
			'dateFormat' => 'YMD',
			'monthNames' => false,
			'empty' => false,
			'separator' => '/',
			'minYear' => date('Y')-1,
			'maxYear' => date('Y')+1,
		);
	 ?>
		<dt><?php echo $this->Form->label('TScreensync.end_datetime', $GLOBALS['message']->getMessage(MESSAGE_SCREENSYNC_ITEM_006)); ?></dt>
		<dd>
			<?php echo $this->Form->input('TScreensync.end_datetime', $option3); ?>
		</dd>
		<?php echo $this->element('SelectSubject', array("disabled" => true)); ?>
	</dl>
	</fieldset>
	<div class="submit">
<?php
	$button = $GLOBALS['message']->getMessage(MESSAGE_COMMON_ITEM_009);
	echo $this->Form->submit($button, array('div'=>false, 'class'=>'confirm', 'name'=>'complete'));
	$button2 = $GLOBALS['message']->getMessage(MESSAGE_COMMON_ITEM_004);
	echo $this->Form->button($button2, array('div'=>false, 'type'=>'button', 'name'=>'back', 'onClick'=>"location.href='/creaepd-server_php/screensync/index';return false;"));
?>
	</div>
<?php echo $this->Form->end(); ?>

<!-- ポップアップ表示する内容 -->
	<div id="test_popup">
<?php echo $this->Form->create(false, array('type'=>'post','action'=>'./')); ?>
		<div id="popup_header">
			<h3><?php echo $GLOBALS['message']->getMessage(MESSAGE_SCREENSYNC_ITEM_001); ?></h3>
		</div>
		<div id="popup_body">

<?php
$resources = $mBaseResources;
echo $this->Form->input('TScreensyncFile.resource_id', array(
	'type'=>'radio',
	'options'=> $resources,
	'legend' => false,
	'separator' => '<br><br><br>',
	'label' => false ));
 ?>
		</div>
		<div id="popup_footer">
	<div class="submit">
<?php
	$button = $GLOBALS['message']->getMessage(MESSAGE_COMMON_ITEM_016);
	echo $this->Form->button($button, array('div'=>false, 'type'=>'button', 'id'=>'resources_button', 'name'=>'selected', 'class'=>'modal_close'));
?>
	</div>
<?php echo $this->Form->end(); ?>
		</div>
	</div>

<a href="#modal" id="resources_modal" style="display:none;"></a>
<div id="modal" style="display:none;">
  <div id="modal_content"></div>
</div>
<!-- /ポップアップ表示する内容 -->

<?php $this->Html->scriptStart(); ?>
	$(function(){
		$('input.confirm').exJConfirm(
			'<?php echo $GLOBALS['message']->getMessage(MESSAGE_SCREENSYNC_INIT_001); ?>',
			'<?php echo $GLOBALS['message']->getMessage(MESSAGE_COMMON_ITEM_021); ?>'
		);
	});
<?php echo $this->Html->scriptEnd(); ?>
</div>
