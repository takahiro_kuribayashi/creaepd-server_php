<?php // 各種ファイルのインクルード
echo $this->Html->script('jquery-1.10.2', array('inline'=>false));
echo $this->Html->script('jquery.add-input-area.4.7.1.js', array('inline'=>false));
// サイドメニュー
echo $this->element('SideMenu', array('menu_array' => $menu_array));  ?>

<div id="main" class="mResourceSubtitles form">
<?php
	echo $this->Form->create('MResourceSubtitle');
	$this->Form->inputDefaults(array('label' => false));
?>
	<fieldset>
		<h2><?php echo $GLOBALS['message']->getMessage(MESSAGE_COMMON_ITEM_082); ?></h2>
	<div class="submit">
		<?php echo $this->Form->button(__('Cancel'), array('type'=>'button', 'onclick' => 'history.back();')); ?>
	</div>
	<dl class="inline">
		<dt><?php echo $GLOBALS['message']->getMessage(MESSAGE_RESOURCE_ITEM_002); ?></dt>
		<dd>
			<?php echo h($mResource['MResource']['resource_name']); ?>
		</dd>
	</dl>
	<?php echo $this->Html->tag('br/'); ?>
		<table  id="subtitle" cellpadding="0" cellspacing="0">
			<tr>
				<th><?php echo $GLOBALS['message']->getMessage(MESSAGE_RESOURCE_ITEM_015); ?></th>
				<th><?php echo $GLOBALS['message']->getMessage(MESSAGE_RESOURCE_ITEM_016); ?></th>
				<th><?php echo $GLOBALS['message']->getMessage(MESSAGE_RESOURCE_ITEM_017); ?></th>
				<th>&nbsp;</th>
			</tr>
<?php if(!empty($mResourceSubtitles)) { ?>
<?php foreach ($mResourceSubtitles as $key => $mResourceSubtitle): ?>
			<tr class="subtitle_var">
				<td><?php echo $this->Form->input('MResourceSubtitle.' . $key . '.subtitle_name', array('value'=>$mResourceSubtitle['MResourceSubtitle']['subtitle_name'], 'name_format'=>'data[MResourceSubtitle][%d][subtitle_name]')); ?>&nbsp;</td>
				<td><?php echo $this->Form->input('MResourceSubtitle.' . $key . '.question_num', array('size' => '10', 'value'=>$mResourceSubtitle['MResourceSubtitle']['question_num'], 'name_format'=>'data[MResourceSubtitle][%d][question_num]')); ?>&nbsp;</td>
				<td><?php echo $this->Form->input('MResourceSubtitle.' . $key . '.allot', array('size' => '10', 'value'=>$mResourceSubtitle['MResourceSubtitle']['allot'], 'name_format'=>'data[MResourceSubtitle][%d][allot]')); ?>&nbsp;</td>
				<td class="del_actions"><button class="subtitle_del"><?php echo $GLOBALS['message']->getMessage(MESSAGE_COMMON_ITEM_010); ?></button></td>
			</tr>
<?php endforeach; ?>
<?php } else { ?>
			<tr class="subtitle_var">
				<td><?php echo $this->Form->input('MResourceSubtitle.0.subtitle_name', array('name_format'=>'data[MResourceSubtitle][%d][subtitle_name]')); ?>&nbsp;</td>
				<td><?php echo $this->Form->input('MResourceSubtitle.0.question_num', array('size' => '10', 'name_format'=>'data[MResourceSubtitle][%d][question_num]')); ?>&nbsp;</td>
				<td><?php echo $this->Form->input('MResourceSubtitle.0.allot', array('size' => '10', 'name_format'=>'data[MResourceSubtitle][%d][allot]')); ?>&nbsp;</td>
				<td class="del_actions"><button class="subtitle_del"><?php echo $GLOBALS['message']->getMessage(MESSAGE_COMMON_ITEM_010); ?></button></td>
			</tr>
<?php } ?>
		</table>
		<?php $button = $GLOBALS['message']->getMessage(MESSAGE_COMMON_ITEM_013); ?>
		<?php echo $this->Form->input($button, array('type'=>'button', 'name'=>'add', 'class'=>'subtitle_add')); ?>
	</fieldset>
	<div class="submit">
<?php
	echo $this->Form->input('MResource.id');

	$button = $GLOBALS['message']->getMessage(MESSAGE_COMMON_ITEM_001);
	$comment = $GLOBALS['message']->getMessage(MESSAGE_RESOURCE_INIT_002);
	echo $this->Form->submit($button, array('div'=>false, 'name'=>'complete', 'onClick'=>"return confirm('$comment')"));
	$button2 = $GLOBALS['message']->getMessage(MESSAGE_COMMON_ITEM_004);
	echo $this->Form->button($button2, array('div'=>false, 'type'=>'button', 'name'=>'back', 'onClick'=>"history.back();"));
?>
	</div>
<?php echo $this->Form->end()?>
<?php $this->Html->scriptStart(array('inline'=>false)); ?>
$(function(){
	$('#subtitle').addInputArea({
		area_del: '.del_actions',
		maximum : 10
	});
});
<?php echo $this->Html->scriptEnd(); ?>
</div>
