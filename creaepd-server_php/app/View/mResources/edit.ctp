<?php // 各種ファイルのインクルード
echo $this->Html->script('jquery-1.10.2', array('inline'=>false));
echo $this->Html->script('jquery.add-input-area.4.7.1.js', array('inline'=>false));
// サイドメニュー
echo $this->element('SideMenu', array('menu_array' => $menu_array));  ?>

<div id="main" class="mResources form">
<?php
	echo $this->Form->create('MResource', array('type' => 'file'));
	$this->Form->inputDefaults(array('label' => false));
?>
	<fieldset id='resource_main'>
		<h2><?php echo $GLOBALS['message']->getMessage(MESSAGE_COMMON_ITEM_053); ?></h2>
	<dl class="inline">
		<?php
			echo $this->Form->input('MResource.id');
			echo $this->Form->hidden('MResource.version');
		?>
		<dt><?php echo $this->Form->label('MResource.resource_ids', $GLOBALS['message']->getMessage(MESSAGE_RESOURCE_ITEM_001)); ?></dt>
		<dd>
			<?php echo $this->Form->input('MResource.resource_ids', array('type'=>'text','readonly' => 'readonly')); ?>
		</dd>
		<dt><?php echo $this->Form->label('MResource.resource_name', $GLOBALS['message']->getMessage(MESSAGE_RESOURCE_ITEM_002)); ?></dt>
		<dd>
			<?php echo $this->Form->input('MResource.resource_name'); ?>
		</dd>
		<dt><?php echo $this->Form->label('MResource.resource_type', $GLOBALS['message']->getMessage(MESSAGE_RESOURCE_ITEM_003)); ?></dt>
		<dd>
			<?php echo $this->Form->hidden('MResource.resource_type'); ?>
			<?php echo $this->Form->input('MResource.resource_type_name', array('type'=>'text','readonly' => 'readonly','value' => Configure::read("resource_type.{$this->request->data['MResource']['resource_type']}"))); ?>
		</dd>
		<dt id='repeat_item'><?php echo $this->Form->label('MResource.repeat_flg', $GLOBALS['message']->getMessage(MESSAGE_RESOURCE_ITEM_018)); ?></dt>
		<dd id='repeat_item'>
			<?php echo $this->Form->input('MResource.repeat_flg'); ?>
		</dd>
		<dt><?php echo $this->Form->label('MResource.subject_id', $GLOBALS['message']->getMessage(MESSAGE_RESOURCE_ITEM_004)); ?></dt>
		<dd>
			<?php echo $this->Form->input('MResource.subject_id', array('type'=>'select','options'=>$resourceSubjects)); ?>
		</dd>
		<dt><?php echo $this->Form->label('MResource.style_id', $GLOBALS['message']->getMessage(MESSAGE_RESOURCE_ITEM_005)); ?></dt>
		<dd>
			<?php echo $this->Form->hidden('MResource.style_id'); ?>
			<?php echo $this->Form->input('MResource.style_name', array('type'=>'text','readonly' => 'readonly','value' => Configure::read("m_resource_styles.{$this->request->data['MResource']['style_id']}"))); ?>
		</dd>
		<dt id='resource_item0'><?php echo $this->Form->label('MResourceFile.0.resource_file_name', $GLOBALS['message']->getMessage(MESSAGE_RESOURCE_ITEM_006)); ?></dt>
		<dd id='resource_item0'>
<?php if(!empty($this->request->data['MResourceFile'][0]['resource_file_name'])) { ?>
			<?php echo h('（'.$GLOBALS['message']->getMessage(MESSAGE_COMMON_ITEM_020).'）'.$this->request->data['MResourceFile'][0]['resource_file_name']); ?>
<?php } ?>
			<?php echo $this->Form->input('MResourceFile.0.resource_file_type', array('type' => 'hidden', 'value' => '1')); ?>
			<?php echo $this->Form->input('MResourceFile.0.resource_file_name_old', array('type' => 'hidden', 'value' => $this->request->data['MResourceFile'][0]['resource_file_name'])); ?>
				<div>
					<span class="inline_box">
						<?php echo $this->Form->input('MResourceFile.0.resource_file_name', array('error'=>false, 'label' => $GLOBALS['message']->getMessage(MESSAGE_COMMON_ITEM_023),'type' => 'file', 'onChange'=>"uv_0.value = this.value;")); ?>
						<input type="text" id="uv_0" class="uploadValue" disabled />
					</span>
				</div>
<?php if(!empty($validation_errors[0]['resource_file_name'])) { ?>
				<strong style="color:red;"><?php echo h($validation_errors[0]['resource_file_name']); ?></strong>
<?php } ?>
		</dd>
		<dt id='resource_item1'><?php echo $this->Form->label('MResourceFile.1.resource_file_name', $GLOBALS['message']->getMessage(MESSAGE_RESOURCE_ITEM_007)); ?></dt>
		<dd id='resource_item1'>
<?php if(!empty($this->request->data['MResourceFile'][1]['resource_file_name'])) { ?>
			<?php echo h('（'.$GLOBALS['message']->getMessage(MESSAGE_COMMON_ITEM_020).'）'.$this->request->data['MResourceFile'][1]['resource_file_name']); ?>
<?php } ?>
			<?php echo $this->Form->input('MResourceFile.1.resource_file_type', array('type' => 'hidden', 'value' => '1')); ?>
			<?php echo $this->Form->input('MResourceFile.1.resource_file_name_old', array('type' => 'hidden', 'value' => $this->request->data['MResourceFile'][1]['resource_file_name'])); ?>
				<div>
					<span class="inline_box">
						<?php echo $this->Form->input('MResourceFile.1.resource_file_name', array('error'=>false, 'label' => $GLOBALS['message']->getMessage(MESSAGE_COMMON_ITEM_023),'type' => 'file', 'onChange'=>"uv_1.value = this.value;")); ?>
						<input type="text" id="uv_1" class="uploadValue" disabled />
					</span>
				</div>
<?php if(!empty($validation_errors[1]['resource_file_name'])) { ?>
				<strong style="color:red;"><?php echo h($validation_errors[1]['resource_file_name']); ?></strong>
<?php } ?>
		</dd>
		<dt id='resource_item2'><?php echo $this->Form->label('listmp3', $GLOBALS['message']->getMessage(MESSAGE_RESOURCE_ITEM_008)); ?></dt>
		<dd id='resource_item2'>
			<ol id="listmp3" style="list-style:none">
<?php $loop = (count($this->request->data['MResourceFile'])-2 > 0) ? (count($this->request->data['MResourceFile'])-2): 1; ?>
<?php for($i=0; $i < $loop; $i++){ ?>
				<li class="listmp3_var">
<?php if(!empty($this->request->data['MResourceFile'][$i+2]['resource_file_name']) && empty($validation_errors['mp3'][$i]['resource_file_name'])) { ?>
					<span><?php echo h('（'.$GLOBALS['message']->getMessage(MESSAGE_COMMON_ITEM_020).'）'.$this->request->data['MResourceFile'][$i+2]['resource_file_name']); ?></span>
<?php } ?>
			<?php echo $this->Form->input('mp3.{$i}.resource_file_name_old', array('type' => 'hidden', 'value' => $this->request->data['MResourceFile'][$i+2]['resource_file_name'], 'name_format'=>'data[mp3][%d][resource_file_name_old]')); ?>
					<div>
						<span class="inline_box">
							<div class="input file">
							<?php echo $this->Form->input('mp3.{$i}.resource_file_name', array('div'=>false, 'label' => $GLOBALS['message']->getMessage(MESSAGE_COMMON_ITEM_023),'type' => 'file', 'name_format'=>'data[mp3][%d][resource_file_name]', 'onChange'=>'this.parentNode.parentNode.children[1].value = this.value')); ?>
							</div>
							<input type="text" id="uv_mp3" class="uploadValue" disabled />
						</span>
					</div>
<?php if(!empty($validation_errors['mp3'][$i]['resource_file_name'])) { ?>
					<strong style="color:red;"><?php echo h($validation_errors['mp3'][$i]['resource_file_name']); ?></strong>
<?php } ?>
				</li>
<?php } ?>
			</ol>
		</dd>
		</br>
		<dt id='allotment_item'><?php echo $this->Form->label('allotment_button', $GLOBALS['message']->getMessage(MESSAGE_COMMON_ITEM_082)); ?></dt>
		<dd id='allotment_item'>
			<div class="submit">
				<a type="button" class="link_button" id="allotment_button" rel="leanModal" href="#allotment_popup"><?php echo $GLOBALS['message']->getMessage(MESSAGE_COMMON_ITEM_012); ?></a>
	 			<p id='allotment_warn'><strong style="color:red;"><?php echo $GLOBALS['message']->getMessage(MESSAGE_RESOURCE_WARN_001); ?></strong></p>
			</div>
		</dd>
	</dl>
	</fieldset>
	<div class="submit" id="resource_main_submit">
<?php
	$button = $GLOBALS['message']->getMessage(MESSAGE_COMMON_ITEM_009);
	echo $this->Form->submit($button, array('div'=>false, 'class'=>'confirm', 'name'=>'complete'));
	$button2 = $GLOBALS['message']->getMessage(MESSAGE_COMMON_ITEM_004);
	echo $this->Form->button($button2, array('div'=>false, 'type'=>'button', 'name'=>'back', 'onClick'=>"location.href='/creaepd-server_php/mresources/index';return false;"));
?>
	</div>

<!-- ポップアップ表示する内容 -->
	<div id="allotment_popup">
		<div id="popup_header">
			<h3><?php echo $GLOBALS['message']->getMessage(MESSAGE_COMMON_ITEM_082); ?></h3>
		</div>
		<div id="popup_body">

	<fieldset>
		<table  id="subtitle" cellpadding="0" cellspacing="0">
			<tr>
				<th><?php echo $GLOBALS['message']->getMessage(MESSAGE_RESOURCE_ITEM_015); ?></th>
				<th><?php echo $GLOBALS['message']->getMessage(MESSAGE_RESOURCE_ITEM_016); ?></th>
				<th><?php echo $GLOBALS['message']->getMessage(MESSAGE_RESOURCE_ITEM_017); ?></th>
				<th>&nbsp;</th>
			</tr>
<?php if(!empty($mResourceSubtitles)) { ?>
<?php foreach ($mResourceSubtitles as $key => $mResourceSubtitle): ?>
			<tr class="subtitle_var">
				<?php echo $this->Form->input('MResourceSubtitle' . $key . '.id', array('type' => 'hidden', 'value' => $mResourceSubtitle['MResourceSubtitle']['id'], 'name_format'=>'data[MResourceSubtitle][%d][id]')); ?>
				<td><?php echo $this->Form->input('MResourceSubtitle.' . $key . '.subtitle_name', array('class' => 'ajax-check', 'id' => 'subtitle_name@_' . $key, 'id_format'=>'subtitle_name@_%d', 'value'=>$mResourceSubtitle['MResourceSubtitle']['subtitle_name'], 'name_format'=>'data[MResourceSubtitle][%d][subtitle_name]')); ?>&nbsp;</td>
				<td><?php echo $this->Form->input('MResourceSubtitle.' . $key . '.question_num', array('class' => 'ajax-check', 'size' => '10', 'id' => 'question_num@_' . $key, 'id_format'=>'question_num@_%d', 'value'=>$mResourceSubtitle['MResourceSubtitle']['question_num'], 'name_format'=>'data[MResourceSubtitle][%d][question_num]')); ?>&nbsp;</td>
				<td><?php echo $this->Form->input('MResourceSubtitle.' . $key . '.allot', array('class' => 'ajax-check', 'size' => '10', 'id' => 'allot@_' . $key, 'id_format'=>'allot@_%d', 'value'=>$mResourceSubtitle['MResourceSubtitle']['allot'], 'name_format'=>'data[MResourceSubtitle][%d][allot]')); ?>&nbsp;</td>
				<td class="del_actions"><button class="subtitle_del"><?php echo $GLOBALS['message']->getMessage(MESSAGE_COMMON_ITEM_010); ?></button></td>
			</tr>
<?php endforeach; ?>
<?php } else { ?>
			<tr class="subtitle_var">
				<td><?php echo $this->Form->input('MResourceSubtitle.0.subtitle_name', array('class' => 'ajax-check', 'id' => 'subtitle_name@_0', 'id_format'=>'subtitle_name@_%d', 'name_format'=>'data[MResourceSubtitle][%d][subtitle_name]')); ?>&nbsp;</td>
				<td><?php echo $this->Form->input('MResourceSubtitle.0.question_num', array('class' => 'ajax-check', 'size' => '10', 'id' => 'question_num@_0', 'id_format'=>'question_num@_%d', 'name_format'=>'data[MResourceSubtitle][%d][question_num]')); ?>&nbsp;</td>
				<td><?php echo $this->Form->input('MResourceSubtitle.0.allot', array('class' => 'ajax-check', 'size' => '10', 'id' => 'allot@_0', 'id_format'=>'allot@_%d', 'name_format'=>'data[MResourceSubtitle][%d][allot]')); ?>&nbsp;</td>
				<td class="del_actions"><button class="subtitle_del"><?php echo $GLOBALS['message']->getMessage(MESSAGE_COMMON_ITEM_010); ?></button></td>
			</tr>
<?php } ?>
		</table>
		<?php $button = $GLOBALS['message']->getMessage(MESSAGE_COMMON_ITEM_013); ?>
		<?php echo $this->Form->input($button, array('type'=>'button', 'name'=>'add', 'class'=>'subtitle_add')); ?>
	</fieldset>

		</div>
		<div id="popup_footer">
			<div class="submit">
<?php
	$button = $GLOBALS['message']->getMessage(MESSAGE_COMMON_ITEM_016);
	echo $this->Form->button($button, array('div'=>false, 'type'=>'button', 'id'=>'allotment_button', 'name'=>'selected', 'class'=>'modal_close'));
?>
			</div>
		</div>
	</div>

<a href="#modal" id="allotment_modal" style="display:none;"></a>
<div id="modal" style="display:none;">
  <div id="modal_content"></div>
</div>
<!-- /ポップアップ表示する内容 -->

<?php echo $this->Form->end(); ?>
<?php $this->Html->scriptStart(array('inline'=>false)); ?>
$(function(){
	$('#listmp3').addInputArea({
		maximum : 10,
		after_add: function () {
			// コピー元の登録ファイルクをクリア
			$("#listmp3").find("li").filter(":last").children("span").html('');
			//alert('Added!');
		}
	});
	$('#subtitle').addInputArea({
		area_del: '.del_actions',
		maximum : 10
	});
	$(document).on('blur','.ajax-check', function() {
		var target = $(this).parent();
		var key = $(this).attr('id');
		var checkData = {'key': key, 'value': $(this).val()};
		// 採点設定個々チェック
		$.ajax({
			type:'POST',
			url:'../ajaxSubtitleValidate',
			data:checkData
		}).done(function(data) {
			data = JSON.parse(data);
			target.children('div.error-message').each(function() {$(this).remove();});
			if (!data.check) {
			  target.append(data.message);
			}
		});
	});
	$("p#allotment_warn").hide(); // 非表示
	$('input.confirm').exJConfirm(
		'<?php echo $GLOBALS['message']->getMessage(MESSAGE_RESOURCE_INIT_002); ?>',
		'<?php echo $GLOBALS['message']->getMessage(MESSAGE_COMMON_ITEM_021); ?>',
		{
			preCallback : function(){
				var formData = $('#MResourceEditForm').serialize();
				$("p#allotment_warn").hide(); // 非表示
				// 採点設定一括チェック
				$.ajax({
					type:'POST',
					url:'../ajaxAllSubtitleValidate',
					data:formData
				}).done(function(data) {
					data = JSON.parse(data);
					if (!data.check) {
					  $("p#allotment_warn").toggle(); // 表示
					}
				});

				return true;
			}
		}
	);
	// 子画面起動時はdisabledを掛ける
	$(window).on('open:leanModal', function() {
		setDisabled(1);
	})
	$(window).on('close:leanModal', function() {
		setDisabled(0);
	})
	function setDisabled(flag) {
		if (flag==1) {
			//disabledONの場合
			$("#resource_main dd :input").attr("disabled", "disabled");
			$("#resource_main_submit :input").attr("disabled", "disabled");
		} else {
			//disabledOFFの場合
			$("#resource_main dd :input").removeAttr("disabled");
			$("#resource_main_submit :input").removeAttr("disabled");
		}
	}
});
<?php echo $this->Html->scriptEnd(); ?>
</div>
