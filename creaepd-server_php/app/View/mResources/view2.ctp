<?php // サイドメニュー
echo $this->element('SideMenu', array('menu_array' => $menu_array));  ?>

<div id="main" class="mResources view2">
<h2><?php echo $GLOBALS['message']->getMessage(MESSAGE_COMMON_ITEM_055); ?></h2>
	<dl id="modal_list">
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($mResource['MResource']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo $GLOBALS['message']->getMessage(MESSAGE_RESOURCE_ITEM_001); ?></dt>
		<dd>
			<?php echo h($mResource['MResource']['resource_ids']); ?>
			&nbsp;
		</dd>
		<dt><?php echo $GLOBALS['message']->getMessage(MESSAGE_RESOURCE_ITEM_002); ?></dt>
		<dd>
			<?php echo h($mResource['MResource']['resource_name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo $GLOBALS['message']->getMessage(MESSAGE_RESOURCE_ITEM_003); ?></dt>
		<dd>
			<?php echo h(Configure::read("resource_type.{$mResource['MResource']['resource_type']}")); ?>
			&nbsp;
		</dd>
		<dt><?php echo $GLOBALS['message']->getMessage(MESSAGE_RESOURCE_ITEM_018); ?></dt>
		<dd>
			<?php echo h($mResource['MResource']['repeat_flg']); ?>
			&nbsp;
		</dd>
		<dt><?php echo $GLOBALS['message']->getMessage(MESSAGE_RESOURCE_ITEM_004); ?></dt>
		<dd>
			<?php echo h($mResource['ResourceSubject']['subject_name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo $GLOBALS['message']->getMessage(MESSAGE_RESOURCE_ITEM_005); ?></dt>
		<dd>
			<?php echo h($mResource['ResourceStyle']['style_name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo $GLOBALS['message']->getMessage(MESSAGE_RESOURCE_ITEM_006); ?></dt>
		<dd>
			<?php echo h($mResource['MResource']['pdf_file_1']); ?>
			&nbsp;
		</dd>
		<dt><?php echo $GLOBALS['message']->getMessage(MESSAGE_RESOURCE_ITEM_007); ?></dt>
		<dd>
			<?php echo h($mResource['MResource']['pdf_file_2']); ?>
			&nbsp;
		</dd>
		<dt><?php echo $GLOBALS['message']->getMessage(MESSAGE_RESOURCE_ITEM_008); ?></dt>
		<dd>
			<?php
			$count = 0;
			$splits = split(',',$mResource['MResource']['mp3_file']);
			foreach( $splits as $name ) {
				if( $count++ < 2 ) {
					echo h($name);
					echo "<br />";
				} else if($count == 3){
					echo h('etc');
				} else {
				}
			}
			?>
			&nbsp;
		</dd>
	</dl>
	<div class="submit">
<?php
	$button = $GLOBALS['message']->getMessage(MESSAGE_COMMON_ITEM_004);
	echo $this->Form->button($button, array('div'=>false, 'type'=>'button', 'name'=>'back', 'class'=>'modal_close'));	
?>
	</div>
</div>