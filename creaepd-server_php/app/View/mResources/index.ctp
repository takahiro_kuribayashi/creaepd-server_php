<?php
$this->PaginatorExt->options( array('url' => $this->passedArgs) );
// サイドメニュー
echo $this->element('SideMenu', array('menu_array' => $menu_array));  ?>

<div id="main" class="mResources index">
	<h2><?php echo $GLOBALS['message']->getMessage(MESSAGE_COMMON_ITEM_052); ?></h2>
	<?php echo $this->element('searchForm_resource')?>

	<table cellpadding="0" cellspacing="0">
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('resource_ids', $GLOBALS['message']->getMessage(MESSAGE_RESOURCE_ITEM_001)); ?></th>
			<th><?php echo $this->Paginator->sort('resource_name', $GLOBALS['message']->getMessage(MESSAGE_RESOURCE_ITEM_002)); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($mResources as $mResource): ?>
	<tr>
		<td><?php echo h($mResource['MResource']['id']); ?>&nbsp;</td>
		<td><?php echo h($mResource['MResource']['resource_ids']); ?>&nbsp;</td>
		<td><?php echo h($mResource['MResource']['resource_name']); ?>&nbsp;</td>
		<td class="actions">
			<?php
				$button = $GLOBALS['message']->getMessage(MESSAGE_COMMON_ITEM_008);
				echo $this->Html->link($button, array('action' => 'view', $mResource['MResource']['id']));
			?>
<?php if($mResource['MResource']['delivered'] != 0): ?>
			<?php
				$button = $GLOBALS['message']->getMessage(MESSAGE_COMMON_ITEM_009);
				echo $this->Form->button($button, array('type'=>'button', 'disabled' => true));
			?>
<?php else: ?>
			<?php
				$button = $GLOBALS['message']->getMessage(MESSAGE_COMMON_ITEM_009);
				echo $this->Html->link($button, array('action' => 'edit', $mResource['MResource']['id']));
			?>
<?php endif; ?>

<?php if($mResource['MResource']['delete_flg']): ?>
			<?php
				$button = $GLOBALS['message']->getMessage(MESSAGE_COMMON_ITEM_010);
				echo $this->Form->button($button, array('type'=>'button', 'disabled' => true));
			?>
<?php else: ?>
<?php if($mResource['MResource']['delivered'] != 0): ?>
			<?php
				$button = $GLOBALS['message']->getMessage(MESSAGE_COMMON_ITEM_010);
				echo $this->Form->button($button, array('type'=>'button', 'disabled' => true));
			?>
<?php else: ?>
			<?php
				$button = $GLOBALS['message']->getMessage(MESSAGE_COMMON_ITEM_010);
				echo $this->Html->link($button, array('action' => 'delete', $mResource['MResource']['id']));
			?>
<?php endif; ?>
<?php endif; ?>

		</td>
	</tr>
<?php endforeach; ?>
	</table>
	<table class="page_n">
		<tr>
		<td class="first"><?php echo $this->PaginatorExt->first(__('<<'), array('class' => 'first')); ?></td>
		<td class="prev"><?php echo $this->PaginatorExt->prev(__('<'), array(), null, array('class' => 'prev disabled')); ?></td>
		<?php echo $this->PaginatorExt->numbers(array('separator' => '', 'tag'=>'td', 'currentClass'=>'active', 'first' => false, 'last' => false)); ?>
		<td class="next"><?php echo $this->PaginatorExt->next(__('>'), array(), null, array('class' => 'next disabled')); ?></td>
		<td class="last"><?php echo $this->PaginatorExt->last(__('>>'), array('class' => 'last')); ?></td>
		</tr>
	</table>
<!-- ポップアップ表示する内容 -->
	<div id="test_popup">
<?php echo $this->Form->create(false, array('type'=>'post','action'=>'index')); ?>
		<div id="popup_header">
			<h3><?php echo $GLOBALS['message']->getMessage(MESSAGE_RESOURCE_ITEM_004); ?></h3>
		</div>
		<div id="popup_body">

<?php
$subjects = $resourceSubjects;
echo $this->Form->input('MResource.subject_id', array(
	'type'=>'select',
	'multiple' => 'checkbox',
	'options'=> $subjects,
	'label' => false ));
 ?>
		</div>
		<div id="popup_footer">
	<div class="submit">
<?php
	$button = $GLOBALS['message']->getMessage(MESSAGE_COMMON_ITEM_016);
	echo $this->Form->button($button, array('div'=>false, 'type'=>'button', 'id'=>'subjects_button', 'name'=>'selected', 'class'=>'modal_close'));
?>
	</div>
<?php echo $this->Form->end(); ?>
		</div>
	</div>
<!-- /ポップアップ表示する内容 -->
</div>