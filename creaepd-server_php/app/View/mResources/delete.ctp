<?php // サイドメニュー
echo $this->element('SideMenu', array('menu_array' => $menu_array));  ?>

<div id="main" class="mResources form">
<?php echo $this->Form->create('MResource'); ?>
	<fieldset>
		<h2><?php echo $GLOBALS['message']->getMessage(MESSAGE_COMMON_ITEM_054); ?></h2>
	<?php
		echo $this->Form->input('id', array('value' => $mResource['MResource']['id']));
		echo $this->Form->hidden('delete_flg', array('value' => '1'));
	?>
	</fieldset>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($mResource['MResource']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo $GLOBALS['message']->getMessage(MESSAGE_RESOURCE_ITEM_001); ?></dt>
		<dd>
			<?php echo h($mResource['MResource']['resource_ids']); ?>
			&nbsp;
		</dd>
		<dt><?php echo $GLOBALS['message']->getMessage(MESSAGE_RESOURCE_ITEM_002); ?></dt>
		<dd>
			<?php echo h($mResource['MResource']['resource_name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo $GLOBALS['message']->getMessage(MESSAGE_RESOURCE_ITEM_003); ?></dt>
		<dd>
			<?php echo h(Configure::read("resource_type.{$mResource['MResource']['resource_type']}")); ?>
			&nbsp;
		</dd>
		<dt><?php echo $GLOBALS['message']->getMessage(MESSAGE_RESOURCE_ITEM_018); ?></dt>
		<dd>
			<?php echo h($mResource['MResource']['repeat_flg']); ?>
			&nbsp;
		</dd>
		<dt><?php echo $GLOBALS['message']->getMessage(MESSAGE_RESOURCE_ITEM_004); ?></dt>
		<dd>
			<?php echo h($mResource['ResourceSubject']['subject_name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo $GLOBALS['message']->getMessage(MESSAGE_RESOURCE_ITEM_005); ?></dt>
		<dd>
			<?php echo h($mResource['ResourceStyle']['style_name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo $GLOBALS['message']->getMessage(MESSAGE_RESOURCE_ITEM_006); ?></dt>
		<dd>
			<?php echo h($mResource['MResource']['pdf_file_1']); ?>
			&nbsp;
		</dd>
		<dt><?php echo $GLOBALS['message']->getMessage(MESSAGE_RESOURCE_ITEM_007); ?></dt>
		<dd>
			<?php echo h($mResource['MResource']['pdf_file_2']); ?>
			&nbsp;
		</dd>
		<dt><?php echo $GLOBALS['message']->getMessage(MESSAGE_RESOURCE_ITEM_008); ?></dt>
		<dd>
			<?php
			$splits = split(',',$mResource['MResource']['mp3_file']);
			foreach( $splits as $name ) {
				echo h($name);
				echo "<br />";
			}
			?>
			&nbsp;
		</dd>
	</dl>
	<div class="submit">
<?php
	$button = $GLOBALS['message']->getMessage(MESSAGE_COMMON_ITEM_010);
	echo $this->Form->submit($button, array('div'=>false, 'class'=>'confirm', 'name'=>'complete'));
	$button2 = $GLOBALS['message']->getMessage(MESSAGE_COMMON_ITEM_004);
	echo $this->Form->button($button2, array('div'=>false, 'type'=>'button', 'name'=>'back', 'onClick'=>"location.href='/creaepd-server_php/mresources/index';return false;"));
?>
	</div>
<?php echo $this->Form->end(); ?>

<?php $this->Html->scriptStart(); ?>
	$(function(){
		$('input.confirm').exJConfirm(
			'<?php echo $GLOBALS['message']->getMessage(MESSAGE_RESOURCE_INIT_003); ?>',
			'<?php echo $GLOBALS['message']->getMessage(MESSAGE_COMMON_ITEM_021); ?>'
		);
	});
<?php echo $this->Html->scriptEnd(); ?>
</div>