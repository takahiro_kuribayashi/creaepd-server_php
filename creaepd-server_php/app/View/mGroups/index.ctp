<?php
$this->PaginatorExt->options( array('url' => $this->passedArgs) );
// サイドメニュー
echo $this->element('SideMenu', array('menu_array' => $menu_array));  ?>

<div id="main" class="mGroups index">
	<h2><?php echo $GLOBALS['message']->getMessage(MESSAGE_COMMON_ITEM_073); ?></h2>
	<?php echo $this->element('searchForm_group')?>

	<table cellpadding="0" cellspacing="0">
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('group_ids', $GLOBALS['message']->getMessage(MESSAGE_GROUP_ITEM_001)); ?></th>
			<th><?php echo $this->Paginator->sort('group_name', $GLOBALS['message']->getMessage(MESSAGE_GROUP_ITEM_002)); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($mGroups as $mGroup): ?>
	<tr>
		<td><?php echo h($mGroup['MGroup']['id']); ?>&nbsp;</td>
		<td><?php echo h($mGroup['MGroup']['group_ids']); ?>&nbsp;</td>
		<td><?php echo h($mGroup['MGroup']['group_name']); ?>&nbsp;</td>
		<td class="actions">
			<?php
				$button = $GLOBALS['message']->getMessage(MESSAGE_COMMON_ITEM_008);
				echo $this->Html->link($button, array('action' => 'view',$mGroup['MGroup']['id']));
			?>
<?php if($mGroup['MGroup']['id'] == 1): ?>
			<?php
				$button = $GLOBALS['message']->getMessage(MESSAGE_COMMON_ITEM_009);
				echo $this->Form->button($button, array('type' => 'button', 'disabled' => true));
			?>
<?php else: ?>
			<?php
				$button = $GLOBALS['message']->getMessage(MESSAGE_COMMON_ITEM_009);
				echo $this->Html->link($button, array('action' => 'edit', $mGroup['MGroup']['id']));
			?>
<?php endif; ?>
<?php if($mGroup['MGroup']['id'] == 1): ?>
			<?php
				$button = $GLOBALS['message']->getMessage(MESSAGE_COMMON_ITEM_010);
				echo $this->Form->button($button, array('type' => 'button', 'disabled' => true));
			?>
<?php else: ?>
			<?php
				$button = $GLOBALS['message']->getMessage(MESSAGE_COMMON_ITEM_010);
				echo $this->Html->link($button, array('action' => 'delete', $mGroup['MGroup']['id']));
			?>
<?php endif; ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	<table class="page_n">
		<tr>
		<td class="first"><?php echo $this->PaginatorExt->first(__('<<'), array('class' => 'first')); ?></td>
		<td class="prev"><?php echo $this->PaginatorExt->prev(__('<'), array(), null, array('class' => 'prev disabled')); ?></td>
		<?php echo $this->PaginatorExt->numbers(array('separator' => '', 'tag'=>'td', 'currentClass'=>'active', 'first' => false, 'last' => false)); ?>
		<td class="next"><?php echo $this->PaginatorExt->next(__('>'), array(), null, array('class' => 'next disabled')); ?></td>
		<td class="last"><?php echo $this->PaginatorExt->last(__('>>'), array('class' => 'last')); ?></td>
		</tr>
	</table>
</div>