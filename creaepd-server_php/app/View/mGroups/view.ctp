<?php // サイドメニュー
echo $this->element('SideMenu', array('menu_array' => $menu_array));  ?>

<div id="main" class="mGroups view">
<h2><?php echo $GLOBALS['message']->getMessage(MESSAGE_COMMON_ITEM_076); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($mGroup['MGroup']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo $GLOBALS['message']->getMessage(MESSAGE_GROUP_ITEM_001); ?></dt>
		<dd>
			<?php echo h($mGroup['MGroup']['group_ids']); ?>
			&nbsp;
		</dd>
		<dt><?php echo $GLOBALS['message']->getMessage(MESSAGE_GROUP_ITEM_002); ?></dt>
		<dd>
			<?php echo h($mGroup['MGroup']['group_name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo $GLOBALS['message']->getMessage(MESSAGE_GROUP_ITEM_003); ?></dt>
		<dd>
			<?php echo h($mGroup['GroupType']['group_type_name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo $GLOBALS['message']->getMessage(MESSAGE_GROUP_ITEM_004); ?></dt>
		<dd>
			<?php echo h($ancestorGroup); ?>
			&nbsp;
		</dd>
<?php
	$array_first_info = each($tSectionUsers);
	$array_first_key = $array_first_info["key"];
?>
<?php foreach ($tSectionUsers as $key => $tUser): ?>
	<?php if ($key === $array_first_key): ?>
		<dt><?php echo $GLOBALS['message']->getMessage(MESSAGE_GROUP_ITEM_005); ?></dt>
		<dd>
			<?php echo h($tUser); ?>
			&nbsp;
		</dd>
	<?php else: ?>
		<dt>&nbsp;</dt>
		<dd>
			<?php echo h($tUser); ?>
			&nbsp;
		</dd>
	<?php endif; ?>
<?php endforeach; ?>
	</dl>
	<div class="submit">
<?php
	$button = $GLOBALS['message']->getMessage(MESSAGE_COMMON_ITEM_004);
	echo $this->Form->button($button, array('div'=>false, 'type'=>'button', 'name'=>'back', 'onClick' => "location.href='/creaepd-server_php/mgroups/index';return false;"));
?>
	</div>
</div>