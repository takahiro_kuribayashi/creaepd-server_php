<?php // サイドメニュー
echo $this->element('SideMenu', array('menu_array' => $menu_array));  ?>

<div id="main" class="mGroups form">
<?php echo $this->Form->create('MGroup'); ?>
	<fieldset>
		<h2><?php echo $GLOBALS['message']->getMessage(MESSAGE_COMMON_ITEM_075); ?></h2>
	<?php
		echo $this->Form->input('id', array('value' => $mGroup['MGroup']['id']));
		echo $this->Form->hidden('delete_flg', array('value' => '1'));
	?>
	</fieldset>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($mGroup['MGroup']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo $GLOBALS['message']->getMessage(MESSAGE_GROUP_ITEM_001); ?></dt>
		<dd>
			<?php echo h($mGroup['MGroup']['group_ids']); ?>
			&nbsp;
		</dd>
		<dt><?php echo $GLOBALS['message']->getMessage(MESSAGE_GROUP_ITEM_002); ?></dt>
		<dd>
			<?php echo h($mGroup['MGroup']['group_name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo $GLOBALS['message']->getMessage(MESSAGE_GROUP_ITEM_003); ?></dt>
		<dd>
			<?php echo h($mGroup['GroupType']['group_type_name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo $GLOBALS['message']->getMessage(MESSAGE_GROUP_ITEM_004); ?></dt>
		<dd>
			<?php echo h($ancestorGroup); ?>
			&nbsp;
		</dd>
<?php
	$array_first_info = each($tSectionUsers);
	$array_first_key = $array_first_info["key"];
?>
<?php foreach ($tSectionUsers as $key => $tUser): ?>
	<?php if ($key === $array_first_key): ?>
		<dt><?php echo $GLOBALS['message']->getMessage(MESSAGE_GROUP_ITEM_005); ?></dt>
		<dd>
			<?php echo h($tUser); ?>
			&nbsp;
		</dd>
	<?php else: ?>
		<dt>&nbsp;</dt>
		<dd>
			<?php echo h($tUser); ?>
			&nbsp;
		</dd>
	<?php endif; ?>
<?php endforeach; ?>
	</dl>
	<div class="submit">
<?php if(isset($errorMessage)) { ?>
	 <p><strong style="color:red;"><?php echo h($errorMessage); ?></strong></p>
<?php } ?>
<?php
	$button = $GLOBALS['message']->getMessage(MESSAGE_COMMON_ITEM_010);
	echo $this->Form->submit($button, array('div'=>false, 'class'=>'confirm', 'name'=>'complete'));
	$button2 = $GLOBALS['message']->getMessage(MESSAGE_COMMON_ITEM_004);
	echo $this->Form->button($button2, array('div'=>false, 'type'=>'button', 'name'=>'back', 'onClick'=>"location.href='/creaepd-server_php/mgroups/index';return false;"));
?>
	</div>
<?php echo $this->Form->end(); ?>

<?php $this->Html->scriptStart(); ?>
	$(function(){
		$('input.confirm').exJConfirm(
			'<?php echo $GLOBALS['message']->getMessage(MESSAGE_GROUP_INIT_003); ?>',
			'<?php echo $GLOBALS['message']->getMessage(MESSAGE_COMMON_ITEM_021); ?>'
		);
	});
<?php echo $this->Html->scriptEnd(); ?>
</div>