<?php // サイドメニュー
echo $this->element('SideMenu', array('menu_array' => $menu_array));  ?>

<div id="main" class="mGroups form">
<?php
	echo $this->Form->create('MGroup');
	$this->Form->inputDefaults(array('label' => false));
?>
	<fieldset>
		<h2><?php echo $GLOBALS['message']->getMessage(MESSAGE_COMMON_ITEM_074); ?></h2>
	<dl class="inline">
		<?php echo $this->Form->input('MGroup.id'); ?>
		<dt><?php echo $this->Form->label('MGroup.group_ids', $GLOBALS['message']->getMessage(MESSAGE_GROUP_ITEM_001)); ?></dt>
		<dd>
			<?php echo $this->Form->input('MGroup.group_ids', array('type'=>'text','readonly' => 'readonly')); ?>
		</dd>
		<dt><?php echo $this->Form->label('MGroup.group_name', $GLOBALS['message']->getMessage(MESSAGE_GROUP_ITEM_002)); ?></dt>
		<dd>
			<?php echo $this->Form->input('MGroup.group_name'); ?>
		</dd>
		<dt><div><?php echo $this->Form->label('GroupType.group_type_name', $GLOBALS['message']->getMessage(MESSAGE_GROUP_ITEM_003)); ?></div></dt>
		<dd>
			<?php echo $this->Form->input('GroupType.group_type_name', array('type'=>'text','readonly' => 'readonly')); ?>
			<?php echo $this->Form->hidden('MGroup.group_type_id', array('value' => $this->Form->value('MGroup.group_type_id'))); ?>
		</dd>
		<dt><?php echo $this->Form->label('TSection.ancestor_group_id', $GLOBALS['message']->getMessage(MESSAGE_GROUP_ITEM_004)); ?></dt>
		<dd>
			<?php echo $this->Form->input('TSection.ancestor_group_id', array('type'=>'select','options'=>$allGroups,'value'=>$valueTGroup)); ?>
		</dd>
		<dt><div><?php echo $this->Form->label('TSection.user_id', $GLOBALS['message']->getMessage(MESSAGE_GROUP_ITEM_005)); ?></div></dt>
		<dd>
			<?php echo $this->Form->input('TSection.user_id', array('type'=>'select','disabled'=>'disabled','multiple'=> true,'size' => 5,'options'=>$groupTUsers,'value'=>$valueTUsers)); ?>
			<div><p><strong><?php echo sprintf($GLOBALS['message']->getMessage(MESSAGE_GROUP_WARN_001), $GLOBALS['message']->getMessage(MESSAGE_COMMON_ITEM_067)); ?></strong></p></div>
		</dd>
	</dl>
	</fieldset>
	<div class="submit">
<?php
	$button = $GLOBALS['message']->getMessage(MESSAGE_COMMON_ITEM_009);
	echo $this->Form->submit($button, array('div'=>false, 'class'=>'confirm', 'name'=>'complete'));
	$button2 = $GLOBALS['message']->getMessage(MESSAGE_COMMON_ITEM_004);
	echo $this->Form->button($button2, array('div'=>false, 'type'=>'button', 'name'=>'back', 'onClick'=>"location.href='/creaepd-server_php/mgroups/index';return false;"));
?>
	</div>
<?php echo $this->Form->end(); ?>

<?php $this->Html->scriptStart(); ?>
	$(function(){
		$('input.confirm').exJConfirm(
			'<?php echo $GLOBALS['message']->getMessage(MESSAGE_GROUP_INIT_002); ?>',
			'<?php echo $GLOBALS['message']->getMessage(MESSAGE_COMMON_ITEM_021); ?>'
		);
	});
<?php echo $this->Html->scriptEnd(); ?>
</div>
