<?php // サイドメニュー
echo $this->element('SideMenu', array('menu_array' => $menu_array));  ?>

<div id="main" class="mGroups form">
<?php
	echo $this->Form->create('MGroup');
	$this->Form->inputDefaults(array('label' => false));
?>
	<fieldset>
		<h2><?php echo $GLOBALS['message']->getMessage(MESSAGE_COMMON_ITEM_072); ?></h2>
	<dl class="inline">
		<dt><?php echo $this->Form->label('MGroup.group_ids', $GLOBALS['message']->getMessage(MESSAGE_GROUP_ITEM_001)); ?></dt>
		<dd>
			<?php echo $this->Form->input('MGroup.group_ids', array('type'=>'text','readonly' => 'readonly')); ?>
		</dd>
		<dt><?php echo $this->Form->label('MGroup.group_name', $GLOBALS['message']->getMessage(MESSAGE_GROUP_ITEM_002)); ?></dt>
		<dd>
			<?php echo $this->Form->input('MGroup.group_name'); ?>
		</dd>
		<dt><div><?php echo $this->Form->label('MGroup.group_type_id', $GLOBALS['message']->getMessage(MESSAGE_GROUP_ITEM_003)); ?></div></dt>
		<dd>
			<?php echo $this->Form->input('MGroup.group_type_id', array('type'=>'select','options'=>$groupTypes)); ?>
		</dd>
		<dt><?php echo $this->Form->label('TSection.ancestor_group_id', $GLOBALS['message']->getMessage(MESSAGE_GROUP_ITEM_004)); ?></dt>
		<dd>
			<?php echo $this->Form->input('TSection.ancestor_group_id', array('type'=>'select','options'=>$allGroups)); ?>
		</dd>
	</dl>
	</fieldset>
	<div class="submit">
<?php
	$button = $GLOBALS['message']->getMessage(MESSAGE_COMMON_ITEM_001);
	echo $this->Form->submit($button, array('div'=>false, 'class'=>'confirm', 'name'=>'complete'));
	$button2 = $GLOBALS['message']->getMessage(MESSAGE_COMMON_ITEM_004);
	echo $this->Form->button($button2, array('div'=>false, 'type'=>'button', 'name'=>'back', 'onClick'=>"location.href='/creaepd-server_php/mgroups/index';return false;"));
?>
	</div>
<?php echo $this->Form->end(); ?>

<?php $this->Html->scriptStart(); ?>
	$(function(){
		$('input.confirm').exJConfirm(
			'<?php echo $GLOBALS['message']->getMessage(MESSAGE_GROUP_INIT_001); ?>',
			'<?php echo $GLOBALS['message']->getMessage(MESSAGE_COMMON_ITEM_021); ?>'
		);
	});
<?php echo $this->Html->scriptEnd(); ?>
</div>
