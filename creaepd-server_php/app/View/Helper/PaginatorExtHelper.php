<?php
App::uses('PaginatorHelper', 'View/Helper');

class PaginatorExtHelper extends PaginatorHelper {

	public function url($options = array(), $asArray = false, $model = null) {
		$paging = $this->params($model);
		$url = array_merge(array_filter($paging['options']), $options);

		if (isset($url['order'])) {
			$sort = $direction = null;
			if (is_array($url['order'])) {
				list($sort, $direction) = array($this->sortKey($model, $url), current($url['order']));
			}
			unset($url['order']);
			$url = array_merge($url, compact('sort', 'direction'));
		}
		$url = $this->_convertUrlKeys($url, $paging['paramType']);
		if ($asArray) {
			return $url;
		}
		return parent::url($url);
	}

	public function numbers($options = array()) {
		if ($options === true) {
			$options = array(
					'before' => ' | ', 'after' => ' | ', 'first' => 'first', 'last' => 'last'
			);
		}

		$defaults = array(
				'tag' => 'span', 'before' => null, 'after' => null, 'model' => $this->defaultModel(), 'class' => null,
				'modulus' => '8', 'separator' => ' | ', 'first' => null, 'last' => null, 'ellipsis' => '...',
				'currentClass' => 'current', 'currentTag' => null
		);
		$options += $defaults;

		$params = (array)$this->params($options['model']) + array('page' => 1);
		unset($options['model']);

/*		if ($params['pageCount'] <= 1) {
			return false;
		}*/

		extract($options);
		unset($options['tag'], $options['before'], $options['after'], $options['model'],
				$options['modulus'], $options['separator'], $options['first'], $options['last'],
				$options['ellipsis'], $options['class'], $options['currentClass'], $options['currentTag']
		);

		$out = '';

		if ($modulus && $params['pageCount'] > $modulus) {
			$half = intval($modulus / 2);
			$end = $params['page'] + $half;

			if ($end > $params['pageCount']) {
				$end = $params['pageCount'];
			}
			$start = $params['page'] - ($modulus - ($end - $params['page']));
			if ($start <= 1) {
				$start = 1;
				$end = $params['page'] + ($modulus - $params['page']) + 1;
			}

			if ($first && $start > 1) {
				$offset = ($start <= (int)$first) ? $start - 1 : $first;
				if ($offset < $start - 1) {
					$out .= $this->first($offset, compact('tag', 'separator', 'ellipsis', 'class'));
				} else {
					$out .= $this->first($offset, compact('tag', 'separator', 'class', 'ellipsis') + array('after' => $separator));
				}
			}

			$out .= $before;

			for ($i = $start; $i < $params['page']; $i++) {
				$out .= $this->Html->tag($tag, $this->link($i, array('page' => $i), $options), compact('class')) . $separator;
			}

			if ($class) {
				$currentClass .= ' ' . $class;
			}
			if ($currentTag) {
				$out .= $this->Html->tag($tag, $this->Html->tag($currentTag, $params['page']), array('class' => $currentClass));
			} else {
				$out .= $this->Html->tag($tag, $params['page'], array('class' => $currentClass));
			}
			if ($i != $params['pageCount']) {
				$out .= $separator;
			}

			$start = $params['page'] + 1;
			for ($i = $start; $i < $end; $i++) {
				$out .= $this->Html->tag($tag, $this->link($i, array('page' => $i), $options), compact('class')) . $separator;
			}

			if ($end != $params['page']) {
				$out .= $this->Html->tag($tag, $this->link($i, array('page' => $end), $options), compact('class'));
			}

			$out .= $after;

			if ($last && $end < $params['pageCount']) {
				$offset = ($params['pageCount'] < $end + (int)$last) ? $params['pageCount'] - $end : $last;
				if ($offset <= $last && $params['pageCount'] - $end > $offset) {
					$out .= $this->last($offset, compact('tag', 'separator', 'ellipsis', 'class'));
				} else {
					$out .= $this->last($offset, compact('tag', 'separator', 'class', 'ellipsis') + array('before' => $separator));
				}
			}

		} else {
			$out .= $before;

			for ($i = 1; $i <= $params['pageCount']; $i++) {
				if ($i == $params['page']) {
					if ($class) {
						$currentClass .= ' ' . $class;
					}
					if ($currentTag) {
						$out .= $this->Html->tag($tag, $this->Html->tag($currentTag, $i), array('class' => $currentClass));
					} else {
						$out .= $this->Html->tag($tag, $i, array('class' => $currentClass));
					}
				} else {
					$out .= $this->Html->tag($tag, $this->link($i, array('page' => $i), $options), compact('class'));
				}
				if ($i != $params['pageCount']) {
					$out .= $separator;
				}
			}

			$out .= $after;
		}

		return $out;
	}

	public function first($first = '<< first', $options = array()) {
		$options = array_merge(
				array(
						'tag' => 'span',
						'after' => null,
						'model' => $this->defaultModel(),
						'separator' => ' | ',
						'ellipsis' => '...',
						'class' => null
				),
				(array)$options);

		$params = array_merge(array('page' => 1), (array)$this->params($options['model']));
		unset($options['model']);

/*		if ($params['pageCount'] <= 1) {
			return false;
		}*/
		extract($options);
		unset($options['tag'], $options['after'], $options['model'], $options['separator'], $options['ellipsis'], $options['class']);

		$out = '';

		if (is_int($first) && $params['page'] >= $first) {
			if ($after === null) {
				$after = $ellipsis;
			}
			for ($i = 1; $i <= $first; $i++) {
				$out .= $this->Html->tag($tag, $this->link($i, array('page' => $i), $options), compact('class'));
				if ($i != $first) {
					$out .= $separator;
				}
			}
			$out .= $after;
		} elseif ($params['page'] >= 1 && is_string($first)) {
			$options += array('rel' => 'first');
			if($params['page'] == 1){
				$out = $this->Html->tag($tag, $first, compact('class')) . $after;
			}else{
				$out = $this->Html->tag($tag, $this->link($first, array('page' => 1), $options), compact('class')) . $after;
			}
		}
		return $out;
	}

	public function last($last = 'last >>', $options = array()) {
		$options = array_merge(
				array(
						'tag' => 'span',
						'before' => null,
						'model' => $this->defaultModel(),
						'separator' => ' | ',
						'ellipsis' => '...',
						'class' => null
				),
				(array)$options);

		$params = array_merge(array('page' => 1), (array)$this->params($options['model']));
		unset($options['model']);

/*		if ($params['pageCount'] <= 1) {
			return false;
		}*/

		extract($options);
		unset($options['tag'], $options['before'], $options['model'], $options['separator'], $options['ellipsis'], $options['class']);

		$out = '';
		$lower = $params['pageCount'] - $last + 1;

		if (is_int($last) && $params['page'] <= $lower) {
			if ($before === null) {
				$before = $ellipsis;
			}
			for ($i = $lower; $i <= $params['pageCount']; $i++) {
				$out .= $this->Html->tag($tag, $this->link($i, array('page' => $i), $options), compact('class'));
				if ($i != $params['pageCount']) {
					$out .= $separator;
				}
			}
			$out = $before . $out;
		} elseif ($params['page'] <= $params['pageCount'] && is_string($last)) {
			$options += array('rel' => 'last');

			if($params['page'] == $params['pageCount']){
				$out = $before . $this->Html->tag(
						$tag, $last, compact('class')
				);
			}else{
				$out = $before . $this->Html->tag(
						$tag, $this->link($last, array('page' => $params['pageCount']), $options), compact('class')
				);
			}
		}
		return $out;
	}
}