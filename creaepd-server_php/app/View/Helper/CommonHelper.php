<?php
App::import('Helper', 'App');

/**
 * 共通ヘルパー
 * @author takashi_hayashi
 *
 */
class CommonHelper extends AppHelper {
	/**
	 * メッセージを取得
	 * @param string $word_key
	 */
	public function getMessage( $word_key) {
		// メッセージを取得して返す
		return $GLOBALS['message']->getMessage($word_key);
	}


	/**
	 * メッセージを表示
	 * @param string $word_key
	 */
	public function printMessage( $word_key) {
		// メッセージを表示
		print $this->getMessage($word_key);
	}
}
