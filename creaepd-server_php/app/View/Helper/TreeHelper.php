<?php
App::import('Helper', 'App');

/**
 * ツリー表示用ヘルパー
 * @author takashi_hayashi
 *
 */
class TreeHelper extends AppHelper {
	/**
	* ツリーを表示
	*/
	function printMenuTree($htmlHelper, $formHelper, $menu_array) {
		if (is_array($menu_array)) {
			echo $htmlHelper->tag('dl');
			foreach ($menu_array as $index => $menu) {
				if (!empty($menu->getChild_list())) { // 子要素があれば開閉用のボタンを配置
					echo $htmlHelper->tag('dt', null, array('class' => 'dt_tree' .' '. $menu->getActive()));
					echo $htmlHelper->tag('span', null, array('class' => $menu->getKey()));
					echo $htmlHelper->link('　', 'javascript:void(0)', array('id' => 'closer_'.$menu->getKey(), 'class' => 'tree_tab', 'onclick' => "openchild('opener_".$menu->getKey()."', 'closer_".$menu->getKey()."')"));
				} else {
					// (ホームの表示のみ)
					if ($menu->getKey() == 'home') {
						echo $htmlHelper->tag('dt', null, array('class' => 'dt_tree_none')); // 矢印なし
						echo $htmlHelper->tag('span', null, array('class' => $menu->getKey()));
						echo '　';
					} else {
						echo $htmlHelper->tag('dt', null, array('class' => 'dt_tree link'));
						echo $htmlHelper->tag('span', null);
						echo '　';
					}
				}
				// メニュー本体の表示
				echo $menu->getHtml($htmlHelper, $formHelper);
				echo $htmlHelper->tag('/span');
				echo $htmlHelper->tag('/dt');
				if ( is_array($menu->getChild_list())) { // 子メニューがあれば子メニューを表示
					if ($menu->getActive() != '' && $menu->getKey() != 'home') {
						$style = 'display:block';
					} else {
						$style = 'display:none';
					}
					echo $htmlHelper->tag('dd', null, array('id' => 'opener_'.$menu->getKey(), 'style'=>$style));
					// 子ツリーの表示(再帰構造)
					$this->printMenuTree($htmlHelper, $formHelper, $menu->getChild_list());
					echo $htmlHelper->tag('/dd');
				}
			}
			echo $htmlHelper->tag('/dl');
		}
	}
}
