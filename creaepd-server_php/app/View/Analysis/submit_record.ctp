<?php // 各種ファイルのインクルード
echo $this->Html->script('datetimeselect.js');

$this->PaginatorExt->options( array('url' => $this->passedArgs) );
// サイドメニュー
echo $this->element('SideMenu', array('menu_array' => $menu_array));  ?>

<div id="main" class="tSubmits index">
	<h2><?php echo $GLOBALS['message']->getMessage(MESSAGE_COMMON_ITEM_086); ?></h2>
	<?php echo $this->element('searchForm_analysis_submit_record')?>

	<table cellpadding="0" cellspacing="0">
	<tr>
			<th><?php echo $this->Paginator->sort('group_name', $GLOBALS['message']->getMessage(MESSAGE_ANALYSIS_ITEM_001)); ?></th>
			<th><?php echo $this->Paginator->sort('resource_name', $GLOBALS['message']->getMessage(MESSAGE_ANALYSIS_ITEM_008)); ?></th>
			<th><?php echo $this->Paginator->sort('subtitle_name', $GLOBALS['message']->getMessage(MESSAGE_ANALYSIS_ITEM_013)); ?></th>
			<th class="align-right"><?php echo $this->Paginator->sort('allot', $GLOBALS['message']->getMessage(MESSAGE_ANALYSIS_ITEM_014)); ?></th>
			<th class="align-right"><?php echo $this->Paginator->sort('avg_score', $GLOBALS['message']->getMessage(MESSAGE_ANALYSIS_ITEM_011)); ?></th>
	</tr>
	<?php foreach ($tSubmits as $tSubmit): ?>
	<tr>
		<td><?php echo h($tSubmit['MGroup']['group_name']); ?>&nbsp;</td>
		<td><?php echo h($tSubmit['mResource']['resource_name'].'['.$tSubmit['TSubmit']['submit_no'].']'); ?>&nbsp;</td>
<?php if(!empty($tSubmit['MResourceSubtitle']['subtitle_name'])) { ?>
		<td><?php echo h($tSubmit['MResourceSubtitle']['subtitle_name']); ?>&nbsp;</td>
		<td class="align-right"><?php echo h($tSubmit['TScore']['allot']); ?>&nbsp;</td>
		<td class="align-right"><?php echo h($tSubmit['TSubmit']['avg_score']); ?>&nbsp;</td>
<?php } else { ?>
		<td><?php echo h(__('－')); ?>&nbsp;</td>
		<td><?php echo h(__('－')); ?>&nbsp;</td>
		<td class="align-right"><?php echo h($tSubmit['TSubmit']['result']); ?>&nbsp;</td>
<?php } ?>
	</tr>
<?php endforeach; ?>
	</table>
	<table class="page_n">
		<?php echo $this->PaginatorExt->options(array('url' => $this->request->query,'convertKeys' => array_keys($this->request->query))); ?>
		<tr>
		<td class="first"><?php echo $this->PaginatorExt->first(__('<<'), array('class' => 'first')); ?></td>
		<td class="prev"><?php echo $this->PaginatorExt->prev(__('<'), array(), null, array('class' => 'prev disabled')); ?></td>
		<?php echo $this->PaginatorExt->numbers(array('separator' => '', 'tag'=>'td', 'currentClass'=>'active', 'first' => false, 'last' => false)); ?>
		<td class="next"><?php echo $this->PaginatorExt->next(__('>'), array(), null, array('class' => 'next disabled')); ?></td>
		<td class="last"><?php echo $this->PaginatorExt->last(__('>>'), array('class' => 'last')); ?></td>
		</tr>
	</table>
</div>
