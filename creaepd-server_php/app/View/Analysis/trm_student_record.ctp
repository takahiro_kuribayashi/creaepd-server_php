<?php
$this->PaginatorExt->options( array('url' => $this->passedArgs) );
?>
<div id="main">
	<table cellpadding="0" cellspacing="0">
	<tr>
			<th><?php echo $this->Paginator->sort('resource_name', $GLOBALS['message']->getMessage(MESSAGE_ANALYSIS_ITEM_008)); ?></th>
			<th class="align-right"><?php echo $this->Paginator->sort('result', $GLOBALS['message']->getMessage(MESSAGE_ANALYSIS_ITEM_015)); ?></th>
			<th class="align-right"><?php echo $this->Paginator->sort('rank', $GLOBALS['message']->getMessage(MESSAGE_ANALYSIS_ITEM_016)); ?></th>
			<th class="align-right"><?php echo $this->Paginator->sort('max_score', $GLOBALS['message']->getMessage(MESSAGE_ANALYSIS_ITEM_009)); ?></th>
			<th class="align-right"><?php echo $this->Paginator->sort('min_score', $GLOBALS['message']->getMessage(MESSAGE_ANALYSIS_ITEM_010)); ?></th>
			<th class="align-right"><?php echo $this->Paginator->sort('avg_score', $GLOBALS['message']->getMessage(MESSAGE_ANALYSIS_ITEM_011)); ?></th>
	</tr>
	<?php foreach ($tSubmits as $tSubmit): ?>
	<tr>
		<td><?php echo h($tSubmit['MResource']['resource_name']); ?>&nbsp;</td>
		<td class="align-right"><?php echo h($tSubmit['TSubmit']['result']); ?>&nbsp;</td>
		<td class="align-right"><?php echo h($tSubmit['Rank']['rank'].'/'.$tSubmit['Score']['result_count']); ?>&nbsp;</td>
		<td class="align-right"><?php echo h($tSubmit['Score']['max_score']); ?>&nbsp;</td>
		<td class="align-right"><?php echo h($tSubmit['Score']['min_score']); ?>&nbsp;</td>
		<td class="align-right"><?php echo h($tSubmit['Score']['avg_score']); ?>&nbsp;</td>
	</tr>
	<?php endforeach; ?>
	</table>
	<table class="page_n">
		<tr>
		<td class="first"><?php echo $this->PaginatorExt->first(__('<<'), array('class' => 'first', 'url'=>array('?'=>$query_data))); ?></td>
		<td class="prev"><?php echo $this->PaginatorExt->prev(__('<'), array('url'=>array('?'=>$query_data)), null, array('class' => 'prev disabled')); ?></td>
		<?php echo $this->PaginatorExt->numbers(array('url'=>array('?'=>$query_data) ,'separator' => '', 'tag'=>'td', 'currentClass'=>'active', 'first' => false, 'last' => false)); ?>
		<td class="next"><?php echo $this->PaginatorExt->next(__('>'), array('url'=>array('?'=>$query_data)), null, array('class' => 'next disabled')); ?></td>
		<td class="last"><?php echo $this->PaginatorExt->last(__('>>'), array('url'=>array('?'=>$query_data), 'class' => 'last')); ?></td>
		</tr>
	</table>
</div>
