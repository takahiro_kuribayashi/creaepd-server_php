<?php
$this->PaginatorExt->options( array('url' => $this->passedArgs) );
// サイドメニュー
echo $this->element('SideMenu', array('menu_array' => $menu_array));  ?>

<div id="main" class="tSubmits index">
	<h2><?php echo $GLOBALS['message']->getMessage(MESSAGE_COMMON_ITEM_085); ?></h2>
	<?php echo $this->element('searchForm_analysis_group_record')?>

	<table cellpadding="0" cellspacing="0">
<?php if ($recordType ==='1') { // 教材別・ユーザ別 ?>
	<tr>
			<th><?php echo $this->Paginator->sort('resource_name', $GLOBALS['message']->getMessage(MESSAGE_ANALYSIS_ITEM_008)); ?></th>
			<th class="align-right"><?php echo $this->Paginator->sort('max_score', $GLOBALS['message']->getMessage(MESSAGE_ANALYSIS_ITEM_009)); ?></th>
			<th class="align-right"><?php echo $this->Paginator->sort('min_score', $GLOBALS['message']->getMessage(MESSAGE_ANALYSIS_ITEM_010)); ?></th>
			<th class="align-right"><?php echo $this->Paginator->sort('avg_score', $GLOBALS['message']->getMessage(MESSAGE_ANALYSIS_ITEM_011)); ?></th>
	</tr>
	<?php foreach ($tSubmits as $tSubmit): ?>
	<tr>
		<td>
			<?php echo $this->Html->link($tSubmit['MResource']['resource_name'].'['.$tSubmit['TSubmit']['submit_no'].']', array('controller' => 'analysis', 'action' => 'group_record_resources', $tSubmit['TSubmit']['submit_no'])); ?>
		</td>
		<td class="align-right"><?php echo h($tSubmit['TSubmit']['max_score']); ?>&nbsp;</td>
		<td class="align-right"><?php echo h($tSubmit['TSubmit']['min_score']); ?>&nbsp;</td>
		<td class="align-right"><?php echo h($tSubmit['TSubmit']['avg_score']); ?>&nbsp;</td>
	</tr>
	<?php endforeach; ?>
<?php } else { ?>
	<tr>
			<th><?php echo $this->Paginator->sort('user_name', $GLOBALS['message']->getMessage(MESSAGE_ANALYSIS_ITEM_007)); ?></th>
			<th class="align-right"><?php echo $this->Paginator->sort('max_score', $GLOBALS['message']->getMessage(MESSAGE_ANALYSIS_ITEM_009)); ?></th>
			<th class="align-right"><?php echo $this->Paginator->sort('min_score', $GLOBALS['message']->getMessage(MESSAGE_ANALYSIS_ITEM_010)); ?></th>
			<th class="align-right"><?php echo $this->Paginator->sort('avg_score', $GLOBALS['message']->getMessage(MESSAGE_ANALYSIS_ITEM_011)); ?></th>
	</tr>
	<?php foreach ($tSubmits as $tSubmit): ?>
	<tr>
		<td>
			<?php echo $this->Html->link($tSubmit['MUser']['user_name'] . '[' . $tSubmit['MUser']['user_ids'] . ']', array('controller' => 'analysis', 'action' => 'group_record_users', $tSubmit['MUser']['id'])); ?>
		</td>
		<td class="align-right"><?php echo h($tSubmit['TSubmit']['max_score']); ?>&nbsp;</td>
		<td class="align-right"><?php echo h($tSubmit['TSubmit']['min_score']); ?>&nbsp;</td>
		<td class="align-right"><?php echo h($tSubmit['TSubmit']['avg_score']); ?>&nbsp;</td>
	</tr>
	<?php endforeach; ?>
<?php } ?>
	</table>
	<table class="page_n">
		<tr>
		<td class="first"><?php echo $this->PaginatorExt->first(__('<<'), array('class' => 'first')); ?></td>
		<td class="prev"><?php echo $this->PaginatorExt->prev(__('<'), array(), null, array('class' => 'prev disabled')); ?></td>
		<?php echo $this->PaginatorExt->numbers(array('separator' => '', 'tag'=>'td', 'currentClass'=>'active', 'first' => false, 'last' => false)); ?>
		<td class="next"><?php echo $this->PaginatorExt->next(__('>'), array(), null, array('class' => 'next disabled')); ?></td>
		<td class="last"><?php echo $this->PaginatorExt->last(__('>>'), array('class' => 'last')); ?></td>
		</tr>
	</table>
</div>