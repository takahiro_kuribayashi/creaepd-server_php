<?php // 各種ファイルのインクルード
echo $this->Html->script('datetimeselect.js');

$this->PaginatorExt->options( array('url' => $this->passedArgs) );
// サイドメニュー
echo $this->element('SideMenu', array('menu_array' => $menu_array));  ?>

<div id="main" class="tSubmits index">
	<h2><?php echo $GLOBALS['message']->getMessage(MESSAGE_COMMON_ITEM_093); ?></h2>
	<?php echo $this->element('searchForm_analysis_student_record')?>

	<table cellpadding="0" cellspacing="0">
	<tr>
			<th><?php echo $this->Paginator->sort('resource_name', $GLOBALS['message']->getMessage(MESSAGE_ANALYSIS_ITEM_008)); ?></th>
			<th class="align-right"><?php echo $this->Paginator->sort('result', $GLOBALS['message']->getMessage(MESSAGE_ANALYSIS_ITEM_015)); ?></th>
			<th class="align-right"><?php echo $this->Paginator->sort('rank', $GLOBALS['message']->getMessage(MESSAGE_ANALYSIS_ITEM_016)); ?></th>
			<th class="align-right"><?php echo $this->Paginator->sort('max_score', $GLOBALS['message']->getMessage(MESSAGE_ANALYSIS_ITEM_009)); ?></th>
			<th class="align-right"><?php echo $this->Paginator->sort('min_score', $GLOBALS['message']->getMessage(MESSAGE_ANALYSIS_ITEM_010)); ?></th>
			<th class="align-right"><?php echo $this->Paginator->sort('avg_score', $GLOBALS['message']->getMessage(MESSAGE_ANALYSIS_ITEM_011)); ?></th>
	</tr>
	<?php foreach ($tSubmits as $tSubmit): ?>
	<tr>
		<td><?php echo h($tSubmit['MResource']['resource_name'].'['.$tSubmit['TSubmit']['submit_no'].']'); ?>&nbsp;</td>
		<td class="align-right"><?php echo h($tSubmit['TSubmit']['result']); ?>&nbsp;</td>
		<td class="align-right"><?php echo h($tSubmit['Rank']['rank'].'/'.$tSubmit['Score']['result_count']); ?>&nbsp;</td>
		<td class="align-right"><?php echo h($tSubmit['Score']['max_score']); ?>&nbsp;</td>
		<td class="align-right"><?php echo h($tSubmit['Score']['min_score']); ?>&nbsp;</td>
		<td class="align-right"><?php echo h($tSubmit['Score']['avg_score']); ?>&nbsp;</td>
	</tr>
	<?php endforeach; ?>
	</table>
	<table class="page_n">
		<?php echo $this->PaginatorExt->options(array('url' => $this->request->query,'convertKeys' => array_keys($this->request->query))); ?>
		<tr>
		<td class="first"><?php echo $this->PaginatorExt->first(__('<<'), array('class' => 'first')); ?></td>
		<td class="prev"><?php echo $this->PaginatorExt->prev(__('<'), array(), null, array('class' => 'prev disabled')); ?></td>
		<?php echo $this->PaginatorExt->numbers(array('separator' => '', 'tag'=>'td', 'currentClass'=>'active', 'first' => false, 'last' => false)); ?>
		<td class="next"><?php echo $this->PaginatorExt->next(__('>'), array(), null, array('class' => 'next disabled')); ?></td>
		<td class="last"><?php echo $this->PaginatorExt->last(__('>>'), array('class' => 'last')); ?></td>
		</tr>
	</table>
</div>
