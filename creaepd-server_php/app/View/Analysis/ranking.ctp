<?php // 各種ファイルのインクルード
echo $this->Html->script('datetimeselect.js');

$this->PaginatorExt->options( array('url' => $this->passedArgs) );
// サイドメニュー
echo $this->element('SideMenu', array('menu_array' => $menu_array));  ?>

<div id="main" class="tSubmits index">
	<h2><?php echo $GLOBALS['message']->getMessage(MESSAGE_COMMON_ITEM_084); ?></h2>
	<?php echo $this->element('searchForm_analysis_ranking')?>

	<table cellpadding="0" cellspacing="0">
	<tr>
			<th><?php echo $this->Paginator->sort('rank', $GLOBALS['message']->getMessage(MESSAGE_ANALYSIS_ITEM_016)); ?></th>
			<th><?php echo $this->Paginator->sort('user_ids', $GLOBALS['message']->getMessage(MESSAGE_ANALYSIS_ITEM_006)); ?></th>
			<th><?php echo $this->Paginator->sort('user_name', $GLOBALS['message']->getMessage(MESSAGE_ANALYSIS_ITEM_007)); ?></th>
			<th><?php echo $this->Paginator->sort('submit_no', $GLOBALS['message']->getMessage(MESSAGE_DELIVER_ITEM_001)); ?></th>
			<th class="align-right"><?php echo $this->Paginator->sort('result', $GLOBALS['message']->getMessage(MESSAGE_ANALYSIS_ITEM_015)); ?></th>
	</tr>
	<?php foreach ($wResults as $wResult): ?>
	<tr>
		<td><?php echo h($wResult['User']['rank'].'/'.count($wResults)); ?>&nbsp;</td>
		<td><?php echo h($wResult['User']['user_ids']); ?>&nbsp;</td>
		<td><?php echo h($wResult['User']['user_name']); ?>&nbsp;</td>
		<td><?php echo h($wResult['mResource']['resource_name'] . '[' .$wResult['TSubmit']['submit_no']. ']'); ?>&nbsp;</td>
		<td class="align-right"><?php echo h($wResult['TSubmit']['result']); ?>&nbsp;</td>
	</tr>
<?php endforeach; ?>
	</table>
	<table class="page_n">
		<?php echo $this->PaginatorExt->options(array('url' => $this->request->query, 'convertKeys' => array_keys($this->request->query))); ?>
		<tr>
		<td class="first"><?php echo $this->PaginatorExt->first(__('<<'), array('class' => 'first')); ?></td>
		<td class="prev"><?php echo $this->PaginatorExt->prev(__('<'), array(), null, array('class' => 'prev disabled')); ?></td>
		<?php echo $this->PaginatorExt->numbers(array('separator' => '', 'tag'=>'td', 'currentClass'=>'active', 'first' => false, 'last' => false)); ?>
		<td class="next"><?php echo $this->PaginatorExt->next(__('>'), array(), null, array('class' => 'next disabled')); ?></td>
		<td class="last"><?php echo $this->PaginatorExt->last(__('>>'), array('class' => 'last')); ?></td>
		</tr>
	</table>
</div>