<?php // サイドメニュー
echo $this->element('SideMenu', array('menu_array' => $menu_array));  ?>

<div id="main" class="tSubmits index">
	<h2><?php echo $GLOBALS['message']->getMessage(MESSAGE_COMMON_ITEM_085) . '(' . $GLOBALS['message']->getMessage(MESSAGE_SCHEDULE_ITEM_005) . ')'; ?></h2>

	<?php echo $this->Html->tag('br/'); ?>
	<span><b><?php echo  $GLOBALS['message']->getMessage(MESSAGE_STATUS_ITEM_004) . ' : ' . $resourceName; ?></b></span>
	<table cellpadding="0" cellspacing="0">
	<tr>
			<th><?php echo $this->Paginator->sort('check_date', $GLOBALS['message']->getMessage(MESSAGE_ANALYSIS_ITEM_017)); ?></th>
			<th><?php echo $this->Paginator->sort('user_ids', $GLOBALS['message']->getMessage(MESSAGE_ANALYSIS_ITEM_006)); ?></th>
			<th><?php echo $this->Paginator->sort('user_name', $GLOBALS['message']->getMessage(MESSAGE_ANALYSIS_ITEM_007)); ?></th>
			<th class="align-right"><?php echo $this->Paginator->sort('result', $GLOBALS['message']->getMessage(MESSAGE_ANALYSIS_ITEM_015)); ?></th>
	</tr>
	<?php foreach ($tSubmits as $tSubmit): ?>
	<tr>
		<td><?php echo h($tSubmit['TSubmit']['check_date']); ?>&nbsp;</td>
		<td><?php echo h($tSubmit['MUser']['user_ids']); ?>&nbsp;</td>
		<td><?php echo h($tSubmit['MUser']['user_name']); ?>&nbsp;</td>
		<td class="align-right"><?php echo h($tSubmit['TSubmit']['result']); ?>&nbsp;</td>
	</tr>
<?php endforeach; ?>
	</table>
	<table class="page_n">
		<tr>
		<td class="first"><?php echo $this->PaginatorExt->first(__('<<'), array('url' => array('controller' => $this->name, 'action' => $this->action, $submit_no), 'class' => 'first')); ?></td>
		<td class="prev"><?php echo $this->PaginatorExt->prev(__('<'), array('url' => array('controller' => $this->name, 'action' => $this->action, $submit_no)), null, array('class' => 'prev disabled')); ?></td>
		<?php echo $this->PaginatorExt->numbers(array('url' => array('controller' => $this->name, 'action' => $this->action, $submit_no) ,'separator' => '', 'tag'=>'td', 'currentClass'=>'active', 'first' => false, 'last' => false)); ?>
		<td class="next"><?php echo $this->PaginatorExt->next(__('>'), array('url' => array('controller' => $this->name, 'action' => $this->action, $submit_no)), null, array('class' => 'next disabled')); ?></td>
		<td class="last"><?php echo $this->PaginatorExt->last(__('>>'), array('url' => array('controller' => $this->name, 'action' => $this->action, $submit_no), 'class' => 'last')); ?></td>
		</tr>
	</table>
	<div class="submit">
		<?php
			$button = $GLOBALS['message']->getMessage(MESSAGE_COMMON_ITEM_004);
			echo $this->Form->button($button, array('div'=>false, 'type'=>'button', 'name'=>'back', 'onClick' => "location.href='/creaepd-server_php/analysis/group_record';return false;"));
		?>
	</div>
</div>
