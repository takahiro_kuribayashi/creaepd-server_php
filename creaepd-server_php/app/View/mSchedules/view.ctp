<?php // サイドメニュー
echo $this->element('SideMenu', array('menu_array' => $menu_array));  ?>

<div id="main" class="mSchedules view">
<h2><?php echo $GLOBALS['message']->getMessage(MESSAGE_COMMON_ITEM_066); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($mSchedule['MSchedule']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo $GLOBALS['message']->getMessage(MESSAGE_SCHEDULE_ITEM_001); ?></dt>
		<dd>
			<?php echo h($mSchedule['MSchedule']['schedule_ids']); ?>
			&nbsp;
		</dd>
		<dt><?php echo $GLOBALS['message']->getMessage(MESSAGE_SCHEDULE_ITEM_002); ?></dt>
		<dd>
			<?php echo h($mSchedule['MSchedule']['title']); ?>
			&nbsp;
		</dd>
		<dt><?php echo $GLOBALS['message']->getMessage(MESSAGE_SCHEDULE_ITEM_005); ?></dt>
		<dd>
			<?php echo nl2br($mSchedule['MSchedule']['detail']); ?>
			&nbsp;
		</dd>
		<dt><?php echo $GLOBALS['message']->getMessage(MESSAGE_SCHEDULE_ITEM_003); ?></dt>
		<dd>
			<?php echo h($mSchedule['MSchedule']['start_datetime']); ?>
			&nbsp;
		</dd>
		<dt><?php echo $GLOBALS['message']->getMessage(MESSAGE_SCHEDULE_ITEM_004); ?></dt>
		<dd>
			<?php echo h($mSchedule['MSchedule']['end_datetime']); ?>
			&nbsp;
		</dd>
<?php if (isset($tScheduleUsers)): ?>
<?php
	$array_first_info = each($tScheduleUsers);
	$array_first_key = $array_first_info["key"];
?>
<?php foreach ($tScheduleUsers as $key => $tUser): ?>
	<?php if ($key === $array_first_key): ?>
		<dt><?php echo $GLOBALS['message']->getMessage(MESSAGE_COMMON_ITEM_087); ?></dt>
		<dd>
			<?php echo h($tUser); ?>
			&nbsp;
		</dd>
	<?php else: ?>
		<dt>&nbsp;</dt>
		<dd>
			<?php echo h($tUser); ?>
			&nbsp;
		</dd>
	<?php endif; ?>
<?php endforeach; ?>

<?php elseif (isset($tScheduleGroups)): ?>

<?php
	$array_first_info = each($tScheduleGroups);
	$array_first_key = $array_first_info["key"];
?>
<?php foreach ($tScheduleGroups as $key => $tGroup): ?>
	<?php if ($key === $array_first_key): ?>
		<dt><?php echo $GLOBALS['message']->getMessage(MESSAGE_COMMON_ITEM_087); ?></dt>
		<dd>
			<?php echo h($tGroup); ?>
			&nbsp;
		</dd>
	<?php else: ?>
		<dt>&nbsp;</dt>
		<dd>
			<?php echo h($tGroup); ?>
			&nbsp;
		</dd>
	<?php endif; ?>
<?php endforeach; ?>
<?php endif; ?>
	</dl>
	<div class="submit">
<?php
	$button = $GLOBALS['message']->getMessage(MESSAGE_COMMON_ITEM_004);
	echo $this->Form->button($button, array('div'=>false, 'type'=>'button', 'name'=>'back', 'onClick' => "location.href='/creaepd-server_php/mschedules/index';return false;"));
?>
	</div>
</div>