<?php // 各種ファイルのインクルード
echo $this->Html->script('datetimeselect.js');
// サイドメニュー
echo $this->element('SideMenu', array('menu_array' => $menu_array));  ?>

<div id="main" class="mSchedules form">
<?php
	echo $this->Form->create('MSchedule');
	$this->Form->inputDefaults(array('label' => false));
?>
	<fieldset>
		<h2><?php echo $GLOBALS['message']->getMessage(MESSAGE_COMMON_ITEM_064); ?></h2>
	<dl class="inline">
		<?php
			echo $this->Form->input('MSchedule.id');
			echo $this->Form->hidden('MSchedule.version');
		?>
		<dt><?php echo $this->Form->label('MSchedule.schedule_ids', $GLOBALS['message']->getMessage(MESSAGE_SCHEDULE_ITEM_001)); ?></dt>
		<dd>
			<?php echo $this->Form->input('MSchedule.schedule_ids', array('type'=>'text','readonly' => 'readonly')); ?>
		</dd>
		<dt><?php echo $this->Form->label('MSchedule.title', $GLOBALS['message']->getMessage(MESSAGE_SCHEDULE_ITEM_002)); ?></dt>
		<dd>
			<?php echo $this->Form->input('MSchedule.title'); ?>
		</dd>
		<dt><?php echo $this->Form->label('MSchedule.detail', $GLOBALS['message']->getMessage(MESSAGE_SCHEDULE_ITEM_005)); ?></dt>
		<dd>
			<?php echo $this->Form->input('MSchedule.detail', array('type'=>'textarea', 'cols' => 20, 'rows' => 5)); ?>
		</dd>
	<?php
		$option = array(
			'class' => 'datepicker',
			'default' => date('Y-m-d H:i', strtotime($this->data['MSchedule']['start_datetime'])),
			'timeFormat' => '24',
			'dateFormat' => 'YMD',
			'monthNames' => false,
			'empty' => false,
			'separator' => '/',
			'minYear' => date('Y')-1,
			'maxYear' => date('Y')+1,
		);
	 ?>
		<dt><div><?php echo $this->Form->label('MSchedule.start_datetime', $GLOBALS['message']->getMessage(MESSAGE_SCHEDULE_ITEM_003)); ?></div></dt>
		<dd>
			<?php echo $this->Form->input('MSchedule.start_datetime', $option); ?>
		</dd>
	<?php
		$option2 = array(
			'class' => 'datepicker',
			'default' => date('Y-m-d H:i', strtotime($this->data['MSchedule']['end_datetime'])),
			'timeFormat' => '24',
			'dateFormat' => 'YMD',
			'monthNames' => false,
			'empty' => false,
			'separator' => '/',
			'minYear' => date('Y')-1,
			'maxYear' => date('Y')+1,
		);
	 ?>
		<dt><div><?php echo $this->Form->label('MSchedule.end_datetime', $GLOBALS['message']->getMessage(MESSAGE_SCHEDULE_ITEM_004)); ?></div></dt>
		<dd>
			<?php echo $this->Form->input('MSchedule.end_datetime', $option); ?>
		</dd>
		<?php echo $this->element('SelectSubject2', array("disabled" => true)); ?>
	</dl>
	</fieldset>
	<div class="submit">
<?php
	$button = $GLOBALS['message']->getMessage(MESSAGE_COMMON_ITEM_009);
	echo $this->Form->submit($button, array('div'=>false, 'class'=>'confirm', 'name'=>'complete'));
	$button2 = $GLOBALS['message']->getMessage(MESSAGE_COMMON_ITEM_004);
	echo $this->Form->button($button2, array('div'=>false, 'type'=>'button', 'name'=>'back', 'onClick'=>"location.href='/creaepd-server_php/mschedules/index';return false;"));
?>
	</div>
<?php echo $this->Form->end(); ?>

<?php $this->Html->scriptStart(); ?>
	$(function(){
		$('input.confirm').exJConfirm(
			'<?php echo $GLOBALS['message']->getMessage(MESSAGE_SCHEDULE_INIT_002); ?>',
			'<?php echo $GLOBALS['message']->getMessage(MESSAGE_COMMON_ITEM_021); ?>'
		);
	});
<?php echo $this->Html->scriptEnd(); ?>
</div>
