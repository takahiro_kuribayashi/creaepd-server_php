<?php // サイドメニュー
echo $this->element('SideMenu', array('menu_array' => $menu_array));  ?>

<div id="main" class="mSchedules form">
<?php echo $this->Form->create('MSchedule'); ?>
	<fieldset>
		<h2><?php echo $GLOBALS['message']->getMessage(MESSAGE_COMMON_ITEM_065); ?></h2>
	<?php
		echo $this->Form->input('id', array('value' => $mSchedule['MSchedule']['id']));
		echo $this->Form->hidden('delete_flg', array('value' => '1'));
	?>
	</fieldset>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($mSchedule['MSchedule']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo $GLOBALS['message']->getMessage(MESSAGE_SCHEDULE_ITEM_001); ?></dt>
		<dd>
			<?php echo h($mSchedule['MSchedule']['schedule_ids']); ?>
			&nbsp;
		</dd>
		<dt><?php echo $GLOBALS['message']->getMessage(MESSAGE_SCHEDULE_ITEM_002); ?></dt>
		<dd>
			<?php echo h($mSchedule['MSchedule']['title']); ?>
			&nbsp;
		</dd>
		<dt><?php echo $GLOBALS['message']->getMessage(MESSAGE_SCHEDULE_ITEM_005); ?></dt>
		<dd>
			<?php echo nl2br($mSchedule['MSchedule']['detail']); ?>
			&nbsp;
		</dd>
		<dt><?php echo $GLOBALS['message']->getMessage(MESSAGE_SCHEDULE_ITEM_003); ?></dt>
		<dd>
			<?php echo h($mSchedule['MSchedule']['start_datetime']); ?>
			&nbsp;
		</dd>
		<dt><?php echo $GLOBALS['message']->getMessage(MESSAGE_SCHEDULE_ITEM_004); ?></dt>
		<dd>
			<?php echo h($mSchedule['MSchedule']['end_datetime']); ?>
			&nbsp;
		</dd>
	</dl>
	<div class="submit">
<?php
	$button = $GLOBALS['message']->getMessage(MESSAGE_COMMON_ITEM_010);
	echo $this->Form->submit($button, array('div'=>false, 'class'=>'confirm', 'name'=>'complete'));
	$button2 = $GLOBALS['message']->getMessage(MESSAGE_COMMON_ITEM_004);
	echo $this->Form->button($button2, array('div'=>false, 'type'=>'button', 'name'=>'back', 'onClick'=>"location.href='/creaepd-server_php/mschedules/index';return false;"));
?>
	</div>
<?php echo $this->Form->end(); ?>

<?php $this->Html->scriptStart(); ?>
	$(function(){
		$('input.confirm').exJConfirm(
			'<?php echo $GLOBALS['message']->getMessage(MESSAGE_SCHEDULE_INIT_003); ?>',
			'<?php echo $GLOBALS['message']->getMessage(MESSAGE_COMMON_ITEM_021); ?>'
		);
	});
<?php echo $this->Html->scriptEnd(); ?>
</div>