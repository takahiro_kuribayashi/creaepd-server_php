<?php // 各種ファイルのインクルード
echo $this->Html->script('datetimeselect.js');
// サイドメニュー
echo $this->element('SideMenu', array('menu_array' => $menu_array));  ?>

<div id="main" class="mSettings form">
<?php
	echo $this->Form->create('MSetting');
	$this->Form->inputDefaults(array('label' => false));
?>
	<fieldset>

		<h2><?php echo $GLOBALS['message']->getMessage(MESSAGE_COMMON_ITEM_080); ?></h2>
	<dl class="inline">
		<?php echo $this->Form->input('MSetting.id'); ?>
		<dt><?php echo $GLOBALS['message']->getMessage(MESSAGE_SYSTEM_ITEM_003); ?></dt>
		<dd>
			<?php echo $this->Form->input('MSetting.language_id', array('type'=>'radio','options'=>$mLanguages,'legend' => false)); ?>
		</dd>
	</dl>
	</fieldset>
	<div class="submit">
<?php
	$button = $GLOBALS['message']->getMessage(MESSAGE_COMMON_ITEM_001);
	echo $this->Form->submit($button, array('div'=>false, 'class'=>'confirm', 'name'=>'complete'));
	$button2 = $GLOBALS['message']->getMessage(MESSAGE_COMMON_ITEM_004);
	echo $this->Form->button($button2, array('div'=>false, 'type'=>'button', 'name'=>'back', 'onClick'=>"location.href='/creaepd-server_php/top/';return false;"));
?>
	</div>
<?php echo $this->Form->end(); ?>

<?php $this->Html->scriptStart(); ?>
	$(function(){
		$('input.confirm').exJConfirm(
			'<?php echo $GLOBALS['message']->getMessage(MESSAGE_SYSTEM_INIT_001); ?>',
			'<?php echo $GLOBALS['message']->getMessage(MESSAGE_COMMON_ITEM_021); ?>'
		);
	});
<?php echo $this->Html->scriptEnd(); ?>
</div>
