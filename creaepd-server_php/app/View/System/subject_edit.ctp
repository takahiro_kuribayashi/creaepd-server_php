<?php // サイドメニュー
echo $this->element('SideMenu', array('menu_array' => $menu_array));  ?>

<div id="main" class="mResource form">
<?php
	echo $this->Form->create('MResourceSubject');
	$this->Form->inputDefaults(array('label' => false));
?>
	<fieldset>
		<h2><?php echo $GLOBALS['message']->getMessage(MESSAGE_COMMON_ITEM_081); ?></h2>

	<dl class="inline">
		<dt><?php echo $this->Form->label('MResourceSubject.subject_name', $GLOBALS['message']->getMessage(MESSAGE_SYSTEM_ITEM_004), array('class'=>'subtittle')); ?></dt>
		<dd>
			<?php echo $this->Form->input('MResourceSubject.subject_name'); ?>
		</dd>
	</dl>
	</fieldset>
	<div class="submit">
<?php
	$button = $GLOBALS['message']->getMessage(MESSAGE_COMMON_ITEM_001);
	echo $this->Form->submit($button, array('div'=>false, 'class'=>'confirm', 'name'=>'complete'));
	$button2 = $GLOBALS['message']->getMessage(MESSAGE_COMMON_ITEM_004);
	echo $this->Form->button($button2, array('div'=>false, 'type'=>'button', 'name'=>'back', 'onClick'=>"location.href='/creaepd-server_php/system/subject_index';return false;"));
?>
	</div>
<?php echo $this->Form->end(); ?>

<?php $this->Html->scriptStart(); ?>
	$(function(){
		$('input.confirm').exJConfirm(
			'<?php echo $GLOBALS['message']->getMessage(MESSAGE_SYSTEM_INIT_001); ?>',
			'<?php echo $GLOBALS['message']->getMessage(MESSAGE_COMMON_ITEM_021); ?>'
		);
	});
<?php echo $this->Html->scriptEnd(); ?>
</div>
