<?php
$this->PaginatorExt->options( array('url' => $this->passedArgs) );
// サイドメニュー
echo $this->element('SideMenu', array('menu_array' => $menu_array));  ?>

<div id="main" class="mResourceSubjects index">
	<h2><?php echo $GLOBALS['message']->getMessage(MESSAGE_COMMON_ITEM_094); ?></h2>
	<?php echo $this->element('searchForm_subject')?>

	<table cellpadding="0" cellspacing="0">
	<tr>
			<th><?php echo $this->Paginator->sort('id', __('No.')); ?></th>
			<th><?php echo $this->Paginator->sort('subject_name', $GLOBALS['message']->getMessage(MESSAGE_SYSTEM_ITEM_004)); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($mResourceSubjects as $mResourceSubject): ?>
	<tr>
		<td><?php echo h($mResourceSubject['MResourceSubject']['id']); ?>&nbsp;</td>
		<td><?php echo h($mResourceSubject['MResourceSubject']['subject_name']); ?>&nbsp;</td>
		<td class="actions">
			<?php
				$button = $GLOBALS['message']->getMessage(MESSAGE_COMMON_ITEM_009);
				echo $this->Html->link($button, array('action' => 'subject_edit', $mResourceSubject['MResourceSubject']['id']));
			?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	<table class="page_n">
		<tr>
		<td class="first"><?php echo $this->PaginatorExt->first(__('<<'), array('class' => 'first')); ?></td>
		<td class="prev"><?php echo $this->PaginatorExt->prev(__('<'), array(), null, array('class' => 'prev disabled')); ?></td>
		<?php echo $this->PaginatorExt->numbers(array('separator' => '', 'tag'=>'td', 'currentClass'=>'active', 'first' => false, 'last' => false)); ?>
		<td class="next"><?php echo $this->PaginatorExt->next(__('>'), array(), null, array('class' => 'next disabled')); ?></td>
		<td class="last"><?php echo $this->PaginatorExt->last(__('>>'), array('class' => 'last')); ?></td>
		</tr>
	</table>
</div>
