<?php // サイドメニュー
echo $this->element('SideMenu', array('menu_array' => $menu_array));  ?>

<div id="main" class="mUpdates view">
<h2><?php echo $GLOBALS['message']->getMessage(MESSAGE_COMMON_ITEM_083); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($mUpdate['MUpdate']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo $GLOBALS['message']->getMessage(MESSAGE_SYSTEM_ITEM_006); ?></dt>
		<dd>
			<?php echo h($mUpdate['MUpdate']['version']); ?>
			&nbsp;
		</dd>
		<dt><?php echo $GLOBALS['message']->getMessage(MESSAGE_SYSTEM_ITEM_007); ?></dt>
		<dd>
			<?php echo h($mUpdate['MUpdate']['apk_file']); ?>
			&nbsp;
		</dd>
		<dt><?php echo $GLOBALS['message']->getMessage(MESSAGE_SYSTEM_ITEM_008); ?></dt>
		<dd>
			<?php echo nl2br($mUpdate['MUpdate']['description']); ?>
			&nbsp;
		</dd>
		<dt><?php echo $GLOBALS['message']->getMessage(MESSAGE_SYSTEM_ITEM_009); ?></dt>
		<dd>
			<?php echo h($mUpdate['MUpdate']['created']); ?>
			&nbsp;
		</dd>
	</dl>
	<div class="submit">
<?php
	$button = $GLOBALS['message']->getMessage(MESSAGE_COMMON_ITEM_004);
	echo $this->Form->button($button, array('div'=>false, 'type'=>'button', 'name'=>'back', 'onClick' => "location.href='/creaepd-server_php/system/update_index';return false;"));
?>
	</div>
</div>