<?php // サイドメニュー
echo $this->element('SideMenu', array('menu_array' => $menu_array));  ?>

<div id="main" class="mSettings form">
<?php
	echo $this->Form->create('MSetting', array('type' => 'file'));
	$this->Form->inputDefaults(array('label' => false));
?>
	<fieldset>
		<h2><?php echo $GLOBALS['message']->getMessage(MESSAGE_COMMON_ITEM_079); ?></h2>
	<dl class="inline">
		<?php echo $this->Form->input('MSetting.id'); ?>
		<dt><?php echo $this->Form->label('MSetting.logo_file', $GLOBALS['message']->getMessage(MESSAGE_SYSTEM_ITEM_001)); ?></dt>
		<dd>
<?php if(!empty($this->request->data['MSetting']['logo_file'])) { ?>
			<?php echo h('（'.$GLOBALS['message']->getMessage(MESSAGE_COMMON_ITEM_020).'）'.$this->request->data['MSetting']['logo_file']); ?>
<?php } ?>
			<div>
				<span class="inline_box">
					<?php echo $this->Form->input('MSetting.logo_file', array('error'=>false, 'label' => $GLOBALS['message']->getMessage(MESSAGE_COMMON_ITEM_023),'type' => 'file', 'onChange'=>"uv.value = this.value;")); ?>
					<input type="text" id="uv" class="uploadValue" disabled />
				</span>
			</div>
<?php if(!empty($validation_errors['logo_file'][0])) { ?>
				<strong style="color:red;"><?php echo h($validation_errors['logo_file'][0]); ?></strong>
<?php } ?>
		</dd>
		<dt><?php echo $this->Form->label('MSetting.basecolor_id', $GLOBALS['message']->getMessage(MESSAGE_SYSTEM_ITEM_002)); ?></dt>
		<dd>
			<?php echo $this->Form->input('MSetting.basecolor_id', array('type'=>'select','options'=>$baseColors)); ?>
		</dd>
	</dl>
	</fieldset>
	<div class="submit">
<?php
	$button = $GLOBALS['message']->getMessage(MESSAGE_COMMON_ITEM_001);
	echo $this->Form->submit($button, array('div'=>false, 'class'=>'confirm', 'name'=>'complete'));
	$button2 = $GLOBALS['message']->getMessage(MESSAGE_COMMON_ITEM_004);
	echo $this->Form->button($button2, array('div'=>false, 'type'=>'button', 'name'=>'back', 'onClick'=>"location.href='/creaepd-server_php/top/';return false;"));
?>
	</div>
<?php echo $this->Form->end(); ?>

<?php $this->Html->scriptStart(); ?>
	$(function(){
		$('input.confirm').exJConfirm(
			'<?php echo $GLOBALS['message']->getMessage(MESSAGE_SYSTEM_INIT_001); ?>',
			'<?php echo $GLOBALS['message']->getMessage(MESSAGE_COMMON_ITEM_021); ?>'
		);
	});
<?php echo $this->Html->scriptEnd(); ?>
</div>
