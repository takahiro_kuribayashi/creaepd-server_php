<?php // サイドメニュー
echo $this->element('SideMenu', array('menu_array' => $menu_array));  ?>

<div id="main" class="mUpdates form">
<?php
	echo $this->Form->create('MUpdate', array('type' => 'file'));
	$this->Form->inputDefaults(array('label' => false));
?>
	<fieldset>
		<h2><?php echo $GLOBALS['message']->getMessage(MESSAGE_COMMON_ITEM_083); ?></h2>
	<dl class="inline">
		<dt><?php echo $this->Form->label('MUpdate.version', $GLOBALS['message']->getMessage(MESSAGE_SYSTEM_ITEM_006)); ?></dt>
		<dd>
			<?php echo $this->Form->input('MUpdate.version'); ?>
		</dd>
		<dt><?php echo $this->Form->label('MUpdate.apk_file', $GLOBALS['message']->getMessage(MESSAGE_SYSTEM_ITEM_007)); ?></dt>
		<dd>
			<?php echo $this->Form->input('MUpdate.apk_file', array('type' => 'file', 'required' => true)); ?>
		</dd>
		<dt><div><?php echo $this->Form->label('MUpdate.description', $GLOBALS['message']->getMessage(MESSAGE_SYSTEM_ITEM_008)); ?></div></dt>
		<dd>
			<?php echo $this->Form->input('MUpdate.description', array('type'=>'textarea', 'cols' => 20, 'rows' => 5)); ?>
		</dd>
	</dl>
	</fieldset>
	<div class="submit">
<?php
	$button = $GLOBALS['message']->getMessage(MESSAGE_COMMON_ITEM_001);
	echo $this->Form->submit($button, array('div'=>false, 'class'=>'confirm', 'name'=>'complete'));
	$button2 = $GLOBALS['message']->getMessage(MESSAGE_COMMON_ITEM_004);
	echo $this->Form->button($button2, array('div'=>false, 'type'=>'button', 'name'=>'back', 'onClick'=>"location.href='/creaepd-server_php/system/update_index';return false;"));
?>
	</div>
<?php echo $this->Form->end(); ?>

<?php $this->Html->scriptStart(); ?>
	$(function(){
		$('input.confirm').exJConfirm(
			'<?php echo $GLOBALS['message']->getMessage(MESSAGE_SYSTEM_INIT_001); ?>',
			'<?php echo $GLOBALS['message']->getMessage(MESSAGE_COMMON_ITEM_021); ?>'
		);
	});
<?php echo $this->Html->scriptEnd(); ?>
</div>
