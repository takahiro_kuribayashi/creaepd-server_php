<?php // サイドメニュー
echo $this->element('SideMenu', array('menu_array' => $menu_array));  ?>

<div id="main" class="mUpdates index">
	<h2><?php echo $GLOBALS['message']->getMessage(MESSAGE_COMMON_ITEM_083); ?></h2>

<div class="submit">
	<?php
		$button = $GLOBALS['message']->getMessage(MESSAGE_COMMON_ITEM_001);
		echo $this->Form->button($button, array('div'=>false, 'type'=>'button', 'name'=>'add', 'onClick'=>"location.href='update_add';return false;"));
		$button = $GLOBALS['message']->getMessage(MESSAGE_COMMON_ITEM_004);
		echo $this->Form->button($button, array('div'=>false, 'type'=>'button', 'name'=>'back', 'onClick' => "location.href='/creaepd-server_php/top/';return false;"));
	?>
</div>
	<?php echo $this->Html->tag('br/'); ?>
	<table cellpadding="0" cellspacing="0">
	<tr>
			<th><?php echo $this->Paginator->sort('id', __('No.')); ?></th>
			<th><?php echo $this->Paginator->sort('version', $GLOBALS['message']->getMessage(MESSAGE_SYSTEM_ITEM_006)); ?></th>
			<th><?php echo $this->Paginator->sort('created', $GLOBALS['message']->getMessage(MESSAGE_SYSTEM_ITEM_009)); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($mUpdates as $mUpdate): ?>
	<tr>
		<td><?php echo h($mUpdate['MUpdate']['id']); ?>&nbsp;</td>
		<td><?php echo h($mUpdate['MUpdate']['version']); ?>&nbsp;</td>
		<td><?php echo h($mUpdate['MUpdate']['created']); ?>&nbsp;</td>
		<td class="actions">
			<?php
				$button = $GLOBALS['message']->getMessage(MESSAGE_COMMON_ITEM_008);
				echo $this->Html->link($button, array('action' => 'update_view', $mUpdate['MUpdate']['id']));
			?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
</div>
