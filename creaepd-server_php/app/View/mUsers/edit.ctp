<?php // サイドメニュー
echo $this->element('SideMenu', array('menu_array' => $menu_array));  ?>

<div id="main" class="mUsers form">
<?php
	echo $this->Form->create('MUser');
	$this->Form->inputDefaults(array('label' => false));
?>
	<fieldset>
		<h2><?php echo $GLOBALS['message']->getMessage(MESSAGE_COMMON_ITEM_069); ?></h2>
	<dl class="inline">
		<?php echo $this->Form->hidden('reset_flg', array('default' => '0')); ?>
		<?php echo $this->Form->hidden('MUser.superuser_flg', array('default' => '0')); ?>
		<?php echo $this->Form->input('MUser.id'); ?>
		<dt><?php echo $this->Form->label('user_ids', $GLOBALS['message']->getMessage(MESSAGE_USER_ITEM_001)); ?></dt>
		<dd>
			<?php echo $this->Form->input('user_ids', array('style'=>'ime-mode:disabled;')); ?>
		</dd>
		<dt><?php echo $this->Form->label('user_name', $GLOBALS['message']->getMessage(MESSAGE_USER_ITEM_002)); ?></dt>
		<dd>
			<?php echo $this->Form->input('user_name'); ?>
		</dd>
		<dt><?php echo $this->Form->label('auth_pass', $GLOBALS['message']->getMessage(MESSAGE_COMMON_ITEM_006)); ?></dt>
		<dd>
			<?php
				if(isset($this->request->data['MUser']['reset_flg']) && $this->request->data['MUser']['reset_flg']=='1') {
					echo $this->Form->input('auth_pass', array('type'=>'text', 'default' => $auth_pass));
				} else {
					echo $this->Form->input('auth_pass', array('type'=>'password', 'readonly' => 'readonly', 'default' => $auth_pass));
				}
			?>
	<?php
		$button = $GLOBALS['message']->getMessage(MESSAGE_COMMON_ITEM_015);
		if(!($this->request->data['MUser']['superuser_flg']) || ($this->request->data['MUser']['id'] == $userinfo['id'])) {
			echo $this->Form->submit($button, array('class'=>'reset_confirm', 'name'=>'pass_reset'));
		}
	 ?>
		</dd>
		<dt><div><?php echo $this->Form->label('parent_address1', $GLOBALS['message']->getMessage(MESSAGE_USER_ITEM_003)); ?></div></dt>
		<dd>
			<?php echo $this->Form->input('parent_address1'); ?>
			<?php echo $this->Form->input('parent_address2'); ?>
		</dd>
		<dt><?php echo $this->Form->label('user_type_id', $GLOBALS['message']->getMessage(MESSAGE_USER_ITEM_004)); ?></dt>
		<dd>
			<?php echo $this->Form->input('user_type_id', array('type'=>'select','options'=>$userTypes)); ?>
		</dd>
		<dt><div><?php echo $this->Form->label('admin_address', $GLOBALS['message']->getMessage(MESSAGE_USER_ITEM_005)); ?></div></dt>
		<dd>
			<?php echo $this->Form->input('admin_address'); ?>
		</dd>
		<dt><?php echo $GLOBALS['message']->getMessage(MESSAGE_USER_ITEM_006); ?></dt>
		<dd>
			<?php echo $this->Form->input('ancestor_group_id', array('type'=>'select','options'=>$allGroups)); ?>
		</dd>
	</dl>
	</fieldset>
	<div class="submit">
<?php
	if(!($this->request->data['MUser']['superuser_flg'])) {
		$button = $GLOBALS['message']->getMessage(MESSAGE_COMMON_ITEM_009);
		echo $this->Form->submit($button, array('div'=>false, 'class'=>'confirm', 'name'=>'complete'));
	}
	$button2 = $GLOBALS['message']->getMessage(MESSAGE_COMMON_ITEM_004);
	echo $this->Form->button($button2, array('div'=>false, 'type'=>'button', 'name'=>'back', 'onClick'=>"location.href='/creaepd-server_php/musers/index';return false;"));
?>
<?php if($this->request->data['MUser']['superuser_flg']) { ?>
	 <p><strong style="color:red;"><?php echo $GLOBALS['message']->getMessage(MESSAGE_USER_WARN_001); ?></strong></p>
<?php } ?>
	</div>
<?php echo $this->Form->end(); ?>

<?php $this->Html->scriptStart(); ?>
	$(function(){
		$('input.confirm').exJConfirm(
			'<?php echo $GLOBALS['message']->getMessage(MESSAGE_USER_INIT_002); ?>',
			'<?php echo $GLOBALS['message']->getMessage(MESSAGE_COMMON_ITEM_021); ?>'
		);
		$('input.reset_confirm').exJConfirm(
			'<?php echo $GLOBALS['message']->getMessage(MESSAGE_USER_INIT_005); ?>',
			'<?php echo $GLOBALS['message']->getMessage(MESSAGE_COMMON_ITEM_021); ?>'
		);
	});
<?php echo $this->Html->scriptEnd(); ?>
</div>
