<?php // サイドメニュー
echo $this->element('SideMenu', array('menu_array' => $menu_array));  ?>

<div id="main" class="mUsers view">
<h2><?php echo $GLOBALS['message']->getMessage(MESSAGE_COMMON_ITEM_071); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($mUser['MUser']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo $GLOBALS['message']->getMessage(MESSAGE_USER_ITEM_001); ?></dt>
		<dd>
			<?php echo h($mUser['MUser']['user_ids']); ?>
			&nbsp;
		</dd>
		<dt><?php echo $GLOBALS['message']->getMessage(MESSAGE_USER_ITEM_002); ?></dt>
		<dd>
			<?php echo h($mUser['MUser']['user_name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo $GLOBALS['message']->getMessage(MESSAGE_USER_ITEM_003); ?></dt>
		<dd>
			<?php echo h($parent1); ?>
			<?php echo h($parent2); ?>
			&nbsp;
		</dd>
		<dt><?php echo $GLOBALS['message']->getMessage(MESSAGE_USER_ITEM_004); ?></dt>
		<dd>
			<?php echo h($mUser['UserType']['user_type_name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo $GLOBALS['message']->getMessage(MESSAGE_USER_ITEM_005); ?></dt>
		<dd>
			<?php echo h($mUser['MUser']['admin_address']); ?>
			&nbsp;
		</dd>
	</dl>
	<div class="submit">
<?php
	$button = $GLOBALS['message']->getMessage(MESSAGE_COMMON_ITEM_004);
	echo $this->Form->button($button, array('div'=>false, 'type'=>'button', 'name'=>'back', 'onClick' => "location.href='/creaepd-server_php/musers/index';return false;"));
?>
	</div>
</div>
