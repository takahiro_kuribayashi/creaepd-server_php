<?php // サイドメニュー
echo $this->element('SideMenu', array('menu_array' => $menu_array));  ?>

<div id="main" class="mUsers form">
<?php echo $this->Form->create('MUser'); ?>
	<fieldset>
		<h2><?php echo $GLOBALS['message']->getMessage(MESSAGE_COMMON_ITEM_070); ?></h2>
	<?php
		echo $this->Form->input('id', array('value' => $mUser['MUser']['id']));
		echo $this->Form->hidden('delete_flg', array('value' => '1'));
	?>
	</fieldset>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($mUser['MUser']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo $GLOBALS['message']->getMessage(MESSAGE_USER_ITEM_001); ?></dt>
		<dd>
			<?php echo h($mUser['MUser']['user_ids']); ?>
			&nbsp;
		</dd>
		<dt><?php echo $GLOBALS['message']->getMessage(MESSAGE_USER_ITEM_002); ?></dt>
		<dd>
			<?php echo h($mUser['MUser']['user_name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo $GLOBALS['message']->getMessage(MESSAGE_USER_ITEM_003); ?></dt>
		<dd>
			<?php echo h($parent1); ?>
			<?php echo h($parent2); ?>
			&nbsp;
		</dd>
		<dt><?php echo $GLOBALS['message']->getMessage(MESSAGE_USER_ITEM_004); ?></dt>
		<dd>
			<?php echo h($mUser['UserType']['user_type_name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo $GLOBALS['message']->getMessage(MESSAGE_USER_ITEM_005); ?></dt>
		<dd>
			<?php echo h($mUser['MUser']['admin_address']); ?>
			&nbsp;
		</dd>
	</dl>
	<div class="submit">
<?php
	if(!($mUser['MUser']['superuser_flg'])) {
		$button = $GLOBALS['message']->getMessage(MESSAGE_COMMON_ITEM_010);
		echo $this->Form->submit($button, array('div'=>false, 'class'=>'confirm', 'name'=>'complete'));
	}
	$button2 = $GLOBALS['message']->getMessage(MESSAGE_COMMON_ITEM_004);
	echo $this->Form->button($button2, array('div'=>false, 'type'=>'button', 'name'=>'back', 'onClick'=>"location.href='/creaepd-server_php/musers/index';return false;"));
?>
<?php if($mUser['MUser']['superuser_flg']) { ?>
	 <p><strong style="color:red;"><?php echo $GLOBALS['message']->getMessage(MESSAGE_USER_WARN_002); ?></strong></p>
<?php } ?>
	</div>
<?php echo $this->Form->end(); ?>

<?php $this->Html->scriptStart(); ?>
	$(function(){
		$('input.confirm').exJConfirm(
			'<?php echo $GLOBALS['message']->getMessage(MESSAGE_USER_INIT_003); ?>',
			'<?php echo $GLOBALS['message']->getMessage(MESSAGE_COMMON_ITEM_021); ?>'
		);
	});
<?php echo $this->Html->scriptEnd(); ?>
</div>
