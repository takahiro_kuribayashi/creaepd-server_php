<?php // サイドメニュー
echo $this->element('SideMenu', array('menu_array' => $menu_array));  ?>

<div id="main" class="mUsers form">
<?php
	echo $this->Form->create('MUser');
	$this->Form->inputDefaults(array('label' => false));
?>
	<fieldset>
		<h2><?php echo $GLOBALS['message']->getMessage(MESSAGE_COMMON_ITEM_067); ?></h2>
	<dl class="inline">
		<dt><?php echo $this->Form->label('user_ids', $GLOBALS['message']->getMessage(MESSAGE_USER_ITEM_001)); ?></dt>
		<dd>
			<?php echo $this->Form->input('user_ids', array('style'=>'ime-mode:disabled;')); ?>
		</dd>
		<dt><?php echo $this->Form->label('user_name', $GLOBALS['message']->getMessage(MESSAGE_USER_ITEM_002)); ?></dt>
		<dd>
			<?php echo $this->Form->input('user_name'); ?>
		</dd>
		<dt><?php echo $this->Form->label('auth_pass', $GLOBALS['message']->getMessage(MESSAGE_COMMON_ITEM_006)); ?></dt>
		<dd>
			<?php echo $this->Form->input('auth_pass', array('default' => $auth_pass)); ?>
		</dd>
		<dt><div><?php echo $this->Form->label('parent_address1', $GLOBALS['message']->getMessage(MESSAGE_USER_ITEM_003)); ?></div></dt>
		<dd>
			<?php echo $this->Form->input('parent_address1'); ?>
			<?php echo $this->Form->input('parent_address2'); ?>
		</dd>
		<dt><?php echo $this->Form->label('user_type_id', $GLOBALS['message']->getMessage(MESSAGE_USER_ITEM_004)); ?></dt>
		<dd>
			<?php echo $this->Form->input('user_type_id', array('type'=>'select','options'=>$userTypes)); ?>
		</dd>
		<dt><div><?php echo $this->Form->label('admin_address', $GLOBALS['message']->getMessage(MESSAGE_USER_ITEM_005)); ?></div></dt>
		<dd>
			<?php echo $this->Form->input('admin_address'); ?>
		</dd>
		<dt><?php echo $GLOBALS['message']->getMessage(MESSAGE_USER_ITEM_006); ?></dt>
		<dd>
			<?php echo $this->Form->input('ancestor_group_id', array('type'=>'select','options'=>$allGroups)); ?>
		</dd>
	</dl>
	</fieldset>
	<div class="submit">
<?php
	$button = $GLOBALS['message']->getMessage(MESSAGE_COMMON_ITEM_001);
	echo $this->Form->submit($button, array('div'=>false, 'class'=>'confirm', 'name'=>'complete'));
	$button2 = $GLOBALS['message']->getMessage(MESSAGE_COMMON_ITEM_004);
	echo $this->Form->button($button2, array('div'=>false, 'type'=>'button', 'name'=>'back', 'onClick'=>"location.href='/creaepd-server_php/musers/index';return false;"));
?>
	</div>
<?php echo $this->Form->end(); ?>

<?php $this->Html->scriptStart(); ?>
	$(function(){
		$('input.confirm').exJConfirm(
			'<?php echo $GLOBALS['message']->getMessage(MESSAGE_USER_INIT_001); ?>',
			'<?php echo $GLOBALS['message']->getMessage(MESSAGE_COMMON_ITEM_021); ?>'
		);
	});
<?php echo $this->Html->scriptEnd(); ?>
</div>
