<?php echo $this->Html->script('jquery-1.10.2', array( 'inline' => false ));  ?>

<?php // サイドメニュー
echo $this->element('SideMenu', array('menu_array' => $menu_array));  ?>

<div id="main" class="mUsers form">
<?php
	echo $this->Form->create('Post', array('type' => 'file', 'url' => '/musers/multi_add', 'method' => 'post'));
	$this->Form->inputDefaults(array('label' => false));
?>
	<fieldset>
		<h2><?php echo $GLOBALS['message']->getMessage(MESSAGE_COMMON_ITEM_078); ?></h2>
		<dl class="inline">
			<dt><?php echo $this->Form->label('user_type_id', $GLOBALS['message']->getMessage(MESSAGE_USER_ITEM_006)); ?></dt>
			<dd>
				<?php echo $this->Form->input('group_id', array('type'=>'select','options'=>$mGroups)); ?>
			</dd>
			<dt><?php echo $this->Form->label('csv.attachment', $GLOBALS['message']->getMessage(MESSAGE_USER_ITEM_007)); ?></dt>
			<dd>
				<div>
					<span class="inline_box">
						<?php echo $this->Form->input('csv.attachment', array('label' => $GLOBALS['message']->getMessage(MESSAGE_COMMON_ITEM_023),'type' => 'file', 'onChange'=>"uv.value = this.value;")); ?>
						<input type="text" id="uv" class="uploadValue" disabled />
					</span>
				</div>
				<?php
					if (isset($validationErrors) && is_array($validationErrors)) {
						foreach ($validationErrors as $key => $validations) {
							$row = $key + 1;
							foreach ($validations as $errors) {
								if (is_array($errors)) {
									// CSV読み込みエラー
									foreach ($errors as $values) {
										foreach ($values as $value) {
											echo '<strong style="color:red;">['.$row.'行目]'.$value.'</strong></br>';
										}
									}
								} else {
									// ファイル拡張子エラー
									echo '<strong style="color:red;">'.$errors.'</strong></br>';
								}
							}
						}
					}
				?>
			</dd>
		</dl>
	</fieldset>
	<div class="submit">
<?php
	$button = $GLOBALS['message']->getMessage(MESSAGE_COMMON_ITEM_011);
	echo $this->Form->submit($button, array('div'=>false, 'class'=>'confirm', 'name'=>'complete'));
	$button2 = $GLOBALS['message']->getMessage(MESSAGE_COMMON_ITEM_004);
	echo $this->Form->button($button2, array('div'=>false, 'type'=>'button', 'name'=>'back', 'onClick'=>"location.href='/creaepd-server_php/musers/index';return false;"));
?>
	</div>
<?php echo $this->Form->end(); ?>

<?php $this->Html->scriptStart(); ?>
	$(function(){
		$('input.confirm').exJConfirm(
			'<?php echo $GLOBALS['message']->getMessage(MESSAGE_USER_INIT_001); ?>',
			'<?php echo $GLOBALS['message']->getMessage(MESSAGE_COMMON_ITEM_021); ?>'
		);
	});
<?php echo $this->Html->scriptEnd(); ?>

<?php if (Configure::read('database') === 'development' && DIRECTORY_SEPARATOR == '\\'): ?>
<form enctype="multipart/form-data" action="" method="post" id="upload_form">
	<input type="hidden" name="user_id" value="crea0002" />
	<input type="hidden" name="access_key" value="abcdefghijklmnopqrstuvwxyz012345" />
	<input type="hidden" name="check_status" value="2" />

	<input name="resource_file" type="file" />
</form>

<input id="urlText" />
<input id="sendData" type="submit" />
<img id="img" src="http://localhost/creaepd-server_php/creaepd-server_php/img/cake.icon.png" />
	<?php $this->Html->scriptStart(array('inline'=>false)); ?>
	function ajaxMethod() {
/*		$.ajax({
			url: "http://ec2-54-64-191-24.ap-northeast-1.compute.amazonaws.com/creaepd-server_php/api_resources/index",
			type: "POST",
			data: {
				user_id: "crea0002",
				access_key: "abcdefghijklmnopqrstuvwxyz012345"
			},
			dataType: "json",
			success : function(response){
				//通信成功時の処理
				alert(response);
			},
			error: function(){
				//通信失敗時の処理
				alert('通信失敗');
			}
		}); */
		$.ajax({
			url: "http://localhost/creaepd-server_php/creaepd-server_php/api_authentication/index",
			type: "POST",
			data: {
					user_id: "crea0002",
					password: "test2000",
					api_key: "abcdefghijklmnopqrstuvwxyz"
				},
			dataType: "json",
			success: function(data, text_status, xhr){
				var res = data;
				if (res.err_msg != undefined ){
					alert( res.err_msg );
					return;
				}
				alert(res);
				//location.href = res.response['MResource'].url;
			},
			error: function(xhr, text_status, err_thrown){
				alert(err_thrown + ' ファイルダウンロードに失敗しました');
			}
		});
/*		$form = $('#upload_form');
		formdata = new FormData($form[0]);
		$.ajax({
			url: "http://localhost/creaepd-server_php/creaepd-server_php/api_resources/edit/2",
			type: "POST",
			dataType: "json",
			data: formdata,
			processData: false,
			contentType: false,
			success: function(response) {
				//通信成功時の処理
				alert(response);
			},
			error: function(res) {
				//通信失敗時の処理
				alert('通信失敗');
			 }
		}); */
	}
$(function(){
	var imgElem = document.getElementById('img');
	$('#urlText').keyup(function(){
		$('#img').attr('src',$('#urlText').val());
	});

	$('#sendData').click(function(){
	//var imgData = JSON.stringify(getBase64Image(imgElem));
		$.ajax({
			url: 'http://localhost/creaepd-server_php/creaepd-server_php/api_logs/add',
			dataType: 'json',
			data: {
				user_id: "crea0002",
				access_key: "jd893pc51sieklhsr6sbd9dmr1",
				log_name: "log_file_format.csv",
				log_file: "dXVpZCx1c2VyX2lkLHN1Ym1pdF9pZCxjcmVhdGVfZGF0ZXRpbWUsZXZlbnRfaWQscGFnZV9ubyxvcHRpb25fZGF0YQ0KZWU5Mjk2ZDctZjdjZC00ZmVlLThiMjYtZWFkODg0ZWJmMzk4LDIsNCwyMDE0MTIzMTIxMDAwMywxLDEsDQplZTkyOTZkNy1mN2NkLTRmZWUtOGIyNi1lYWQ4ODRlYmYzOTgsMiw0LDIwMTQxMjMxMjEwNTAzLDMsMSwyDQplZTkyOTZkNy1mN2NkLTRmZWUtOGIyNi1lYWQ4ODRlYmYzOTgsMiw0LDIwMTUwMTAxMDEwMDU5LDIsMiwNCg=="
			},
			type: 'POST',
			success: function(data) {
				console.log(data);
			}
		});
	});

	function getBase64Image(imgElem) {
	// imgElem must be on the same server otherwise a cross-origin error will be thrown "SECURITY_ERR: DOM Exception 18"
		var canvas = document.createElement("canvas");
		canvas.width = imgElem.clientWidth;
		canvas.height = imgElem.clientHeight;
		var ctx = canvas.getContext("2d");
		ctx.drawImage(imgElem, 0, 0);
		var dataURL = canvas.toDataURL("image/png");
		return dataURL.replace(/^data:image\/(png|jpg);base64,/, "");
	}
});
	<?php echo $this->Html->scriptEnd(); ?>
	<button onclick="ajaxMethod();return false;">ajaxTest</button>
<?php endif; ?>
</div>
