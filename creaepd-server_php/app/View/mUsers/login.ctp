<?php
// 各種ファイルのインクルード
echo $this->Html->css('loginForm.css');

$class = $GLOBALS['msetting']['MBaseColor']['class_name'];
echo '<div id="loginForm" class="'.$class.'">';

echo $this->Form->create('MUser', array('url' => 'login'));

$label1 = $GLOBALS['message']->getMessage(MESSAGE_COMMON_ITEM_005);
echo '<p><strong>'.$label1.' : </strong>';
echo $this->Form->input('user_ids', array('size'=>22, 'label'=>false, 'error'=>false, 'div'=>false));
echo '</p>';

$label2 = $GLOBALS['message']->getMessage(MESSAGE_COMMON_ITEM_006);
echo '<p><strong>'.$label2.' : </strong>';
echo $this->Form->input('password', array('size'=>22, 'label'=>false, 'error'=>false, 'div'=>false));
// ボタン
$button = $GLOBALS['message']->getMessage(MESSAGE_COMMON_ITEM_007);
echo $this->Form->submit($button, array('name'=>'login', 'div'=>false));
echo '</p>';

echo $this->Session->flash('auth');

echo $this->Form->end();
echo '</div>';

