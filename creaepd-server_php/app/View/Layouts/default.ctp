<?php
/**
 *
 *
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.View.Layouts
 * @since         CakePHP(tm) v 0.10.0.1076
 */

$cakeDescription = __d('cake_dev', 'CakePHP: the rapid development php framework');
?>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Pragma" content="no-cache">
    <meta http-equiv="Cache-Control" content="no-cache">
    <meta http-equiv="Expires" content="-1">
	<?php echo $this->Html->charset(); ?>
	<?php echo $this->Html->meta("Pragma", "no-cache"); ?>
	<?php echo $this->Html->meta("Cache-Control", "no-cache"); ?>
	<?php echo $this->Html->meta("Expires", "-1"); ?>
	<title>
<?php if($GLOBALS['msetting']['MSetting']['language_id'] === '1'): ?>
		<?php echo 'EPDシステム管理' ?>
<?php if (Configure::read('database') === 'development'): ?>
<?php echo '（開発環境）' ?>:
<?php else: ?>
<?php echo '（検証環境）' ?>:
<?php endif; ?>
<?php else: ?>
		<?php echo 'EPD Syetem' ?>
<?php if (Configure::read('database') === 'development'): ?>
<?php echo '（development）' ?>:
<?php else: ?>
<?php echo '（production）' ?>:
<?php endif; ?>
<?php endif; ?>
		<?php echo $title_for_layout; ?>
	</title>
	<?php
		echo $this->Html->meta('icon');

		$class = $GLOBALS['msetting']['MBaseColor']['class_name'];
		echo $this->Html->css($class.'.cake.generic')
		. $this->Html->css('jquery-ui-1.10.4.custom.css', null, array('inline' => false))
		. $this->Html->css('jquery.alerts.css', null, array('inline' => false));

		echo $this->fetch('meta');
		echo $this->fetch('css');
		//外部ファイル読み込み
		echo $this->Html->script('jquery-1.10.2', array('inline' => false))
		. $this->Html->script('jquery-ui-1.10.4.custom', array('inline' => false))
		. $this->Html->script('jquery.ui.datepicker-ja', array('inline' => false))
		. $this->Html->script('jquery.ui.draggable', array('inline' => false))
		. $this->Html->script('jquery.alerts', array('inline' => false))
		. $this->Html->script('jquery.exalertdialogs', array('inline' => false))
		. $this->Html->script('jquery.leanModal.min', array('inline' => false));
		echo $this->fetch('script');
	?>
</head>
<body>
	<div id="container">
		<div id="header">
			<div id="header_menu">
				<?php if(isset($userinfo)): ?>
					<div>
						<ul>
							<li><?php echo h(' ');/*h('サンプル　ロゴサイト');*/ ?></li>
							<li><?php echo h($userinfo['user_name']); ?></li>
							<?php
							$label = $GLOBALS['message']->getMessage(MESSAGE_COMMON_ITEM_019);
							echo '<li>'.$label.'：'.$userinfo['logout_time'].'</li>';
							?>
						</ul>
					</div>
					<div class="button">
						<?php
							$button = $GLOBALS['message']->getMessage(MESSAGE_COMMON_ITEM_018);
							echo $this->Html->link($button, '/musers/logout');
						?>
					</div>
				<?php endif; ?>
		 	</div>
			<div id="header_logo">
				<?php
				$logo_file = $GLOBALS['msetting']['MSetting']['logo_file'];
				echo '<h1 style="background-image: url('.$logo_file.');">&nbsp</h1>';/*'<h1 style="background-image: url('.$logo_file.');">ここにロゴを表示する</h1>';*/
				?>
			</div>
		</div>
		<div id="content">

			<?php echo $this->Session->flash(); ?>

			<?php echo $this->fetch('content'); ?>
		</div>
		<div id="footer">
			<p>Copyright(c) 2014 CREA co.,ltd. All Rights Reserved.</p>
		</div>
	</div>
	<?php //echo $this->element('sql_dump'); ?>
</body>
</html>
