※このメールは送信専用アドレスで送信しております。このメールアドレスへの返信はお受けできませんのでご了承ください。

保護者様

平素は弊社サービスをご利用いただき、誠にありがとうございます。

下記の通り教材の配信を開始しましたので、ご連絡いたします。

──────────────────────────────
□■教材の配信に関して
──────────────────────────────

【教材名】　<?php if($resource_type == '1'): ?>（課題）<?php endif; ?><?php echo $resource_name; ?>

【配信開始日】　<?php echo $deliver_start; ?>

【提出期限】　<?php echo $deliver_end; ?>迄

教材の提出が完了しましたら、再度メールにてご連絡差し上げます。

何かご不明な点がございましたら、お手数ではございますが、
下記連絡先までお問い合わせ下さいますようお願い申し上げます。

──────────────────────────────
（教材配信元の連絡先）
──────────────────────────────
