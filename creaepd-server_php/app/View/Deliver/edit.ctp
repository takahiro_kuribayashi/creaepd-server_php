<?php // 各種ファイルのインクルード
echo $this->Html->script('datetimeselect.js');
// サイドメニュー
echo $this->element('SideMenu', array('menu_array' => $menu_array));  ?>

<div id="main" class="tSubmits form">
<?php
	echo $this->Form->create('TSubmit');
	$this->Form->inputDefaults(array('label' => false));
?>
	<fieldset>
		<h2><?php echo $GLOBALS['message']->getMessage(MESSAGE_COMMON_ITEM_056); ?></h2>
	<dl class="inline">
		<?php echo $this->Form->input('TSubmit.id'); ?>
		<dt><?php echo $this->Form->label('TSubmit.submit_no', $GLOBALS['message']->getMessage(MESSAGE_DELIVER_ITEM_001)); ?></dt>
		<dd>
			<?php echo $this->Form->input('TSubmit.submit_no', array('type'=>'text','readonly' => 'readonly')); ?>
		</dd>
		<dt><?php echo $this->Form->label('TSubmit.resource_id', $GLOBALS['message']->getMessage(MESSAGE_DELIVER_ITEM_002)); ?></dt>
		<dd>
			<span class="inline_box">
				<?php echo $this->Form->input('TSubmit.resource_id', array('type'=>'select', 'options'=>$mResources)); ?>
				<a type="button" id="resources_button" rel="leanModal" href="#test_popup"><?php echo $GLOBALS['message']->getMessage(MESSAGE_COMMON_ITEM_017); ?></a>
			</span>
		</dd>
	<?php
		$option = array(
			'class' => 'datepicker',
			'timeFormat' => '24',
			'dateFormat' => 'YMD',
			'monthNames' => false,
			'empty' => false,
			'separator' => '/',
			'minYear' => date('Y'),
			'maxYear' => date('Y')+1,
		);
	 ?>
		<dt><div><?php echo $this->Form->label('TSubmit.start_datetime', $GLOBALS['message']->getMessage(MESSAGE_DELIVER_ITEM_003)); ?></div></dt>
		<dd>
			<?php echo $this->Form->input('TSubmit.start_datetime', $option); ?>
		</dd>
		<dt><div><?php echo $this->Form->label('TSubmit.end_datetime', $GLOBALS['message']->getMessage(MESSAGE_DELIVER_ITEM_004)); ?></div></dt>
		<dd>
			<?php echo $this->Form->input('TSubmit.end_datetime', $option); ?>
		</dd>
		<dt id = 'check_user'><?php echo $this->Form->label('TSubmit.check_user_id', $GLOBALS['message']->getMessage(MESSAGE_DELIVER_ITEM_005)); ?></dt>
		<dd id = 'check_user'>
			<?php echo $this->Form->input('TSubmit.check_user_id', array('type'=>'select','options'=>$mUsers)); ?>
		</dd>
<?php if(!empty($this->request->data['TSubmit']['id'])) { ?>
		<dt><?php echo $this->Form->label('TSubmit.unsubscribe', $GLOBALS['message']->getMessage(MESSAGE_DELIVER_ITEM_006)); ?></dt>
		<dd>
			<?php echo $this->Form->input('TSubmit.unsubscribe', array('type' => 'checkbox')); ?>
		</dd>
<?php } ?>
		<?php echo $this->element('SelectSubject', array("disabled" => false)); ?>
	</dl>
	</fieldset>
	<div class="submit">
<?php
	$button = $GLOBALS['message']->getMessage(MESSAGE_COMMON_ITEM_001);
	echo $this->Form->submit($button, array('div'=>false, 'class'=>'confirm', 'name'=>'complete'));
	$button2 = $GLOBALS['message']->getMessage(MESSAGE_COMMON_ITEM_004);
	echo $this->Form->button($button2, array('div'=>false, 'type'=>'button', 'name'=>'back', 'onClick'=>"location.href='/creaepd-server_php/deliver/index';return false;"));
?>
	</div>
<?php echo $this->Form->end(); ?>

<!-- ポップアップ表示する内容 -->
	<div id="test_popup">
<?php echo $this->Form->create(false, array('type'=>'post','action'=>'./')); ?>
		<div id="popup_header">
			<h3><?php echo $GLOBALS['message']->getMessage(MESSAGE_DELIVER_ITEM_002); ?></h3>
		</div>
		<div id="popup_body">

<?php
$resources = $mBaseResources;
echo $this->Form->input('TSubmit.resource_id', array(
	'type'=>'radio',
	'options'=> $resources,
	'legend' => false,
	'separator' => '<br><br><br>',
	'label' => false ));
 ?>
		</div>
		<div id="popup_footer">
	<div class="submit">
<?php
	$button = $GLOBALS['message']->getMessage(MESSAGE_COMMON_ITEM_016);
	echo $this->Form->button($button, array('div'=>false, 'type'=>'button', 'id'=>'resources_button', 'name'=>'selected', 'class'=>'modal_close'));
?>
	</div>
<?php echo $this->Form->end(); ?>
		</div>
	</div>

<a href="#modal" id="resources_modal" style="display:none;"></a>
<div id="modal" style="display:none;">
  <div id="modal_content"></div>
</div>
<!-- /ポップアップ表示する内容 -->

<?php $this->Html->scriptStart(); ?>
	$(function(){
		$('input.confirm').exJConfirm(
			'<?php echo $GLOBALS['message']->getMessage(MESSAGE_DELIVER_INIT_001); ?>',
			'<?php echo $GLOBALS['message']->getMessage(MESSAGE_COMMON_ITEM_021); ?>'
		);
	});
<?php echo $this->Html->scriptEnd(); ?>
</div>
