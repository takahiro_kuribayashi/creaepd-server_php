<?php
$this->PaginatorExt->options( array('url' => $this->passedArgs) );
// サイドメニュー
echo $this->element('SideMenu', array('menu_array' => $menu_array));  ?>

<div id="main" class="tSubmits index">
	<h2><?php echo $GLOBALS['message']->getMessage(MESSAGE_COMMON_ITEM_059); ?></h2>
	<?php echo $this->element('searchForm_status_submit')?>

	<table cellpadding="0" cellspacing="0">
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('submit_no', $GLOBALS['message']->getMessage(MESSAGE_STATUS_ITEM_005)); ?></th>
			<th><?php echo $this->Paginator->sort('resource_name', $GLOBALS['message']->getMessage(MESSAGE_STATUS_ITEM_004)); ?></th>
			<th><?php echo $this->Paginator->sort('user_name', $GLOBALS['message']->getMessage(MESSAGE_COMMON_ITEM_106)); ?></th>
			<th><?php echo $this->Paginator->sort('group_name', $GLOBALS['message']->getMessage(MESSAGE_COMMON_ITEM_107)); ?></th>
			<th><?php echo $this->Paginator->sort('submit_state', $GLOBALS['message']->getMessage(MESSAGE_STATUS_ITEM_008)); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($tSubmits as $tSubmit): ?>
	<tr>
		<td><?php echo h($tSubmit['TSubmit']['id']); ?>&nbsp;</td>
		<td><?php echo h($tSubmit['TSubmit']['submit_no']); ?>&nbsp;</td>
		<td><?php echo h($tSubmit['mResource']['resource_name']); ?>&nbsp;</td>
<?php if(!empty($tSubmit['mUser']['user_name'])) { ?>
		<td><?php echo h($tSubmit['mUser']['user_name']); ?>&nbsp;</td>
<?php } else { ?>
		<td><?php echo h(__('－')); ?>&nbsp;</td>
<?php } ?>
<?php if(!empty($tSubmit['mGroup']['group_name'])) { ?>
		<td><?php echo h($tSubmit['mGroup']['group_name']); ?>&nbsp;</td>
<?php } else { ?>
		<td><?php echo h(__('－')); ?>&nbsp;</td>
<?php } ?>
		<td><?php echo h(Configure::read("submit_state.{$tSubmit['TSubmit']['submit_state']}")); ?>&nbsp;</td>
<?php if($tSubmit['TSubmit']['submit_state'] !== '0') { ?>
		<td class="actions">
	<?php if($tSubmit['TSubmit']['check_state'] !== '2') { ?>
			<?php
				$button = $GLOBALS['message']->getMessage(MESSAGE_COMMON_ITEM_015);
				echo $this->Html->link($button, array('action' => 'submit_reset', $tSubmit['TSubmit']['id']), array('class'=>'submit_reset link_button'));
			?>
	<?php } else { ?>
			<?php
				$button = $GLOBALS['message']->getMessage(MESSAGE_COMMON_ITEM_015);
				echo $this->Form->button($button, array('type'=>'button', 'disabled' => true));
			?>
	<?php } ?>
		</td>
<?php } else { ?>
		<td>&nbsp;</td>
<?php } ?>
	</tr>
<?php endforeach; ?>
	</table>
	<table class="page_n">
		<tr>
		<td class="first"><?php echo $this->PaginatorExt->first(__('<<'), array('class' => 'first')); ?></td>
		<td class="prev"><?php echo $this->PaginatorExt->prev(__('<'), array(), null, array('class' => 'prev disabled')); ?></td>
		<?php echo $this->PaginatorExt->numbers(array('separator' => '', 'tag'=>'td', 'currentClass'=>'active', 'first' => false, 'last' => false)); ?>
		<td class="next"><?php echo $this->PaginatorExt->next(__('>'), array(), null, array('class' => 'next disabled')); ?></td>
		<td class="last"><?php echo $this->PaginatorExt->last(__('>>'), array('class' => 'last')); ?></td>
		</tr>
	</table>
<?php $this->Html->scriptStart(); ?>
	$(function(){
		$('a.submit_reset').exJConfirm(
			'<?php echo $GLOBALS['message']->getMessage(MESSAGE_STATUS_INIT_001); ?>',
			'<?php echo $GLOBALS['message']->getMessage(MESSAGE_COMMON_ITEM_021); ?>'
		);
	});
<?php echo $this->Html->scriptEnd(); ?>
</div>