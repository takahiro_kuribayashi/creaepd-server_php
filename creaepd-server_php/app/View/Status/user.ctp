<?php // サイドメニュー
echo $this->element('SideMenu', array('menu_array' => $menu_array));  ?>

<div id="main" class="tSubmits index">
	<h2><?php echo $GLOBALS['message']->getMessage(MESSAGE_COMMON_ITEM_058); ?></h2>
	<?php echo $this->element('searchForm_status_user')?>

	<table cellpadding="0" cellspacing="0">
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('submit_no', $GLOBALS['message']->getMessage(MESSAGE_STATUS_ITEM_005)); ?></th>
			<th><?php echo $this->Paginator->sort('resource_name', $GLOBALS['message']->getMessage(MESSAGE_STATUS_ITEM_004)); ?></th>
			<th><?php echo $this->Paginator->sort('start_datetime', $GLOBALS['message']->getMessage(MESSAGE_STATUS_ITEM_006)); ?></th>
			<th><?php echo $this->Paginator->sort('end_datetime', $GLOBALS['message']->getMessage(MESSAGE_STATUS_ITEM_007)); ?></th>
			<th><?php echo $this->Paginator->sort('submit_state', $GLOBALS['message']->getMessage(MESSAGE_STATUS_ITEM_008)); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($tSubmits as $tSubmit): ?>
	<tr>
		<td><?php echo h($tSubmit['TSubmit']['id']); ?>&nbsp;</td>
		<td><?php echo h($tSubmit['TSubmit']['submit_no']); ?>&nbsp;</td>
		<td><?php echo h($tSubmit['mResource']['resource_name']); ?>&nbsp;</td>
		<td><?php echo h($tSubmit['TSubmit']['start_datetime']); ?>&nbsp;</td>
		<td><?php echo h($tSubmit['TSubmit']['end_datetime']); ?>&nbsp;</td>
		<td><?php echo h(Configure::read("submit_state.{$tSubmit['TSubmit']['submit_state']}")); ?>&nbsp;</td>
<?php if($tSubmit['TSubmit']['submit_state'] !== '0') { ?>
		<td class="actions">
			<?php
				$button = $GLOBALS['message']->getMessage(MESSAGE_COMMON_ITEM_015);
				$comment = $GLOBALS['message']->getMessage(MESSAGE_STATUS_INIT_001);
				echo $this->Html->link($button, array('action' => 'submit_reset', $tSubmit['TSubmit']['id']), array('class'=>'link_button'), $comment);
			?>
		</td>
<?php } else { ?>
		<td>&nbsp;</td>
<?php } ?>
	</tr>
<?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>