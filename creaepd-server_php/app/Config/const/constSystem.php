<?php
/* システム設定画面用定数 */
define('MESSAGE_SYSTEM_ITEM_001', 'item00901'); // ロゴファイル
define('MESSAGE_SYSTEM_ITEM_002', 'item00902'); // ベースカラー
define('MESSAGE_SYSTEM_ITEM_003', 'item00903'); // 言語
define('MESSAGE_SYSTEM_ITEM_004', 'item00904'); // ジャンル名
define('MESSAGE_SYSTEM_ITEM_005', 'item00905'); // 子ジャンル名
define('MESSAGE_SYSTEM_ITEM_006', 'item00906'); // バージョン
define('MESSAGE_SYSTEM_ITEM_007', 'item00907'); // apkファイル
define('MESSAGE_SYSTEM_ITEM_008', 'item00908'); // 概要
define('MESSAGE_SYSTEM_ITEM_009', 'item00909'); // 登録日時

define('MESSAGE_SYSTEM_INIT_001', 'info00001'); // システム設定ー追加確認

define('MESSAGE_SYSTEM_UPDATE_001', 'error0019'); // システム設定-アップデート登録
define('MESSAGE_SYSTEM_UPDATE_002', 'error0020'); // システム設定-アップデート登録

?>