<?php
// ユーザ管理画面共通（仮）
define('PREFIX_USER_IDS', 'crea'); // クレア用ユーザID プレフィックス
define('COLUMN_USER_IDX', 'user_idx'); // クレア用ユーザID 採番インデックス
// CSVディレクトリなど
define('DIR_CSVFILE_UPLOAD', '/app/webroot/files/csv'); // CSVアップロード用ディレクトリ

/* ユーザ管理画面用定数 */
define('MESSAGE_USER_ITEM_001', 'item00301'); // ユーザID
define('MESSAGE_USER_ITEM_002', 'item00302'); // ユーザ名称
define('MESSAGE_USER_ITEM_003', 'item00303'); // 保護者メールアドレス
define('MESSAGE_USER_ITEM_004', 'item00304'); // ユーザ区分
define('MESSAGE_USER_ITEM_005', 'item00305'); // 管理者メールアドレス
define('MESSAGE_USER_ITEM_006', 'item00306'); // 所属グループ
define('MESSAGE_USER_ITEM_007', 'item00307'); // 登録ファイル

define('MESSAGE_USER_INIT_001', 'info00001'); // ユーザ登録ー追加確認
define('MESSAGE_USER_INIT_002', 'info00002'); // ユーザ更新ー更新確認
define('MESSAGE_USER_INIT_003', 'info00003'); // ユーザ削除ー削除確認
define('MESSAGE_USER_INIT_005', 'info00005'); // ユーザ更新ーPWリセット確認
define('MESSAGE_USER_WARN_001', 'warm00001'); // ユーザ更新ー更新不可
define('MESSAGE_USER_WARN_002', 'warm00002'); // ユーザ削除ー削除不可
?>