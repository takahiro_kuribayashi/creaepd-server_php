<?php
/* データ分析画面用定数 */
define('MESSAGE_ANALYSIS_ITEM_001', 'item01001'); // グループ
define('MESSAGE_ANALYSIS_ITEM_002', 'item01002'); // ユーザ
define('MESSAGE_ANALYSIS_ITEM_003', 'item01003'); // 配信期間
define('MESSAGE_ANALYSIS_ITEM_004', 'item01004'); // ジャンル
define('MESSAGE_ANALYSIS_ITEM_005', 'item01005'); // 教材
define('MESSAGE_ANALYSIS_ITEM_006', 'item01006'); // ユーザID
define('MESSAGE_ANALYSIS_ITEM_007', 'item01007'); // ユーザ名称
define('MESSAGE_ANALYSIS_ITEM_008', 'item01008'); // 教材名
define('MESSAGE_ANALYSIS_ITEM_009', 'item01009'); // 最高点
define('MESSAGE_ANALYSIS_ITEM_010', 'item01010'); // 最低点
define('MESSAGE_ANALYSIS_ITEM_011', 'item01011'); // 平均点
define('MESSAGE_ANALYSIS_ITEM_012', 'item01012'); // 回数
define('MESSAGE_ANALYSIS_ITEM_013', 'item01013'); // 設問名
define('MESSAGE_ANALYSIS_ITEM_014', 'item01014'); // 配点
define('MESSAGE_ANALYSIS_ITEM_015', 'item01015'); // 点数
define('MESSAGE_ANALYSIS_ITEM_016', 'item01016'); // 順位
define('MESSAGE_ANALYSIS_ITEM_017', 'item01017'); // 採点日時

?>