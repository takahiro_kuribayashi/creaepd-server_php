<?php
// スケジュール管理画面共通
define('PREFIX_SCHEDULE_IDS', 'SCHEDULE_'); // クレア用スケジュールID プレフィックス
define('COLUMN_SCHEDULE_IDX', 'schedule_idx'); // クレア用スケジュールID 採番インデックス

/* スケジュール管理画面用定数 */
define('MESSAGE_SCHEDULE_ITEM_001', 'item00601'); // スケジュールID
define('MESSAGE_SCHEDULE_ITEM_002', 'item00602'); // タイトル
define('MESSAGE_SCHEDULE_ITEM_003', 'item00603'); // 開始日時
define('MESSAGE_SCHEDULE_ITEM_004', 'item00604'); // 終了日時
define('MESSAGE_SCHEDULE_ITEM_005', 'item00605'); // 詳細

define('MESSAGE_SCHEDULE_INIT_001', 'info00001'); // スケジュール登録ー追加確認
define('MESSAGE_SCHEDULE_INIT_002', 'info00002'); // スケジュール更新ー更新確認
define('MESSAGE_SCHEDULE_INIT_003', 'info00003'); // スケジュール削除ー削除確認
?>