<?php
// ユーザ管理画面共通（仮）
define('PREFIX_GROUP_IDS', 'GROUP_'); // クレア用グループID プレフィックス
define('COLUMN_GROUP_IDX', 'group_idx'); // クレア用グループID 採番インデックス

/* ユーザ（グループ）管理画面用定数 */
define('MESSAGE_GROUP_ITEM_001', 'item00401'); // グループID
define('MESSAGE_GROUP_ITEM_002', 'item00402'); // グループ名
define('MESSAGE_GROUP_ITEM_003', 'item00403'); // グループ種別
define('MESSAGE_GROUP_ITEM_004', 'item00404'); // 上位グループ
define('MESSAGE_GROUP_ITEM_005', 'item00405'); // 所属ユーザ
define('MESSAGE_GROUP_ITEM_006', 'item00406'); // (上位グループなし)

define('MESSAGE_GROUP_INIT_001', 'info00001'); // グループ登録ー追加確認
define('MESSAGE_GROUP_INIT_002', 'info00002'); // グループ更新ー更新確認
define('MESSAGE_GROUP_INIT_003', 'info00003'); // グループ削除ー削除確認
define('MESSAGE_GROUP_WARN_001', 'warm00003'); // グループ登録ー注意内容
?>