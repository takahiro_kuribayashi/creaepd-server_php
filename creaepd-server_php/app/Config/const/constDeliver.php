<?php
// 配信管理画面共通
define('PREFIX_DELIVER_IDS', 'DELIVER_'); // クレア用配信ID プレフィックス
define('COLUMN_DELIVER_IDX', 'submit_idx'); // クレア用配信ID 採番インデックス

/* 配信管理画面用定数 */
define('MESSAGE_DELIVER_ITEM_001', 'item00701'); // 配信ID
define('MESSAGE_DELIVER_ITEM_002', 'item00702'); // 教材名
define('MESSAGE_DELIVER_ITEM_003', 'item00703'); // 配信日時
define('MESSAGE_DELIVER_ITEM_004', 'item00704'); // 提出日時
define('MESSAGE_DELIVER_ITEM_005', 'item00705'); // 採点者
define('MESSAGE_DELIVER_ITEM_006', 'item00706'); // 配信停止
define('MESSAGE_DELIVER_ITEM_007', 'item00707'); // 指定なし
define('MESSAGE_DELIVER_ITEM_008', 'item00708'); // 停止する
define('MESSAGE_DELIVER_ITEM_009', 'item00709'); // 配信日
define('MESSAGE_DELIVER_ITEM_010', 'item00710'); // 提出日

define('MESSAGE_DELIVER_INIT_001', 'info00001'); // 配信登録ー追加確認
define('MESSAGE_DELIVER_ERROR_002', 'error00024'); // 採点中のため、%sと%sは変更できません
?>