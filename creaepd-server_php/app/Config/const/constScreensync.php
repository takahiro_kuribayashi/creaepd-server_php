<?php
/* 画面同期画面用定数 */
define('MESSAGE_SCREENSYNC_ITEM_001', 'item01101'); // 同期ファイル
define('MESSAGE_SCREENSYNC_ITEM_002', 'item01102'); // 同期モード
define('MESSAGE_SCREENSYNC_ITEM_003', 'item01103'); // 同期状態
define('MESSAGE_SCREENSYNC_ITEM_004', 'item01104'); // 同期開始日時
define('MESSAGE_SCREENSYNC_ITEM_005', 'item01105'); // 配信開始日時
define('MESSAGE_SCREENSYNC_ITEM_006', 'item01106'); // 配信終了日時
define('MESSAGE_SCREENSYNC_ITEM_007', 'item01107'); // 開始ユーザ
define('MESSAGE_SCREENSYNC_ITEM_008', 'item01108'); // 配信対象
define('MESSAGE_SCREENSYNC_ITEM_009', 'item01109'); // 配信期間

define('MESSAGE_SCREENSYNC_INIT_001', 'info00001'); // 画面同期ー追加確認
?>