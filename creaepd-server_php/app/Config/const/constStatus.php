<?php
/* 進捗確認画面用定数 */
define('MESSAGE_STATUS_ITEM_001', 'item00801'); // ユーザID
define('MESSAGE_STATUS_ITEM_002', 'item00802'); // ユーザ名
define('MESSAGE_STATUS_ITEM_003', 'item00803'); // 教材ID
define('MESSAGE_STATUS_ITEM_004', 'item00804'); // 教材名
define('MESSAGE_STATUS_ITEM_005', 'item00805'); // 配信ID
define('MESSAGE_STATUS_ITEM_006', 'item00806'); // 配信開始日
define('MESSAGE_STATUS_ITEM_007', 'item00807'); // 配信期限
define('MESSAGE_STATUS_ITEM_008', 'item00808'); // 状態
define('MESSAGE_STATUS_ITEM_009', 'item00809'); // 生徒
define('MESSAGE_STATUS_ITEM_010', 'item00810'); // グループ
define('MESSAGE_STATUS_ITEM_011', 'item00811'); // 提出期間
define('MESSAGE_STATUS_ITEM_012', 'item00812'); // 未提出
define('MESSAGE_STATUS_ITEM_013', 'item00813'); // 採点者

define('MESSAGE_STATUS_INIT_001', 'info00006'); // 進捗確認ーリセット確認
?>