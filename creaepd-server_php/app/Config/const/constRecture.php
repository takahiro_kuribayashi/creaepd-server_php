<?php
// 教材管理画面共通
define('PREFIX_RESOURCE_IDS', 'RESOURCE_'); // クレア用教材ID プレフィックス
define('COLUMN_RESOURCE_IDX', 'resource_idx'); // クレア用教材ID 採番インデックス

/* 教材管理画面用定数 */
define('MESSAGE_RESOURCE_ITEM_001', 'item00501'); // 教材ID
define('MESSAGE_RESOURCE_ITEM_002', 'item00502'); // 教材名
define('MESSAGE_RESOURCE_ITEM_003', 'item00503'); // 教材タイプ
define('MESSAGE_RESOURCE_ITEM_004', 'item00504'); // ジャンル
define('MESSAGE_RESOURCE_ITEM_005', 'item00505'); // スタイル
define('MESSAGE_RESOURCE_ITEM_006', 'item00506'); // pdfファイル１
define('MESSAGE_RESOURCE_ITEM_007', 'item00507'); // pdfファイル２
define('MESSAGE_RESOURCE_ITEM_008', 'item00508'); // mp3ファイル
define('MESSAGE_RESOURCE_ITEM_009', 'item00509'); // 配信ID
define('MESSAGE_RESOURCE_ITEM_010', 'item00510'); // 配信日時
define('MESSAGE_RESOURCE_ITEM_011', 'item00511'); // 提出日時
define('MESSAGE_RESOURCE_ITEM_012', 'item00512'); // 採点者
define('MESSAGE_RESOURCE_ITEM_013', 'item00513'); // 配信停止
define('MESSAGE_RESOURCE_ITEM_014', 'item00514'); // 配信対象
define('MESSAGE_RESOURCE_ITEM_015', 'item00515'); // 設問名
define('MESSAGE_RESOURCE_ITEM_016', 'item00516'); // 問題数
define('MESSAGE_RESOURCE_ITEM_017', 'item00517'); // 配点
define('MESSAGE_RESOURCE_ITEM_018', 'item00518'); // 繰り返し学習

define('MESSAGE_RESOURCE_INIT_001', 'info00001'); // 教材登録ー追加確認
define('MESSAGE_RESOURCE_INIT_002', 'info00002'); // 教材更新ー更新確認
define('MESSAGE_RESOURCE_INIT_003', 'info00003'); // 教材削除ー削除確認
define('MESSAGE_RESOURCE_INIT_004', 'info00004'); // 採点設定ー更新確認
define('MESSAGE_RESOURCE_WARN_001', 'warn00004'); // 採点設定ー入力エラー
?>