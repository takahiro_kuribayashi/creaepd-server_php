<?php
/* 共通定数 */
// ディレクトリなど
define('DIR_FILE_UPLOAD', '/app/webroot/files/image'); // ファイルアップロード用ディレクトリ
// AmazonS3保存ディレクトリ
if(Configure::read('database') === 'development'){
	define('S3_DIR_SOFTWARE_APK', '/software');		// EPD端末apkファイル更新用ディレクトリ
	define('S3_DIR_SYSTEM_LOGO', '/system/logo');	// サーバロゴファイル設定用ディレクトリ
	define('S3_DIR_RESOURCE_FILE', '/resource');	// 教材ファイル配信用ディレクトリ
	define('S3_DIR_SCREENSYNC_FILE', '/screensync');	// 同期ファイル配信用ディレクトリ
} else {
	define('S3_DIR_SOFTWARE_APK', '/production/software');		// EPD端末apkファイル更新用ディレクトリ
	define('S3_DIR_SYSTEM_LOGO', '/production/system/logo');	// サーバロゴファイル設定用ディレクトリ
	define('S3_DIR_RESOURCE_FILE', '/production/resource');	// 教材ファイル配信用ディレクトリ
	define('S3_DIR_SCREENSYNC_FILE', '/production/screensync');	// 同期ファイル配信用ディレクトリ
}

// メッセージ
define('MESSAGE_COMMON_ITEM_001', 'item00001'); // 登録
define('MESSAGE_COMMON_ITEM_002', 'item00002'); // 検索
define('MESSAGE_COMMON_ITEM_003', 'item00003'); // 設定
define('MESSAGE_COMMON_ITEM_004', 'item00004'); // キャンセル
define('MESSAGE_COMMON_ITEM_005', 'item00005'); // ログインID
define('MESSAGE_COMMON_ITEM_006', 'item00006'); // パスワード
define('MESSAGE_COMMON_ITEM_007', 'item00007'); // ログイン
define('MESSAGE_COMMON_ITEM_008', 'item00008'); // 閲覧
define('MESSAGE_COMMON_ITEM_009', 'item00009'); // 更新
define('MESSAGE_COMMON_ITEM_010', 'item00010'); // 削除
define('MESSAGE_COMMON_ITEM_011', 'item00011'); // 一括登録
define('MESSAGE_COMMON_ITEM_012', 'item00012'); // 詳細
define('MESSAGE_COMMON_ITEM_013', 'item00013'); // 追加
define('MESSAGE_COMMON_ITEM_014', 'item00014'); // 子設定
define('MESSAGE_COMMON_ITEM_015', 'item00015'); // リセット
define('MESSAGE_COMMON_ITEM_016', 'item00016'); // 選択
define('MESSAGE_COMMON_ITEM_017', 'item00017'); // 一覧選択
define('MESSAGE_COMMON_ITEM_018', 'item00018'); // ログアウト
define('MESSAGE_COMMON_ITEM_019', 'item00019'); // 前回ログアウト
define('MESSAGE_COMMON_ITEM_020', 'item00020'); // 登録済みのファイル
define('MESSAGE_COMMON_ITEM_021', 'item00021'); // 確認
define('MESSAGE_COMMON_ITEM_022', 'item00022'); // 警告
define('MESSAGE_COMMON_ITEM_023', 'item00023'); // ファイルを選択

define('MESSAGE_COMMON_ITEM_100', 'item00100'); // 教材
define('MESSAGE_COMMON_ITEM_101', 'item00101'); // 進捗確認
define('MESSAGE_COMMON_ITEM_102', 'item00102'); // 生徒
define('MESSAGE_COMMON_ITEM_103', 'item00103'); // 提出物
define('MESSAGE_COMMON_ITEM_104', 'item00104'); // 採点状況
define('MESSAGE_COMMON_ITEM_105', 'item00105'); // スケジュール
define('MESSAGE_COMMON_ITEM_106', 'item00106'); // ユーザ
define('MESSAGE_COMMON_ITEM_107', 'item00107'); // グループ
define('MESSAGE_COMMON_ITEM_108', 'item00108'); // システム
define('MESSAGE_COMMON_ITEM_109', 'item00109'); // サーバ
define('MESSAGE_COMMON_ITEM_110', 'item00110'); // ホーム
define('MESSAGE_COMMON_ITEM_111', 'item00111'); // データ分析
define('MESSAGE_COMMON_ITEM_112', 'item00112'); // 管理メニュー

// 画面タイトル
define('MESSAGE_COMMON_ITEM_051', 'item00051'); // 教材登録
define('MESSAGE_COMMON_ITEM_052', 'item00052'); // 教材検索
define('MESSAGE_COMMON_ITEM_053', 'item00053'); // 教材更新
define('MESSAGE_COMMON_ITEM_054', 'item00054'); // 教材削除
define('MESSAGE_COMMON_ITEM_055', 'item00055'); // 教材閲覧
define('MESSAGE_COMMON_ITEM_056', 'item00056'); // 配信管理
define('MESSAGE_COMMON_ITEM_057', 'item00057'); // 配信検索
define('MESSAGE_COMMON_ITEM_058', 'item00058'); // ユーザ進捗確認
define('MESSAGE_COMMON_ITEM_059', 'item00059'); // 提出物進捗確認
define('MESSAGE_COMMON_ITEM_060', 'item00060'); // 採点状況確認
define('MESSAGE_COMMON_ITEM_061', 'item00061'); // トップ
define('MESSAGE_COMMON_ITEM_062', 'item00062'); // スケジュール登録
define('MESSAGE_COMMON_ITEM_063', 'item00063'); // スケジュール検索
define('MESSAGE_COMMON_ITEM_064', 'item00064'); // スケジュール更新
define('MESSAGE_COMMON_ITEM_065', 'item00065'); // スケジュール削除
define('MESSAGE_COMMON_ITEM_066', 'item00066'); // スケジュール閲覧
define('MESSAGE_COMMON_ITEM_067', 'item00067'); // ユーザ登録
define('MESSAGE_COMMON_ITEM_068', 'item00068'); // ユーザ検索
define('MESSAGE_COMMON_ITEM_069', 'item00069'); // ユーザ更新
define('MESSAGE_COMMON_ITEM_070', 'item00070'); // ユーザ削除
define('MESSAGE_COMMON_ITEM_071', 'item00071'); // ユーザ閲覧
define('MESSAGE_COMMON_ITEM_072', 'item00072'); // グループ登録
define('MESSAGE_COMMON_ITEM_073', 'item00073'); // グループ検索
define('MESSAGE_COMMON_ITEM_074', 'item00074'); // グループ更新
define('MESSAGE_COMMON_ITEM_075', 'item00075'); // グループ削除
define('MESSAGE_COMMON_ITEM_076', 'item00076'); // グループ閲覧
define('MESSAGE_COMMON_ITEM_077', 'item00077'); // ログイン認証
define('MESSAGE_COMMON_ITEM_078', 'item00078'); // ユーザ一括登録
define('MESSAGE_COMMON_ITEM_079', 'item00079'); // デザイン変更
define('MESSAGE_COMMON_ITEM_080', 'item00080'); // 言語設定
define('MESSAGE_COMMON_ITEM_081', 'item00081'); // ジャンル設定
define('MESSAGE_COMMON_ITEM_082', 'item00082'); // 採点設定
define('MESSAGE_COMMON_ITEM_083', 'item00083'); // アップデート管理
define('MESSAGE_COMMON_ITEM_084', 'item00084'); // 成績ランキング
define('MESSAGE_COMMON_ITEM_085', 'item00085'); // グループ成績一覧
define('MESSAGE_COMMON_ITEM_086', 'item00086'); // 教材・配信成績一覧
define('MESSAGE_COMMON_ITEM_087', 'item00087'); // 配信対象
define('MESSAGE_COMMON_ITEM_088', 'item00088'); // 生徒を指定
define('MESSAGE_COMMON_ITEM_089', 'item00089'); // グループを指定
define('MESSAGE_COMMON_ITEM_090', 'item00090'); // 指定しない
define('MESSAGE_COMMON_ITEM_091', 'item00091'); // 画面同期検索
define('MESSAGE_COMMON_ITEM_092', 'item00092'); // 画面同期管理
define('MESSAGE_COMMON_ITEM_093', 'item00093'); // 生徒成績一覧
define('MESSAGE_COMMON_ITEM_094', 'item00094'); // ジャンル検索

// 画面メッセージ
define('MESSAGE_LOGIN_ERROR_001', 'error00001'); // ログインー未入力
define('MESSAGE_LOGIN_ERROR_002', 'error00002'); // ログインー不一致
define('MESSAGE_LOGIN_ERROR_003', 'error00003'); // ログインー不一致
define('MESSAGE_LOGIN_ERROR_004', 'error00004'); // ログインー不一致
define('MESSAGE_LOGIN_ERROR_005', 'error00005'); // ログインー不一致
define('MESSAGE_LOGIN_ERROR_006', 'error00006'); // ログインー不一致
define('MESSAGE_LOGIN_ERROR_007', 'error00007'); // 全角・半角%s文字以内で入力して下さい。
define('MESSAGE_LOGIN_ERROR_008', 'error00008'); // 半角%s文字以内で入力して下さい。
define('MESSAGE_LOGIN_ERROR_010', 'error00010'); // 有効な日付を入力してください。
define('MESSAGE_LOGIN_ERROR_011', 'error00011'); // %sより上の所属グループを設定できません。
define('MESSAGE_LOGIN_ERROR_012', 'error00012'); // %sより下のグループ種別を設定できません。
define('MESSAGE_LOGIN_ERROR_013', 'error00013'); // %sは必須となります。
define('MESSAGE_LOGIN_ERROR_014', 'error00014'); // 関連する所属グループ・ユーザが登録されているため、削除できません。
define('MESSAGE_LOGIN_ERROR_015', 'error00015'); // 関連するスケジュールが登録されているため、削除できません。
define('MESSAGE_LOGIN_ERROR_016', 'error00016'); // 関連する教材配信が登録されているため、削除できません。
define('MESSAGE_LOGIN_ERROR_017', 'error00017'); // 関連する同期配信が登録されているため、削除できません。
define('MESSAGE_LOGIN_ERROR_018', 'error00018'); // 選択の同期モードでは、複数のユーザを選択することはできません。
define('MESSAGE_LOGIN_ERROR_021', 'error00021'); // %sから%sの値を入力してください
define('MESSAGE_LOGIN_ERROR_022', 'error00022'); // メールの送信に失敗しました。
define('MESSAGE_LOGIN_ERROR_023', 'error00023'); // %sより前の日時を設定できません。

// APIメッセージ
define('MESSAGE_API_ERROR_001', 'error00201'); // 必須項目チェック
define('MESSAGE_API_ERROR_002', 'error00202'); // フォルダ作成失敗
define('MESSAGE_API_ERROR_003', 'error00203');
define('MESSAGE_API_ERROR_004', 'error00204');
define('MESSAGE_API_ERROR_005', 'error00205'); // フォルダ削除失敗
define('MESSAGE_API_ERROR_006', 'error00206'); // フォルダ読み取り失敗
define('MESSAGE_API_ERROR_007', 'error00207'); // ファイルCOPY失敗
define('MESSAGE_API_ERROR_008', 'error00208'); // ファイル読み取り失敗
define('MESSAGE_API_ERROR_009', 'error00209'); // ファイル削除失敗
define('MESSAGE_API_ERROR_010', 'error00210'); // ファイル保存失敗
define('MESSAGE_API_ERROR_011', 'error00211'); // ログイン認証失敗
define('MESSAGE_API_ERROR_012', 'error00212'); // 教材マスタ取得失敗
define('MESSAGE_API_ERROR_013', 'error00213'); // スケジュールマスタ取得失敗
define('MESSAGE_API_ERROR_014', 'error00214'); // スケジュール情報保存失敗
define('MESSAGE_API_ERROR_015', 'error00215'); // スケジュール情報取得失敗
define('MESSAGE_API_ERROR_016', 'error00216'); // 画面同期情報取得失敗
define('MESSAGE_API_ERROR_017', 'error00217'); // 指定の画面同期が開始されていません
define('MESSAGE_API_ERROR_018', 'error00218'); // 指定の画面同期の配信期間が過ぎています
define('MESSAGE_API_ERROR_019', 'error00219'); // 指定のユーザで開始・終了操作をおこなって下さい
define('MESSAGE_API_ERROR_020', 'error00220'); // 提出テーブル情報保存失敗
define('MESSAGE_API_ERROR_021', 'error00221'); // 採点テーブル情報複写失敗
define('MESSAGE_API_ERROR_022', 'error00222'); // 提出テーブル情報取得失敗

Configure::write('message_error_code', array(
	'error00201'=>'201',
	'error00202'=>'202',
	'error00203'=>'203',
	'error00204'=>'204',
	'error00205'=>'205',
	'error00206'=>'206',
	'error00207'=>'207',
	'error00208'=>'208',
	'error00209'=>'209',
	'error00210'=>'210',
	'error00211'=>'211',
	'error00212'=>'212',
	'error00213'=>'213',
	'error00214'=>'214',
	'error00215'=>'215',
	'error00216'=>'216',
	'error00217'=>'217',
	'error00218'=>'218',
));

// 区分・コード
define('MESSAGE_COMMON_CODE_001', 'code00001'); // 教科書
define('MESSAGE_COMMON_CODE_002', 'code00002'); // テスト
define('MESSAGE_COMMON_CODE_003', 'code00003'); // PDF
define('MESSAGE_COMMON_CODE_004', 'code00004'); // MP3
define('MESSAGE_COMMON_CODE_005', 'code00005'); // 未着手
define('MESSAGE_COMMON_CODE_006', 'code00006'); // 着手済み
define('MESSAGE_COMMON_CODE_007', 'code00007'); // 提出済み
define('MESSAGE_COMMON_CODE_008', 'code00008'); // 確認済み
define('MESSAGE_COMMON_CODE_009', 'code00009'); // 未着手
define('MESSAGE_COMMON_CODE_010', 'code00010'); // 着手済み
define('MESSAGE_COMMON_CODE_011', 'code00011'); // 採点済み
define('MESSAGE_COMMON_CODE_012', 'code00012'); // 配信中
define('MESSAGE_COMMON_CODE_013', 'code00013'); // 配信停止
define('MESSAGE_COMMON_CODE_014', 'code00014'); // 配信停止
define('MESSAGE_COMMON_CODE_015', 'code00015'); // 配信待ち
define('MESSAGE_COMMON_CODE_016', 'code00016'); // 配信済み
define('MESSAGE_COMMON_CODE_017', 'code00017'); // 教材別
define('MESSAGE_COMMON_CODE_018', 'code00018'); // ユーザ別
define('MESSAGE_COMMON_CODE_019', 'code00019'); // 削除済み
define('MESSAGE_COMMON_CODE_020', 'code00020'); // 一対多
define('MESSAGE_COMMON_CODE_021', 'code00021'); // 一対一
define('MESSAGE_COMMON_CODE_022', 'code00022'); // 停止中
define('MESSAGE_COMMON_CODE_023', 'code00023'); // 同期中
define('MESSAGE_COMMON_CODE_024', 'code00024'); // 同期終了
define('MESSAGE_COMMON_CODE_025', 'code00025'); // レッド
define('MESSAGE_COMMON_CODE_026', 'code00026'); // ブルー
define('MESSAGE_COMMON_CODE_027', 'code00027'); // グレー
define('MESSAGE_COMMON_CODE_028', 'code00028'); // オレンジ
define('MESSAGE_COMMON_CODE_029', 'code00029'); // グリーン
define('MESSAGE_COMMON_CODE_030', 'code00030'); // 日本語
define('MESSAGE_COMMON_CODE_031', 'code00031'); // 英語
define('MESSAGE_COMMON_CODE_032', 'code00032'); // 1画面表示
define('MESSAGE_COMMON_CODE_033', 'code00033'); // 1画面表示(リスニング問題)
define('MESSAGE_COMMON_CODE_034', 'code00034'); // 2画面表示(長文問題)
define('MESSAGE_COMMON_CODE_035', 'code00035'); // 生徒G
define('MESSAGE_COMMON_CODE_036', 'code00036'); // 教師G
define('MESSAGE_COMMON_CODE_037', 'code00037'); // 管理G
define('MESSAGE_COMMON_CODE_038', 'code00038'); // 生徒
define('MESSAGE_COMMON_CODE_039', 'code00039'); // 教師
define('MESSAGE_COMMON_CODE_040', 'code00040'); // 管理者
define('MESSAGE_COMMON_CODE_041', 'code00041'); // システム管理者

Configure::write('resource_type', array(
	'0'=>$GLOBALS['message']->getMessage(MESSAGE_COMMON_CODE_001),
	'1'=>$GLOBALS['message']->getMessage(MESSAGE_COMMON_CODE_002)
));

Configure::write('resource_file_type', array(
	'0'=>$GLOBALS['message']->getMessage(MESSAGE_COMMON_CODE_003),
	'1'=>$GLOBALS['message']->getMessage(MESSAGE_COMMON_CODE_004)
));

Configure::write('submit_state', array(
	'0'=>$GLOBALS['message']->getMessage(MESSAGE_COMMON_CODE_005),
	'1'=>$GLOBALS['message']->getMessage(MESSAGE_COMMON_CODE_006),
	'2'=>$GLOBALS['message']->getMessage(MESSAGE_COMMON_CODE_007),
	'3'=>$GLOBALS['message']->getMessage(MESSAGE_COMMON_CODE_008),
	'9'=>$GLOBALS['message']->getMessage(MESSAGE_COMMON_CODE_019)
));

Configure::write('check_state', array(
	'0'=>$GLOBALS['message']->getMessage(MESSAGE_COMMON_CODE_009),
	'1'=>$GLOBALS['message']->getMessage(MESSAGE_COMMON_CODE_010),
	'2'=>$GLOBALS['message']->getMessage(MESSAGE_COMMON_CODE_011)
));

Configure::write('unsubscribe', array(
	'0'=>$GLOBALS['message']->getMessage(MESSAGE_COMMON_CODE_012),
	'1'=>$GLOBALS['message']->getMessage(MESSAGE_COMMON_CODE_013)
));

Configure::write('schedule_status', array(
	'0'=>$GLOBALS['message']->getMessage(MESSAGE_COMMON_CODE_014),
	'1'=>$GLOBALS['message']->getMessage(MESSAGE_COMMON_CODE_015),
	'2'=>$GLOBALS['message']->getMessage(MESSAGE_COMMON_CODE_016)
));

Configure::write('group_record_type', array(
	'1'=>$GLOBALS['message']->getMessage(MESSAGE_COMMON_CODE_017),
	'2'=>$GLOBALS['message']->getMessage(MESSAGE_COMMON_CODE_018)
));

Configure::write('sync_mode', array(
	'1'=>$GLOBALS['message']->getMessage(MESSAGE_COMMON_CODE_020),
	'2'=>$GLOBALS['message']->getMessage(MESSAGE_COMMON_CODE_021)
));

Configure::write('sync_state', array(
	'0'=>$GLOBALS['message']->getMessage(MESSAGE_COMMON_CODE_022),
	'1'=>$GLOBALS['message']->getMessage(MESSAGE_COMMON_CODE_023),
	'2'=>$GLOBALS['message']->getMessage(MESSAGE_COMMON_CODE_024)
));

//  マスタ多言語対応
Configure::write('m_base_colors', array(
'1'=>$GLOBALS['message']->getMessage(MESSAGE_COMMON_CODE_025),
'2'=>$GLOBALS['message']->getMessage(MESSAGE_COMMON_CODE_026),
'3'=>$GLOBALS['message']->getMessage(MESSAGE_COMMON_CODE_027),
'4'=>$GLOBALS['message']->getMessage(MESSAGE_COMMON_CODE_028),
'5'=>$GLOBALS['message']->getMessage(MESSAGE_COMMON_CODE_029)
));

Configure::write('m_languages', array(
'1'=>$GLOBALS['message']->getMessage(MESSAGE_COMMON_CODE_030),
'2'=>$GLOBALS['message']->getMessage(MESSAGE_COMMON_CODE_031)
));

Configure::write('m_resource_styles', array(
'1'=>$GLOBALS['message']->getMessage(MESSAGE_COMMON_CODE_032),
'3'=>$GLOBALS['message']->getMessage(MESSAGE_COMMON_CODE_034),
'2'=>$GLOBALS['message']->getMessage(MESSAGE_COMMON_CODE_033)
));

Configure::write('m_group_types', array(
'1'=>$GLOBALS['message']->getMessage(MESSAGE_COMMON_CODE_035),
'2'=>$GLOBALS['message']->getMessage(MESSAGE_COMMON_CODE_036),
'3'=>$GLOBALS['message']->getMessage(MESSAGE_COMMON_CODE_037)
));

Configure::write('m_user_types', array(
'1'=>$GLOBALS['message']->getMessage(MESSAGE_COMMON_CODE_038),
'2'=>$GLOBALS['message']->getMessage(MESSAGE_COMMON_CODE_039),
'3'=>$GLOBALS['message']->getMessage(MESSAGE_COMMON_CODE_040),
'9'=>$GLOBALS['message']->getMessage(MESSAGE_COMMON_CODE_041)
));

?>
