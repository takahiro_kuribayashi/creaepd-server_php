<?php
/* トップ画面用定数 */
define('MESSAGE_TOP_ITEM_001', 'item00200'); // 最新のお知らせ情報
define('MESSAGE_TOP_ITEM_002', 'item00201'); // 課題の提出状況
define('MESSAGE_TOP_ITEM_003', 'item00202'); // 採点の進捗状況
define('MESSAGE_TOP_ITEM_004', 'item00203'); // 提出数/総数の進捗状況
define('MESSAGE_TOP_ITEM_005', 'item00204'); // 採点数/総数の進捗状況
?>