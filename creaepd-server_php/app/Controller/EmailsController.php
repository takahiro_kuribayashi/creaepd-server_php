<?php

App::uses('CakeEmail', 'Network/Email');

class EmailsController extends AppController {
	public $uses = array('MMaildelivery');

	// 配信メールKEY
	CONST MAIL_KEY_USER_ENTRY = 'user_entry_mail';
	CONST MAIL_KEY_RESOURCE_DELIVER = 'resource_deliver_mail';
	CONST MAIL_KEY_RESOURCE_SUBMIT = 'resource_submit_mail';
	CONST MAIL_KEY_SOCORE_CHECK = 'socore_check_mail';
	CONST MAIL_KEY_SOCORE_CONFIRM = 'socore_confirm_mail';

	public function send($mail_key, $param_body = null)
	{
		$allCols = $this->MMaildelivery->getColumnTypes();
		$options = array('conditions' => array('MMaildelivery.mail_key' => $mail_key));
		$data_mail = $this->MMaildelivery->find('first', $options);

		// メール配信が有効の場合のみ、メール送信する
		if (!empty($data_mail) && $data_mail['MMaildelivery']['delivery_flg']) {
			// メール内容
			$mailbody = $param_body;

			// メール送信実行
			$email = new CakeEmail('smtp');		// ←手順2で編集した配列名を指定
			$sent = $email
				->template($data_mail['MMaildelivery']['template_name'], 'default')			// ←テンプレ名
				->viewVars($mailbody)			// ←メール内容配列をテンプレに渡す
				->from(array($data_mail['MMaildelivery']['from_addr'] => $data_mail['MMaildelivery']['from_name']))
				->to($param_body['to'])
				->subject($data_mail['MMaildelivery']['subject'].'('.$param_body['user_ids'].')')
				->send();

			if ( $sent ) {
				 //echo 'メール送信成功！' ;
			} else {
				 //echo 'メール送信失敗' ;
			}
		}
	}

}