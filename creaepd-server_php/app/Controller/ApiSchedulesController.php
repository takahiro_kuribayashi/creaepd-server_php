<?php
App::uses('ApiController', 'Controller');
/**
 * API Resources Controller
 *
 * @property MSchedule $MSchedule
 * @property SessionComponent $Session
 * @property RequestHandlerComponent $RequestHandler
*/
class ApiSchedulesController extends ApiController {
	public $uses = array('MSchedule','MUser','TSession','TSchedule','TSection','TScreensyncFile', 'S3Service', 'TSubmit');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		try {
			if (empty($this->request->data['user_id'])) {
				throw new BadRequestException(MESSAGE_API_ERROR_001);
			}
			if (empty($this->request->data['access_key'])) {
				throw new BadRequestException(MESSAGE_API_ERROR_001);
			}
			$user_ids = $this->request->data['user_id'];
			$access_key = $this->request->data['access_key'];
			//parent::accessAuth($user_ids, $access_key);

			// user_idsからユーザIDに変換
			$options = array('conditions' => array('MUser.user_ids' => $user_ids));
			$muser = $this->MUser->find('first', $options);
			if (empty($muser)) {
				throw new BadRequestException(MESSAGE_API_ERROR_011);
			}
			$user_id = $muser['MUser']['id'];

			$options = array('conditions' => array('TSection.user_id' => $user_id));
			$mgroup = $this->TSection->find('first', $options);
			$group_id = !empty($mgroup['TSection']) ? $mgroup['TSection']['group_id']: 0;

			// 検索条件（グループ指定のみ）
			$start = date("Y-m-d H:i:s"); // ～当日
			$end = date("Y-m-d H:i:s", strtotime("-1 month")); //（期限日時が）1ヶ月前～
			$conditions = array(
					array(
							'MSchedule.start_datetime <= CAST(? AS DATETIME)'=>array($start),
							'MSchedule.end_datetime >= CAST(? AS DATETIME)'=>array($end),
							'TSchedule.status' => 1,
							'TSchedule.group_id' => $group_id,
							'TSchedule.user_id' => 0
					)
			);

			// グループ指定の場合、細分化してスケジュールIDを再登録する
			$options = array('conditions' => $conditions);
			$this->TSchedule->recursive = 0;
			$tschedule_data = $this->TSchedule->find('all', $options);
			if (!empty($tschedule_data)) {
				foreach($tschedule_data as $tschedule){
					$options = array('conditions' => array(
							'TSchedule.schedule_id' => $tschedule['TSchedule']['schedule_id'],
							'TSchedule.group_id' => $group_id,
							'TSchedule.user_id' => $user_id));
					$first_data = $this->TSchedule->find('first', $options);

					if (empty($first_data)) {
						//$this->log('スケジュールが空の場合', LOG_DEBUG);
						if( $tschedule['TSchedule']['submit_id'] === 0 &&
							$tschedule['TSchedule']['sync_id'] === 0 ) {
							//$this->log('普通のスケジュールが空の場合', LOG_DEBUG);
							// 同期、教材配信でないスケジュールの場合
							$this->TSchedule->create();
							$tschedule['TSchedule']['id'] = NULL;
							$tschedule['TSchedule']['user_id'] = $user_id;
							if (!$result = $this->TSchedule->save($tschedule['TSchedule'])) {
								throw new BadRequestException(MESSAGE_API_ERROR_014);
							}
						} else  {
							//$this->log('提出スケジュールが空の場合', LOG_DEBUG);
							// 教材配信、同期の場合
							// 新規に追加する際には教材と同期の個別のレコードが存在するかをチェックする
							if( $tschedule['TSchedule']['submit_id'] != 0 ) {
								//$this->log('提出IDの場合' . $tschedule['TSchedule']['submit_id'], LOG_DEBUG);
								//$this->log($tschedule, LOG_DEBUG);
								// 教材配信の場合
								$submit_id = $tschedule['TSchedule']['submit_id'];
								//$this->log('サブミットID', LOG_DEBUG);
								//$this->log($submit_id, LOG_DEBUG);
								$t_submit_conditions = array('TSubmit.id' => $submit_id);
								$t_submit_options = array('conditions' => $t_submit_conditions, 'fields' => 'id, submit_no');
								$t_submit = $this->TSubmit->find('first', $t_submit_options);

								// submit_noの取得「DELIVER_XXXX」形式のデータ
								$t_submit_no = $t_submit['TSubmit']['submit_no'];

								//$this->log('サブミットナンバー', LOG_DEBUG);
								//$this->log($t_submit_no, LOG_DEBUG);

								// 配信設定が既に行われているかを判定
								$t_submit_conditions2 = array('submit_no' => $t_submit_no, 'user_id' => $user_id);
								$t_submit_options2 = array('conditions' => $t_submit_conditions2, 'fields' => 'id');
								$t_submit2 = $this->TSubmit->find('first', $t_submit_options2);
								//$this->log('$t_submit2', LOG_DEBUG);
								//$this->log($t_submit2, LOG_DEBUG);

								$submit_count = count($t_submit2);
								//$this->log('提出IDのカウント', LOG_DEBUG);
								//$this->log($submit_count, LOG_DEBUG);

								if( $submit_count > 0 ) {
									// 配信設定が既に行われている場合
									$t_submit_id = $t_submit2['TSubmit']['id'];

									$options = array('conditions' => array(
																		'schedule_id' => $tschedule['TSchedule']['schedule_id'],
																		'group_id' => $group_id,
																		'user_id' => $user_id,
																		'submit_id' => $t_submit_id));
									$t_submit_schedule_count = $this->TSchedule->find('count', $options);

									//$this->log('提出スケジュールのカウント', LOG_DEBUG);
									//$this->log($t_submit_schedule_count, LOG_DEBUG);

									if( $t_submit_schedule_count === 0 ) {
										$this->TSchedule->create();
										$tschedule['TSchedule']['id'] = NULL;
										$tschedule['TSchedule']['user_id'] = $user_id;
										$tschedule['TSchedule']['submit_id'] = $t_submit_id;

										//$this->log('グループスケジュール新規作成', LOG_DEBUG);
										//$this->log($tschedule, LOG_DEBUG);
										if (!$result = $this->TSchedule->save($tschedule['TSchedule'])) {
											//$this->log('グループスケジュール作成失敗', LOG_DEBUG);
											throw new BadRequestException(MESSAGE_API_ERROR_014);
										}
									}
								}
							} else if( $tschedule['TSchedule']['sync_id'] !== 0 ) {
								//$this->log('同期のスケジュールが空の場合', LOG_DEBUG);
								// 同期の場合
								$this->TSchedule->create();
								$tschedule['TSchedule']['id'] = NULL;
								$tschedule['TSchedule']['user_id'] = $user_id;
								if (!$result = $this->TSchedule->save($tschedule['TSchedule'])) {
									throw new BadRequestException(MESSAGE_API_ERROR_014);
								}
							}
						}
					}
				}
			}

			// 結合条件（スケジュールテーブルと所属テーブル）
			$joins = array(
					array(
							'table'		 => 't_schedules',
							'alias'		 => 'TSchedule',
							'type'		 => 'LEFT',
							'conditions' => array('MSchedule.id = TSchedule.schedule_id')
					),
					array(
							'table'		 => 't_submits',
							'alias'		 => 'TSubmit',
							'type'		 => 'LEFT',
							'conditions' => array('TSchedule.submit_id = TSubmit.id')
					),
					array(
							'table'		 => 't_screensync_files',
							'alias'		 => 'TScreensyncFile',
							'type'		 => 'LEFT',
							'conditions' => array('TScreensyncFile.submit_id = TSchedule.sync_id', 'TScreensyncFile.order_no'=> 0)
					),
					array(
							'table'		 => 'm_resources',
							'alias'		 => 'MResource',
							'type'		 => 'LEFT',
							'conditions' => array('OR' => array(
								array('TSubmit.resource_id = MResource.id'),
								array('TScreensyncFile.resource_id = MResource.id')))
					)
			);
			// 検索条件
			$start = date("Y-m-d H:i:s"); // ～当日
			$end = date("Y-m-d H:i:s", strtotime("-1 month")); //（期限日時が）1ヶ月前～
			$conditions = array(
					'AND' => array(
							'MSchedule.start_datetime <= CAST(? AS DATETIME)'=>array($start),
							'MSchedule.end_datetime >= CAST(? AS DATETIME)'=>array($end),
							'TSchedule.status' => 1,
							'TSchedule.user_id' => $user_id
					)
			);
			$ary_fields = array('id','schedule_ids','title','detail','start_datetime','end_datetime','delete_flg',
								'TSchedule.submit_id AS submit_id','TSchedule.sync_id AS sync_id',
								'MResource.resource_ids AS resource_id','MResource.resource_name','MResource.resource_type','MResource.repeat_flg',
								'MResource.version','MResource.create_user_id','MResource.subject_id','MResource.style_id');
			$options = array('conditions' => $conditions, 'joins' => $joins, 'fields' => $ary_fields);
			$result = $this->MSchedule->find('all', $options);
			// 取得した提出ID・同期IDを配列に追加する
			foreach($result as $key => $data){
				if ($data['TSchedule']['sync_id'] !== '0') {
					$result[$key]['MSchedule']['sync_id'] = $data['TSchedule']['sync_id'];
				} else {
					$result[$key]['MSchedule']['submit_id'] = $data['TSchedule']['submit_id'];
				}
				unset($result[$key]['TSchedule']);
				// 教材が紐づいている場合のみ、返す
				if (empty($data['MResource']['resource_id'])) {
					unset($result[$key]['MResource']);
				}
			}
//$this->log($result, LOG_DEBUG);
		} catch (BadRequestException $exc) {
			$error_id = $exc->getMessage();
			if (empty($GLOBALS['message']->getMessage($error_id))) {
				throw new BadRequestException($error_id);
			}
			$error_message = $GLOBALS['message']->getMessage($error_id);
			$error_code = Configure::read("message_error_code.{$error_id}");
			return $this->error($error_message, $error_code);
		}
		return $this->success($result);
	}

/**
 * view_sync method
 *
 * @throws BadRequestException
 * @param string $sync_id
 * @return void
 */
	public function view_sync($sync_id = null) {
		try {
			if (empty($this->request->data['user_id'])) {
				throw new BadRequestException(MESSAGE_API_ERROR_001);
			}
			if (empty($this->request->data['access_key'])) {
				throw new BadRequestException(MESSAGE_API_ERROR_001);
			}
			$user_ids = $this->request->data['user_id'];
			$access_key = $this->request->data['access_key'];
			//parent::accessAuth($user_ids, $access_key);

			$result = false;
			$options = array('conditions' => array('TScreensyncFile.submit_id' => $sync_id));
			$tscreensync_data = $this->TScreensyncFile->find('first', $options);
			if (!empty($tscreensync_data)) {
				$document_id = $tscreensync_data['TScreensyncFile']['document_id'];
				// 作業フォルダを作成
				$work_dir = WWW_ROOT . 'files' . DS . $user_ids . DS;
				$dir = new Folder();
				if (!$dir->create($work_dir)) {
					throw new BadRequestException(MESSAGE_API_ERROR_002);
				}
				// コピー先の同期ファイル
				$file_name = $tscreensync_data['TScreensyncFile']['sync_file_name'];
				$base_file = $work_dir . $file_name;

				// apkファイルをS3からダウンロード
				require_once(APP . "Vendor" . DS . "aws.phar");
				$file_dir = S3_DIR_SCREENSYNC_FILE . "/" . $document_id . "/0/" . $file_name;

				// S3からgetする
				$result = $this->S3Service->getFile($file_dir);
				if (!$result) {
					throw new BadRequestException(MESSAGE_API_ERROR_006);
				}
				$result = file_put_contents($base_file, $result);
				if (!$result) {
					throw new BadRequestException(MESSAGE_API_ERROR_007);
				}
			} else {
				throw new BadRequestException(MESSAGE_API_ERROR_013);
			}
//$this->log($result, LOG_DEBUG);
		} catch (BadRequestException $exc) {
			$error_id = $exc->getMessage();
			if (empty($GLOBALS['message']->getMessage($error_id))) {
				throw new BadRequestException($error_id);
			}
			$error_message = $GLOBALS['message']->getMessage($error_id);
			$error_code = Configure::read("message_error_code.{$error_id}");
			return $this->error($error_message, $error_code);
		}

		if ($result) {
			// 同期ファイルをbase64エンコードし、JSONにセット
			$file_data = base64_encode(file_get_contents($base_file));
			$res_ary = array(
					'TScreensync' => array(
							'id' => $tscreensync_data['TScreensync']['id'],
							'sync_name' => $file_name,
							'sync_file' => $file_data
					)
			);
		}
		return $this->success($res_ary);
	}

/**
 * edit method
 *
 * @throws BadRequestException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
			try {
			if (empty($this->request->data['user_id'])) {
				throw new BadRequestException(MESSAGE_API_ERROR_001);
			}
			if (empty($this->request->data['access_key'])) {
				throw new BadRequestException(MESSAGE_API_ERROR_001);
			}

			if (!$this->MSchedule->exists($id)) {
				throw new BadRequestException(MESSAGE_API_ERROR_013);
			}
			$user_ids = $this->request->data['user_id'];
			$access_key = $this->request->data['access_key'];
			//parent::accessAuth($user_ids, $access_key);

			// user_idsからユーザIDに変換
			$options = array('conditions' => array('MUser.user_ids' => $user_ids));
			$muser = $this->MUser->find('first', $options);
			if (empty($muser)) {
				throw new BadRequestException(MESSAGE_API_ERROR_011);
			}
			$user_id = $muser['MUser']['id'];

			// スケジュール配信完了
			$result = false;
			// 検索条件
			$conditions = array(
					array(
							'TSchedule.schedule_id' => $id,
							'TSchedule.user_id' => $user_id
					)
			);
			// スケジュールテーブル配信状態の更新
			$options = array('conditions' => $conditions);
			$tschedule_datas = $this->TSchedule->find('all', $options);

			foreach($tschedule_datas as $tschedule_data){
				if (!empty($tschedule_data)) {
					$tschedule_data['TSchedule']['status'] = 2; // ステータスを配信済みに更新（1→2）
					//$this->log($tschedule_data['TSchedule'], LOG_DEBUG);

					if (!$result = $this->TSchedule->save($tschedule_data['TSchedule'])) {
						throw new BadRequestException(MESSAGE_API_ERROR_014);
					}
				} else {
					throw new BadRequestException(MESSAGE_API_ERROR_015);
				}
			}
		} catch (BadRequestException $exc) {
			$error_id = $exc->getMessage();
			if (empty($GLOBALS['message']->getMessage($error_id))) {
				throw new BadRequestException($error_id);
			}
			$error_message = $GLOBALS['message']->getMessage($error_id);
			$error_code = Configure::read("message_error_code.{$error_id}");
			return $this->error($error_message, $error_code);
		}

		$res_ary = array(
				$id => array(
						'result' => is_array($result)
				)
		);
		return $this->success($res_ary);
	}
}
