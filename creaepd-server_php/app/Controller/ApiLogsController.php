<?php
App::uses('ApiController', 'Controller');
/**
 * API Log Controller
 *
 * @property SessionComponent $Session
 * @property RequestHandlerComponent $RequestHandler
*/
class ApiLogsController extends ApiController {
	public $uses = array('TLog','TSession','MUser');

	/**
	 * add method
	 *
	 * @return void
	 */
	public function add() {
		try {
			if (empty($this->request->data['user_id'])) {
				throw new BadRequestException(MESSAGE_API_ERROR_001);
			}
			if (empty($this->request->data['access_key'])) {
				throw new BadRequestException(MESSAGE_API_ERROR_001);
			}

			// ***** user_idからユーザマスタ取得が必要 *****
			$user_ids = $this->request->data['user_id'];
			$access_key = $this->request->data['access_key'];
			//parent::accessAuth($user_ids, $access_key);
			
			// user_idsからユーザIDに変換
			$options = array('conditions' => array('MUser.user_ids' => $user_ids));
			$muser = $this->MUser->find('first', $options);
			if (empty($muser)) {
				throw new BadRequestException(MESSAGE_API_ERROR_011);
			}
			$user_id = $muser['MUser']['id'];
			$log_name = $this->request->data['log_name'];

			//parent::accessAuth($user_ids, $access_key);

			if (isset($this->request->data['log_name'])) {
				// 作業フォルダを作成
				$work_dir = WWW_ROOT . 'files' . DS . $user_ids . DS;
				$dir = new Folder();
				if (!$dir->create($work_dir)) {
					throw new BadRequestException(MESSAGE_API_ERROR_002);
				}

				// base64デコードし、教材ファイルを取得
				$file_name = $this->request->data['log_name'];
				$file_data = $this->request->data['log_file'];
				$line_arr = preg_split("/\R/", $file_data);
				$base_file = $work_dir . $file_name;
				$file = new File($base_file, true, 0644);
				$file->open('wb');
				foreach($line_arr as $line){
					$file->write(base64_decode($line));
					$file->write("\r\n");
				}
				$file->close();
			}

			if(file_exists($base_file)) {
				// （テストCSVインポート）
				//$this->Property->importCsv($filename);
				$result = $this->import($base_file);
			}
		} catch (BadRequestException $exc) {
			$error_id = $exc->getMessage();
			if (empty($GLOBALS['message']->getMessage($error_id))) {
				throw new BadRequestException($error_id);
			}
			$error_message = $GLOBALS['message']->getMessage($error_id);
			$error_code = Configure::read("message_error_code.{$error_id}");
			return $this->error($error_message, $error_code);
		}

		$res_ary = array(
				$user_id => array(
						'log_name' => $log_name,
						'result' => $result
				)
		);

		return $this->success($res_ary);
	}

/**
 * import method
 *
 * @return void
 */
	private function import($file_name = null) {
		$modelClass = $this->modelClass;
		$result = $this->$modelClass->importCSV($file_name);
/*		if($errors = $this->$modelClass->getImportErrors()) {
			$this->log($errors, LOG_DEBUG);
		}*/
		return $result;
	}
}
