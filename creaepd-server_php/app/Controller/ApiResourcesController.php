<?php
App::uses('ApiController', 'Controller');
/**
 * API Resources Controller
 *
 * @property MResource $MResource
 * @property SessionComponent $Session
 * @property RequestHandlerComponent $RequestHandler
*/
class ApiResourcesController extends ApiController {
	public $uses = array('MResource', 'MResourceSubject', 'MResourceStyle', 'MUser', 'TSession', 'TSection', 'TSchedule', 'TSubmit', 'S3Service');

	protected $submit_status = array(
			'0' => array( //未提出
					'0' => '0',
					'1' => '0',
					'2' => '0',
			),
			'1' => array( //着手済み
					'0' => '1',
					'1' => '1',
					'2' => '1',
			),
			'2' => array( //提出済み
					'0' => '2', //未採点
					'1' => '3', //採点中
					'2' => '4', //採点済み
			),
			'3' => array( //確認済み
					'0' => '5',
					'1' => '5',
					'2' => '5',
			),
	);

/**
 * index method
 *
 * @return void
 */
	public function index() {
		try {
			if (empty($this->request->data['user_id'])) {
				throw new BadRequestException(MESSAGE_API_ERROR_001);
			}
			if (empty($this->request->data['access_key'])) {
				throw new BadRequestException(MESSAGE_API_ERROR_001);
			}
			$user_ids = $this->request->data['user_id'];
			$access_key = $this->request->data['access_key'];
			//parent::accessAuth($user_ids, $access_key);

			// user_idsからユーザIDに変換
			$options = array('conditions' => array('MUser.user_ids' => $user_ids));
			$muser = $this->MUser->find('first', $options);
			if (empty($muser)) {
				throw new BadRequestException(MESSAGE_API_ERROR_011);
			}
			$user_id = $muser['MUser']['id'];

			$options = array('conditions' => array('TSection.user_id' => $user_id));
			$mgroup = $this->TSection->find('first', $options);
			$group_id = isset($mgroup['TSection']['group_id']) ? $mgroup['TSection']['group_id']: 0;

			// 検索条件（グループ指定のみ）
			$start = date("Y-m-d H:i:s"); // ～当日
			$end = date("Y-m-d H:i:s", strtotime("-1 month")); //（期限日時が）1ヶ月前～
			$conditions = array(
					'AND' => array(
							'TSubmit.start_datetime <= CAST(? AS DATETIME)'=>array($start),
							'TSubmit.end_datetime >= CAST(? AS DATETIME)'=>array($end),
							'NOT' => array('TSubmit.submit_state' => 9),
							'TSubmit.group_id' => $group_id,
							'TSubmit.user_id' => 0
					)
			);

			// グループ指定の場合、細分化して提出IDを再登録する
			$options = array('conditions' => $conditions);
			$this->TSubmit->recursive = -1;
			$tsubmit_data = $this->TSubmit->find('all', $options);
			if (!empty($tsubmit_data)) {
				foreach($tsubmit_data as $tsubmit){
					$options = array('conditions' => array(
						'TSubmit.submit_no' => $tsubmit['TSubmit']['submit_no'],
						'TSubmit.user_id' => $user_id));
					$first_data = $this->TSubmit->find('first', $options);
					if (empty($first_data)) {
						$this->TSubmit->create();
						$tsubmit['TSubmit']['id'] = NULL;
						$tsubmit['TSubmit']['user_id'] = $user_id;
						if (!$result = $this->TSubmit->save($tsubmit['TSubmit'])) {
							throw new BadRequestException(MESSAGE_API_ERROR_020);
						}
					}
				}
			}

			// 検索条件（ユーザ指定のみ）
			$start = date("Y-m-d H:i:s"); // ～当日
			$end = date("Y-m-d H:i:s", strtotime("-1 month")); //（期限日時が）1ヶ月前～
			$conditions = array(
					'AND' => array(
							'TSubmit.start_datetime <= CAST(? AS DATETIME)'=>array($start),
							'TSubmit.end_datetime >= CAST(? AS DATETIME)'=>array($end),
							'NOT' => array('TSubmit.submit_state' => 9),
							'TSubmit.user_id' => $user_id
					)
			);

			// 採点設定をテーブルに複写登録する
			$options = array('conditions' => $conditions);
			$this->TSubmit->recursive = -1;
			$tsubmit_data = $this->TSubmit->find('all', $options);
			if (!empty($tsubmit_data)) {
				foreach($tsubmit_data as $tsubmit){
					$tsubmit_id = $tsubmit['TSubmit']['id'];
					$parent_resource_id = $tsubmit['TSubmit']['resource_id'];

					//（未登録の場合のみ）
					$options = array('conditions' => array('TScore.submit_id' => $tsubmit_id));
					$tscore_data = $this->TSubmit->TScore->find('all', $options);
					if (empty($tscore_data)) {
						$options = array('conditions' => array('MResourceSubtitle.resource_id' => $parent_resource_id));
						$msubtitle_data = $this->MResource->MResourceSubtitle->find('all', $options);
						if (count($msubtitle_data)>0) {
							$tscore_data =array();
							foreach($msubtitle_data as $key => $msubtitle){
								$tscore_data[$key]['id'] = NULL;
								$tscore_data[$key]['submit_id'] = $tsubmit_id;
								$tscore_data[$key]['subtitle_id'] = $msubtitle['MResourceSubtitle']['id'];
								$tscore_data[$key]['allot'] = $msubtitle['MResourceSubtitle']['allot'];
								$tscore_data[$key]['result'] = $msubtitle['MResourceSubtitle']['result'];
							}
							$this->TSubmit->TScore->create();
							if (!$result = $this->TSubmit->TScore->saveAll($tscore_data)) {
								throw new BadRequestException(MESSAGE_API_ERROR_021);
							}
						}
					}
				}
			}

			// 結合条件（提出テーブルと所属テーブルと教材マスタ）
			$joins = array(
					array(
							'table'		 => 't_submits',
							'alias'		 => 'TSubmit',
							'type'		 => 'INNER',
							'conditions' => array('TSubmit.resource_id = MResource.id', 'TSubmit.user_id <> 0')
					),
					array(
							'table'		 => 'm_users',
							'alias'		 => 'MUser',
							'type'		 => 'LEFT',
							'conditions' => array('TSubmit.user_id = MUser.id', 'TSubmit.user_id <> 0')
					),
					array(
							'table'		 => 'm_users',
							'alias'		 => 'MCheckUser',
							'type'		 => 'LEFT',
							'conditions' => array('TSubmit.check_user_id = MCheckUser.id', 'TSubmit.check_user_id <> 0')
					),
					array(
							'table'		 => 'm_resource_subjects',
							'alias'		 => 'MResourceSubject',
							'type'		 => 'LEFT',
							'conditions' => array('MResourceSubject.id = MResource.subject_id')
					)
			);
			// 検索条件
			$start = date("Y-m-d H:i:s"); // ～当日
			$end = date("Y-m-d H:i:s", strtotime("-1 month")); //（期限日時が）1ヶ月前～
			$conditions = array(
					'AND' => array(
							'TSubmit.start_datetime <= CAST(? AS DATETIME)'=>array($start),
							'TSubmit.end_datetime >= CAST(? AS DATETIME)'=>array($end),
							'NOT' => array('TSubmit.submit_state' => 9),
							'OR' => array(
									array(
											'TSubmit.user_id' => $user_id
									),
									array( //（教師ユーザの採点教材のチェック）
											'TSubmit.submit_state' => 2,
											'TSubmit.check_user_id' => $user_id
									)
							)
					)
			);
			$ary_fields = array('TSubmit.id AS submit_id','resource_name','resource_type','repeat_flg','version','create_user_id','subject_id','style_id',
					'TSubmit.start_datetime','TSubmit.end_datetime','MResourceSubject.subject_name','TSubmit.submit_state','TSubmit.check_state','TSubmit.unsubscribe',
					'MUser.id','MUser.user_name','MUser.user_type_id','MCheckUser.id','MCheckUser.user_name','MCheckUser.user_type_id');
			$groups = array('TSubmit.id');
			$options = array('conditions' => $conditions, 'joins' => $joins, 'group' => $groups, 'fields' => $ary_fields);
			$result = $this->MResource->find('all', $options);
			// 取得した配信開始・終了日時を配列に追加する
			foreach($result as $key => $data){
				$result[$key]['MResource']['start_datetime'] = $data['TSubmit']['start_datetime'];
				$result[$key]['MResource']['end_datetime'] = $data['TSubmit']['end_datetime'];
				$result[$key]['MResource']['submit_id'] = $data['TSubmit']['submit_id'];
				$result[$key]['MResource']['unsubscribe'] = $data['TSubmit']['unsubscribe'];
				$submit_state = $data['TSubmit']['submit_state'];
				$check_state = $data['TSubmit']['check_state'];
				$result[$key]['MResource']['submit_status'] = isset($this->submit_status[$submit_state][$check_state]) ? $this->submit_status[$submit_state][$check_state]: 0;
				unset($result[$key]['TSubmit']);
			}
//$this->log($result, LOG_DEBUG);
		} catch (BadRequestException $exc) {
			$error_id = $exc->getMessage();
			if (empty($GLOBALS['message']->getMessage($error_id))) {
				throw new BadRequestException($error_id);
			}
			$error_message = $GLOBALS['message']->getMessage($error_id);
			$error_code = Configure::read("message_error_code.{$error_id}");
			return $this->error($error_message, $error_code);
		}

		$this->log($result, LOG_DEBUG);
		return $this->success($result);
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		try {
			if (empty($this->request->data['user_id'])) {
				throw new BadRequestException(MESSAGE_API_ERROR_001);
			}
			if (empty($this->request->data['access_key'])) {
				throw new BadRequestException(MESSAGE_API_ERROR_001);
			}
			$user_ids = $this->request->data['user_id'];
			$access_key = $this->request->data['access_key'];
			//parent::accessAuth($user_ids, $access_key);

			// 提出テーブルの配信NO、配信状態
			$options = array('conditions' => array('TSubmit.' . $this->TSubmit->primaryKey => $id));
			$tsubmit_data = $this->TSubmit->find('all', $options);
			if (!empty($tsubmit_data)) {
				$resource_ids = $tsubmit_data[0]['mResource']['resource_ids'];
				$submit_state = $tsubmit_data[0]['TSubmit']['submit_state'];
				$user_ids = $tsubmit_data[0]['mUser']['user_ids'];
				//（提出済みは採点状態でフォルダを判定する）
				if ($submit_state !== '0' && $submit_state !== '1') {
					$check_state = $tsubmit_data[0]['TSubmit']['check_state'];
					if ($check_state !== '2') {
						$submit_state = '1';
					} else {
						$submit_state = '2';
					}
				} else {
					$submit_state = '0'; // 初回、再取得時
				}
			} else {
				$resource_ids = '';
				$submit_state = '0';
			}

			$result = false;
			// 作業フォルダを作成
			$work_dir = WWW_ROOT . 'files' . DS . $user_ids . DS;
			$dir = new Folder();
			if (!$dir->create($work_dir)) {
				throw new BadRequestException(MESSAGE_API_ERROR_002);
			}
			// コピー先のzipファイル
			$file_name = $resource_ids . '.zip';
			$base_file = $work_dir . $file_name;

			// zipファイルをS3からダウンロード
			require_once(APP . "Vendor" . DS . "aws.phar");
			$submit_id = $id;
			if ($submit_state === '0') {
				// 教材配信
				$file_dir = S3_DIR_RESOURCE_FILE . "/{$submit_state}/" . $file_name;
			} else {
				// 教材採点(1)、教材確認(2)
				$file_dir = S3_DIR_RESOURCE_FILE . "/{$submit_state}/" . $user_ids . "/{$submit_id}/" . $file_name;
			}

			// S3からgetする
			$result = $this->S3Service->getFile($file_dir);
			if (!$result) {
				throw new BadRequestException(MESSAGE_API_ERROR_006);
			}
			$result = file_put_contents($base_file, $result);
			if (!$result) {
				throw new BadRequestException(MESSAGE_API_ERROR_007);
			}
		} catch (BadRequestException $exc) {
			$error_id = $exc->getMessage();
			if (empty($GLOBALS['message']->getMessage($error_id))) {
				throw new BadRequestException($error_id);
			}
			$error_message = $GLOBALS['message']->getMessage($error_id);
			$error_code = Configure::read("message_error_code.{$error_id}");
			return $this->error($error_message, $error_code);
		}

		if ($result) {
			// 教材ファイルをbase64エンコードし、JSONにセット
			$file_data = base64_encode(file_get_contents($base_file));
			$res_ary = array(
					'MResource' => array(
							'id' => $id,
							'resource_name' => $file_name,
							'resource_file' => $file_data
					)
			);
		} else {
			$res_ary = array(
					'MResource' => $result
			);
		}
		return $this->success($res_ary);
	}

	/**
	 * edit method
	 *
	 * @throws NotFoundException
	 * @param string $id
	 * @return void
	 */
	public function edit($id = null) {
		try {
		if (empty($this->request->data['user_id'])) {
				throw new BadRequestException(MESSAGE_API_ERROR_001);
			}
			if (empty($this->request->data['access_key'])) {
				throw new BadRequestException(MESSAGE_API_ERROR_001);
			}
			if (empty($this->request->data['submit_status']) && empty($this->request->data['check_status'])) {
				throw new BadRequestException(MESSAGE_API_ERROR_001);
			}
			if (!empty($this->request->data['check_status']) && empty($this->request->data['check_score'])) {
				throw new BadRequestException(MESSAGE_API_ERROR_001);
			}
			$user_ids = $this->request->data['user_id'];
			$access_key = $this->request->data['access_key'];
			//parent::accessAuth($user_ids, $access_key);

			// user_idsのユーザマスタ存在チェック
			$options = array('conditions' => array('MUser.user_ids' => $user_ids));
			$muser = $this->MUser->find('first', $options);
			if (empty($muser)) {
				throw new BadRequestException(MESSAGE_API_ERROR_011);
			}
			$user_type_id = $muser['MUser']['user_type_id'];

			$result = false;
			if (isset($this->request->data['submit_status'])) {
				$submit_status = $this->request->data['submit_status']; // 提出状態
				$submit_status = mb_ereg_replace('\"', '', $submit_status);
				// 教材配信完了
				// 作業フォルダを削除
				$work_dir = WWW_ROOT . 'files' . DS . $user_ids . DS;
				$dir = new Folder($work_dir);
				if (!$dir->delete()) {
					//throw new BadRequestException(MESSAGE_API_ERROR_005);
				}

				// 提出テーブル配信状態の更新
				$options = array('conditions' => array('TSubmit.' . $this->TSubmit->primaryKey => $id));
				$tsubmit_data = $this->TSubmit->find('first', $options);
				if (empty($tsubmit_data)) {
					throw new BadRequestException(MESSAGE_API_ERROR_022);
				}

				if ($submit_status === '2') {
					// 提出テーブル配信状態の更新
					$tsubmit_data['TSubmit']['submit_state'] = 3; // ステータスを確認済みに更新（2→3）
					if (!$result = $this->TSubmit->save($tsubmit_data['TSubmit'])) {
						throw new BadRequestException(MESSAGE_API_ERROR_020);
					}
				} elseif ($submit_status === '9') {
					// 提出テーブル配信状態を削除済みに更新
					$tsubmit_data['TSubmit']['submit_state'] = 9; // ステータスを削除済みに更新（x→9）
					if (!$result = $this->TSubmit->save($tsubmit_data['TSubmit'])) {
						throw new BadRequestException(MESSAGE_API_ERROR_020);
					}
				} else {
					if ($tsubmit_data['TSubmit']['submit_state'] === '2' && $user_type_id >= 2) {
						$tsubmit_data['TSubmit']['check_state'] = 1; // ステータスを採点中に更新（0→1）
					} elseif ($tsubmit_data['TSubmit']['check_state'] === '2') {
						$tsubmit_data['TSubmit']['submit_state'] = 3; // ステータスを確認済みに更新（2→3）
					} else {
						$tsubmit_data['TSubmit']['submit_state'] = 1; // ステータスを着手済みに更新（0→1）
					}
//$this->log($tsubmit_data['TSubmit'], LOG_DEBUG);
					if (!$result = $this->TSubmit->save($tsubmit_data['TSubmit'])) {
						throw new BadRequestException(MESSAGE_API_ERROR_020);
					}
					// スケジュール配信状態の更新
					$submit_id = $tsubmit_data['TSubmit']['id'];
					$options = array('conditions' => array('TSchedule.submit_id' => $submit_id));
					$tschedule_data = $this->TSchedule->find('all', $options);
					if (!empty($tschedule_data)) {
						$tschedule_data = $tschedule_data[0];
						$tschedule_data['TSchedule']['status'] = 2; // ステータスを配信済みに更新（1→2）
						if (!$result = $this->TSchedule->save($tschedule_data['TSchedule'])) {
							throw new BadRequestException(MESSAGE_API_ERROR_014);
						}
					}
				}
			} elseif (isset($this->request->data['check_status'])) {
				// 生徒・教師提出用
				$check_status = $this->request->data['check_status']; // 採点状態
				$check_status = mb_ereg_replace('\"', '', $check_status);
				$check_score = $this->request->data['check_score']; // 採点結果
				$check_subscore = array();
				foreach($this->request->data as $key => $value) {
					if(preg_match('/^check_subscore/', $key)) {
						$len = mb_strlen('check_subscore');
						$index_no = mb_substr($key, $len);
						$check_subscore[$index_no] = $value;
					}
				}
				//（教師提出の場合）生徒フォルダに保存
				if ($check_status !== '1') {
					$options = array('conditions' => array('TSubmit.' . $this->TSubmit->primaryKey => $id));
					$tsubmit_data = $this->TSubmit->find('all', $options);
					$user_ids = $tsubmit_data[0]['mUser']['user_ids'];
				}

				// 作業フォルダを作成
				$work_dir = WWW_ROOT . 'files' . DS . $user_ids . DS;
				$dir = new Folder();
				if (!$dir->create($work_dir)) {
					throw new BadRequestException(MESSAGE_API_ERROR_002);
				}

				// base64デコードし、教材ファイルを取得
				$file_name = $this->request->data['resource_name'];
				$file_data = $this->request->data['resource_file'];
				$base_file = $work_dir . $file_name;
				$file = new File($base_file, true, 0644);
				$file->open('wb');
				$file->write(base64_decode($file_data));
				$file->close();

				// zipファイルをS3にアップロード
				require_once(APP . "Vendor" . DS . "aws.phar");
				$submit_id = $id;
				if ($check_status === '1') {
					// 生徒提出
					$file_dir = S3_DIR_RESOURCE_FILE . "/{$check_status}/" . $user_ids . "/{$submit_id}/" . $file_name;
				} else {
					// 教師提出
					$file_dir = S3_DIR_RESOURCE_FILE . "/{$check_status}/" . $user_ids . "/{$submit_id}/" . $file_name;
				}

				// S3にdeleteする（存在しなくても続行）
				$result = $this->S3Service->deleteFile($file_dir);
				// S3にputする
				$result = $this->S3Service->putFile($base_file, $file_dir);
				if (!$result) {
					throw new BadRequestException(MESSAGE_API_ERROR_010);
				}

				// 教材IDのフォルダ/ファイルを削除
				$file = new File($base_file);
				if (!$file->delete()) {
					//throw new BadRequestException(MESSAGE_API_ERROR_009);
				}
				$dir = new Folder($work_dir);
				if (!$dir->delete()) {
					//throw new BadRequestException(MESSAGE_API_ERROR_005);
				}

				// 提出テーブル配信状態の更新
				$options = array('conditions' => array('TSubmit.' . $this->TSubmit->primaryKey => $id));
				$tsubmit_data = $this->TSubmit->find('first', $options);
				if (!empty($tsubmit_data)) {
					if ($check_status === '1') {
						$tsubmit_data['TSubmit']['submit_date'] = date('Y-m-d H:i:s');; // 提出日時を登録
						$tsubmit_data['TSubmit']['submit_state'] = 2; // ステータスを提出済みに更新（1→2）
					} else {
						$tsubmit_data['TSubmit']['check_date'] = date('Y-m-d H:i:s');; // 採点日時を登録
						$tsubmit_data['TSubmit']['check_state'] = 2; // ステータスを採点済みに更新（1→2）
						$tsubmit_data['TSubmit']['result'] = $check_score; // 採点結果
					}
//$this->log($tsubmit_data['TSubmit'], LOG_DEBUG);
					if (!$result = $this->TSubmit->save($tsubmit_data['TSubmit'])) {
						throw new BadRequestException(MESSAGE_API_ERROR_020);
					} else {
						$options = array('conditions' => array('TScore.submit_id' => $id));
						$tscore_data = $this->TSubmit->TScore->find('all', $options);
						// TScoreの採点結果をあわせて更新
						foreach($tscore_data as $key => $tscore){
							$subtitle_id = $tscore['TScore']['subtitle_id'];
							if (isset($check_subscore[$subtitle_id])) {
								$tscore_data[$key]['TScore']['result'] = $check_subscore[$subtitle_id];
							}
						}
						if (!empty($tscore_data)) {
							$this->TSubmit->TScore->saveAll($tscore_data);
						}
					}
				} else {
					throw new BadRequestException(MESSAGE_API_ERROR_022);
				}
			}
		} catch (BadRequestException $exc) {
			$error_id = $exc->getMessage();
			if (empty($GLOBALS['message']->getMessage($error_id))) {
				throw new BadRequestException($error_id);
			}
			$error_message = $GLOBALS['message']->getMessage($error_id);
			$error_code = Configure::read("message_error_code.{$error_id}");
			return $this->error($error_message, $error_code);
		}


		try {
			// 保護者メールアドレスをカンマ区切りで読み込み
			if (!strstr($tsubmit_data['mUser']['parent_address'], ',')) {
				$tparent_address[0] = $tsubmit_data['mUser']['parent_address'];
				$parent1 = $tparent_address[0];
				$parent2 = '';
			} else {
				$tparent_address = split( ',', $tsubmit_data['mUser']['parent_address']);
				$parent1 = $tparent_address[0];
				$parent2 = $tparent_address[1];
			}

			App::import("Controller", "Emails");
			// （お知らせメール送信）
			$sent_email = new EmailsController();
			if (isset($this->request->data['submit_status'])) {
				$submit_state = $tsubmit_data['TSubmit']['submit_state'];

				if ($submit_status === '1' && $submit_state !== 3) { // 生徒のみメール送信
					if ($submit_state != 2 && $submit_state !== 3) { // 教材配信時（採点開始と採点確認を除く）
						if( !empty($parent1) ) {
							$param = array(
									'resource_type' => $tsubmit_data['mResource']['resource_type'],
									'resource_name' => $tsubmit_data['mResource']['resource_name'],
									'deliver_start' => date("m月d日", strtotime($tsubmit_data['TSubmit']['start_datetime'])),
									'deliver_end' => date("m月d日 H時i分", strtotime($tsubmit_data['TSubmit']['end_datetime'])),
									'user_ids' => $tsubmit_data['mUser']['user_ids'],
									'to' => $parent1,
							);
							$sent_email->send(EmailsController::MAIL_KEY_RESOURCE_DELIVER, $param);
						}

						if( !empty($parent2) ) {
							$param = array(
									'resource_type' => $tsubmit_data['mResource']['resource_type'],
									'resource_name' => $tsubmit_data['mResource']['resource_name'],
									'deliver_start' => date("m月d日", strtotime($tsubmit_data['TSubmit']['start_datetime'])),
									'deliver_end' => date("m月d日 H時i分", strtotime($tsubmit_data['TSubmit']['end_datetime'])),
									'user_ids' => $tsubmit_data['mUser']['user_ids'],
									'to' => $parent2,
							);
							$sent_email->send(EmailsController::MAIL_KEY_RESOURCE_DELIVER, $param);
						}
					}
				} elseif ($submit_state === 3) {
					if( !empty($parent1) ) {
						$param = array(
								'resource_type' => $tsubmit_data['mResource']['resource_type'],
								'resource_name' => $tsubmit_data['mResource']['resource_name'],
								'deliver_start' => date("m月d日", strtotime($tsubmit_data['TSubmit']['start_datetime'])),
								'confirm_date' => date("m月d日 H時i分"),
								'user_ids' => $tsubmit_data['mUser']['user_ids'],
								'to' => $parent1,
						);
						$sent_email->send(EmailsController::MAIL_KEY_SOCORE_CONFIRM, $param);
					}

					if( !empty($parent2) ) {
						$param = array(
								'resource_type' => $tsubmit_data['mResource']['resource_type'],
								'resource_name' => $tsubmit_data['mResource']['resource_name'],
								'deliver_start' => date("m月d日", strtotime($tsubmit_data['TSubmit']['start_datetime'])),
								'confirm_date' => date("m月d日 H時i分"),
								'user_ids' => $tsubmit_data['mUser']['user_ids'],
								'to' => $parent2,
						);
						$sent_email->send(EmailsController::MAIL_KEY_SOCORE_CONFIRM, $param);
					}
				}
			} elseif (isset($this->request->data['check_status'])) {
				$check_status = $this->request->data['check_status']; // 採点状態
				$check_status = mb_ereg_replace('\"', '', $check_status);
				if ($check_status === '1') {
					if( !empty($parent1) ) {
						$param = array(
								'resource_type' => $tsubmit_data['mResource']['resource_type'],
								'resource_name' => $tsubmit_data['mResource']['resource_name'],
								'deliver_start' => date("m月d日", strtotime($tsubmit_data['TSubmit']['start_datetime'])),
								'submit_date' => date("m月d日 H時i分", strtotime($tsubmit_data['TSubmit']['submit_date'])),
								'user_ids' => $tsubmit_data['mUser']['user_ids'],
								'to' => $parent1,
						);
						$sent_email->send(EmailsController::MAIL_KEY_RESOURCE_SUBMIT, $param);
					}

					if( !empty($parent2) ) {
						$param = array(
								'resource_type' => $tsubmit_data['mResource']['resource_type'],
								'resource_name' => $tsubmit_data['mResource']['resource_name'],
								'deliver_start' => date("m月d日", strtotime($tsubmit_data['TSubmit']['start_datetime'])),
								'submit_date' => date("m月d日 H時i分", strtotime($tsubmit_data['TSubmit']['submit_date'])),
								'user_ids' => $tsubmit_data['mUser']['user_ids'],
								'to' => $parent2,
						);
						$sent_email->send(EmailsController::MAIL_KEY_RESOURCE_SUBMIT, $param);
					}

				} elseif ($check_status === '2') {
					if( !empty($parent1) ) {
						$param = array(
								'resource_type' => $tsubmit_data['mResource']['resource_type'],
								'resource_name' => $tsubmit_data['mResource']['resource_name'],
								'deliver_start' => date("m月d日", strtotime($tsubmit_data['TSubmit']['start_datetime'])),
								'check_date' => date("m月d日 H時i分", strtotime($tsubmit_data['TSubmit']['check_date'])),
								'user_ids' => $tsubmit_data['mUser']['user_ids'],
								'to' => $parent1,
						);
						$sent_email->send(EmailsController::MAIL_KEY_SOCORE_CHECK, $param);
					}

					if( !empty($parent2) ) {
						$param = array(
								'resource_type' => $tsubmit_data['mResource']['resource_type'],
								'resource_name' => $tsubmit_data['mResource']['resource_name'],
								'deliver_start' => date("m月d日", strtotime($tsubmit_data['TSubmit']['start_datetime'])),
								'check_date' => date("m月d日 H時i分", strtotime($tsubmit_data['TSubmit']['check_date'])),
								'user_ids' => $tsubmit_data['mUser']['user_ids'],
								'to' => $parent2,
						);
						$sent_email->send(EmailsController::MAIL_KEY_SOCORE_CHECK, $param);
					}
				}
			}
		} catch (Exception $e) {
			// todo nothing...
		}


		$res_ary = array(
				$id => array(
						'result' => is_array($result)
				)
		);
		return $this->success($res_ary);
	}
}
