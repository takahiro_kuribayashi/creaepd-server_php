<?php
App::uses('ApiController', 'Controller');
/**
 * API ScreenSync Controller
 *
 * @property TScreensync $TScreensync
 * @property SessionComponent $Session
 * @property RequestHandlerComponent $RequestHandler
*/
class ApiScreenSyncController extends ApiController {
	public $uses = array('TSubmit', 'TScreensync', 'TSession', 'TSection', 'S3Service', 'MUser');

	/**
	 * initialize method
	 *
	 * @throws NotFoundException
	 * @param string $sync_id
	 * @return void
	 */
	public function initialize($sync_id = null) {
		try {
			if (empty($this->request->data['user_id'])) {
				throw new BadRequestException(MESSAGE_API_ERROR_001);
			}
			if (empty($this->request->data['access_key'])) {
				throw new BadRequestException(MESSAGE_API_ERROR_001);
			}
			if (empty($this->request->data['sync_command']) ||
				($this->request->data['sync_command'] === '1' && $this->request->data['sync_command'] === '2')) {
				throw new BadRequestException(MESSAGE_API_ERROR_001);
			}
			$user_ids = $this->request->data['user_id'];
			$access_key = $this->request->data['access_key'];
			//parent::accessAuth($user_ids, $access_key);

			// 同期テーブル状態の更新
			$options = array('conditions' => array('TScreensync.' . $this->TScreensync->primaryKey => $sync_id));
			$tscreensync_data = $this->TScreensync->find('first', $options);

			if (empty($tscreensync_data)) {
				throw new BadRequestException(__('Invalid t screensync'));
			} else {
				// 同期を開始する先生のユーザIDを取得
				$init_user_id = $tscreensync_data['TScreensync']['sync_user_id'];

				// MUserテーブルから接続ユーザのIDを取得
				$options_muser = array('conditions' => array('MUser.user_ids' => $user_ids));
				$user_data = $this->MUser->find('first', $options_muser);

				// 開始ユーザであるかどうかを保持
				$init_user = false;

				if( empty($user_data)) {
					throw new BadRequestException(MESSAGE_API_ERROR_001);
				} else {
					if( $user_data['MUser']['id'] === $init_user_id ) {
						// 開始ユーザであるかどうかを設定
						$init_user = true;
					}
				}

				// 開始ユーザでない場合はセッションを開始せずにエラーとなる
				if( true !== $init_user &&
					'1' === $this->request->data['sync_command'] ) {
					// 接続開始コマンド、かつ開始ユーザでない場合
					if('0' === $tscreensync_data['TScreensync']['sync_state'] ) {
						// 同期が始まっていない場合
						throw new BadRequestException(MESSAGE_API_ERROR_017);
					} else if('2' === $tscreensync_data['TScreensync']['sync_state']) {
						// 同期が終了している場合
						throw new BadRequestException(MESSAGE_API_ERROR_018);
					}
				}

				if( true === $init_user ) {
					$sync_time = $tscreensync_data['TScreensync']['sync_datetime'];
					$now = date('Y-m-d H:i:s');
					$this->log($sync_time, LOG_DEBUG);
					$this->log($now, LOG_DEBUG);
					if (strtotime($now) <= strtotime($sync_time)) {
						// 現在時刻が同期開始時刻よりも前の場合
						throw new BadRequestException(MESSAGE_API_ERROR_017);
					}
					// 開始ユーザの場合は同期状態を更新する
					if ($this->request->data['sync_command'] === '1') {
						$tscreensync_data['TScreensync']['sync_state'] = '1'; // 同期中に更新（0→1）
					} else {
						$tscreensync_data['TScreensync']['sync_state'] = '2'; // 停止中に更新（1→2）
					}
				}
				if (!$result = $this->TScreensync->save($tscreensync_data['TScreensync'])) {
					throw new BadRequestException(__('The t screensync could not be saved. Please, try again.'));
				}
			}
//$this->log($result, LOG_DEBUG);
		} catch (Exception $exc) {
			$error_id = $exc->getMessage();
			if (empty($GLOBALS['message']->getMessage($error_id))) {
				throw new BadRequestException($error_id);
			}
			$error_message = $GLOBALS['message']->getMessage($error_id);
			$error_code = Configure::read("message_error_code.{$error_id}");
			return $this->error($error_message, $error_code);
		}

		$res_ary = array(
				$result
		);
		return $this->success($res_ary);
	}

	/**
	 * index method
	 *
	 * @throws NotFoundException
	 * @param string $sync_id
	 * @return void
	 */
	public function index($sync_id = 0) {
		try {
			if (empty($this->request->data['user_id'])) {
				throw new BadRequestException(MESSAGE_API_ERROR_001);
			}
			if (empty($this->request->data['access_key'])) {
				throw new BadRequestException(MESSAGE_API_ERROR_001);
			}
			$user_ids = $this->request->data['user_id'];
			$access_key = $this->request->data['access_key'];
			//parent::accessAuth($user_ids, $access_key);
			$param_order_no = !empty($this->request->data['order_no']) ? $this->request->data['order_no'] : 0;

			// user_idsからユーザIDに変換
			$options = array('conditions' => array('MUser.user_ids' => $user_ids));
			$muser = $this->MUser->find('first', $options);
			if (empty($muser)) {
				throw new BadRequestException(MESSAGE_API_ERROR_011);
			}
			$user_id = $muser['MUser']['id'];

			$options = array('conditions' => array('TSection.user_id' => $user_id));
			$mgroup = $this->TSection->find('first', $options);
			$group_id = !empty($mgroup['TSection']) ? $mgroup['TSection']['group_id']: 0;

			// 同期テーブルのレコードを取得
			$options = array('conditions' => array('TScreensync.' . $this->TScreensync->primaryKey => $sync_id));
			$this->TScreensync->recursive = -1;
			$tsync_data = $this->TScreensync->find('first', $options);
			if (!empty($tsync_data)) {
				// 同期状態が停止中・終了の場合、エラーを返す
				if ($tsync_data['TScreensync']['sync_state'] == '0') {
					throw new BadRequestException(MESSAGE_API_ERROR_017);
				} elseif ($tsync_data['TScreensync']['sync_state'] == '2') {
					throw new BadRequestException(MESSAGE_API_ERROR_018);
				}
			} else {
				throw new BadRequestException(MESSAGE_API_ERROR_016);
			}

			// 検索条件
			$conditions = array(
					'AND' => array(
							'TScreensync.id' => $sync_id,
							'TScreensync.sync_state' => 1,
							'OR' => array(
									array(
											'TScreensyncUser.group_id' => $group_id
									),
									array(
											'TScreensyncUser.user_id' => $user_id
									),
									array( //（同期開始ユーザのチェック）
											'TScreensync.sync_user_id' => $user_id
									)
							)
					)
			);
			// 結合条件（同期ユーザテーブル）
			$joins = array(
					array(
							'table'		 => 't_screensync_users',
							'alias'		 => 'TScreensyncUser',
							'type'		 => 'LEFT',
							'conditions' => array('TScreensync.id = TScreensyncUser.sync_id')
					),
			);
			$options = array('conditions' => $conditions, 'joins' => $joins);
			$this->TScreensync->recursive = 1;
			$result = $this->TScreensync->find('all', $options);

			if (!empty($result)) {
				$tsync_array = array();
				// 同期ファイルをS3からダウンロード
				require_once(APP . "Vendor" . DS . "aws.phar");
				// 取得した配列にファイル名・バイナリデータを追加する
				foreach($result[0]['TScreensyncFile'] as $key => $tsync_data){
					if ($tsync_data['order_no'] <= $param_order_no) {
						continue;
					} elseif ($tsync_data['create_user_id'] == $user_id) {
						continue;
					}
					$document_id = $tsync_data['document_id'];
					$order_no = $tsync_data['order_no'];
					$file_name = $tsync_data['sync_file_name'];

					$file_dir = S3_DIR_SCREENSYNC_FILE . "/" . $document_id . "/{$order_no}/" . $file_name;

					// S3からgetする
					$file_hander = $this->S3Service->getFile($file_dir);
					if (!$result) {
						throw new BadRequestException(MESSAGE_API_ERROR_008);
					} else {
						// 同期ファイルをbase64エンコードし、JSONにセット
						$file_data = base64_encode($file_hander);
					}

					$tsync_data['sync_data'] = $file_data;
					$tsync_array[] =$tsync_data;
				}
				// 同期ファイル情報を更新
				$result[0]['TScreensyncFile'] = $tsync_array;
			}
//$this->log($result, LOG_DEBUG);
		} catch (Exception $exc) {
			$error_id = $exc->getMessage();
			if (empty($GLOBALS['message']->getMessage($error_id))) {
				throw new BadRequestException($error_id);
			}
			$error_message = $GLOBALS['message']->getMessage($error_id);
			$error_code = Configure::read("message_error_code.{$error_id}");
			return $this->error($error_message, $error_code);
		}
		return $this->success($result);
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		try {
			if (empty($this->request->data['user_id'])) {
				throw new BadRequestException(MESSAGE_API_ERROR_001);
			}
			if (empty($this->request->data['access_key'])) {
				throw new BadRequestException(MESSAGE_API_ERROR_001);
			}
			$user_ids = $this->request->data['user_id'];
			$access_key = $this->request->data['access_key'];
			//parent::accessAuth($user_ids, $access_key);

			// user_idsからユーザIDに変換
			$options = array('conditions' => array('MUser.user_ids' => $user_ids));
			$muser = $this->MUser->find('first', $options);
			if (empty($muser)) {
				throw new BadRequestException(MESSAGE_API_ERROR_011);
			}
			$user_id = $muser['MUser']['id'];

// jsonデコードで配列で受信、展開後にS3・DBに登録する
			// 同期テーブルのレコードを取得
			$options = array('conditions' => array('TScreensync.' . $this->TScreensync->primaryKey => $id));
			$tsync_data = $this->TScreensync->find('first', $options);
			$result = false;
			if (!empty($tsync_data)) {
				// 同期状態が停止中・終了の場合、エラーを返す
				if ($tsync_data['TScreensync']['sync_state'] == '0') {
					throw new BadRequestException(MESSAGE_API_ERROR_017);
				} elseif ($tsync_data['TScreensync']['sync_state'] == '2') {
					throw new BadRequestException(MESSAGE_API_ERROR_018);
				}

				$file_name = $this->request->data['sync_name'];
				$file_data = $this->request->data['sync_file'];
				$document_id = $tsync_data['TScreensync']['document_id'];
				// 同期ファイルテーブルの履歴NOを取得
				$options = array('order' => array('TScreensyncFile.order_no desc'),
						'conditions' => array('TScreensyncFile.submit_id' => $id));
				$order_data = $this->TScreensync->TScreensyncFile->find('first', $options);
				$order_no = $order_data['TScreensyncFile']['order_no'] + 1; // 履歴NOを更新
			} else {
				// デバッグ用
				$file_name = $this->request->data['sync_name'];
				$file_data = $this->request->data['sync_file'];
				$order_no = 1; // 履歴NOを更新
				$document_id = substr(md5(uniqid(microtime())), 0, 16);
				$submit_id = 18; //（テスト用に固定）
			}

				// 作業フォルダを作成
				$work_dir = WWW_ROOT . 'files' . DS . $user_ids . DS;
				$dir = new Folder();
				if (!$dir->create($work_dir)) {
					//$this->Session->setFlash(MESSAGE_API_ERROR_002);
				}

				// base64デコードし、同期ファイルを取得
				$base_file = $work_dir . $file_name;
				$file = new File($base_file, true, 0644);
				$file->open('wb');
				$file->write(base64_decode($file_data));
				$file->close();

				// zipファイルをS3にアップロード
				require_once(APP . "Vendor" . DS . "aws.phar");
				$file_dir = S3_DIR_SCREENSYNC_FILE . "/" . $document_id . "/{$order_no}/" . $file_name;

				// S3にdeleteする（存在しなくても続行）
				$result = $this->S3Service->deleteFile($file_dir);
				// S3にputする
				$result = $this->S3Service->putFile($base_file, $file_dir);
				if (!$result) {
					//$this->Session->setFlash(MESSAGE_API_ERROR_010);
				}

				// 作業フォルダ/ファイルを削除
				$file = new File($base_file);
				if (!$file->delete()) {
					//$this->Session->setFlash(MESSAGE_API_ERROR_009);
				}
				$dir = new Folder($work_dir);
				if (!$dir->delete()) {
					//$this->Session->setFlash(MESSAGE_API_ERROR_005);
				}

				// デバッグ用
				if (empty($tsync_data)) {
					$tsync_data['TScreensyncFile']['submit_id'] = $submit_id;
					$tsync_data['TScreensyncFile']['document_id'] = $document_id;
				} else {
					$tsync_data['TScreensyncFile']['submit_id'] = $tsync_data['TScreensyncFile'][0]['submit_id'];
					$tsync_data['TScreensyncFile']['document_id'] = $tsync_data['TScreensyncFile'][0]['document_id'];
				}
				// 同期テーブル履歴を更新
				$tsync_data['TScreensyncFile']['id'] = NULL;
				$tsync_data['TScreensyncFile']['order_no'] = $order_no;
				$tsync_data['TScreensyncFile']['sync_file_name'] = $file_name;
				$tsync_data['TScreensyncFile']['create_user_id'] = $user_id;
				if (!$result = $this->TScreensync->TScreensyncFile->save($tsync_data['TScreensyncFile'])) {
					throw new BadRequestException(__('The t screensync files could not be saved. Please, try again.'));
				}
		} catch (Exception $exc) {
			$error_id = $exc->getMessage();
			if (empty($GLOBALS['message']->getMessage($error_id))) {
				throw new BadRequestException($error_id);
			}
			$error_message = $GLOBALS['message']->getMessage($error_id);
			$error_code = Configure::read("message_error_code.{$error_id}");
			return $this->error($error_message, $error_code);
		}

		$res_ary = array(
				$id => array(
						'result' => is_array($result)
				)
		);
		return $this->success($res_ary);
	}
}
