<?php
App::uses('AppController', 'Controller');
/**
 * MGroups Controller
 *
 * @property MGroup $MGroup
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class MGroupsController extends AppController {
	public $name = 'mGroups';
	public $uses = array('MGroup', 'MGroupType', 'MIndexNumber', 'TSchedule', 'TSubmit', 'TScreensync');

/**
 * Components
 *
 * @var array
 */
	public $components = array('PagingSupport', 'Search.Prg', 'Paginator', 'Session');
	public $presetVars = array();

	public function __construct($request, $response) {
		parent::__construct($request, $response);
		// ユーザ（グループ）管理画面用の定数ファイルを読み込み
		config('const/constGroup');
	}


/**
 * beforeFilter method
 *
 * @return void
 */
	public function beforeFilter() {
		// 親クラスのbeforeFilter()を呼び出す
		parent::beforeFilter();
		// 検索対象のフィールド設定代入
		$this->presetVars = $this->MGroup->presetVars;

		// ページャ設定
		$pager_numbers = array(
				'before' => ' - ',
				'after'=>' - ',
				'modulus'=> 10,
				'separator'=> ' ',
				'class'=>'pagenumbers'
		);
		$this->set('pager_numbers', $pager_numbers);
	}

/**
 * isAuthorized method
 *
 * @return void
 */
	public function isAuthorized() {
		if ($this->Auth->User('user_type_id') == '3' || $this->Auth->User('user_type_id') == '9') {
			//admin権限を持つユーザは全てのページにアクセスできる
			return true;
		} elseif ($this->Auth->User('user_type_id') == '2') {
			if (in_array($this->action, array('index', 'view'))) {
				//教師権限を持つユーザは検索・閲覧のページにアクセスできる
				return true;
			}
		}
		return false;
	}

/**
 * index method
 *
 * @return void
 */
	public function index() {
		// 検索条件設定
		$this->Prg->commonProcess();
		$this->PagingSupport->inheritPostData(); //$this->dataの値の引き継ぎが行われる

		// 検索条件取得
		$conditions = $this->MGroup->parseCriteria($this->passedArgs);
		//（管理者－所属グループ絞込み）
		$conditions += $this->slctGroupConditions($this->MGroup->alias);
		$conditions += array('MGroup.delete_flg = 0');

		// 結合条件設定（所属テーブルと上位の所属テーブル）
		$joins = array(
				array(
						'table'		 => 't_sections',
						'alias'		 => 'tSection',
						'type'		 => 'LEFT',
						'conditions' => array('MGroup.id = tSection.group_id', 'tSection.user_id = 0')
				),
				array(
						'table'		 => 't_sections',
						'alias'		 => 'tSection0',
						'type'		 => 'LEFT',
						'conditions' => array('tSection.parent_id = tSection0.id')
				),
				array(
						'table'		 => 't_sections',
						'alias'		 => 'tSection1',
						'type'		 => 'LEFT',
						'conditions' => array('MGroup.id = tSection1.group_id', 'tSection1.user_id <> 0')
				),
				array(
						'table'		 => 'm_users',
						'alias'		 => 'MUser',
						'type'		 => 'LEFT',
						'conditions' => array('MUser.id = tSection1.user_id')
				)
		);
		$groups = array('MGroup.id');

		$this->paginate = array(
		  'conditions' => $conditions,
		  'joins' => $joins,
		  'group' => $groups,
		  'limit' => 20,
		  'order' => array('id' => 'desc')
		);

		$this->MGroup->recursive = 0;
		$this->set('mGroups', $this->Paginator->paginate());

		$groupTypes = array('' => $GLOBALS['message']->getMessage(MESSAGE_COMMON_ITEM_090));
		$groupTypes += Configure::read('m_group_types');
		$this->set(compact('groupTypes'));

		//（管理者－所属グループ絞込み）
		$conditions = $this->slctGroupConditions();
		$conditions += array('MGroup.delete_flg = 0');
		$allGroups = array(0 => $GLOBALS['message']->getMessage(MESSAGE_COMMON_ITEM_090));
		$allGroups += $this->MGroup->find('list', array('fields' => array('id','group_name'), 'conditions' => $conditions));
		$this->set(compact('allGroups'));
/* アソシエーション検索（例）
$this->paginate = array(
    'limit' => 10,
    'order' => 'Cooking.id',
);

$dataList = $this->Cooking->CookingFood->find('list', array(
	'fields' => array('CookingFood.cooking_id'),
	'conditions' => array('CookingFood.food_id' => 5),
));
$results = $this->paginate($this->Cooking, array(
	'Cooking.category_id' = 3,
	'Cooking.id' = $dataList,
));

		$user_name = $this->request->data['MGroup']['user_name'];
		$options = array('type' => 'like', 'fields' => array('id'), 'conditions' => array('MUser.user_name' => 'テスト'));
		$userList = $this->MGroup->tSection->mUser->find('list', $options);
*/	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->MGroup->exists($id)) {
			throw new NotFoundException(__('Invalid m group'));
		}
		$options = array('conditions' => array('MGroup.' . $this->MGroup->primaryKey => $id));
		$this->set('mGroup', $this->MGroup->find('first', $options));

		$options = array('conditions' => array('tSection.group_id' . $this->MGroup->tSection->group_id => $id), 'fields' => array('user_id'));
		$ary_users = $this->MGroup->tSection->find('list', $options);

		$options = array('conditions' => array($this->MGroup->tSection->mUser->alias . '.id' => $ary_users, $this->MGroup->tSection->mUser->alias . '.delete_flg <> 1'), 'fields' => array('id','user_name'));
		$groupTUsers = $this->MGroup->tSection->mUser->find('list', $options);
		$this->set('tSectionUsers', $groupTUsers);

		// 上位グループIDを取得（先頭）
		$options = array('conditions' => array('tSection.group_id' . $this->MGroup->tSection->group_id => $id), 'fields' => array('parent_id'));
		$tsection = $this->MGroup->tSection->find('first', $options);
		$parent_id = isset($tsection['tSection']['parent_id']) ? $tsection['tSection']['parent_id']: 0;
		$options = array('conditions' => array('tSection.' . $this->MGroup->tSection->primaryKey => $parent_id), 'fields' => array('group_id'));
		$tsectionAncestor = $this->MGroup->tSection->find('first', $options);
		$ancestor_group = isset($tsectionAncestor['tSection']['group_id']) ? $tsectionAncestor['tSection']['group_id']: 0;

		$options = array('conditions' => array('MGroup.' . $this->MGroup->primaryKey => $ancestor_group), 'fields' => array('id','group_name'));
		$ancestorGroup = $this->MGroup->find('first', $options);
		if (isset($ancestorGroup['MGroup'])) {
			$this->set('ancestorGroup', $ancestorGroup['MGroup']['group_name']);
		} else {
			$this->set('ancestorGroup', $GLOBALS['message']->getMessage(MESSAGE_GROUP_ITEM_006));
		}
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			// シーケンスNOが更新されていた場合、メッセージを表示
			$add_index_number = $this->get_index_number(COLUMN_GROUP_IDX);
			$add_index_number = PREFIX_GROUP_IDS.$add_index_number;
			if ($add_index_number !== $this->request->data['MGroup']['group_ids']) {
				// $this->Session->setFlash(__('The m group ID overlaps.'));
			} else {
				$this->MGroup->create();
				if ($this->MGroup->save($this->request->data)) {
					// 追加のグループ・所属ユーザを登録
					$this->save_tsection($this->request->data['TSection'], 0);

					// シーケンスNOを更新する　※採番テーブルからIndexNoを更新する
					$next_index_number = $this->set_index_number(COLUMN_GROUP_IDX);

					// $this->Session->setFlash(__('The m group has been saved.'));
					return $this->redirect(array('action' => 'index'));
				} else {
					// $this->Session->setFlash(__('The m group could not be saved. Please, try again.'));
				}
			}
		}

		// グループID 初期値生成　※採番テーブルからIndexNoを取得する
		$new_index_number = $this->get_index_number(COLUMN_GROUP_IDX);
		$this->request->data['MGroup']['group_ids'] = PREFIX_GROUP_IDS.$new_index_number;

		$groupTypes = Configure::read('m_group_types');
		$this->set(compact('groupTypes'));

		// 検索条件取得
		$conditions = $this->setUserConditions();
		$groupTUsers = $this->MGroup->tSection->mUser->find('list', array('fields' => array('id','user_name'), 'conditions' => $conditions));
		$this->set(compact('groupTUsers'));

		// 検索条件取得
		$conditions = $this->setGroupConditions();
		$conditions += array('MGroup.delete_flg = 0');
		$allGroups = $this->MGroup->find('list', array('fields' => array('id','group_name'), 'conditions' => $conditions));
		$this->set(compact('allGroups'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->MGroup->exists($id)) {
			throw new NotFoundException(__('Invalid m group'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->MGroup->save($this->request->data)) { // 単一データ保存
				// 追加のグループ・所属ユーザを登録
				$this->save_tsection($this->request->data['TSection'], $id);

				// $this->Session->setFlash(__('The m group has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				// $this->Session->setFlash(__('The m group could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('MGroup.' . $this->MGroup->primaryKey => $id));
			$this->request->data = $this->MGroup->find('first', $options);
		}
		$groupTypes = Configure::read('m_group_types');
		$this->set(compact('groupTypes'));

		// 検索条件取得
		$conditions = $this->setUserConditions();
		$conditions += array($this->MGroup->tSection->mUser->alias . '.delete_flg <> 1');
		$groupTUsers = $this->MGroup->tSection->mUser->find('list', array('fields' => array('id','user_name'), 'conditions' => $conditions));
		$this->set(compact('groupTUsers'));

		// 検索条件取得
		$conditions = $this->setGroupConditions();
		$conditions += array('MGroup.delete_flg = 0', 'MGroup.id <> ' => $id);
		$allGroups = $this->MGroup->find('list', array('fields' => array('id','group_name'), 'conditions' => $conditions));
		$this->set(compact('allGroups'));

		if (isset($this->data['TSection'])) {
			$this->set('valueTGroup', $this->data['TSection']['ancestor_group_id']);
			$this->set('valueTUsers', $this->data['TSection']['user_id']);
		} else {
			// 所属グループの取得(必ず登録するので先頭から取得)
			$tSection = $this->data['tSection'][0];
			$options = array('conditions' => array('tSection.' . $this->MGroup->tSection->primaryKey => $tSection['parent_id']), 'fields' => array('group_id'));
			$tsectionAncestor = $this->MGroup->tSection->find('first', $options);
			$ancestor_group = isset($tsectionAncestor['tSection']['group_id']) ? $tsectionAncestor['tSection']['group_id']: 0;
			$this->set('valueTGroup', $ancestor_group);

			// 所属ユーザの取得
			$valueTUsers = array();
			foreach ($this->data['tSection'] as $tSection) {
				$valueTUsers[] = $tSection['user_id'];
			}
			$this->set('valueTUsers', $valueTUsers);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		if (!$this->MGroup->exists($id)) {
			throw new NotFoundException(__('Invalid m group'));
		}
		if ($this->request->is(array('post', 'put'))) {
			// グループ削除時に関連テーブルに登録がある場合、エラーで中断
			$delete_exc = true;
			// 同じグループIDの登録の取得
			$options = array('conditions' => array('tSection.group_id' . $this->MGroup->tSection->group_id => $id), 'fields' => array('id'));
			$tsection = $this->MGroup->tSection->find('first', $options);
			// 同じグループIDの所属グループ・ユーザ登録の取得
			if ($delete_exc) {
				$parent_id = isset($tsection['tSection']['id']) ? $tsection['tSection']['id']: 0;
				$options = array('conditions' => array('tSection.parent_id' => $parent_id), 'fields' => array('id'));
				$tsectionChildren = $this->MGroup->tSection->find('first', $options);
				if (!empty($tsectionChildren)) {
					$delete_exc = false;
					$this->set('errorMessage', $GLOBALS['message']->getMessage(MESSAGE_LOGIN_ERROR_014));
				}
			}
			// 同じグループIDのスケジュール配信対象の取得
			if ($delete_exc) {
				$options = array('conditions' => array('TSchedule.group_id'=> $id));
				$tschedule = $this->TSchedule->find('first', $options);
				if (!empty($tschedule)) {
					$delete_exc = false;
					$this->set('errorMessage', $GLOBALS['message']->getMessage(MESSAGE_LOGIN_ERROR_015));
				}
			}
			// 同じグループIDの教材配信対象の取得
			if ($delete_exc) {
				$options = array('conditions' => array('TSubmit.group_id'=> $id));
				$tsubmit = $this->TSubmit->find('first', $options);
				if (!empty($tsubmit)) {
					$delete_exc = false;
					$this->set('errorMessage', $GLOBALS['message']->getMessage(MESSAGE_LOGIN_ERROR_016));
				}
			}
			// 同じグループIDの同期配信対象の取得
			if ($delete_exc) {
				$options = array('conditions' => array('TScreensyncUser.group_id' => $id));
				$tscreensyncUser = $this->TScreensync->TScreensyncUser->find('first', $options);
				if (!empty($tscreensyncUser)) {
					$delete_exc = false;
					$this->set('errorMessage', $GLOBALS['message']->getMessage(MESSAGE_LOGIN_ERROR_017));
				}
			}
			// エラーがなければ、削除
			if ($delete_exc && $this->MGroup->save($this->request->data)) {
				// $this->Session->setFlash(__('The m group has been deleted.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$options = array('conditions' => array('MGroup.' . $this->MGroup->primaryKey => $id));
				$this->set('mGroup', $this->MGroup->find('first', $options));
				// $this->Session->setFlash(__('The m group could not be deleted. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('MGroup.' . $this->MGroup->primaryKey => $id));
			$this->set('mGroup', $this->MGroup->find('first', $options));
		}
		$options = array('conditions' => array('tSection.group_id' . $this->MGroup->tSection->group_id => $id), 'fields' => array('user_id'));
		$ary_users = $this->MGroup->tSection->find('list', $options);

		$options = array('conditions' => array($this->MGroup->tSection->mUser->alias . '.id' => $ary_users, $this->MGroup->tSection->mUser->alias . '.delete_flg <> 1'), 'fields' => array('id','user_name'));
		$groupTUsers = $this->MGroup->tSection->mUser->find('list', $options);
		$this->set('tSectionUsers', $groupTUsers);

		// 上位グループIDを取得（先頭）
		$options = array('conditions' => array('tSection.group_id' . $this->MGroup->tSection->group_id => $id), 'fields' => array('parent_id'));
		$tsection = $this->MGroup->tSection->find('first', $options);
		$parent_id = isset($tsection['tSection']['parent_id']) ? $tsection['tSection']['parent_id']: 0;
		$options = array('conditions' => array('tSection.' . $this->MGroup->tSection->primaryKey => $parent_id), 'fields' => array('group_id'));
		$tsectionAncestor = $this->MGroup->tSection->find('first', $options);
		$ancestor_group = isset($tsectionAncestor['tSection']['group_id']) ? $tsectionAncestor['tSection']['group_id']: 0;

		$options = array('conditions' => array('MGroup.' . $this->MGroup->primaryKey => $ancestor_group), 'fields' => array('id','group_name'));
		$ancestorGroup = $this->MGroup->find('first', $options);
		if (isset($ancestorGroup['MGroup'])) {
			$this->set('ancestorGroup', $ancestorGroup['MGroup']['group_name']);
		} else {
			$this->set('ancestorGroup', $GLOBALS['message']->getMessage(MESSAGE_GROUP_ITEM_006));
		}
	}

	/**
	 * save_tsection method
	 *
	 * @return void
	 */
	private function save_tsection($post_data, $id = 0) {
		$data_tsection['TSection'] = array();
		// 上位グループの所属先IDを取得
		$parent_id = $post_data['ancestor_group_id'];
		$options = array('conditions' => array('tSection.group_id' => $parent_id), 'fields' => array('id'));
		$tsectionParent = $this->MGroup->tSection->find('first', $options);
		$tsection_parent_id = !empty($tsectionParent['tSection']['id']) ? $tsectionParent['tSection']['id']: 0;

		$options = array('conditions' => array('tSection.group_id' => $id, 'tSection.user_id' => 0), 'fields' => array('id'));
		$tsectionParent = $this->MGroup->tSection->find('first', $options);
		$tsection_id = !empty($tsectionParent['tSection']['id']) ? $tsectionParent['tSection']['id']: NULL;

		if ($id === 0) {
			$id = $this->MGroup->getLastInsertID();
		}

		// 所属グループを保存
		$data_tsection['TSection'][0]['id'] = $tsection_id;
		$data_tsection['TSection'][0]['parent_id'] = $tsection_parent_id;
		$data_tsection['TSection'][0]['group_id'] = $id;
		$data_tsection['TSection'][0]['user_id'] = 0;
		$this->MGroup->tSection->saveAll($data_tsection['TSection']); // 複数データ保存
	}

/**
 * setUserConditions method
 *
 * @return void
*/
	private function setUserConditions() {
		// 検索条件取得（管理者のみ）
		$conditions = array();
		if ($this->Auth->User('user_type_id') == '3') {
			// 自身の上位グループIDを取得
			$options = array('conditions' => array('tSection.user_id' => $this->Auth->User('id')), 'fields' => array('parent_id'));
			$tsectionAncestor = $this->MGroup->tSection->find('first', $options);
			$parent_id = isset($tsectionAncestor['tSection']['parent_id']) ? $tsectionAncestor['tSection']['parent_id']: 0;

			// 自身の所属配下のユーザ ※自身は含まない
			$ary_tsections = $this->MGroup->tSection->children($parent_id);
			$valueUsers = array();
			foreach ($ary_tsections as $tsection) {
				if ($tsection['tSection']['user_id'] !== '0' && $tsection['tSection']['user_id'] !== $this->Auth->User('id')) {
					$valueUsers[] = $tsection['tSection']['user_id'];
				}
			}
			$conditions += array(
				'AND' => array(
						'OR' => array(
								array(
										'mUser.id' => $valueUsers
								),
								array( //（登録時のユーザで絞込む）
										'mUser.create_user_id' => $this->Auth->User('id')
								)
						)
				)
			);
		}
		return $conditions;
	}

/**
 * setGroupConditions method
 *
 * @return void
 */
	private function setGroupConditions() {
		// 検索条件取得（管理者のみ）
		$conditions = array();
		if ($this->Auth->User('user_type_id') == '3') {
			// 自身の上位グループIDを取得
			$options = array('conditions' => array('tSection.user_id' => $this->Auth->User('id')), 'fields' => array('parent_id','group_id'));
			$tsectionAncestor = $this->MGroup->tSection->find('first', $options);
			$parent_id = isset($tsectionAncestor['tSection']['parent_id']) ? $tsectionAncestor['tSection']['parent_id']: 0;

			// 自身の所属配下のグループ ※自身のグループは含む
			$ary_tsections = $this->MGroup->tSection->children($parent_id);
			$valueGroups = array();
			foreach ($ary_tsections as $tsection) {
				if ($tsection['tSection']['user_id'] == '0' || $tsection['tSection']['user_id'] == $this->Auth->User('id')) {
					$valueGroups[] = $tsection['tSection']['group_id'];
				}
			}
			$conditions += array('id' => $valueGroups);
		}
		return $conditions;
	}
}
