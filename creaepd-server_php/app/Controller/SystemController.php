<?php
App::uses('AppController', 'Controller');
/**
 * MSettings Controller
 *
 * @property System $System
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class SystemController extends AppController {
	// paginate優先するジャンルModelを設定
	public $uses = array('MResourceSubject','MUpdate','MSetting', 'S3Service');

/**
 * Components
 *
 * @var array
 */
	public $components = array('PagingSupport', 'Search.Prg', 'Paginator', 'Session');

	public function __construct($request, $response) {
		parent::__construct($request, $response);
		// システム設定画面用の定数ファイルを読み込み
		config('const/constSystem');
	}

/**
 * beforeFilter method
 *
 * @return void
 */
	public function beforeFilter() {
		// 親クラスのbeforeFilter()を呼び出す
		parent::beforeFilter();
		// 検索対象のフィールド設定代入
		$this->presetVars = $this->MResourceSubject->presetVars;

		// ページャ設定
		$pager_numbers = array(
				'before' => ' - ',
				'after'=>' - ',
				'modulus'=> 10,
				'separator'=> ' ',
				'class'=>'pagenumbers'
		);
		$this->set('pager_numbers', $pager_numbers);
	}


/**
 * isAuthorized method
 *
 * @return void
 */
	public function isAuthorized() {
		if ($this->Auth->User('user_type_id') == '3' || $this->Auth->User('user_type_id') == '9') {
			//admin権限を持つユーザは全てのページにアクセスできる
			return true;
		}
		return false;
	}

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->MSetting->recursive = 0;
		$this->set('mSettings', $this->Paginator->paginate('MSetting'));
	}

/**
 * design method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function design($id = 1) {
		if (!$this->MSetting->exists($id)) {
			throw new NotFoundException(__('Invalid m setting'));
		}
		if ($this->request->is(array('post', 'put'))) {
			$logoFile = $this->request->data['MSetting'];
			if (!empty($logoFile['logo_file']['tmp_name'])){
				// アップロードファイルの一時ファイル名・ファイル名の取得
				$tmp_name = $logoFile['logo_file']['tmp_name'];
				$file_name = $logoFile['logo_file']['name'];

				// imgDIRにアップロードファイルを移動
				move_uploaded_file($tmp_name, WWW_ROOT . 'img' . DS . $file_name);
/*				require_once(APP . "Vendor" . DS . "aws.phar");
				try {
					$file_dir = S3_DIR_SYSTEM_LOGO . "/" . $file_name;

					// S3にputする
					$result = $this->S3Service->putFile($tmp_name, $file_dir);
					if ($result) {
						// $this->Session->setFlash("${result}にファイルを保存しました");
					} else {
						// $this->Session->setFlash("ファイルの保存に失敗しました");
					}
				} catch (Exception $e) {
					// $this->Session->setFlash($e->getMessage());
				}*/
				// ファイル名をセット
				$this->request->data['MSetting']['logo_file'] = $file_name;
			} else {
				unset($this->request->data['MSetting']['logo_file']);
			}
			//$this->log($this->request->data, LOG_DEBUG);
			if ($this->MSetting->save($this->request->data)) {
				$options = array('conditions' => array('MSetting.' . $this->MSetting->primaryKey => $id));
				$this->request->data = $this->MSetting->find('first', $options);

				$this->redirect(Router::url(null, true));
			} else {
				$this->set('validation_errors', $this->MSetting->validationErrors);
				// $this->Session->setFlash(__('The m setting could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('MSetting.' . $this->MSetting->primaryKey => $id));
			$this->request->data = $this->MSetting->find('first', $options);
		}

		//$baseColors = $this->MSetting->MBaseColor->find('list', array('fields' => array('id','color_name')));
		$baseColors = Configure::read('m_base_colors');
		$this->set(compact('baseColors'));
	}

/**
 * language method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function language($id = 1) {
		if (!$this->MSetting->exists($id)) {
			throw new NotFoundException(__('Invalid m setting'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->MSetting->save($this->request->data)) {
				// $this->Session->setFlash(__('The m setting has been saved.'));
			} else {
				// $this->Session->setFlash(__('The m setting could not be saved. Please, try again.'));
			}
			//$mLanguages = $this->MSetting->MLanguage->find('list', array('fields' => array('id','language_name')));
			$mLanguages = Configure::read('m_languages');
			$this->set(compact('mLanguages'));

			$this->redirect(Router::url(null, true));
		} else {
			$options = array('conditions' => array('MSetting.' . $this->MSetting->primaryKey => $id));
			$this->request->data = $this->MSetting->find('first', $options);
		}
		$mLanguages = Configure::read('m_languages');
		$this->set(compact('mLanguages'));
	}

/**
 * subject_index method
 *
 * @return void
 */
	public function subject_index() {
		// 検索条件設定
		$this->Prg->commonProcess();
		$this->PagingSupport->inheritPostData(); //$this->dataの値の引き継ぎが行われる

		// 検索条件取得
		$conditions = $this->MResourceSubject->parseCriteria($this->passedArgs);

		$this->paginate = array(
			'MResourceSubject' => array(
				'conditions' => $conditions,
				'limit' => 20,
			)
		);

		$this->MResourceSubject->recursive = 0;
		$this->set('mResourceSubjects', $this->Paginator->paginate('MResourceSubject'));
	}

/**
 * subject_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function subject_edit($id = null) {
		if ($id === null) {
			$this->MResourceSubject->create();
		} elseif (!$this->MResourceSubject->exists($id)) {
			throw new NotFoundException(__('Invalid m resource subject'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->MResourceSubject->save($this->request->data)) {
				// $this->Session->setFlash(__('The m resource subject has been saved.'));
				return $this->redirect(array('action' => 'subject_index'));
			} else {
				// $this->Session->setFlash(__('The m resource subject could not be saved. Please, try again.'));
			}
		} else {
			if ($id !== null) {
				$options = array('conditions' => array('MResourceSubject.' . $this->MResourceSubject->primaryKey => $id));
				$this->request->data = $this->MResourceSubject->find('first', $options);
			}
		}
	}

/**
 * update_index method
 *
 * @return void
 */
	public function update_index() {
		$this->paginate = array(
				'MUpdate' => array(
						'limit' =>20,
						'order' => array('id' => 'desc'),
				)
		);

		$this->MUpdate->recursive = 0;
		$this->set('mUpdates', $this->Paginator->paginate('MUpdate'));
	}

/**
 * update_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function update_view($id = null) {
		if (!$this->MUpdate->exists($id)) {
			throw new NotFoundException(__('Invalid m update'));
		}
		$options = array('conditions' => array('MUpdate.' . $this->MUpdate->primaryKey => $id));
		$this->set('mUpdate', $this->MUpdate->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function update_add() {
		if ($this->request->is('post')) {
			$apkFile = $this->request->data['MUpdate'];
			if (!empty($apkFile)){
				$version = $this->request->data['MUpdate']['version'];
				// アップロードファイルの一時ファイル名・ファイル名の取得
				$tmp_name = $apkFile['apk_file']['tmp_name'];
				$file_name = $apkFile['apk_file']['name'];

				$size = $apkFile['apk_file']['size'];

				if( $size > 50*1024*1024/* 50MB */) {
					// 50MB以上の場合はエラーとする
					$this->MUpdate->invalidate("apk_file",sprintf($GLOBALS['message']->getMessage(MESSAGE_SYSTEM_UPDATE_002), "50"));
					return false;
				}

				require_once(APP . "Vendor" . DS . "aws.phar");
				try {
					$file_dir = S3_DIR_SOFTWARE_APK . "/" . $version . "/" . $file_name;

					// S3にputする
					$result = $this->S3Service->putFile($tmp_name, $file_dir);
					if ($result) {
						// $this->Session->setFlash("${result}にファイルを保存しました");
					} else {
						// $this->Session->setFlash("ファイルの保存に失敗しました");
					}
				} catch (Exception $e) {
					// $this->Session->setFlash($e->getMessage());
				}
			}
			$this->MUpdate->create();
//$this->log($this->request->data, LOG_DEBUG);
			// ファイル名をセット
			$this->request->data['MUpdate']['apk_file'] = $file_name;
			if ($this->MUpdate->save($this->request->data)) {
				// $this->Session->setFlash(__('The m update has been saved.'));
				return $this->redirect(array('action' => 'update_index'));
			} else {
				// $this->Session->setFlash(__('The m update could not be saved. Please, try again.'));
			}
		}
	}
}
