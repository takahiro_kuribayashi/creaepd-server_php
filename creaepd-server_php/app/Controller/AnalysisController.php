<?php
App::uses('AppController', 'Controller');
/**
 * Analysis Controller
 *
 * @property TSubmit $TSubmit
*/
class AnalysisController extends AppController {
	public $uses = array('TSubmit', 'MResource', 'MGroup', 'MUser');

	/**
	 * Components
	 *
	 * @var array
	*/
	public $components = array('PagingSupport', 'Search.Prg', 'Paginator', 'Session');

	public function __construct($request, $response) {
		parent::__construct($request, $response);
		// データ分析画面用の定数ファイルを読み込み
		config('const/constAnalysis');
		config('const/constSchedule');
		config('const/constDeliver');
		config('const/constStatus');
	}

	/**
	 * beforeFilter method
	 *
	 * @return void
	 */
	public function beforeFilter() {
		// 親クラスのbeforeFilter()を呼び出す
		parent::beforeFilter();

		// trm_student_recordのみは認証なしで使用可能にする
		$this->Auth->allow('trm_student_record');

		// 検索対象のフィールド設定代入
		$this->presetVars = $this->TSubmit->presetVars;

		// ページャ設定
		$pager_numbers = array(
				'before' => ' - ',
				'after'=>' - ',
				'modulus'=> 10,
				'separator'=> ' ',
				'class'=>'pagenumbers'
		);
		$this->set('pager_numbers', $pager_numbers);
	}

	/**
	 * isAuthorized method
	 *
	 * @return void
	 */
	public function isAuthorized() {
		if ($this->Auth->User('user_type_id') == '3' || $this->Auth->User('user_type_id') == '9') {
			//admin権限を持つユーザは全てのページにアクセスできる
			return true;
		} elseif ($this->Auth->User('user_type_id') == '2') {
			//教師権限を持つユーザも全てのページにアクセスできる
			return true;
		}
		return false;
	}

	public function index() {
	}

	/**
	 * ranking method
	 *
	 * @return void
	 */
	public function ranking() {
		// 検索条件設定
		$this->Prg->commonProcess();
		$this->PagingSupport->inheritPostData(); //$this->dataの値の引き継ぎが行われる

		// 検索条件取得（採点済み以上のみ抽出）
		$conditions = $this->TSubmit->parseCriteria($this->passedArgs);

		if (empty($this->request->data)) {
			// 一致しない条件で表示しない
			$conditions += array('TSubmit.id' => 0);
		}
		$conditions += array('TSubmit.check_state >=' => 2);
		$rank_conditions = $this->setTSubmitUserConditions('t_submits');
					
		$this->TSubmit->recursive = -1;
		$this->TSubmit->virtualFields = array(
				'rank' => 'User.rank',
				'user_ids' => 'User.user_ids',
				'user_name' => 'User.user_name',
				'submit_no' => 'TSubmit.submit_no'
		);
		
		$rank_conditions2 = array('TSubmit.id=User.id');
		
		if( !empty($this->request->data['TSubmit']['resource_id']) ) {
			$resourceId = $this->request->data['TSubmit']['resource_id'];
			$rank_conditions2 += array('TSubmit.resource_id = ' => $resourceId);
			
			$rank_conditions = $rank_conditions . ' AND t.resource_id IN (' . $resourceId . ')';
		} else {
			$resourceId = 0;
		}
		
		$this->paginate = array(
				'TSubmit' => array(
						'conditions' => $conditions,
						'fields' => array(
								'User.rank', 'User.user_ids', 'User.user_name','TSubmit.submit_no','TSubmit.result','mResource.resource_name'
						),
						'joins' => array(
								array(
										'type' => 'LEFT',
										'table' => "(SELECT a.*,COUNT(b.id)+1 AS rank
										FROM (SELECT t.id, t.resource_id, t.result, u.user_ids, u.user_name, u.id AS user_id
										FROM t_submits AS t LEFT JOIN m_users AS u ON t.user_id=u.id GROUP BY t.id $rank_conditions ) AS a
										LEFT JOIN (SELECT t.id, t.resource_id, t.result, u.user_ids, u.user_name, u.id AS user_id
										FROM t_submits AS t LEFT JOIN m_users AS u ON t.user_id=u.id GROUP BY t.id $rank_conditions ) AS b
										ON a.result<b.result GROUP BY a.id)",
								'alias' => 'User',
								'conditions' => array('TSubmit.id=User.id')
						),
						array(
								'table'		 => 'm_resources',
								'alias'		 => 'mResource',
						'type'		 => 'INNER',
						'conditions' => array('TSubmit.resource_id = mResource.id')
				),
				array(
						'table'		 => 't_sections',
						'alias'		 => 'tSection',
						'type'		 => 'LEFT',
						'conditions' => array('TSubmit.user_id = tSection.user_id')
						),
				),
				'order' => 'TSubmit.rank ASC'
			)
		);

		$this->Paginator->settings['paramType'] = 'querystring';
		$paginate = $this->Paginator->paginate('TSubmit');

		$this->set('wResults', $paginate);

		//（管理者－所属グループ絞込み）
		$conditions = $this->slctGroupConditions();
		$conditions += array('MGroup.delete_flg = 0', 'MGroup.group_type_id = 1');
		$groupObjs = array('' => $GLOBALS['message']->getMessage(MESSAGE_COMMON_ITEM_090));	//指定しない
		$groupObjs += $this->MGroup->find('list', array('fields' => array('id','group_name'), 'conditions' => $conditions));
		$this->set(compact('groupObjs'));

		$resourceSubjects = array('' => $GLOBALS['message']->getMessage(MESSAGE_COMMON_ITEM_090));	//指定しない
		$resourceSubjects += $this->MResource->ResourceSubject->find('list', array('fields' => array('id','subject_name')));
		$this->set(compact('resourceSubjects'));

		//（管理者or教師－教材作成者絞込み）
		$conditions = $this->slctResourceUserConditions();
		$conditions += array($this->MResource->alias . '.resource_type = 1');
		$resourceObjs = array('' => $GLOBALS['message']->getMessage(MESSAGE_COMMON_ITEM_090));		//指定しない
		$resourceObjs += $this->MResource->find('list', array('fields' => array('id','resource_name'), 'conditions' => $conditions));
		$this->set(compact('resourceObjs'));
	}

/**
 * group_record method
 *
 * @return void
 */
	public function group_record() {
		$record_type = '1';
		if ($this->request->is('post')) {
			if (isset($this->data['TSubmit']['record_type'])) {
				// 成績一覧のタイプを指定
				$record_type = $this->data['TSubmit']['record_type'];
			}
		} elseif ($this->request->is('get')) {
			if(isset($this->request->query['record_type'])){
				$record_type = $this->request->query['record_type'];
			}
		}

		// 検索条件設定
		$this->Prg->commonProcess();
		$this->PagingSupport->inheritPostData(); //$this->dataの値の引き継ぎが行われる

		// 検索条件取得（採点済み以上のみ抽出）
		$conditions = $this->TSubmit->parseCriteria($this->passedArgs);
		$conditions += array('TSubmit.check_state >=' => 2); // 返却済の物だけを表示する
		// 検索条件取得（管理者・教師のみ）
		$conditions += $this->setTSubmitUserConditions();
		// 結合条件
		$joins = array(
				array(
						'table'		 => 'm_resources',
						'alias'		 => 'MResource',
						'type'		 => 'INNER',
						'conditions' => array('TSubmit.resource_id = MResource.id')
				),
				array(
						'table'		 => 'm_users',
						'alias'		 => 'MUser',
						'type'		 => 'INNER',
						'conditions' => array('TSubmit.user_id = MUser.id')
				),
				array(
						'table'		 => 't_sections',
						'alias'		 => 'tSection',
						'type'		 => 'LEFT',
						'conditions' => array('TSubmit.user_id = tSection.user_id')
				),
				array(
						'table'		 => 'm_groups',
						'alias'		 => 'MGroup',
						'type'		 => 'INNER',
						'conditions' => array('tSection.group_id = MGroup.id')
				)
		);


		if ($record_type == '1') {
			$fields = array('MResource.id',
					'MResource.resource_ids',
					'MResource.resource_name',
					'TSubmit.submit_no',
					'TSubmit.max_score',	//最高点
					'TSubmit.min_score',	//最低点
					'TSubmit.avg_score'		//平均点
			);
			$groups = array('TSubmit.submit_no');
		} else {
			$fields = array('MUser.id',
					'MUser.user_ids',
					'MUser.user_name',
					'TSubmit.max_score',	//最高点
					'TSubmit.min_score',	//最低点
					'TSubmit.avg_score'		//平均点
			);
			$groups = array('TSubmit.user_id');
		}
		$this->TSubmit->virtualFields = array(
				'user_name' => 'MUser.user_name',
				'resource_name' => 'MResource.resource_name',
				'max_score' => 'max(TSubmit.result)',
				'min_score' => 'min(TSubmit.result)',
				'avg_score' => 'sum(TSubmit.result)/count(TSubmit.id)'
		);

		$this->TSubmit->recursive = -1;
		$this->paginate = array(
				'conditions' => $conditions,
				'group' => $groups,
				'joins' => $joins,
				'fields' => $fields,
				'limit' => 20,
		);

		$this->set('recordType', $record_type);
		$this->set('tSubmits', $this->Paginator->paginate());

		//（管理者－所属グループ絞込み）
		$conditions = $this->slctGroupConditions();
		$conditions += array('MGroup.delete_flg = 0', 'MGroup.group_type_id = 1');
		$groupObjs = $this->MGroup->find('list', array('fields' => array('id','group_name'), 'conditions' => $conditions));
		$this->set(compact('groupObjs'));

		$recordTypeObjs = Configure::read('group_record_type');
		$this->set(compact('recordTypeObjs'));
	}

/**
 * group_record_resources method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function group_record_resources($id = null) {
		// 検索条件設定
		$this->Prg->commonProcess();
		// 検索条件取得（採点済み以上のみ抽出）
		$conditions = array('TSubmit.submit_no ' => $id, 'TSubmit.check_state >=' => 2);
		// 検索条件取得（管理者・教師のみ）
		$conditions += $this->setTSubmitUserConditions();
		// 結合条件
		$joins = array(
				array(
						'table'		 => 'm_users',
						'alias'		 => 'MUser',
						'type'		 => 'INNER',
						'conditions' => array('TSubmit.user_id = MUser.id')
				)
		);
		$fields = array('TSubmit.check_date',
				'MUser.user_ids',
				'MUser.user_name',
				'TSubmit.result'
		);

		$this->TSubmit->recursive = -1;
		$this->paginate = array(
				'conditions' => $conditions,
				'joins' => $joins,
				'fields' => $fields,
				'limit' => 20,
		);

		$this->set('tSubmits', $this->Paginator->paginate());

		// 検索条件取得（採点済み以上のみ抽出）
		$conditions = array('TSubmit.submit_no ' => $id, 'TSubmit.check_state >=' => 2);
		// 検索条件取得（管理者・教師のみ）
		$conditions += $this->setTSubmitUserConditions();
		// 結合条件
		$joins = array(
				array(
						'table'		 => 'm_resources',
						'alias'		 => 'MResources',
						'type'		 => 'INNER',
						'conditions' => array('TSubmit.resource_id = MResources.id')
				)
		);
		$fields = array('TSubmit.submit_no',
				'MResources.resource_name'
		);

		$options = array('fields' => $fields, 'conditions' => $conditions, 'joins' => $joins);

		$resource = $this->TSubmit->find('first', $options);
		$resource_name = $resource['MResources']['resource_name'] . '[' . $resource['TSubmit']['submit_no'] . ']';
		$this->set('resourceName', $resource_name);

		$this->set('submit_no', $id);
	}

/**
 * group_record_users method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function group_record_users($id = null) {
		// 検索条件設定
		$this->Prg->commonProcess();
		// 検索条件取得（採点済み以上のみ抽出）
		$conditions = array('TSubmit.user_id ' => $id, 'TSubmit.check_state >=' => 2);
		// 検索条件取得（管理者・教師のみ）
		$conditions += $this->setTSubmitUserConditions();
		// 結合条件
		$joins = array(
				array(
						'table'		 => 'm_resources',
						'alias'		 => 'MResource',
						'type'		 => 'INNER',
						'conditions' => array('TSubmit.resource_id = MResource.id')
				)
		);
		$fields = array('TSubmit.check_date',
				'TSubmit.submit_no',
				'MResource.resource_ids',
				'MResource.resource_name',
				'TSubmit.result'
		);

		$this->TSubmit->recursive = -1;
		$this->paginate = array(
				'conditions' => $conditions,
				'joins' => $joins,
				'fields' => $fields,
				'limit' => 20,
		);

		$this->set('tSubmits', $this->Paginator->paginate());

		// 検索条件取得（採点済み以上のみ抽出）
		$conditions = array('TSubmit.user_id ' => $id);
		// 結合条件
		$joins = array(
				array(
						'table'		 => 'm_users',
						'alias'		 => 'MUser',
						'type'		 => 'INNER',
						'conditions' => array('TSubmit.user_id = MUser.id')
				)
		);
		$fields = array(
				'MUser.user_ids',
				'MUser.user_name'
		);

		$options = array('fields' => $fields, 'conditions' => $conditions, 'joins' => $joins);

		$user = $this->TSubmit->find('first', $options);
		$user_name = $user['MUser']['user_name'] . '[' . $user['MUser']['user_ids'] . ']';
		$this->set('userName', $user_name);

		$this->set('user_id', $id);
	}

/**
 * submit_record method
 *
 * @return void
 */
	public function submit_record() {
		// 検索条件設定
		$this->Prg->commonProcess();
		$this->PagingSupport->inheritPostData(); //$this->dataの値の引き継ぎが行われる

		// 検索条件取得（採点済み以上のみ抽出）
		$conditions = $this->TSubmit->parseCriteria($this->passedArgs);
		$conditions += array('TSubmit.check_state >=' => 2);
		// 検索条件取得（管理者・教師のみ）
		$conditions += $this->setTSubmitUserConditions();
		// 結合条件
		$joins = array(
				array(
						'table'		 => 'm_resources',
						'alias'		 => 'mResource',
						'type'		 => 'LEFT',
						'conditions' => array('TSubmit.resource_id = mResource.id')
				),
				array(
						'table'		 => 't_scores',
						'alias'		 => 'TScore',
						'type'		 => 'LEFT',
						'conditions' => array('TSubmit.id = TScore.submit_id')
				),
				array(
						'table'		 => 'm_resource_subtitles',
						'alias'		 => 'MResourceSubtitle',
						'type'		 => 'LEFT',
						'conditions' => array('TScore.subtitle_id = MResourceSubtitle.id')
				),
				array(
						'table'		 => 't_sections',
						'alias'		 => 'tSection',
						'type'		 => 'LEFT',
						'conditions' => array('TSubmit.user_id = tSection.user_id')
				),
				array(
						'table'		 => 'm_groups',
						'alias'		 => 'MGroup',
						'type'		 => 'INNER',
						'conditions' => array('tSection.group_id = MGroup.id')
				)
		);

		$fields = array('TSubmit.id',
				'TSubmit.submit_no',
				'TSubmit.result',
				'MGroup.group_name',
				'mResource.resource_name',
				'MResourceSubtitle.subtitle_name',
				'TScore.allot',
				'TSubmit.avg_score'		//平均点
		);
		$groups = array('TSubmit.group_id', 'TScore.subtitle_id');
		$this->TSubmit->virtualFields = array(
				'group_name' => 'MGroup.group_name',
				'resource_name' => 'mResource.resource_name',
				'subtitle_name' => 'MResourceSubtitle.subtitle_name',
				'allot' => 'TScore.allot',
				'avg_score' => 'sum(TScore.result)/count(TScore.id)'
		);

		$this->TSubmit->recursive = -1;
		$this->paginate = array(
				'conditions' => $conditions,
				'group' => $groups,
				'joins' => $joins,
				'fields' => $fields,
				'limit' => 20,
		);

		$this->Paginator->settings['paramType'] = 'querystring';
		$this->set('tSubmits', $this->Paginator->paginate());

		//（管理者－所属グループ絞込み）
		$conditions = $this->slctGroupConditions();
		$conditions += array('MGroup.delete_flg = 0', 'MGroup.group_type_id = 1');
		$groupObjs = $this->MGroup->find('list', array('fields' => array('id','group_name'), 'conditions' => $conditions));
		$this->set(compact('groupObjs'));

		$resourceSubjects = array('' => $GLOBALS['message']->getMessage(MESSAGE_COMMON_ITEM_090));	//指定しない
		$resourceSubjects += $this->MResource->ResourceSubject->find('list', array('fields' => array('id','subject_name')));
		$this->set(compact('resourceSubjects'));

		//（管理者or教師－教材作成者絞込み）
		$conditions = $this->slctResourceUserConditions();
		$conditions += array($this->MResource->alias . '.resource_type = 1');
		$resourceObjs = array('' => $GLOBALS['message']->getMessage(MESSAGE_COMMON_ITEM_090));	//指定しない
		$resourceObjs += $this->MResource->find('list', array('fields' => array('id','resource_name'), 'conditions' => $conditions));
		$this->set(compact('resourceObjs'));
	}

	/**
	 * student_record method
	 *
	 * @return void
	 */
	public function student_record() {
		// 検索条件設定
		$this->Prg->commonProcess();
		$this->PagingSupport->inheritPostData(); //$this->dataの値の引き継ぎが行われる

		// 検索条件取得（採点済み以上のみ抽出）
		$conditions = $this->TSubmit->parseCriteria($this->passedArgs);
		if (empty($this->request->data)) {
			// 一致しない条件で表示しない（初期表示）
			$conditions += array('TSubmit.id' => 0);
		}
		$conditions += array('TSubmit.check_state =' => 2);
		// 検索条件取得（管理者・教師のみ）
		$conditions += $this->setTSubmitUserConditions();

		$rank_conditions = $this->setTSubmitUserConditions('t_submits');
		
		// 結合条件
		$joins = array(
				array(
						'table'		 => 'm_users',
						'alias'		 => 'MUser',
						'type'		 => 'LEFT',
						'conditions' => array('TSubmit.user_id = MUser.id')
				),
				array(
						'table'		 => 'm_groups',
						'alias'		 => 'MGroup',
						'type'		 => 'LEFT',
						'conditions' => array('TSubmit.group_id = MGroup.id')
				),
				array(
						'table'		 => 'm_resources',
						'alias'		 => 'MResource',
						'type'		 => 'LEFT',
						'conditions' => array('TSubmit.resource_id = MResource.id')
				),
				array(
						'type' => 'LEFT',
						'table' => "(SELECT TSubmitAve.id, TSubmitAve.submit_no, TSubmitAve.check_state, TSubmitAve.unsubscribe, max(TSubmitAve.result) AS max_score, min(TSubmitAve.result) AS min_score,
									sum(TSubmitAve.result)/count(TSubmitAve.result) AS avg_score, count(TSubmitAve.result) AS result_count FROM t_submits AS TSubmitAve GROUP BY TSubmitAve.submit_no, TSubmitAve.check_state HAVING TSubmitAve.check_state IN (2) )",
						'alias' => 'Score',
						'conditions' => array('TSubmit.submit_no=Score.submit_no')
				),
				array(
						'type' => 'LEFT',
						'table' => "(SELECT a.*,COUNT(b.id)+1 AS rank
						FROM (SELECT t.id, t.submit_no, t.resource_id, t.result, u.user_ids, u.user_name, u.id AS user_id
						FROM t_submits AS t LEFT JOIN m_users AS u ON t.user_id=u.id GROUP BY t.id) AS a
						LEFT JOIN (SELECT t.id, t.submit_no, t.resource_id, t.result, u.user_ids, u.user_name, u.id AS user_id
						FROM t_submits AS t LEFT JOIN m_users AS u ON t.user_id=u.id GROUP BY t.id) AS b
						ON a.result<b.result AND a.submit_no=b.submit_no GROUP BY a.id)",
						'alias' => 'Rank',
						'conditions' => array('TSubmit.id=Rank.id')
				),
		);
		$fields = array('TSubmit.id',
				'TSubmit.submit_no',
				'TSubmit.result',
				'MGroup.group_ids',
				'MResource.resource_name',
				'Score.max_score',
				'Score.min_score',
				'Score.avg_score',
				'Score.result_count',
				'Rank.rank',
		);

		$this->TSubmit->virtualFields = array(
				'resource_name' => 'MResource.resource_name',
				'rank' => 'Rank.rank',
				'max_score' => 'Score.max_score',
				'min_score' => 'Score.min_score',
				'avg_score' => 'Score.avg_score'
		);
		
		$this->TSubmit->recursive = -1;
		$groups = array('TSubmit.submit_no');
		$this->paginate = array(
				'conditions' => $conditions,
				'group' => $groups,
				'joins' => $joins,
				'fields' => $fields,
				'limit' => 20,
		);

		$this->Paginator->settings['paramType'] = 'querystring';
		$tsubmit_data = $this->Paginator->paginate();
		$this->set('tSubmits', $tsubmit_data);

		//（管理者－所属グループ絞込み）
		$conditions = $this->slctUserConditions();
		$conditions += array($this->MUser->alias . '.user_type_id=1');
		$conditions += array($this->MUser->alias . '.delete_flg = 0'); // 削除ユーザを除く
		$options = array('fields' => array('id','user_name'), 'conditions' => $conditions);
		$userData = $this->MUser->find('list', $options);
		$this->set(compact('userData'));
	}

	/**
	 * trmstudent_record method
	 *
	 * @return void
	 */
	public function trm_student_record() {
		$request_data = $this->Session->read($this->Session->id());
		if (!empty($this->request->data)) {
			$query_data = $request_data->query();
		} else {
			$query_data = $this->params['url'];
		}
		unset($query_data['password']);
		$userIds = isset($this->params['url']['userId']) ? $this->params['url']['userId']: $query_data['userId'];
		$password = $this->params['url']['password'];
		$startDate = $this->params['url']['startDate'];
		$endDate = $this->params['url']['endDate'];

		// 認証をかける
		// user_idsのユーザマスタ存在チェック
		$options = array('conditions' => array('MUser.user_ids' => $userIds, 'MUser.delete_flg' => 0));
		$muser = $this->MUser->find('first', $options);
		if (empty($muser)) {
			throw new UnauthorizedException(__('Authentication information is required '));
		}
		$userId = $muser['MUser']['id'];
		$this->set('query_data', $query_data);
		$this->Session->write($this->Session->id(), $this->params);

		$this->layout = "trm_default";
		// 検索条件設定
		$this->Prg->commonProcess();
		$this->PagingSupport->inheritPostData(); //$this->dataの値の引き継ぎが行われる

		// 検索条件取得（採点済み以上のみ抽出）
		$conditions = $this->TSubmit->parseCriteria($this->passedArgs);
		$conditions += array('TSubmit.user_id = ' => $userId);
		$conditions += array('TSubmit.start_datetime >= ' => $startDate.'000000');
		$conditions += array('TSubmit.start_datetime <= ' => $endDate.'235959');
		$conditions += array('TSubmit.check_state >=' => 2);
		// 検索条件取得（管理者・教師のみ）
		$conditions += $this->setTSubmitUserConditions();

		$rank_conditions = $this->setTSubmitUserConditions('t_submits');
		
		// 結合条件
		$joins = array(
				array(
						'table'		 => 'm_users',
						'alias'		 => 'MUser',
						'type'		 => 'LEFT',
						'conditions' => array('TSubmit.user_id = MUser.id')
				),
				array(
						'table'		 => 'm_groups',
						'alias'		 => 'MGroup',
						'type'		 => 'LEFT',
						'conditions' => array('TSubmit.group_id = MGroup.id')
				),
				array(
						'table'		 => 'm_resources',
						'alias'		 => 'MResource',
						'type'		 => 'LEFT',
						'conditions' => array('TSubmit.resource_id = MResource.id')
				),
				array(
						'type' => 'LEFT',
						'table' => "(SELECT TSubmitAve.id, TSubmitAve.submit_no, TSubmitAve.check_state, TSubmitAve.unsubscribe, max(TSubmitAve.result) AS max_score, min(TSubmitAve.result) AS min_score,
									sum(TSubmitAve.result)/count(TSubmitAve.result) AS avg_score, count(TSubmitAve.result) AS result_count FROM t_submits AS TSubmitAve GROUP BY TSubmitAve.submit_no, TSubmitAve.check_state HAVING TSubmitAve.check_state IN (2) )",
						'alias' => 'Score',
						'conditions' => array('TSubmit.submit_no=Score.submit_no')
				),
				array(
						'type' => 'LEFT',
						'table' => "(SELECT a.*,COUNT(b.id)+1 AS rank
						FROM (SELECT t.id, t.submit_no, t.resource_id, t.result, u.user_ids, u.user_name, u.id AS user_id
						FROM t_submits AS t LEFT JOIN m_users AS u ON t.user_id=u.id GROUP BY t.id) AS a
						LEFT JOIN (SELECT t.id, t.submit_no, t.resource_id, t.result, u.user_ids, u.user_name, u.id AS user_id
						FROM t_submits AS t LEFT JOIN m_users AS u ON t.user_id=u.id GROUP BY t.id) AS b
						ON a.result<b.result AND a.submit_no=b.submit_no GROUP BY a.id)",
						'alias' => 'Rank',
						'conditions' => array('TSubmit.id=Rank.id')
				),
		);
		$fields = array('TSubmit.id',
				'TSubmit.submit_no',
				'TSubmit.result',
				'MGroup.group_ids',
				'MResource.resource_name',
				'Score.max_score',
				'Score.min_score',
				'Score.avg_score',
				'Score.result_count',
				'Rank.rank',
		);
		$this->TSubmit->virtualFields = array(
				'resource_name' => 'MResource.resource_name',
				'rank' => 'Rank.rank',
				'max_score' => 'Score.max_score',
				'min_score' => 'Score.min_score',
				'avg_score' => 'Score.avg_score'
		);
		

		$this->TSubmit->recursive = -1;
		$groups = array('TSubmit.submit_no');
		$this->paginate = array(
				'conditions' => $conditions,
				'group' => $groups,
				'joins' => $joins,
				'fields' => $fields,
				'limit' => 20,
		);

		$this->Paginator->settings['paramType'] = 'querystring';
		$tsubmit_data = $this->Paginator->paginate();
		$this->set('tSubmits', $tsubmit_data);
	}

/**
 * setTSubmitUserConditions method
 *
 * @return void
*/
	private function setTSubmitUserConditions($alias='TSubmit') {
		// 検索条件取得（管理者・教師のみ）
		$conditions = ($alias !== 'TSubmit') ? '': array();
		if ($this->Auth->User('user_type_id') <= '3') {
			// 自身の上位グループIDを取得
			$options = array('conditions' => array('tSection.user_id' => $this->Auth->User('id')), 'fields' => array('parent_id'));
			$tsectionAncestor = $this->MGroup->tSection->find('first', $options);
			$parent_id = isset($tsectionAncestor['tSection']['parent_id']) ? $tsectionAncestor['tSection']['parent_id']: 0;
			$ary_tsections = $this->MGroup->tSection->children($parent_id);

			// 自身の所属配下のユーザ ※自身は含まない
			$valueUsers = array();
			foreach ($ary_tsections as $tsection) {
				if ($tsection['tSection']['user_id'] !== '0' && $tsection['tSection']['user_id'] !== $this->Auth->User('id')) {
					$valueUsers[] = $tsection['tSection']['user_id'];
				}
			}
			if ($alias !== 'TSubmit') {
				$conditions = 'HAVING user_id IN (' . implode(',', $valueUsers) . ')';
			} else {
				$conditions += array('TSubmit.user_id' => $valueUsers);
			}
		}
		return $conditions;
	}
}
