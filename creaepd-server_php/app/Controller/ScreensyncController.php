<?php
App::uses('AppController', 'Controller');
/**
 * Screensyncs Controller
 *
 * @property Screensync $Screensync
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class ScreensyncController extends AppController {
	// paginate優先するジャンルModelを設定
	public $uses = array('TScreensync', 'TSchedule', 'MGroup', 'MUser', 'MResource', 'MSchedule', 'S3Service');

/**
 * Components
 *
 * @var array
 */
	public $components = array('PagingSupport', 'Search.Prg', 'Paginator', 'Session');

	public function __construct($request, $response) {
		parent::__construct($request, $response);
		// 画面同期画面用の定数ファイルを読み込み
		config('const/constScreensync');
		config('const/constDeliver');
	}

/**
 * beforeFilter method
 *
 * @return void
 */
	public function beforeFilter() {
		// 親クラスのbeforeFilter()を呼び出す
		parent::beforeFilter();
		// 検索対象のフィールド設定代入
		$this->presetVars = $this->TScreensync->presetVars;

		// ページャ設定
		$pager_numbers = array(
				'before' => ' - ',
				'after'=>' - ',
				'modulus'=> 10,
				'separator'=> ' ',
				'class'=>'pagenumbers'
		);
		$this->set('pager_numbers', $pager_numbers);
	}


/**
 * isAuthorized method
 *
 * @return void
 */
	public function isAuthorized() {
		if ($this->Auth->User('user_type_id') == '3' || $this->Auth->User('user_type_id') == '9') {
			//admin権限を持つユーザは全てのページにアクセスできる
			return true;
		} elseif ($this->Auth->User('user_type_id') == '2') {
			//教師権限を持つユーザも全てのページにアクセスできる
			return true;
		}
		return false;
	}

/**
 * index method
 *
 * @return void
 */
	public function index() {
		// 検索条件設定
		$this->Prg->commonProcess();
		$this->PagingSupport->inheritPostData(); //$this->dataの値の引き継ぎが行われる

		$this->TScreensync->virtualFields = array(
				'sync_file_name' => 'MResource.resource_name',
				'sync_user_name' => 'MUser.user_name'
		);
		
		// 検索条件取得
		$conditions = $this->TScreensync->parseCriteria($this->passedArgs);
		// 検索条件取得（管理者・教師のみ所属グループ絞込み）
		$conditions += $this->setTScreensyncConditions();
		// 結合条件
		$joins = array(
				array(
						'table'		 => 't_screensync_files',
						'alias'		 => 'TScreensyncFile',
						'type'		 => 'INNER',
						'conditions' => array('TScreensync.id = TScreensyncFile.submit_id', 'TScreensyncFile.order_no'=> 0)
				),
				array(
						'table'		 => 't_screensync_users',
						'alias'		 => 'TScreensyncUser',
						'type'		 => 'INNER',
						'conditions' => array('TScreensync.id = TScreensyncUser.sync_id')
				),
				array(
						'table'		 => 'm_resources',
						'alias'		 => 'MResource',
						'type'		 => 'INNER',
						'conditions' => array('TScreensyncFile.resource_id = MResource.id')
				),
				array(
						'table'		 => 'm_users',
						'alias'		 => 'MUser',
						'type'		 => 'INNER',
						'conditions' => array('TScreensync.sync_user_id = MUser.id')
				),
		);
		$fields = array('TScreensync.id',
				'TScreensync.document_id',
				'TScreensync.sync_mode',
				'MUser.user_name',
				'TScreensync.sync_state',
				'TScreensync.sync_datetime',
				'TScreensyncFile.order_no',
				'TScreensyncFile.sync_file_name',
				'MResource.resource_ids',
				'MResource.resource_name'
		);

		$this->paginate = array(
			'TScreensync' => array(
				'conditions' => $conditions,
				'joins' => $joins,
				'fields' => $fields,
				'limit' => 20,
				'order' => array('id' => 'desc')
			)
		);

		$tmp_resource = array('' => $GLOBALS['message']->getMessage(MESSAGE_DELIVER_ITEM_007)); //指定なし
		$tmp_type = array('0' => $GLOBALS['message']->getMessage(MESSAGE_DELIVER_ITEM_007)); //指定なし
		
		$this->TScreensync->recursive = 0;
		$this->set('tScreensyncs', $this->Paginator->paginate());

		$syncMode = Configure::read('sync_mode');
		
		$syncMode = array_merge($tmp_type, $syncMode);
		$this->set(compact('syncMode'));

		//（管理者or教師－教材作成者絞込み）
		$conditions = $this->slctResourceUserConditions();
		$mResources = $this->MResource->find('all', array('conditions' => array('delete_flg <>' => 1), 'fields' => array('id','resource_ids','resource_name','resource_type'), 'conditions' => $conditions));
		
		$mBaseResources = array();
		foreach($mResources as $resource) {
			$key = $resource['MResource']['id'];
			$mBaseResources[$key] = '<a id="'.Router::url( '/').'mresources/view2/'.$key.'" class="resource_modal">'.
										$resource['MResource']['resource_name'].'</a>';
		}
		$this->set(compact('mBaseResources'));
		foreach($mResources as $key => $resource) {
			$mResources[$key] = array();
			$mResources[$key]['value'] = $resource['MResource']['id'];
			$mResources[$key]['name'] = $resource['MResource']['resource_name'] . '['. $resource['MResource']['resource_ids'] . ']';
			$mResources[$key]['class'] = $resource['MResource']['resource_type'];
		}
		
		$mResources = array_merge($tmp_resource, $mResources);
		
		$this->set(compact('mResources'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if ($id === null) {
			$this->TScreensync->create();
		} elseif (!$this->TScreensync->exists($id)) {
			throw new NotFoundException(__('Invalid t screensync'));
		}
		if ($this->request->is(array('post', 'put'))) {
			// 配信対象を個人・グループで分けて初期化
			if ($this->request->data['TScreensync']['subject_flg'] === 'user') {
				$this->request->data['TScreensync']['group_id'] = 0;
			} else {
				$this->request->data['TScreensync']['user_id'] = 0;
			}
			$syncFile = $this->request->data['TScreensyncFile'];
			if (empty($this->request->data['TScreensync']['document_id'])){
				// 文書IDの生成
				$document_id = substr(md5(uniqid(microtime())), 0, 16);
				// 同期レコードの初期値設定
				$this->request->data['TScreensync']['document_id'] = $document_id;
				$this->request->data['TScreensync']['sync_state'] = 0;
			} else {
				$document_id = $this->request->data['TScreensync']['document_id'];
			}
			if (!empty($syncFile)){
				$resource_id = $this->request->data['TScreensyncFile']['resource_id'];
				$options = array('conditions' => array('MResource.' . $this->MResource->primaryKey => $resource_id));
				$mResource = $this->MResource->find('first', $options);
				if (!empty($mResource)){
					// 教材のファイル名の取得
					$file_name = $mResource['MResource']['resource_ids'] . '.zip';

					// 文書ID配下のディレクトリに同期ファイルを保存
					require_once(APP . "Vendor" . DS . "aws.phar");
					try {
						$base_dir = S3_DIR_SCREENSYNC_FILE . "/" . $document_id . "/0";
						$base_file = S3_DIR_RESOURCE_FILE . "/0/" . $file_name;
						$copy_file = S3_DIR_SCREENSYNC_FILE . "/" . $document_id . "/0/" . $file_name;


						// S3にdeleteする（存在しなくても続行）
						$result = $this->S3Service->deleteFile($base_dir);
						// S3にcopyする
						$result = $this->S3Service->copyFile($base_file, $copy_file);
						if (!$result) {
							// $this->Session->setFlash("ファイルの複写に失敗しました");
						}
					} catch (Exception $e) {
						// $this->Session->setFlash($e->getMessage());
					}
				}
			}

//$this->log($this->request->data, LOG_DEBUG);
			// リクエストdataを保存用に複写
			$request_data = $this->request->data;
			// 複数選択フラグ
			if( is_array($this->request->data['TScreensync']['user_id']) && count($this->request->data['TScreensync']['user_id']) > 1 ) {
				$request_data['TScreensync']['multi'] = true;
			} else {
				$request_data['TScreensync']['multi'] = false;
			}
			// ユーザID配列でバリデーションがかかるため、クリア
			$request_data['TScreensync']['user_id'] = 0;
			if ($this->TScreensync->save($request_data['TScreensync'])) {
				$sync_id = $this->TScreensync->id;
				$this->request->data['TScreensync']['id'] = $sync_id;
				$upload_exc = false;
				// 同期ファイル登録データをセット
				if (!empty($this->request->data['TScreensyncFile'])){
					// アップロードファイルのファイル名の取得
					$file_name = $mResource['MResource']['resource_ids'] . '.zip';
					$resource_id = $this->request->data['TScreensyncFile']['resource_id'];
					$file_data[0]['id'] = null;
					$file_data[0]['submit_id'] = $sync_id;
					$file_data[0]['document_id'] = $document_id;
					$file_data[0]['resource_id'] = $resource_id;
					$file_data[0]['sync_file_name'] = $file_name;
					$file_data[0]['order_no'] = 0;
					$file_data[0]['create_user_id'] = $this->Auth->User('id');
					$upload_exc = true;
				}

				if ($upload_exc) {
					// 同期ファイル登録データ削除
					$this->TScreensync->TScreensyncFile->deleteAll(array('TScreensyncFile.submit_id' => $sync_id), false);
					// 同期ファイル保存
					$this->TScreensync->TScreensyncFile->saveAll($file_data); // 複数データ保存
				} else {
					unset($this->request->data['TScreensyncFile']);
				}

				// 同期ユーザ登録データ削除
				$this->TScreensync->TScreensyncUser->deleteAll(array('TScreensyncUser.sync_id' => $sync_id), false);
				if ($this->request->data['TScreensync']['group_id'] === 0) {
					// ユーザの場合は、複数ユーザ分登録
					$tmpdata_ary = array();
					foreach ($this->request->data['TScreensync']['user_id'] as $user_id) {
						$tmpdata['TScreensyncUser'] = array();
						$tmpdata['TScreensyncUser']['id'] = NULL;
						$tmpdata['TScreensyncUser']['sync_id'] = (int)($sync_id);
						$tmpdata['TScreensyncUser']['user_id'] = $user_id;
						$tmpdata['TScreensyncUser']['group_id'] = 0;
						$tmpdata_ary[] = $tmpdata;
					}
					$this->TScreensync->TScreensyncUser->saveAll($tmpdata_ary); // 複数データ
				} else {
					$tmpdata['TScreensyncUser'] = array();
					$tmpdata['TScreensyncUser']['id'] = NULL;
					$tmpdata['TScreensyncUser']['sync_id'] = (int)($sync_id);
					$tmpdata['TScreensyncUser']['user_id'] = 0;
					$tmpdata['TScreensyncUser']['group_id'] = $this->request->data['TScreensync']['group_id'];
					$this->TScreensync->TScreensyncUser->save($tmpdata['TScreensyncUser']); // 単一データ
				}

				// 同期配信のスケジュールを登録
				$this->save_schedules($this->request->data, $mResource);

				return $this->redirect(array('action' => 'index'));
			} else {
				$this->request->data['TScreensync']['sync_datetime'] = $this->TScreensync->deconstruct('sync_datetime', $this->request->data['TScreensync']['sync_datetime']);
				$this->request->data['TScreensync']['start_datetime'] = $this->TScreensync->deconstruct('start_datetime', $this->request->data['TScreensync']['start_datetime']);
				$this->request->data['TScreensync']['end_datetime'] = $this->TScreensync->deconstruct('end_datetime', $this->request->data['TScreensync']['end_datetime']);
				$this->set('valueMUser', $this->request->data['TScreensync']['user_id']);
				// $this->Session->setFlash(__('The t screensync could not be saved. Please, try again.'));
			}
		} else {
			if ($id !== null) {
				$options = array('conditions' => array('TScreensync.' . $this->TScreensync->primaryKey => $id));
				$this->request->data = $this->TScreensync->find('first', $options);
				if (!empty($this->request->data['TScreensyncUser'])) {
					if ($this->request->data['TScreensyncUser'][0]['user_id'] === '0') {
						$this->request->data['TScreensync']['subject_flg'] = 'group';
						$this->request->data['TScreensync']['group_id'] = $this->request->data['TScreensyncUser'][0]['group_id'];
					} else {
						$this->request->data['TScreensync']['subject_flg'] = 'user';
					}
				}
				if (!empty($this->request->data['TScreensyncFile'])) {
					if ($this->request->data['TScreensyncFile'][0]['resource_id'] !== '') {
						$this->request->data['TScreensyncFile']['resource_id'] = $this->request->data['TScreensyncFile'][0]['resource_id'];
					}
				}
			} else {
				// 新規登録時は現在日時をセット
				$this->request->data['TScreensync']['sync_datetime'] = 'now';
				$this->request->data['TScreensync']['start_datetime'] = 'now';
				$this->request->data['TScreensync']['end_datetime'] = 'now';
			}
			// 配信ユーザの選択（グループ選択時は0しか入らないため、レコード分取得）
			$valueTUsers = array();
			if (!empty($this->request->data['TScreensyncUser'])){
				foreach ($this->request->data['TScreensyncUser'] as $tScreensync) {
					$tscreensync_user_id = $tScreensync['user_id'];
					$valueTUsers[] = $tscreensync_user_id;
				}
			}
			$this->set('valueMUser', $valueTUsers);
		}

		$syncMode = Configure::read('sync_mode');
		$this->set(compact('syncMode'));

		//（管理者or教師－所属ユーザ絞込み）
		$conditions = $this->slctUserConditions();
		$conditions += array('user_type_id =' => 1);
		$conditions += array('delete_flg = 0');
		$setMUsers = $this->MUser->find('list', array('fields' => array('id','user_name'), 'conditions' => $conditions));
		$this->set(compact('setMUsers'));

		//（管理者or教師－所属グループ絞込み）
		$conditions = $this->slctGroupConditions();
		$conditions += array('delete_flg = 0', 'group_type_id = 1');
		$groupObjs = $this->MGroup->find('list', array('fields' => array('id','group_name'), 'conditions' => $conditions));
		$this->set(compact('groupObjs'));

		//（管理者or教師－開始ユーザ絞込み）
		$conditions = $this->slctUserConditions($this->MUser->alias);
		$conditions += array('user_type_id >=' => 2);
		$conditions += array('delete_flg = 0');
		$options = array('fields' => array('id','user_name','user_ids'), 'conditions' => $conditions);
		$mBaseMUsers = $this->MUser->find('all', $options);
		$mUsers = array();
		foreach($mBaseMUsers as $key => $musers) {
			if (isset($musers['MUser']['id'])){
				$id = $musers['MUser']['id'];
				$user_name = $musers['MUser']['user_name'] . '['. $musers['MUser']['user_ids'] . ']';
				$mUsers[$id] = $user_name;
			} else if (isset($musers['mUser']['id'])){
				$id = $musers['mUser']['id'];
				$user_name = $musers['mUser']['user_name'] . '['. $musers['mUser']['user_ids'] . ']';
				$mUsers[$id] = $user_name;
			}
		}
		$this->set(compact('mUsers'));

		//（管理者or教師－教材作成者絞込み）
		$conditions = $this->slctResourceUserConditions();
		$mResources = $this->MResource->find('all', array('conditions' => array('delete_flg <>' => 1), 'fields' => array('id','resource_ids','resource_name','resource_type'), 'conditions' => $conditions));
		$mBaseResources = array();
		foreach($mResources as $resource) {
			$key = $resource['MResource']['id'];
			$mBaseResources[$key] = '<a id="'.Router::url( '/').'mresources/view2/'.$key.'" class="resource_modal">'.
										$resource['MResource']['resource_name'].'</a>';
		}
		$this->set(compact('mBaseResources'));
		foreach($mResources as $key => $resource) {
			$mResources[$key] = array();
			$mResources[$key]['value'] = $resource['MResource']['id'];
			$mResources[$key]['name'] = $resource['MResource']['resource_name'] . '['. $resource['MResource']['resource_ids'] . ']';
			$mResources[$key]['class'] = $resource['MResource']['resource_type'];
		}
		$this->set(compact('mResources'));
	}

	/**
	 * edit2 method
	 *
	 * @throws NotFoundException
	 * @param string $id
	 * @return void
	 */
	public function edit2($id = null) {
		if ($id === null) {
			$this->TScreensync->create();
		} elseif (!$this->TScreensync->exists($id)) {
			throw new NotFoundException(__('Invalid t screensync'));
		}
		if ($this->request->is(array('post', 'put'))) {
			// 配信対象を個人・グループで分けて初期化
			if ($this->request->data['TScreensync']['subject_flg'] === 'user') {
				$this->request->data['TScreensync']['group_id'] = 0;
			} else if( $this->request->data['TScreensync']['subject_flg'] === 'group' ) {
				$this->request->data['TScreensync']['user_id'] = 0;
			} else {
			}
			$syncFile = $this->request->data['TScreensyncFile'];
			if (empty($this->request->data['TScreensync']['document_id'])){
				// 文書IDの生成
				$document_id = substr(md5(uniqid(microtime())), 0, 16);
				// 同期レコードの初期値設定
				$this->request->data['TScreensync']['document_id'] = $document_id;
				$this->request->data['TScreensync']['sync_state'] = 0;
			} else {
				$document_id = $this->request->data['TScreensync']['document_id'];
			}
			if (!empty($syncFile)){
				$resource_id = $this->request->data['TScreensyncFile']['resource_id'];
				$options = array('conditions' => array('MResource.' . $this->MResource->primaryKey => $resource_id));
				$mResource = $this->MResource->find('first', $options);
				if (!empty($mResource)){
					// 教材のファイル名の取得
					$file_name = $mResource['MResource']['resource_ids'] . '.zip';

					// 文書ID配下のディレクトリに同期ファイルを保存
					require_once(APP . "Vendor" . DS . "aws.phar");
					try {
						$base_dir = S3_DIR_SCREENSYNC_FILE . "/" . $document_id . "/0";
						$base_file = S3_DIR_RESOURCE_FILE . "/0/" . $file_name;
						$copy_file = S3_DIR_SCREENSYNC_FILE . "/" . $document_id . "/0/" . $file_name;


						// S3にdeleteする（存在しなくても続行）
						$result = $this->S3Service->deleteFile($base_dir);
						// S3にcopyする
						$result = $this->S3Service->copyFile($base_file, $copy_file);
						if (!$result) {
							// $this->Session->setFlash("ファイルの複写に失敗しました");
						}
					} catch (Exception $e) {
						// $this->Session->setFlash($e->getMessage());
					}
				}
			}

			//$this->log($this->request->data, LOG_DEBUG);
			// リクエストdataを保存用に複写
			$request_data = $this->request->data;
			// ユーザID配列でバリデーションがかかるため、クリア
			$request_data['TScreensync']['user_id'] = 0;
			if ($this->TScreensync->save($request_data['TScreensync'])) {
				$sync_id = $this->TScreensync->id;
				$this->request->data['TScreensync']['id'] = $sync_id;
				$upload_exc = false;
				// 同期ファイル登録データをセット
				if (!empty($this->request->data['TScreensyncFile'])){
					// アップロードファイルのファイル名の取得
					$file_name = $mResource['MResource']['resource_ids'] . '.zip';
					$resource_id = $this->request->data['TScreensyncFile']['resource_id'];
					$file_data[0]['id'] = null;
					$file_data[0]['submit_id'] = $sync_id;
					$file_data[0]['document_id'] = $document_id;
					$file_data[0]['resource_id'] = $resource_id;
					$file_data[0]['sync_file_name'] = $file_name;
					$file_data[0]['order_no'] = 0;
					$file_data[0]['create_user_id'] = $this->Auth->User('id');
					$upload_exc = true;
				}

				if ($upload_exc) {
					// 同期ファイル登録データ削除
					$this->TScreensync->TScreensyncFile->deleteAll(array('TScreensyncFile.submit_id' => $sync_id), false);
					// 同期ファイル保存
					$this->TScreensync->TScreensyncFile->saveAll($file_data); // 複数データ保存
				} else {
					unset($this->request->data['TScreensyncFile']);
				}

				// 同期ユーザ登録データ削除
				$this->TScreensync->TScreensyncUser->deleteAll(array('TScreensyncUser.sync_id' => $sync_id), false);
				$this->log($this->request->data['TScreensync'], LOG_DEBUG);
				if ($this->request->data['TScreensync']['group_id'] == 0) {
					// ユーザの場合は、複数ユーザ分登録
					$tmpdata_ary = array();
					foreach ($this->request->data['TScreensync']['user_id'] as $user_id) {
						$tmpdata['TScreensyncUser'] = array();
						$tmpdata['TScreensyncUser']['id'] = NULL;
						$tmpdata['TScreensyncUser']['sync_id'] = (int)($sync_id);
						$tmpdata['TScreensyncUser']['user_id'] = $user_id;
						$tmpdata['TScreensyncUser']['group_id'] = 0;
						$tmpdata_ary[] = $tmpdata;
					}
					$this->TScreensync->TScreensyncUser->saveAll($tmpdata_ary); // 複数データ
				} else {
					$tmpdata['TScreensyncUser'] = array();
					$tmpdata['TScreensyncUser']['id'] = NULL;
					$tmpdata['TScreensyncUser']['sync_id'] = (int)($sync_id);
					$tmpdata['TScreensyncUser']['user_id'] = 0;
					$tmpdata['TScreensyncUser']['group_id'] = $this->request->data['TScreensync']['group_id'];
					$this->TScreensync->TScreensyncUser->save($tmpdata['TScreensyncUser']); // 単一データ
				}

				// 同期配信のスケジュールを登録
				$this->save_schedules($this->request->data, $mResource);

				return $this->redirect(array('action' => 'index'));
			} else {
				$this->request->data['TScreensync']['sync_datetime'] = $this->TScreensync->deconstruct('sync_datetime', $this->request->data['TScreensync']['sync_datetime']);
				$this->request->data['TScreensync']['start_datetime'] = $this->TScreensync->deconstruct('start_datetime', $this->request->data['TScreensync']['start_datetime']);
				$this->request->data['TScreensync']['end_datetime'] = $this->TScreensync->deconstruct('end_datetime', $this->request->data['TScreensync']['end_datetime']);
				$this->set('valueMUser', $this->request->data['TScreensync']['user_id']);
				// $this->Session->setFlash(__('The t screensync could not be saved. Please, try again.'));
			}
		} else {
			if ($id !== null) {
				$options = array('conditions' => array('TScreensync.' . $this->TScreensync->primaryKey => $id));
				$this->request->data = $this->TScreensync->find('first', $options);
				if (!empty($this->request->data['TScreensyncUser'])) {
					if ($this->request->data['TScreensyncUser'][0]['user_id'] === '0') {
						$this->request->data['TScreensync']['subject_flg'] = 'group';
						$this->request->data['TScreensync']['group_id'] = $this->request->data['TScreensyncUser'][0]['group_id'];
					} else {
						$this->request->data['TScreensync']['subject_flg'] = 'user';
						$this->request->data['TScreensync']['group_id'] = 0;
					}
				}
				if (!empty($this->request->data['TScreensyncFile'])) {
					if ($this->request->data['TScreensyncFile'][0]['resource_id'] !== '') {
						$this->request->data['TScreensyncFile']['resource_id'] = $this->request->data['TScreensyncFile'][0]['resource_id'];
					}
				}
			} else {
				// 新規登録時は現在日時をセット
				$this->request->data['TScreensync']['sync_datetime'] = 'now';
				$this->request->data['TScreensync']['start_datetime'] = 'now';
				$this->request->data['TScreensync']['end_datetime'] = 'now';
			}
			// 配信ユーザの選択（グループ選択時は0しか入らないため、レコード分取得）
			$valueTUsers = array();
			if (!empty($this->request->data['TScreensyncUser'])){
				foreach ($this->request->data['TScreensyncUser'] as $tScreensync) {
					$tscreensync_user_id = $tScreensync['user_id'];
					$valueTUsers[] = $tscreensync_user_id;
				}
			}
			$this->set('valueMUser', $valueTUsers);
		}

		$syncMode = Configure::read('sync_mode');
		$this->set(compact('syncMode'));

		//（管理者or教師－所属ユーザ絞込み）
		$conditions = $this->slctUserConditions();
		$conditions += array('user_type_id =' => 1);
		$conditions += array('delete_flg = 0');
		$setMUsers = $this->MUser->find('list', array('fields' => array('id','user_name'), 'conditions' => $conditions));
		$this->set(compact('setMUsers'));

		//（管理者or教師－所属グループ絞込み）
		$conditions = $this->slctGroupConditions();
		$conditions += array('MGroup.delete_flg = 0');
		$groupObjs = $this->MGroup->find('list', array('fields' => array('id','group_name'), 'conditions' => $conditions));
		$this->set(compact('groupObjs'));

		//（管理者or教師－開始ユーザ絞込み）
		$conditions = $this->slctUserConditions($this->MUser->alias);
		$conditions += array('user_type_id >=' => 2);
		$conditions += array('delete_flg = 0');
		$options = array('fields' => array('id','user_name','user_ids'), 'conditions' => $conditions);
		$mBaseMUsers = $this->MUser->find('all', $options);
		$mUsers = array();
		foreach($mBaseMUsers as $key => $musers) {
			if (isset($musers['MUser']['id'])){
				$id = $musers['MUser']['id'];
				$user_name = $musers['MUser']['user_name'] . '['. $musers['MUser']['user_ids'] . ']';
				$mUsers[$id] = $user_name;
			} else if (isset($musers['mUser']['id'])){
				$id = $musers['mUser']['id'];
				$user_name = $musers['mUser']['user_name'] . '['. $musers['mUser']['user_ids'] . ']';
				$mUsers[$id] = $user_name;
			}
		}
		$this->set(compact('mUsers'));

		//（管理者or教師－教材作成者絞込み）
		$conditions = $this->slctResourceUserConditions();
		$mResources = $this->MResource->find('all', array('conditions' => array('delete_flg <>' => 1), 'fields' => array('id','resource_ids','resource_name','resource_type'), 'conditions' => $conditions));
		$mBaseResources = array();
		foreach($mResources as $resource) {
			$key = $resource['MResource']['id'];
			$mBaseResources[$key] = '<a id="'.Router::url( '/').'mresources/view2/'.$key.'" class="resource_modal">'.
					$resource['MResource']['resource_name'].'</a>';
		}
		$this->set(compact('mBaseResources'));
		foreach($mResources as $key => $resource) {
			$mResources[$key] = array();
			$mResources[$key]['value'] = $resource['MResource']['id'];
			$mResources[$key]['name'] = $resource['MResource']['resource_name'] . '['. $resource['MResource']['resource_ids'] . ']';
			$mResources[$key]['class'] = $resource['MResource']['resource_type'];
		}
		$this->set(compact('mResources'));
	}

/**
 * save_schedules method
 *
 * @return void
 */
	private function save_schedules($request_data, $mresource_data) {
		// 先に登録済みスケジュールデータ削除
		$sync_id = $request_data['TScreensync']['id'];
		if (!empty($sync_id)) {
			$options = array('conditions' => array('TSchedule.sync_id' => $sync_id));
			$tschedule_data = $this->TSchedule->find('all', $options);
			foreach($tschedule_data as $key => $data){
				$tschedule_data[$key]['MSchedule']['delete_flg'] = 1;
				unset($tschedule_data[$key]['TSchedule']);
			}
			$this->MSchedule->saveAll($tschedule_data);
		}

		// 教材のファイル名の取得
		$resource_name = !empty($mresource_data['MResource']) ? $mresource_data['MResource']['resource_name'] : '';
		$document_id = $request_data['TScreensync']['document_id'];
		$sync_term = $GLOBALS['message']->getMessage(MESSAGE_SCREENSYNC_ITEM_009);
		$sync_datetime = $this->TScreensync->deconstruct('sync_datetime', $request_data['TScreensync']['sync_datetime']);
		$sync_end_datetime = $this->TScreensync->deconstruct('end_datetime', $request_data['TScreensync']['end_datetime']);
		// スケジュールマスタに同期テーブルの内容を登録***
		$this->MSchedule->create();
		$tmpdata = array();
			$tmpdata['MSchedule']['id'] = NULL;
			$tmpdata['MSchedule']['schedule_ids'] = $document_id; // 文書IDをスケジュールIDに設定
			$tmpdata['MSchedule']['title'] = $resource_name;
			$tmpdata['MSchedule']['detail'] = $sync_term .':'. $sync_datetime .' - '. $sync_end_datetime;
			$tmpdata['MSchedule']['version'] = 1;
			$tmpdata['MSchedule']['start_datetime'] = $request_data['TScreensync']['start_datetime'];
			$tmpdata['MSchedule']['end_datetime'] = $request_data['TScreensync']['end_datetime'];
			$tmpdata['MSchedule']['delete_flg'] = 0;
		$resault = $this->MSchedule->save($tmpdata['MSchedule']); // 単一データ
		$mschedule_last_id = $this->MSchedule->getLastInsertID();

		// ***スケジュールテーブルに対象者を登録***
		$this->TSchedule->create();
		$tmpdata_ary = array();
		// 同期開始ユーザ分登録
		$tmpdata['TSchedule'] = array();
		$tmpdata['TSchedule']['id'] = NULL;
		$tmpdata['TSchedule']['schedule_id'] = (int)($mschedule_last_id);
		$tmpdata['TSchedule']['group_id'] = 0;
		$tmpdata['TSchedule']['user_id'] = $request_data['TScreensync']['sync_user_id'];
		$tmpdata['TSchedule']['sync_id'] = (int)($sync_id);
		$tmpdata['TSchedule']['status'] = 1;
		$tmpdata_ary[] = $tmpdata;
		if ($request_data['TScreensync']['group_id'] == 0) {
			// ユーザの場合は、複数ユーザ分登録
			foreach ($request_data['TScreensync']['user_id'] as $user_id) {
				$tmpdata['TSchedule']['id'] = NULL;
				$tmpdata['TSchedule']['schedule_id'] = (int)($mschedule_last_id);
				$tmpdata['TSchedule']['group_id'] = 0;
				$tmpdata['TSchedule']['user_id'] = $user_id;
				$tmpdata['TSchedule']['sync_id'] = (int)($sync_id);
				$tmpdata['TSchedule']['status'] = 1;
				$tmpdata_ary[] = $tmpdata;
			}
		} else {
			// グループ分登録
			$tmpdata['TSchedule'] = array();
			$tmpdata['TSchedule']['id'] = NULL;
			$tmpdata['TSchedule']['schedule_id'] = (int)($mschedule_last_id);
			$tmpdata['TSchedule']['group_id'] = $request_data['TScreensync']['group_id'];
			$tmpdata['TSchedule']['user_id'] = 0;
			$tmpdata['TSchedule']['sync_id'] = (int)($sync_id);
			$tmpdata['TSchedule']['status'] = 1;
			$tmpdata_ary[] = $tmpdata;
		}
		$this->TSchedule->saveAll($tmpdata_ary); // 複数データ
	}

/**
 * setTScreensyncConditions method
 *
 * @return void
*/
	private function setTScreensyncConditions() {
		// 検索条件取得（管理者・教師のみ）
		$conditions = array();
		if ($this->Auth->User('user_type_id') <= '3') {
			// 自身の上位グループIDを取得
			$options = array('conditions' => array('tSection.user_id' => $this->Auth->User('id')), 'fields' => array('parent_id'));
			$tsectionAncestor = $this->MGroup->tSection->find('first', $options);
			$parent_id = isset($tsectionAncestor['tSection']['parent_id']) ? $tsectionAncestor['tSection']['parent_id']: 0;
			$ary_tsections = $this->MGroup->tSection->children($parent_id);

			// 自身の所属配下のユーザ・グループ ※自身のグループは含む
			$valueUsers = array();
			$valueGroups = array();
			foreach ($ary_tsections as $tsection) {
				if ($tsection['tSection']['user_id'] !== '0') {
					$valueUsers[] = $tsection['tSection']['user_id'];
				} else {
					$valueGroups[] = $tsection['tSection']['group_id'];
				}
			}
			$conditions += array(
					'OR' => array(
							array(
									'TScreensyncUser.user_id' => $valueUsers,
									'TScreensyncUser.group_id' => 0
							),
							array(
									'TScreensyncUser.user_id' => 0,
									'TScreensyncUser.group_id' => $valueGroups
							)
					)
			);
		}
		return $conditions;
	}
}
