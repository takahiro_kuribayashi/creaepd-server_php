<?php
/**
 * 既存のページング機能をサポートするコンポーネント
 */

class PagingSupportComponent extends Component {

	var $components = array('Session');

	var $_controller = null;

	var $_sessionName = '';

	function startup( & $controller ) {
		$this->_controller = $controller;
		$this->_sessionName = $this->_getSessionName( $controller->params );
	}

	/**
	 * controller->data の値の引き継ぎを行う
	 */
	function inheritPostData(){

		if( !empty( $this->_controller->data ) ){
			$this->Session->write( $this->_sessionName, $this->_controller->data );
		}
		elseif( !empty( $this->_controller->params['named']['page'] ) && $this->Session->check( $this->_sessionName ) ){
			$this->_controller->data = $this->Session->read( $this->_sessionName );
		}
		else{
			$this->Session->delete( $this->_sessionName );
		}
	}

	/**
	 * 現在引き継がれているPostDataを取得
	 * @param $params = array( 'controller' => ..., 'action' => ... )
	 * @return array
	 */
	function getInheritPostdata( $params ){
		if( $this->Session->check( $this->_getSessionName( $params ) ) ){
			return $this->Session->read( $this->_getSessionName( $params ) );
		}
		else{
			return null;
		}
	}

	/**
	 * SESSIONに使用する名前を取得
	 */
	private function _getSessionName( $params ){
		return 'pagingSupport_' . $params['controller'] . $params['action'];
	}
}
