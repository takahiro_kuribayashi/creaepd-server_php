<?php
define('SALT_HEADER', '$2a$04$');

class RandomPasswordComponent extends Component {

    function createPassword($length = 8, $mode = 'alnum') {
        if ($length < 1 || $length > 256) {
            return false;
        }

        $smallAlphabet = 'abcdefghijklmnopqrstuvwxyz';
        $largeAlphabet = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $numeric = '0123456789';
        $sign = '!#$%&*+@?';

        switch ($mode) {
            // 小文字英字
            case 'small':
                $chars = $smallAlphabet;
                break;
            // 大文字英字
            case 'large':
                $chars = $largeAlphabet;
                break;
            // 小文字英数字
            case 'smallalnum':
                $chars = $smallAlphabet . $numeric;
                break;
            // 大文字英数字
            case 'largealnum':
                $chars = $largeAlphabet . $numeric;
                break;
            // 数字
            case 'num':
                $chars = $numeric;
                break;
            // 大小文字英字
            case 'alphabet':
                $chars = $smallAlphabet . $largeAlphabet;
                break;
            // 大小文字英数字
            case 'alnum':
            default:
                $chars = $smallAlphabet . $largeAlphabet . $numeric;
                break;
            // 大小文字英数字 + 記号
            case 'alnumsign':
                $chars = $smallAlphabet . $largeAlphabet . $numeric . $sign;
                break;
        }

        $charsLength = strlen($chars);
        $password = '';
        for ($i = 0; $i < $length; $i++) {
            $num = mt_rand(0, $charsLength - 1);
            $password .= $chars{$num};
        }

        return $password;
    }

    function createSalt() {
        // Blowfishのソルトに使用できる文字種
        $chars = array_merge(range('0', '9'), range('a', 'z'), range('A', 'Z'), array('.', '/'));

        // ソルトを生成（上記文字種からなるランダムな22文字）
        $salt = '';
        for ($i = 0; $i < 22; $i++) {
            $salt .= $chars[mt_rand(0, count($chars) - 1)];
        }

        return SALT_HEADER . $salt;
    }
}
?>