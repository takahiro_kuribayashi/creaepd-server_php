<?php
define('CONFIG_XML', 'config.xml');

class ConfigXmlComponent extends Component {
	function getDirXML($path, $dir_id, $dir_name, $resource_files)
	{
		if (!$tree = $this->getDirTree($path)) {	// ディレクトリの階層が取得できなければ false を返す
			return false;
		}

		// SimpleXMLElement オブジェクトを作成
		$xml = new SimpleXMLElement('<?xml version="1.0" encoding="UTF-8"?><doc></doc>');

		// 属性を追加
		$xml->addAttribute('id', $dir_id);
		$xml->addAttribute('title', $dir_name);

		// 子要素を追加
		foreach ($tree as $key => $value) {
			if (is_array($value)) {
				$dirXml = $xml->addChild('dir');
				$dirXml->addAttribute('name', $key);
				if ($key == 'pdf') {
					// PDFファイルを1,2の順に並び替え
					$pdf_value = array();
					foreach($resource_files as $key => $resource){
						// PDFのアップなしの場合、古いファイル名を取得
						if (!empty($resource['resource_file_name']['name'])) {
							$file_name = $resource['resource_file_name']['name'];
						} else {
							$file_name = $resource['resource_file_name_old'];
						}
						// pdfファイル名にインデックス識別を追記する
						$pdf_file_name = basename($file_name, '.pdf') .'_pdf'. ($key+1) .'.pdf';
						if (isset($value[$pdf_file_name])) {
							$pdf_value[$pdf_file_name] = $value[$pdf_file_name];
						}
					}
					$value = $pdf_value;
				}
				//ディレクトリ配下のファイルを展開
				foreach ($value as $dir_key => $dir_value) {
					$fileXml = $dirXml->addChild('file');
					$fileXml->addAttribute('name', $dir_key);
					$fileXml->addAttribute('size', $dir_value);
				}
			} else {
				$fileXml = $xml->addChild('file');
				$fileXml->addAttribute('name', $key);
				$fileXml->addAttribute('size', $value);
			}
		}

		$configXml = $xml->addChild('file');
		$configXml->addAttribute('name', CONFIG_XML);
		$xml_size = mb_strlen($xml->asXML());
		$configXml->addAttribute('size', $xml_size);

		// xmlを出力
		$xml_str = $xml->asXML();

		return $xml_str;
	}

	function getDirTree($path)
	{
		if (!is_dir($path)) {	// ディレクトリでなければ false を返す
			return false;
		}

		$dir = array();	   // 戻り値用の配列
		if ($handle = opendir($path)) {
			while (false !== ($file = readdir($handle))) {
				if ('.' == $file || '..' == $file) {
					// 自分自身と上位階層のディレクトリを除外
					continue;
				}
				if (is_dir($path . DS . $file)) {
					// ディレクトリならば自分自身を呼び出し
					$dir[$file] = $this->getDirTree($path.'/'.$file);
				} elseif (is_file($path . DS . $file)) {
					// ファイルならばサイズを格納
					$size = filesize($path . DS . $file);
					$dir[$file] = $size;
				}
			}
			closedir($handle);
		}

		return $dir;
	}
}

