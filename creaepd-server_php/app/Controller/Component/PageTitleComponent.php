<?php
/**
 * ページタイトルを設定するコンポーネント
 */
class PageTitleComponent extends Component {

	public $pageTitles = array(
			'Analysis' => array(
					'ranking'	 =>  '成績ランキング',
					'group_record'	 =>  'グループ成績一覧',
					'group_record_resources' => '教材別成績一覧',
					'group_record_users'	 => 'ユーザ別成績一覧',
					'submit_record'	 => '教材・配信成績一覧',
			),
			'MResources' => array(
					'index'		 => '教材検索',
					'view'		 => '教材閲覧',
					'add'		 => '教材登録',
					'edit'		 => '教材更新',
					'delete'	 => '教材削除',
			),
			'Deliver' => array(
					'index'		 => '配信検索',
					'edit'		 => '配信管理',
					'allotment'	 => '配点設定',
			),
			'Status' => array(
					'user'		 => 'ユーザー進捗確認',
					'submit'	 => '提出物進捗確認',
					'check'		 => '採点状況確認',
			),
			'MSchedules' => array(
					'index'		 => 'スケジュール検索',
					'view'		 => 'スケジュール閲覧',
					'add'		 => 'スケジュール登録',
					'edit'		 => 'スケジュール更新',
					'delete'	 => 'スケジュール削除',
			),
			'MUsers' => array(
					'index'		 => 'ユーザ検索',
					'view'		 => 'ユーザ閲覧',
					'add'		 => 'ユーザ登録',
					'edit'		 => 'ユーザ更新',
					'delete'	 => 'ユーザ削除',
					'multi_add'	 => 'ユーザ一括登録',
			),
			'MGroups' => array(
					'index'		 => 'グループ検索',
					'view'		 => 'グループ閲覧',
					'add'		 => 'グループ登録',
					'edit'		 => 'グループ更新',
					'delete'	 => 'グループ削除',
			),
			'System' => array(
					'design'	 => 'デザイン変更',
					'language'	 => '言語設定',
					'subject_index'	 => 'ジャンル設定',
					'subject_edit'	 => 'ジャンル登録',
					'subtitle_edit'	 => '子ジャンル登録',
					'subject_index'	 => 'ジャンル設定',
					'subject_index'	 => 'ジャンル設定',
					'update_index'	 => 'アップデート管理',
					'update_view'	 => 'アップデート閲覧',
					'update_add'	 => 'アップデート登録',
			),
	);

	/**
	 * ページタイトルを設定する
	 *
	 * @param Controller $controller A reference to the instantiating controller object
	 * @return string
	 * @access public
	*/
	public function getPageTitle(Controller $controller) {
		return !empty($this->pageTitles[$controller->name][$controller->action])
		? $this->pageTitles[$controller->name][$controller->action]
		: 'Non Titles';
	}
}
