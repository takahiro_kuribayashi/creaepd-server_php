<?php
App::uses('ApiController', 'Controller');
/**
 * API TimeSync Controller
 *
 * @property SessionComponent $Session
 * @property RequestHandlerComponent $RequestHandler
*/
class ApiTimeSyncController extends ApiController {

	/**
	 * index method
	 *
	 * @return void
	*/
	public function index() {
		// システム時刻 + タイムゾーン
		$time = date('Y-m-d H:i:s e');

		// 取得条件
		$result = array(
				'ServerTime' => $time
		);

		return $this->success($result);
	}
}
