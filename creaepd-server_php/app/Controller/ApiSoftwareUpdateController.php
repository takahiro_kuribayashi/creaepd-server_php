<?php
App::uses('ApiController', 'Controller');
/**
 * API SoftwareUpdate Controller
 *
 * @property MUpdate $MUpdate
 * @property SessionComponent $Session
 * @property RequestHandlerComponent $RequestHandler
*/
class ApiSoftwareUpdateController extends ApiController {
	public $uses = array('MUpdate', 'S3Service');

	/**
	 * index method
	 *
	 * @return void
	*/
	public function index() {
		if (empty($this->request->data['user_id'])) {
			throw new BadRequestException('AJAX処理に必要な項目が設定されていません。');
		}
		if (empty($this->request->data['access_key'])) {
			throw new BadRequestException('AJAX処理に必要な項目が設定されていません。');
		}
		// ***** user_idsからユーザIDに変換が必要 *****
		$user_id = 2;

		// 取得条件
		$options = array('order' => array('MUpdate.id desc'));
		$result = $this->MUpdate->find('first', $options);
//$this->log($result, LOG_DEBUG);
		return $this->success($result);
	}

	/**
	 * view method
	 *
	 * @throws NotFoundException
	 * @param string $id
	 * @return void
	 */
	public function view($id = null) {
		if (empty($this->request->data['user_id'])) {
			throw new BadRequestException('AJAX処理に必要な項目が設定されていません。');
		}
		if (empty($this->request->data['access_key'])) {
			throw new BadRequestException('AJAX処理に必要な項目が設定されていません。');
		}

		// ***** user_idsからユーザIDに変換が必要、versionの存在チェックも *****
		$user_id = 2;
		$user_ids = 'crea0002';

		$result = false;
		$options = array('conditions' => array('MUpdate.' . $this->MUpdate->primaryKey => $id));
		$mupdate_data = $this->MUpdate->find('first', $options);
		if (!empty($mupdate_data)) {
			$version = $mupdate_data['MUpdate']['version'];
			// 作業フォルダを作成
			$work_dir = WWW_ROOT . 'files' . DS . $user_ids . DS;
			$dir = new Folder();
			if (!$dir->create($work_dir)) {
				//$this->Session->setFlash("フォルダの作成に失敗しました");
			}
			// コピー先のapkファイル
			$file_name = $mupdate_data['MUpdate']['apk_file'];
			$base_file = $work_dir . $file_name;

			// apkファイルをS3からダウンロード
			require_once(APP . "Vendor" . DS . "aws.phar");
			try {
				$file_dir = S3_DIR_SOFTWARE_APK . "/" . $version . "/" . $file_name;

				// S3からgetする
				$result = $this->S3Service->getFile($file_dir);
				if (!$result) {
					//$this->Session->setFlash("ファイルの読み込みに失敗しました");
				}
				$result = file_put_contents($base_file, $result);
				if (!$result) {
					//$this->Session->setFlash("ファイルのコピーに失敗しました");
				}
			} catch (Exception $e) {
				//$this->Session->setFlash($e->getMessage());
			}
		} else {
			throw new BadRequestException(__('Invalid m update'));
		}

		if ($result) {
			// apkファイルをbase64エンコードし、JSONにセット
			$file_data = base64_encode(file_get_contents($base_file));
			$res_ary = array(
					'MUpdate' => array(
							'id' => $mupdate_data['MUpdate']['id'],
							'apk_name' => $mupdate_data['MUpdate']['apk_file'],
							'apk_file' => $file_data
					)
			);
		} else {
			$res_ary = array(
					'MUpdate' => $result
			);
		}
		return $this->success($res_ary);
	}
}
