<?php
/**
 * 配信コントローラ
 */

App::uses('AppController', 'Controller');

class DeliverController extends AppController {
	public $uses = array( 'TSubmit', 'TScore', 'TSchedule', 'TSection', 'MSchedule', 'MResource', 'MGroup', 'MUser', 'MIndexNumber');

/**
 * Components
 *
 * @var array
 */
	public $components = array('PagingSupport', 'Search.Prg', 'Paginator', 'Session');

	public function __construct($request, $response) {
		parent::__construct($request, $response);
		// トップ画面用の定数ファイルを読み込み
		config('const/constDeliver');

		// 共通変数の初期化
		$this->set('group_list', "");
	}

/**
 * beforeFilter method
 *
 * @return void
 */
	public function beforeFilter() {
		// 親クラスのbeforeFilter()を呼び出す
		parent::beforeFilter();
		// 検索対象のフィールド設定代入
		$this->presetVars = $this->TSubmit->presetVars;

		// ページャ設定
		$pager_numbers = array(
				'before' => ' - ',
				'after'=>' - ',
				'modulus'=> 10,
				'separator'=> ' ',
				'class'=>'pagenumbers'
		);
		$this->set('pager_numbers', $pager_numbers);
	}

/**
 * isAuthorized method
 *
 * @return void
 */
	public function isAuthorized() {
		if ($this->Auth->User('user_type_id') == '3' || $this->Auth->User('user_type_id') == '9') {
			//admin権限を持つユーザは全てのページにアクセスできる
			return true;
		} elseif ($this->Auth->User('user_type_id') == '2') {
			//教師権限を持つユーザも全てのページにアクセスできる
			return true;
		}
		return false;
	}

/*    public function index() {
    	// グループデータを取得
    	$group_list = $this->MGroup->find('all');
    	$this->set('group_list', $group_list);

    	// ツリー表示用のグループデータを作成
    	App::import('Vendor', 'Menu/GroupTree');
    	$this->set('group_array', GroupTree::getMenuArray($group_list));
    }*/
/**
 * index method
 *
 * @return void
 */
	public function index() {
		// 検索条件設定
		$this->Prg->commonProcess();
		$this->PagingSupport->inheritPostData(); //$this->dataの値の引き継ぎが行われる

		// 検索条件取得
		$conditions = $this->TSubmit->parseCriteria($this->passedArgs);
		$conditions += array('TSubmit.unsubscribe' => 0);
		$conditions += array('mResource.delete_flg <>' => 1); // デフォルト削除済みを除く

		$this->log($conditions, LOG_DEBUG);
		
		$this->TSubmit->virtualFields = array(
				'user_name' => 'mUser.user_name',
				'group_name' => 'mGroup.group_name'
		);
		
		// 検索条件取得（管理者・教師のみ所属グループ絞込み）
		$conditions += $this->setTSubmitConditions();

		$this->paginate = array(
				'conditions' => $conditions,
				'limit' => 20,
				'order' => array('TSubmit.id' => 'desc')
		);

		$this->TSubmit->recursive = 0;
		$this->set('tSubmits', $this->Paginator->paginate());

		//（管理者or教師－採点ユーザ絞込み）
		$conditions = $this->slctUserConditions($this->MUser->alias);
		$conditions += array('user_type_id >=' => 2);
		$options = array('fields' => array('id','user_name','user_ids'), 'conditions' => $conditions);
		$mUsers = array('' => $GLOBALS['message']->getMessage(MESSAGE_DELIVER_ITEM_007)); //指定なし
		$mBaseMUsers = $this->MUser->find('all', $options);
		foreach($mBaseMUsers as $key => $musers) {
			if (isset($musers['mUser']['id'])){
				$id = $musers['mUser']['id'];
				$user_name = $musers['mUser']['user_name'] . '['. $musers['mUser']['user_ids'] . ']';
				$mUsers[$id] = $user_name;
			}
		}
		$this->set(compact('mUsers'));
	}

/**
 * edit2 method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit2($id = null) {
		if ($id === null) {
			$this->TSubmit->create();
		} elseif (!$this->TSubmit->exists($id)) {
			throw new NotFoundException(__('Invalid t submit'));
		}

		if ($this->request->is(array('post', 'put'))) {
			// 配信対象を個人・グループで分けて初期化
			if ($this->request->data['TSubmit']['subject_flg'] === 'user') {
				$this->request->data['TSubmit']['group_id'] = 0;
			} else if( $this->request->data['TSubmit']['subject_flg'] === 'group' ){
				$this->request->data['TSubmit']['user_id'] = 0;
			} else {
			}
			// 教科書の場合は、提出日時・採点者の設定は不要
			if (!isset($this->request->data['TSubmit']['end_datetime'])) {
				$this->request->data['TSubmit']['end_datetime'] = $this->request->data['TSubmit']['start_datetime'];
				$this->request->data['TSubmit']['check_user_id'] = 0;
			}
			// リクエストdataを保存用に複写
			$request_data = $this->request->data;
			// 生徒ユーザ指定の場合は、複数登録
			if (is_array($request_data['TSubmit']['user_id'])) {
				$tsubmit_data = array();
				foreach ($request_data['TSubmit']['user_id'] as $idx => $user_id) {
					if ($idx > 0) {
						$request_data['TSubmit']['id'] = null;
					}
					$request_data['TSubmit']['user_id'] = $user_id;
					$tsubmit_data[] = $request_data;
				}
			} else {
				$tsubmit_data = $this->request->data;
			}
			// シーケンスNOが更新されていた場合、メッセージを表示
			$add_index_number = $this->get_index_number(COLUMN_DELIVER_IDX);
			$add_index_number = PREFIX_DELIVER_IDS.$add_index_number;
			$index_check = true;
			if (empty($id) && $add_index_number !== $this->request->data['TSubmit']['submit_no']) {
				$index_check = false;
			}
			if ($index_check === true && $this->TSubmit->saveAll($tsubmit_data)) {
				$tsubmit_last_id = empty($id) ? $this->TSubmit->getLastInsertID(): $id;

				if ($this->request->data['TSubmit']['unsubscribe'] !== '1') {
//					$tsubmit_id = $this->request->data['TSubmit']['id'];
					$submit_no = $this->request->data['TSubmit']['submit_no'];
					$resource_id = $this->request->data['TSubmit']['resource_id'];
					$options = array('conditions' => array('MResource.' . $this->MResource->primaryKey => $resource_id), 'fields' => array('resource_name'));
					$mResource = $this->MResource->find('first', $options);

					if( empty($id) ) {
						// 新規登録処理の場合
						// ***（仮）スケジュールマスタに提出テーブルの内容を登録***
						$this->MSchedule->create();
						$tmpdata = array();
						$tmpdata['MSchedule']['id'] = NULL;
						$tmpdata['MSchedule']['schedule_ids'] = $submit_no; //'SCHEDULE_002'; 配信IDをスケジュールIDに設定
						$tmpdata['MSchedule']['title'] = $mResource['MResource']['resource_name'];
						$tmpdata['MSchedule']['detail'] = '';
						$tmpdata['MSchedule']['version'] = 1;
						$tmpdata['MSchedule']['start_datetime'] = $this->request->data['TSubmit']['start_datetime'];
						$tmpdata['MSchedule']['end_datetime'] = $this->request->data['TSubmit']['end_datetime'];
						$tmpdata['MSchedule']['delete_flg'] = 0;
						$resault = $this->MSchedule->save($tmpdata['MSchedule']); // 単一データ
						$mschedule_last_id = $this->MSchedule->getLastInsertID();

						// ***スケジュールテーブルに提出テーブルの対象者を登録***
						// 生徒ユーザ指定の場合は、複数登録
						if (is_array($this->request->data['TSubmit']['user_id'])) {
							$tsubmit_data = array();
							foreach ($this->request->data['TSubmit']['user_id'] as $idx => $user_id) {
								$this->TSchedule->create();
								$tmpdata['TSchedule'] = array();
								$tmpdata['TSchedule']['id'] = NULL;
								$tmpdata['TSchedule']['schedule_id'] = (int)($mschedule_last_id);
								$tmpdata['TSchedule']['group_id'] = $this->request->data['TSubmit']['group_id'];
								$tmpdata['TSchedule']['user_id'] = $user_id;
								$tmpdata['TSchedule']['submit_id'] = (int)($tsubmit_last_id);
								$tmpdata['TSchedule']['status'] = 1;
								$this->TSchedule->save($tmpdata['TSchedule']); // 単一データ
							}
						} else {
							$this->TSchedule->create();
							$tmpdata['TSchedule'] = array();
							$tmpdata['TSchedule']['id'] = NULL;
							$tmpdata['TSchedule']['schedule_id'] = (int)($mschedule_last_id);
							$tmpdata['TSchedule']['group_id'] = $this->request->data['TSubmit']['group_id'];
							$tmpdata['TSchedule']['user_id'] = $this->request->data['TSubmit']['user_id'];
							$tmpdata['TSchedule']['submit_id'] = (int)($tsubmit_last_id);
							$tmpdata['TSchedule']['status'] = 1;
							$this->TSchedule->save($tmpdata['TSchedule']); // 単一データ
						}
					} else {
						if ($this->request->data['TSubmit']['group_id'] == 0) {
							// 同じ提出IDの配信対象全てを変更を反映する（複数ユーザ指定のみ）
							$conditions = array(
									array(
											'TSubmit.submit_no' => $this->request->data['TSubmit']['submit_no'],
											'NOT' => array('TSubmit.id' => $id),
									)
							);
							$options = array('conditions' => $conditions);
							$tsubmit_data = $this->TSubmit->find('all', $options);
							if (!empty($tsubmit_data)) {
								foreach($tsubmit_data as $key => $data){
									$tsubmit_data[$key]['TSubmit']['start_datetime'] = $this->request->data['TSubmit']['start_datetime'];
									$tsubmit_data[$key]['TSubmit']['end_datetime'] = $this->request->data['TSubmit']['end_datetime'];
									$tsubmit_data[$key]['TSubmit']['check_user_id'] = $this->request->data['TSubmit']['check_user_id'];
								}
								$this->TSubmit->saveAll($tsubmit_data);
							}
						}
						
						if( $this->request->data['TSubmit']['user_id'] == 0) {
							// グループ登録の場合
							$conditions = array(
									array(
											'TSubmit.submit_no' => $this->request->data['TSubmit']['submit_no'],
									)
							);
													
							$options = array('conditions' => $conditions);
							$tsubmit_data = $this->TSubmit->find('all', $options);
							if (!empty($tsubmit_data)) {
								foreach($tsubmit_data as $key => $data){
									$tsubmit_data[$key]['TSubmit']['start_datetime'] = $this->request->data['TSubmit']['start_datetime'];
									$tsubmit_data[$key]['TSubmit']['end_datetime'] = $this->request->data['TSubmit']['end_datetime'];
									$tsubmit_data[$key]['TSubmit']['check_user_id'] = $this->request->data['TSubmit']['check_user_id'];
								}
								$this->TSubmit->saveAll($tsubmit_data);
							}
						}
						
						// スケジュール更新
						$conditions = array(
								'conditions' => array(
										'submit_id' => $id,
								)
						);
						$t_schedule = $this->TSchedule->find('first', $conditions);
						if (!empty($t_schedule)) {
							// 更新処理の場合
							$this->MSchedule->id = $t_schedule['TSchedule']['schedule_id'];
							$this->log($this->MSchedule->id, LOG_DEBUG);
							$tmpdata = array();
							$tmpdata['MSchedule']['title'] = $mResource['MResource']['resource_name'];
							$tmpdata['MSchedule']['detail'] = '';
							$tmpdata['MSchedule']['version'] = 1;
							$tmpdata['MSchedule']['start_datetime'] = $this->request->data['TSubmit']['start_datetime'];
							$tmpdata['MSchedule']['end_datetime'] = $this->request->data['TSubmit']['end_datetime'];
							$tmpdata['MSchedule']['delete_flg'] = 0;
							$resault = $this->MSchedule->save($tmpdata['MSchedule']); // 単一データ
							$mschedule_last_id = $this->MSchedule->getLastInsertID();

							// ***スケジュールテーブルに提出テーブルの対象者を登録***
							// 生徒ユーザ指定の場合は、複数登録
							if (is_array($this->request->data['TSubmit']['user_id'])) {
								$tsubmit_data = array();
								foreach ($this->request->data['TSubmit']['user_id'] as $idx => $user_id) {
									$this->TSchedule->id = $t_schedule['TSchedule']['id'];
									$tmpdata['TSchedule'] = array();
									$tmpdata['TSchedule']['group_id'] = $this->request->data['TSubmit']['group_id'];
									$tmpdata['TSchedule']['user_id'] = $user_id;
									$tmpdata['TSchedule']['status'] = 1;
									$this->TSchedule->save($tmpdata['TSchedule']); // 単一データ
								}
							} else {
								$this->TSchedule->id = $t_schedule['TSchedule']['id'];
								$tmpdata['TSchedule'] = array();
								$tmpdata['TSchedule']['group_id'] = $this->request->data['TSubmit']['group_id'];
								$tmpdata['TSchedule']['user_id'] = $this->request->data['TSubmit']['user_id'];
								$tmpdata['TSchedule']['status'] = 1;
								$this->TSchedule->save($tmpdata['TSchedule']); // 単一データ
								
								if( $this->request->data['TSubmit']['user_id'] == 0) {
									// グループ登録の場合
									$options = array(
											'conditions' => array(
													'schedule_id' => $t_schedule['TSchedule']['schedule_id'],
											)
									);
								
									$t_schedule_data = $this->TSchedule->find('all', $options); // 複数データ
								
									//$this->log($t_schedule_data, LOG_DEBUG);
								
									foreach( $t_schedule_data as $data ) {
										$data['TSchedule']['status'] = 1;
										//$this->log($data, LOG_DEBUG);
										$this->TSchedule->save($data['TSchedule']);
									}
								}								
							}
						}
					}
				} else {
					if ($this->request->data['TSubmit']['user_id'] == 0) {
						// 同じグループの配信対象全てを配信停止に設定する
						$conditions = array(
								array(
										'TSubmit.resource_id' => $this->request->data['TSubmit']['resource_id'],
										'TSubmit.group_id' => $this->request->data['TSubmit']['group_id'],
										'NOT' => array('TSubmit.submit_state' => 9),
								)
						);
						$options = array('conditions' => $conditions);
						$tsubmit_data = $this->TSubmit->find('all', $options);
						if (!empty($tsubmit_data)) {
							foreach($tsubmit_data as $key => $data){
								$tsubmit_data[$key]['TSubmit']['unsubscribe'] = 1;
							}
							$this->TSubmit->saveAll($tsubmit_data);
						}
					}
					// スケジュールの配信状態全てを未配信に戻す（紐づくマスタも削除）
					$options = array('conditions' => array('TSchedule.submit_id' => $tsubmit_last_id));
					$tschedule_data = $this->TSchedule->find('all', $options);
					if (!empty($tschedule_data)) {
						foreach($tschedule_data as $key => $data){
							$tschedule_data[$key]['TSchedule']['status'] = 1;
							$tschedule_data[$key]['MSchedule']['delete_flg'] = 1; // マスタも削除
						}
						$this->TSchedule->saveAll($tschedule_data);
						$this->MSchedule->saveAll($tschedule_data);
					}
				}

				// $this->Session->setFlash(__('The t submit has been saved.'));
				//return $this->redirect(array('action' => 'index'));
				if (isset($this->request->data['allotment'])) {
					return $this->redirect(array('action' => 'allotment', $id));
				} else {
					return $this->redirect(array('action' => 'index'));
				}
			} elseif ($index_check === false) {
				// $this->Session->setFlash(__('The t submit ID overlaps.'));
			} else {
				// $this->Session->setFlash(__('The t submit could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('TSubmit.' . $this->TSubmit->primaryKey => $id));
			$tsubmit_data = $this->request->data = $this->TSubmit->find('first', $options);
			if (!empty($tsubmit_data)) {
				if ($tsubmit_data['TSubmit']['user_id'] === '0') {
					$this->request->data['TSubmit']['subject_flg'] = 'group';
				} else {
					$this->request->data['TSubmit']['subject_flg'] = 'user';
				}
			}
			$tsubmit_user_id = isset($tsubmit_data['TSubmit']['user_id']) ? $tsubmit_data['TSubmit']['user_id']: 0;
			$this->set('valueMUser', $tsubmit_user_id);
			// 同じ配信NOで採点済みのレコードが存在する時は採点者を変更不可
			$conditions = array(
					array(
							'TSubmit.submit_no' => $tsubmit_data['TSubmit']['submit_no'],
							'NOT' => array('TSubmit.check_state' => 0),
					)
			);
			$options = array('conditions' => $conditions);
			$tsubmit_count = $this->TSubmit->find('count', $options);
			$disabled = false;
			if ($tsubmit_count > 0) {
				$disabled = true;
			}
			$this->set('disabledCheckUser', $disabled);
		}

		if (empty($id)) {
			// 配信ID(提出ID)初期値生成　※採番テーブルからIndexNoを取得する
			$new_index_number = $this->get_index_number(COLUMN_DELIVER_IDX);
			$this->request->data['TSubmit']['submit_no'] = PREFIX_DELIVER_IDS.$new_index_number;
		}

		//（管理者or教師－所属ユーザ絞込み）
		$conditions = $this->slctUserConditions();
		$conditions += array('user_type_id =' => 1);
		$conditions += array('delete_flg = 0');
		$setMUsers = $this->MUser->find('list', array('fields' => array('id','user_name'), 'conditions' => $conditions));
		$this->set(compact('setMUsers'));

		//（管理者or教師－所属グループ絞込み）
		$conditions = $this->slctGroupConditions();
		$conditions += array('delete_flg = 0', 'group_type_id = 1');
		$groupObjs = $this->MGroup->find('list', array('fields' => array('id','group_name'), 'conditions' => $conditions));
		$this->set(compact('groupObjs'));

		//（管理者or教師－採点ユーザ絞込み）
		$conditions = $this->slctUserConditions($this->MUser->alias);
		$conditions += array('user_type_id >=' => 2);
		$conditions += array('delete_flg = 0');
		$options = array('fields' => array('id','user_name','user_ids'), 'conditions' => $conditions);
		$mBaseMUsers = $this->MUser->find('all', $options);
		$muser_alias = $this->MUser->alias;
		$mUsers = array();
		foreach($mBaseMUsers as $key => $musers) {
			if (isset($musers[$muser_alias]['id'])){
				$id = $musers[$muser_alias]['id'];
				$user_name = $musers[$muser_alias]['user_name'] . '['. $musers[$muser_alias]['user_ids'] . ']';
				$mUsers[$id] = $user_name;
			}
		}
		$this->set(compact('mUsers'));

		//（管理者or教師－教材作成者絞込み）
		$conditions = $this->slctResourceUserConditions();
		$mResources = $this->MResource->find('all', array('conditions' => array('delete_flg <>' => 1), 'fields' => array('id','resource_ids','resource_name','resource_type'), 'conditions' => $conditions));
		$mresources_alias = $this->MResource->alias;
		$mBaseResources = array();
		foreach($mResources as $resource) {
			$key = $resource[$mresources_alias]['id'];
			$mBaseResources[$key] = '<a id="'.Router::url( '/').'mresources/view2/'.$key.'" class="resource_modal">'.
										$resource[$mresources_alias]['resource_name'].'</a>';
		}
		$this->set(compact('mBaseResources'));
		foreach($mResources as $key => $resource) {
			$mResources[$key] = array();
			$mResources[$key]['value'] = $resource[$mresources_alias]['id'];
			$mResources[$key]['name'] = $resource[$mresources_alias]['resource_name'] . '['. $resource[$mresources_alias]['resource_ids'] . ']';
			$mResources[$key]['class'] = $resource[$mresources_alias]['resource_type'];
		}
		$this->set(compact('mResources'));
	}

	/**
	 * edit method
	 *
	 * @throws NotFoundException
	 * @param string $id
	 * @return void
	 */
	public function edit($id = null) {
		if ($id === null) {
			$this->TSubmit->create();
		} elseif (!$this->TSubmit->exists($id)) {
			throw new NotFoundException(__('Invalid t submit'));
		}

		if ($this->request->is(array('post', 'put'))) {
			// 配信対象を個人・グループで分けて初期化
			if ($this->request->data['TSubmit']['subject_flg'] === 'user') {
				$this->request->data['TSubmit']['group_id'] = 0;
			} else {
				$this->request->data['TSubmit']['user_id'] = 0;
			}
			// 教科書の場合は、提出日時・採点者の設定は不要
			if (!isset($this->request->data['TSubmit']['end_datetime'])) {
				$this->request->data['TSubmit']['end_datetime'] = $this->request->data['TSubmit']['start_datetime'];
				$this->request->data['TSubmit']['check_user_id'] = 0;
			}
			// リクエストdataを保存用に複写
			$request_data = $this->request->data;
			// 生徒ユーザ指定の場合は、複数登録
			if (is_array($request_data['TSubmit']['user_id'])) {
				$tsubmit_data = array();
				foreach ($request_data['TSubmit']['user_id'] as $idx => $user_id) {
					if ($idx > 0) {
						$request_data['TSubmit']['id'] = null;
					}
					$request_data['TSubmit']['user_id'] = $user_id;
					$tsubmit_data[] = $request_data;
				}
				$this->TSubmit->set($tsubmit_data[0]); // 複数は１件目でバリデーション
			} else {
				$tsubmit_data = $this->request->data;
				$this->TSubmit->set($tsubmit_data); // バリデーション
			}
			// シーケンスNOが更新されていた場合、メッセージを表示
			$add_index_number = $this->get_index_number(COLUMN_DELIVER_IDX);
			$add_index_number = PREFIX_DELIVER_IDS.$add_index_number;
			$index_check = true;
			if (empty($id) && $add_index_number !== $this->request->data['TSubmit']['submit_no']) {
				$index_check = false;
			}
			if ($index_check === true && $this->TSubmit->validates()) {
				$this->TSubmit->saveAll($tsubmit_data);
				$tsubmit_last_id = empty($id) ? $this->TSubmit->getLastInsertID(): $id;
				$tsubmit_id_list = $this->TSubmit->id_list;
				if (!empty($id)) {
					$options = array('conditions' => array('TSchedule.submit_id' => $id));
					$tschedule_data = $this->TSchedule->find('all', $options);
					foreach($tschedule_data as $key => $data){
						$tschedule_data[$key]['MSchedule']['delete_flg'] = 1;
						unset($tschedule_data[$key]['TSchedule']);
					}
					$this->MSchedule->saveAll($tschedule_data);
				} else {
					// シーケンスNOを更新する　※採番テーブルからIndexNoを更新する
					$next_index_number = $this->set_index_number(COLUMN_DELIVER_IDX);
				}

				// 配信停止フラグがない場合も以降の処理を実行
				if (!isset($this->request->data['TSubmit']['unsubscribe']) || $this->request->data['TSubmit']['unsubscribe'] !== '1') {
					//					$tsubmit_id = $this->request->data['TSubmit']['id'];
					$submit_no = $this->request->data['TSubmit']['submit_no'];
					$resource_id = $this->request->data['TSubmit']['resource_id'];
					$options = array('conditions' => array('MResource.' . $this->MResource->primaryKey => $resource_id), 'fields' => array('resource_name'));
					$mResource = $this->MResource->find('first', $options);

					if( empty($id) ) {
						// 新規登録処理の場合
						// ***（仮）スケジュールマスタに提出テーブルの内容を登録***
						$this->MSchedule->create();
						$tmpdata = array();
						$tmpdata['MSchedule']['id'] = NULL;
						$tmpdata['MSchedule']['schedule_ids'] = $submit_no; //'SCHEDULE_002'; 配信IDをスケジュールIDに設定
						$tmpdata['MSchedule']['title'] = $mResource['MResource']['resource_name'];
						$tmpdata['MSchedule']['detail'] = '';
						$tmpdata['MSchedule']['version'] = 1;
						$tmpdata['MSchedule']['start_datetime'] = $this->request->data['TSubmit']['start_datetime'];
						$tmpdata['MSchedule']['end_datetime'] = $this->request->data['TSubmit']['end_datetime'];
						$tmpdata['MSchedule']['delete_flg'] = 0;
						$resault = $this->MSchedule->save($tmpdata['MSchedule']); // 単一データ
						$mschedule_last_id = $this->MSchedule->getLastInsertID();

						// ***スケジュールテーブルに提出テーブルの対象者を登録***
						// 生徒ユーザ指定の場合は、複数登録
						if (is_array($this->request->data['TSubmit']['user_id'])) {
							$tsubmit_data = array();
							foreach ($tsubmit_id_list as $tsubmit_id) {
								// 対象ユーザを取得
								$options = array('conditions' => array('TSubmit.' . $this->TSubmit->primaryKey => $tsubmit_id));
								$tsubmit_data = $this->TSubmit->find('first', $options);
								if (!empty($tsubmit_data)) {
									$this->TSchedule->create();
									$tmpdata['TSchedule'] = array();
									$tmpdata['TSchedule']['id'] = NULL;
									$tmpdata['TSchedule']['schedule_id'] = (int)($mschedule_last_id);
									$tmpdata['TSchedule']['group_id'] = $this->request->data['TSubmit']['group_id'];
									$tmpdata['TSchedule']['user_id'] = $tsubmit_data['TSubmit']['user_id'];
									$tmpdata['TSchedule']['submit_id'] = (int)($tsubmit_id);
									$tmpdata['TSchedule']['status'] = 1;
									$this->TSchedule->save($tmpdata['TSchedule']); // 単一データ
								}
							}
						} else {
							$this->TSchedule->create();
							$tmpdata['TSchedule'] = array();
							$tmpdata['TSchedule']['id'] = NULL;
							$tmpdata['TSchedule']['schedule_id'] = (int)($mschedule_last_id);
							$tmpdata['TSchedule']['group_id'] = $this->request->data['TSubmit']['group_id'];
							$tmpdata['TSchedule']['user_id'] = $this->request->data['TSubmit']['user_id'];
							$tmpdata['TSchedule']['submit_id'] = (int)($tsubmit_last_id);
							$tmpdata['TSchedule']['status'] = 1;
							$this->TSchedule->save($tmpdata['TSchedule']); // 単一データ
						}
					} else {
						$conditions = array(
								array(
										'submit_id' => $id,
								)
						);

						$t_schedule = $this->TSchedule->find('first', $conditions);
						// 更新処理の場合
						$this->MSchedule->id = $t_schedule['TSchedule']['schedule_id'];
						$this->log($this->MSchedule->id, LOG_DEBUG);
						$tmpdata = array();
						$tmpdata['MSchedule']['title'] = $mResource['MResource']['resource_name'];
						$tmpdata['MSchedule']['detail'] = '';
						$tmpdata['MSchedule']['version'] = 1;
						$tmpdata['MSchedule']['start_datetime'] = $this->request->data['TSubmit']['start_datetime'];
						$tmpdata['MSchedule']['end_datetime'] = $this->request->data['TSubmit']['end_datetime'];
						$tmpdata['MSchedule']['delete_flg'] = 0;
						$resault = $this->MSchedule->save($tmpdata['MSchedule']); // 単一データ
						$mschedule_last_id = $this->MSchedule->getLastInsertID();

						// ***スケジュールテーブルに提出テーブルの対象者を登録***
						// 生徒ユーザ指定の場合は、複数登録
						if (is_array($this->request->data['TSubmit']['user_id'])) {
							$tsubmit_data = array();
							foreach ($this->request->data['TSubmit']['user_id'] as $idx => $user_id) {
								$this->TSchedule->id = $t_schedule['TSchedule']['id'];
								$tmpdata['TSchedule'] = array();
								$tmpdata['TSchedule']['group_id'] = $this->request->data['TSubmit']['group_id'];
								$tmpdata['TSchedule']['user_id'] = $user_id;
								$tmpdata['TSchedule']['status'] = 1;
								$this->TSchedule->save($tmpdata['TSchedule']); // 単一データ
							}
						} else {
							$this->TSchedule->id = $t_schedule['TSchedule']['id'];
							$tmpdata['TSchedule'] = array();
							$tmpdata['TSchedule']['group_id'] = $this->request->data['TSubmit']['group_id'];
							$tmpdata['TSchedule']['user_id'] = $this->request->data['TSubmit']['user_id'];
							$tmpdata['TSchedule']['status'] = 1;
							$this->TSchedule->save($tmpdata['TSchedule']); // 単一データ
						}
					}
				} else {
					if ($this->request->data['TSubmit']['user_id'] === 0) {
						// 同じグループの配信対象全てを配信停止に設定する
						$conditions = array(
								array(
										'TSubmit.resource_id' => $this->request->data['TSubmit']['resource_id'],
										'TSubmit.group_id' => $this->request->data['TSubmit']['group_id'],
										'NOT' => array('TSubmit.submit_state' => 9),
								)
						);
						$options = array('conditions' => $conditions);
						$tsubmit_data = $this->TSubmit->find('all', $options);
						if (empty($tsubmit_data)) {
							foreach($tsubmit_data as $key => $data){
								$tsubmit_data[$key]['TSubmit']['unsubscribe'] = 1;
							}
							$this->TSubmit->saveAll($tsubmit_data);
						}
					}
				}

				// $this->Session->setFlash(__('The t submit has been saved.'));
				//return $this->redirect(array('action' => 'index'));
				if (isset($this->request->data['allotment'])) {
					return $this->redirect(array('action' => 'allotment', $id));
				} else {
					return $this->redirect(array('action' => 'index'));
				}
			} elseif ($index_check === false) {
				// $this->Session->setFlash(__('The t submit ID overlaps.'));
			} else {
				// $this->Session->setFlash(__('The t submit could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('TSubmit.' . $this->TSubmit->primaryKey => $id));
			$tsubmit_data = $this->request->data = $this->TSubmit->find('first', $options);
			if (!empty($tsubmit_data)) {
				if ($tsubmit_data['TSubmit']['user_id'] === '0') {
					$this->request->data['TSubmit']['subject_flg'] = 'group';
				} else {
					$this->request->data['TSubmit']['subject_flg'] = 'user';
				}
			}
			$tsubmit_user_id = isset($tsubmit_data['TSubmit']['user_id']) ? $tsubmit_data['TSubmit']['user_id']: 0;
			$this->set('valueMUser', $tsubmit_user_id);
		}

		if (empty($id)) {
			// 配信ID(提出ID)初期値生成　※採番テーブルからIndexNoを取得する
			$new_index_number = $this->get_index_number(COLUMN_DELIVER_IDX);
			$this->request->data['TSubmit']['submit_no'] = PREFIX_DELIVER_IDS.$new_index_number;
		}

		//（管理者or教師－所属ユーザ絞込み）
		$conditions = $this->slctUserConditions();
		$conditions += array('user_type_id =' => 1);
		$conditions += array('delete_flg = 0');
		$setMUsers = $this->MUser->find('list', array('fields' => array('id','user_name'), 'conditions' => $conditions));
		$this->set(compact('setMUsers'));

		//（管理者or教師－所属グループ絞込み）
		$conditions = $this->slctGroupConditions();
		$conditions += array('delete_flg = 0', 'group_type_id = 1');
		$groupObjs = $this->MGroup->find('list', array('fields' => array('id','group_name'), 'conditions' => $conditions));
		$this->set(compact('groupObjs'));

		//（管理者or教師－採点ユーザ絞込み）
		$conditions = $this->slctUserConditions($this->MUser->alias);
		$conditions += array('user_type_id >=' => 2);
		$conditions += array('delete_flg = 0');
		$options = array('fields' => array('id','user_name','user_ids'), 'conditions' => $conditions);
		$mBaseMUsers = $this->MUser->find('all', $options);
		$muser_alias = $this->MUser->alias;
		$mUsers = array();
		foreach($mBaseMUsers as $key => $musers) {
			if (isset($musers[$muser_alias]['id'])){
				$id = $musers[$muser_alias]['id'];
				$user_name = $musers[$muser_alias]['user_name'] . '['. $musers[$muser_alias]['user_ids'] . ']';
				$mUsers[$id] = $user_name;
			}
		}
		$this->set(compact('mUsers'));

		//（管理者or教師－教材作成者絞込み）
		$conditions = $this->slctResourceUserConditions();
		$mResources = $this->MResource->find('all', array('conditions' => array('delete_flg <>' => 1), 'fields' => array('id','resource_ids','resource_name','resource_type'), 'conditions' => $conditions));
		$mresources_alias = $this->MResource->alias;
		$mBaseResources = array();
		foreach($mResources as $resource) {
			$key = $resource[$mresources_alias]['id'];
			$mBaseResources[$key] = '<a id="'.Router::url( '/').'mresources/view2/'.$key.'" class="resource_modal">'.
					$resource[$mresources_alias]['resource_name'].'</a>';
		}
		$this->set(compact('mBaseResources'));
		foreach($mResources as $key => $resource) {
			$mResources[$key] = array();
			$mResources[$key]['value'] = $resource[$mresources_alias]['id'];
			$mResources[$key]['name'] = $resource[$mresources_alias]['resource_name'] . '['. $resource[$mresources_alias]['resource_ids'] . ']';
			$mResources[$key]['class'] = $resource[$mresources_alias]['resource_type'];
		}
		$this->set(compact('mResources'));
	}


/**
 * setTSubmitConditions method
 *
 * @return void
*/
	private function setTSubmitConditions() {
		// 検索条件取得（管理者・教師のみ）
		$conditions = array();
		if ($this->Auth->User('user_type_id') <= '3') {
			// 自身の上位グループIDを取得
			$options = array('conditions' => array('tSection.user_id' => $this->Auth->User('id')), 'fields' => array('parent_id'));
			$tsectionAncestor = $this->MGroup->tSection->find('first', $options);
			$parent_id = isset($tsectionAncestor['tSection']['parent_id']) ? $tsectionAncestor['tSection']['parent_id']: 0;
			$ary_tsections = $this->MGroup->tSection->children($parent_id);

			// 自身の所属配下のユーザ・グループ ※自身のグループは含む
			$valueUsers = array();
			$valueGroups = array();
			foreach ($ary_tsections as $tsection) {
				if ($tsection['tSection']['user_id'] !== '0') {
					$valueUsers[] = $tsection['tSection']['user_id'];
				} else {
					$valueGroups[] = $tsection['tSection']['group_id'];
				}
			}
			$conditions += array(
					'OR' => array(
							array(
									'TSubmit.user_id' => $valueUsers,
									'TSubmit.group_id' => 0
							),
							array(
									'TSubmit.user_id' => 0,
									'TSubmit.group_id' => $valueGroups
							)
					)
			);
		} else {
			//（システム管理者は絞りなし）
			$conditions += array(
					'OR' => array(
							array(
									'TSubmit.user_id' => 0
							),
							array(
									'TSubmit.group_id' => 0,
							)
					)
			);
		}
		return $conditions;
	}
}
