<?php
App::uses('AppController', 'Controller');
/**
 * MResources Controller
 *
 * @property MResource $MResource
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class MResourcesController extends AppController {
	public $name = 'mResources';
	public $uses = array('MResource', 'MResourceSubject', 'MResourceStyle', 'MGroup', 'MIndexNumber', 'S3Service', 'TSubmit');

/**
 * Components
 *
 * @var array
 */
	public $components = array('PagingSupport', 'ConfigXml', 'Search.Prg', 'Paginator', 'Session');
	public $presetVars = array();

	public function __construct($request, $response) {
		parent::__construct($request, $response);
		// 教材管理画面用の定数ファイルを読み込み
		config('const/constRecture');
	}

/**
 * beforeFilter method
 *
 * @return void
 */
	public function beforeFilter() {
		// 親クラスのbeforeFilter()を呼び出す
		parent::beforeFilter();
		// 検索対象のフィールド設定代入
		$this->presetVars = $this->MResource->presetVars;

		// ページャ設定
		$pager_numbers = array(
				'before' => ' - ',
				'after'=>' - ',
				'modulus'=> 10,
				'separator'=> ' ',
				'class'=>'pagenumbers'
		);
		$this->set('pager_numbers', $pager_numbers);

		App::uses('Folder', 'Utility');
		App::uses('File', 'Utility');
	}

/**
 * isAuthorized method
 *
 * @return void
 */
	public function isAuthorized() {
		if ($this->Auth->User('user_type_id') == '3' || $this->Auth->User('user_type_id') == '9') {
			//admin権限を持つユーザは全てのページにアクセスできる
			return true;
		} elseif ($this->Auth->User('user_type_id') == '2') {
			//教師権限を持つユーザも全てのページにアクセスできる
			return true;
		}
		return false;
	}

/**
 * index method
 *
 * @return void
 */
	public function index() {
		// 検索条件設定
		$this->Prg->commonProcess();
		$this->PagingSupport->inheritPostData(); //$this->dataの値の引き継ぎが行われる

		// 検索条件取得
		$conditions = $this->MResource->parseCriteria($this->passedArgs);
		//（デフォルトの検索条件）
		$conditions += array('MResource.delete_flg ' => 0);
		//（管理者or教師－教材作成者絞込み）
		$conditions += $this->slctResourceUserConditions();

		$this->MResource->virtualFields = array(
				'delivered' => '0'
		);

		$this->paginate = array(
				'conditions' => $conditions,
				'limit' => 20,
				'order' => array('id' => 'desc')
		);

		//$this->log($this->Paginator->paginate(), LOG_DEBUG);

		$page = $this->Paginator->paginate();

		foreach( $page as &$item ) {
			$id = $item['MResource']['id'];

			//$this->log($id, LOG_DEBUG);

			$result = $this->TSubmit->find('first', array('conditions' => array('resource_id' => $id, 'submit_state > 0', 'unsubscribe = 0')));

			if( count($result) !== 0 ) {
				$item['MResource']['delivered'] = 1;
			}
		}

		$this->MResource->recursive = 0;
		$this->set('mResources', $page);

		$resourceSubjects = $this->MResource->ResourceSubject->find('list', array('fields' => array('id','subject_name')));
		$this->set(compact('resourceSubjects'));

		$resourceStyles = array('' => $GLOBALS['message']->getMessage(MESSAGE_COMMON_ITEM_090));	//指定しない
		$resourceStyles += Configure::read('m_resource_styles');
		$this->set(compact('resourceStyles'));

		$resourceTypes = array('' => $GLOBALS['message']->getMessage(MESSAGE_COMMON_ITEM_090));		//指定しない
		$resourceTypes += Configure::read('resource_type');
		$this->set(compact('resourceTypes'));
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->MResource->exists($id)) {
			throw new NotFoundException(__('Invalid m resource'));
		}
		$options = array('conditions' => array('MResource.' . $this->MResource->primaryKey => $id));
		$mResource = $this->MResource->find('first', $options);
		// 教材ファイル登録データをセット
		foreach($mResource['MResourceFile'] as $key => $mresourceFile){
			if ($key == 0) {
				$mResource['MResource']['pdf_file_1'] = $mresourceFile['resource_file_name'];
			} elseif ($key == 1) {
				$mResource['MResource']['pdf_file_2'] = $mresourceFile['resource_file_name'];
			} else {
				if (empty($mResource['MResource']['mp3_file'])) {
					$mResource['MResource']['mp3_file'] = $mresourceFile['resource_file_name'];
				} else {
					$mResource['MResource']['mp3_file'] = $mResource['MResource']['mp3_file'].','.$mresourceFile['resource_file_name'];
				}
			}
		}
		$this->set('mResource', $mResource);
	}

	/**
	 * view2 method
	 *
	 * @throws NotFoundException
	 * @param string $id
	 * @return void
	 */
	public function view2($id = null) {
		if (!$this->MResource->exists($id)) {
			throw new NotFoundException(__('Invalid m resource'));
		}
		$options = array('conditions' => array('MResource.' . $this->MResource->primaryKey => $id));
		$mResource = $this->MResource->find('first', $options);
		// 教材ファイル登録データをセット
		foreach($mResource['MResourceFile'] as $key => $mresourceFile){
			if ($key == 0) {
				$mResource['MResource']['pdf_file_1'] = $mresourceFile['resource_file_name'];
			} elseif ($key == 1) {
				$mResource['MResource']['pdf_file_2'] = $mresourceFile['resource_file_name'];
			} else {
				if (empty($mResource['MResource']['mp3_file'])) {
					$mResource['MResource']['mp3_file'] = $mresourceFile['resource_file_name'];
				} else {
					$mResource['MResource']['mp3_file'] = $mResource['MResource']['mp3_file'].','.$mresourceFile['resource_file_name'];
				}
			}
		}
		$this->set('mResource', $mResource);
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		// (pdf×2,mp3×10 各ファイル最大50MB)ファイルサイズ(60MB)エラー
		if ($this->request->is('put')) {
			if ($_SERVER['CONTENT_LENGTH'] > (60*1024*1024)) {
				// $this->Session->setFlash(__('Oh.. too match big'));
				$this->Session->write('tmp_data', $this->request->data);
				$this->redirect($this->referer());
			}
		} elseif ($this->Session->read('tmp_data')) {
			$this->request->data = $this->Session->read('tmp_data');
			$this->Session->delete('tmp_data');
		}
		if ($this->request->is('post')) {
			// シーケンスNOが更新されていた場合、メッセージを表示
			$add_index_number = $this->get_index_number(COLUMN_RESOURCE_IDX);
			$add_index_number = PREFIX_RESOURCE_IDS.$add_index_number;
			if ($add_index_number !== $this->request->data['MResource']['resource_ids']) {
				// $this->Session->setFlash(__('The m resource ID overlaps.'));
			} else {
//$this->log($this->request->data, LOG_DEBUG);
				// 教材スタイルに合わせて、教材ファイルを削除
				if ($this->request->data['MResource']['style_id'] == '1') {
					$this->request->data['MResourceFile'][1]['resource_file_name']['name'] = '';
					$this->request->data['mp3'] = array();
				} else if ($this->request->data['MResource']['style_id'] == '2') {
					$this->request->data['mp3'] = array();
				} else if ($this->request->data['MResource']['style_id'] == '3') {
					$this->request->data['MResourceFile'][1]['resource_file_name']['name'] = '';
				}
				// 元のリクエストdataは保存
				$request_data = $this->request->data;
				// Version・作成者に初期値を設定
				$this->request->data['MResource']['version'] = 1;
				$this->request->data['MResource']['create_user_id'] = $this->Auth->User('id');
				$this->MResource->create();
				if ($mresource = $this->MResource->save($this->request->data)) { // 単一データ保存
					if (!empty($mresource)) {
						$resource_id = $this->MResource->id;
						// 教材ファイル登録データをセット
						foreach($this->request->data['MResourceFile'] as $key => $resourceFile){
							// アップロードファイルのファイル名の取得
							$file_name = $resourceFile['resource_file_name']['name'];
							$this->request->data['MResourceFile'][$key]['resource_file_type'] = '1';
							$this->request->data['MResourceFile'][$key]['resource_id'] = $resource_id;
							$this->request->data['MResourceFile'][$key]['resource_file_name'] = $file_name;
						}
						$i = 2;
						foreach($this->request->data['mp3'] as $appentFile){
							// アップロードファイル(MP3)のファイル名の取得
							$file_name = $appentFile['resource_file_name']['name'];
							$this->request->data['MResourceFile'][$i]['resource_file_type'] = '2';
							$this->request->data['MResourceFile'][$i]['resource_id'] = $resource_id;
							$this->request->data['MResourceFile'][$i]['resource_file_name'] = $file_name;
							$i++;
						}
						$upload_exc = false;
						foreach($this->request->data['MResourceFile'] as $key => $resourceFile){
							// ファイルのUPなければ、処理しない
							if (!empty($this->request->data['MResourceFile'][$key]['resource_file_name'])) {
								$upload_exc = true;
								break;
							}
						}

						if ($upload_exc) {
							// 教材ファイルをS3にアップロード
							$this->upload_resource_s3($request_data);

							// MResourceFileもあわせて保存
							$this->MResource->MResourceFile->saveAll($this->request->data['MResourceFile']); // 複数データ保存
						}

						// MResourceSubtitleもあわせて保存
						if ($this->request->data['MResource']['resource_type'] != 0){
							// 教材タイプ:テストのみ
							$last_id = $this->MResource->getLastInsertID();
							foreach($this->request->data['MResourceSubtitle'] as $key => $data) {
								$this->request->data['MResourceSubtitle'][$key]['resource_id'] = $last_id;
							}
							$this->MResource->MResourceSubtitle->saveAll($this->request->data['MResourceSubtitle']); // 複数データ保存
						}
					}

					// シーケンスNOを更新する　※採番テーブルからIndexNoを更新する
					$next_index_number = $this->set_index_number(COLUMN_RESOURCE_IDX);

					return $this->redirect(array('action' => 'index'));
				} else {
					$this->set('validation_errors', $this->MResource->MResourceFile->validationErrors);
					// $this->Session->setFlash(__('The m resource could not be saved. Please, try again.'));
				}
			}
		}

		// 教材ID初期値生成　※採番テーブルからIndexNoを取得する
		$new_index_number = $this->get_index_number(COLUMN_RESOURCE_IDX);
		$this->request->data['MResource']['resource_ids'] = PREFIX_RESOURCE_IDS.$new_index_number;

		// 配点設定のデフォルト設定
		$mResourceSubtitles[0]['MResourceSubtitle']['subtitle_name'] = $GLOBALS['message']->getMessage(MESSAGE_RESOURCE_ITEM_017).'1';
		$mResourceSubtitles[0]['MResourceSubtitle']['question_num'] = 1;
		$mResourceSubtitles[0]['MResourceSubtitle']['allot'] = 100;
		$this->set('mResourceSubtitles', $mResourceSubtitles);

		$resourceSubjects = $this->MResource->ResourceSubject->find('list', array('fields' => array('id','subject_name')));
		$this->set(compact('resourceSubjects'));
		$resourceStyles = Configure::read('m_resource_styles');
		$this->set(compact('resourceStyles'));
		$resourceTypes = Configure::read('resource_type');
		$this->set(compact('resourceTypes'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->MResource->exists($id)) {
			throw new NotFoundException(__('Invalid m resource'));
		}
		// (pdf×2,mp3×10 各ファイル最大50MB)ファイルサイズ(60MB)エラー
		if ($this->request->is('put')) {
			if ($_SERVER['CONTENT_LENGTH'] > (60*1024*1024)) {
				// $this->Session->setFlash(__('Oh.. too match big'));
				$this->Session->write('tmp_data', $this->request->data);
				$this->redirect($this->referer());
			}
		} elseif ($this->Session->read('tmp_data')) {
			$this->request->data = $this->Session->read('tmp_data');
			$this->Session->delete('tmp_data');
		}
		if ($this->request->is(array('post', 'put'))) {
//$this->log($this->request->data, LOG_DEBUG);
			// 元のリクエストdataは保存
			$request_data = $this->request->data;
			// 教材ファイル登録データをセット
			foreach($this->request->data['MResourceFile'] as $key => $resourceFile){
				// アップロードファイルのファイル名の取得
				if (!empty($resourceFile['resource_file_name']['name'])) {
					$file_name = $resourceFile['resource_file_name']['name'];
				} else {
					$file_name = $resourceFile['resource_file_name_old'];
				}

				$this->request->data['MResourceFile'][$key]['resource_file_name'] = $file_name;
			}
			$i = 2;
			foreach($this->request->data['mp3'] as $appentFile){
				// アップロードファイル(MP3)のファイル名の取得
				if (!empty($appentFile['resource_file_name']['name'])) {
					$file_name = $appentFile['resource_file_name']['name'];
				} else {
					$file_name = $appentFile['resource_file_name_old'];
				}
				$this->request->data['MResourceFile'][$i]['resource_file_type'] = '2';
				$this->request->data['MResourceFile'][$i]['resource_file_name'] = $file_name;
				$i++;
			}
			$upload_exc = false;
			foreach($this->request->data['MResourceFile'] as $key => $resourceFile){
				// ファイルのUPなければ、処理しない
				if (!empty($this->request->data['MResourceFile'][$key]['resource_file_name'])) {
					$upload_exc = true;
					break;
				}
			}

			if (!$upload_exc) {
				// ファイルのUPなければ、登録データ削除
				unset($this->request->data['MResourceFile']);
			}
			// MResourceSubtitle登録データ削除（保存は不要）
			$this->MResource->MResourceSubtitle->deleteAll(array('MResourceSubtitle.resource_id' => $id), false);
			if ($this->request->data['MResource']['resource_type'] == 0){
				// 教材タイプ:教科書のみ
				unset($this->request->data['MResourceSubtitle']);
			}

			// Versionを更新
			$this->request->data['MResource']['version'] = $this->request->data['MResource']['version'] + 1;
			$this->MResource->set($this->request->data);
			if ($this->MResource->validates()) {
				if ($upload_exc) {
					// 教材ファイルをS3にアップロード
					$this->upload_resource_s3($request_data);

					// 教材ファイル登録データ削除
					$this->MResource->MResourceFile->deleteAll(array('MResourceFile.resource_id' => $id), false);
				}

				$this->MResource->saveAll($this->request->data); // 複数データ保存
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->set('validation_errors', $this->MResource->MResourceFile->validationErrors);
				// $this->Session->setFlash(__('The m resource could not be saved. Please, try again.'));
			}

		} else {
			$options = array('conditions' => array('MResource.' . $this->MResource->primaryKey => $id));
			$this->request->data = $this->MResource->find('first', $options);

			// 配点設定の登録値設定
			$options = array('conditions' => array('MResourceSubtitle.resource_id' => $id), 'fields' => array('id','subtitle_name','question_num','allot','result'));
			$mResourceSubtitles = $this->MResource->MResourceSubtitle->find('all', $options);
			//（教科書の場合、デフォルト値表示）
			if (empty($mResourceSubtitles)){
				// 配点設定のデフォルト設定
				$mResourceSubtitles[0]['MResourceSubtitle']['id'] = null;
				$mResourceSubtitles[0]['MResourceSubtitle']['subtitle_name'] = $GLOBALS['message']->getMessage(MESSAGE_RESOURCE_ITEM_017).'1';
				$mResourceSubtitles[0]['MResourceSubtitle']['question_num'] = 1;
				$mResourceSubtitles[0]['MResourceSubtitle']['allot'] = 100;
			}
			$this->set('mResourceSubtitles', $mResourceSubtitles);
		}

		$resourceSubjects = $this->MResource->ResourceSubject->find('list', array('fields' => array('id','subject_name')));
		$this->set(compact('resourceSubjects'));
		$resourceStyles = Configure::read('m_resource_styles');
		$this->set(compact('resourceStyles'));
		$resourceTypes = Configure::read('resource_type');
		$this->set(compact('resourceTypes'));
	}

/**
 * ajaxSubtitleValidate method
 *
 * @return void
 */
	public function ajaxSubtitleValidate() {
		if ($this->request->is('ajax')) {
			$key = $this->request->data['key'];		//カラム
			$key = split("@_",$key);
			$check_key = $key[0];
			$data = array('MResourceSubtitle' => array(			//チェックデータ
					$check_key => $this->request->data['value']
			));
			$check['check'] = true;					// リターン用配列
			$this->MResource->MResourceSubtitle->set($data);				// バリデーション準備
			if (!$this->MResource->MResourceSubtitle->validates(array('fieldList' => array('subtitle_name', 'question_num', 'allot')))) {		// バリデート(エラー時)
				$check['check'] = false;			// check 判定値変更
				$check['message'] = "<div class=\"error-message\">" .
						$this->MResource->MResourceSubtitle->validationErrors[$check_key][0]. "</div>";// エラーメッセージ設定
			}
			$this->render(false, 'ajax');			// Renderなし、Layout = ajax
			echo json_encode($check);				// デコードし出力
			exit;
		}
		throw new MethodNotAllowedException('不正なアクセスです。');
	}

	/**
	 * ajaxSubtitleValidate method
	 *
	 * @return void
	 */
	public function ajaxAllSubtitleValidate() {
		if ($this->request->is('ajax')) {
			$check['check'] = true;
			foreach($this->request->data['MResourceSubtitle'] as $key => $data) {
				$request_data[$key]['MResourceSubtitle'] = $data;
			}
			if (!$this->MResource->MResourceSubtitle->saveAll($request_data, array('validate' => 'only'))) {
				// バリデーションNGの場合の処理
				$check['check'] = false;			// check 判定値変更
			}
			$this->render(false, 'ajax');			// Renderなし、Layout = ajax
			echo json_encode($check);				// デコードし出力
			exit;
		}
		throw new MethodNotAllowedException('不正なアクセスです。');
	}

/**
 * allotment method(Unused)
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function allotment($id = null) {
		if (!$this->MResource->exists($id)) {
			throw new NotFoundException(__('Invalid m resource'));
		}
		if ($this->request->is(array('post', 'put'))) {
			foreach($this->request->data['MResourceSubtitle'] as $key=>$data) {
				$this->request->data['MResourceSubtitle'][$key]['resource_id'] = $id;
			}
			$this->MResource->MResourceSubtitle->deleteAll(array('MResourceSubtitle.resource_id' => $id), false);

			if ($this->MResource->MResourceSubtitle->saveAll($this->request->data['MResourceSubtitle'])) { // 複数データ保存
				// $this->Session->setFlash(__('The m resource subtitle has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				// $this->Session->setFlash(__('The m resource subtitle could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('MResourceSubtitle.resource_id' => $id), 'fields' => array('id','subtitle_name','question_num','allot','result'));
			$this->set('mResourceSubtitles', $this->MResource->MResourceSubtitle->find('all', $options));
		}

		$options = array('conditions' => array('MResource.' . $this->MResource->primaryKey => $id));
		$this->set('mResource', $this->MResource->find('first', $options));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		if (!$this->MResource->exists($id)) {
			throw new NotFoundException(__('Invalid m resource'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->MResource->save($this->request->data)) {
				// 同じ教材の配信対象全てを配信停止に設定する
				$conditions = array(
						array(
								'TSubmit.resource_id' => $id,
								'NOT' => array('TSubmit.submit_state' => 9),
						)
				);
				$options = array('conditions' => $conditions);
				$tsubmit_data = $this->TSubmit->find('all', $options);
				if (!empty($tsubmit_data)) {
					foreach($tsubmit_data as $key => $data){
						$tsubmit_data[$key]['TSubmit']['unsubscribe'] = 1;
					}
					$this->TSubmit->saveAll($tsubmit_data);
				}

				return $this->redirect(array('action' => 'index'));
			} else {
				// $this->Session->setFlash(__('The m resource could not be deleted. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('MResource.' . $this->MResource->primaryKey => $id));
			$mResource = $this->MResource->find('first', $options);
			// 教材ファイル登録データをセット
			foreach($mResource['MResourceFile'] as $key => $mresourceFile){
				if ($key == 0) {
					$mResource['MResource']['pdf_file_1'] = $mresourceFile['resource_file_name'];
				} elseif ($key == 1) {
					$mResource['MResource']['pdf_file_2'] = $mresourceFile['resource_file_name'];
				} else {
					if (empty($mResource['MResource']['mp3_file'])) {
						$mResource['MResource']['mp3_file'] = $mresourceFile['resource_file_name'];
					} else {
						$mResource['MResource']['mp3_file'] = $mResource['MResource']['mp3_file'].','.$mresourceFile['resource_file_name'];
					}
				}
			}
			$this->set('mResource', $mResource);
		}
	}

/**
 * upload_resource_s3 method
 *
 * @return void
 */
	private function upload_resource_s3($request_data) {
		$resource_ids = $request_data['MResource']['resource_ids'];
		$resource_name = $request_data['MResource']['resource_name'];
		if (!empty($request_data['MResource']['id'])) {
			// 教材IDのzipファイル
			$file_name = $resource_ids . '.zip';
			$base_dir = WWW_ROOT . 'files' . DS;
			$base_file = $base_dir . $file_name;

			// zipファイルをS3からダウンロード
			require_once(APP . "Vendor" . DS . "aws.phar");

			$file_dir = S3_DIR_RESOURCE_FILE . "/0/" . $resource_ids . '.zip';
			// S3からgetする
			$result = $this->S3Service->getFile($file_dir);
			if (!$result) {
				throw new BadRequestException(MESSAGE_API_ERROR_006);
			}
			$result = file_put_contents($base_file, $result);
			if (!$result) {
				throw new BadRequestException(MESSAGE_API_ERROR_007);
			}

			// zip形式を解凍
			if (Configure::read('database') === 'development' && DIRECTORY_SEPARATOR == '\\') {
				exec('"C:\Program Files\7-Zip\7z.exe"' . " x -y -o$base_dir $base_file");
				//$this->log('"C:\Program Files\7-Zip\7z.exe"' . " a -ptest ${base_file} ${base_dir}", LOG_DEBUG);
			} else {
				chdir(WWW_ROOT . 'files');
				exec("unzip $base_file");
			}
			// (更新の場合)oldファイルを削除する
			foreach($request_data['MResourceFile'] as $key => $resourceFile){
				// PDFのアップのみ、古いファイルを削除
				if (!empty($resourceFile['resource_file_name']['tmp_name'])) {
					$old_name = $resourceFile['resource_file_name_old'];
					$old_name = basename($old_name, '.pdf') .'_pdf'. ($key+1) .'.pdf';
					$file_dir = WWW_ROOT . 'files' . DS . $resource_ids . DS . 'pdf' . DS;

					$file = new File($file_dir.$old_name);
					$file->delete();
				}
			}
			foreach($request_data['mp3'] as $appentFile){
				// MP3のアップのみ、古いファイルを削除
				if (!empty($appentFile['resource_file_name']['tmp_name'])) {
					$old_name = $appentFile['resource_file_name_old'];
					$file_dir = WWW_ROOT . 'files' . DS . $resource_ids . DS . 'mp3' . DS;

					$file = new File($file_dir.$old_name);
					$file->delete();
				}
			}
		} else {
			// 教材IDのフォルダを作成
			$dir = new Folder();
			if (!$dir->create(WWW_ROOT . 'files' . DS . $resource_ids.DS)) {
				// $this->Session->setFlash("フォルダの作成に失敗しました");
			}
			if (!$dir->create(WWW_ROOT . 'files' . DS . $resource_ids . DS . 'pdf'.DS)) {
				// $this->Session->setFlash("フォルダの作成に失敗しました");
			}
			if (!$dir->create(WWW_ROOT . 'files' . DS . $resource_ids . DS . 'mp3'.DS)) {
				// $this->Session->setFlash("フォルダの作成に失敗しました");
			}
		}

		// 教材IDのフォルダにアップロードファイルを展開
		if (is_array($request_data['MResourceFile'])) {
			// PDFのアップロードファイルを展開
			foreach($request_data['MResourceFile'] as $key => $resourceFile){

				$this->log('fileName', LOG_DEBUG);
				// アップロードファイルの一時ファイル名・ファイル名の取得
				$tmp_name = $resourceFile['resource_file_name']['tmp_name'];
				// pdfファイル名にインデックス識別を追記する
				$file_name = basename($resourceFile['resource_file_name']['name'], '.pdf') .'_pdf'. ($key+1) .'.pdf';
				$this->log('tmpName'. $tmp_name, LOG_DEBUG);
				$this->log('fileName'. $file_name, LOG_DEBUG);
				// 作業DIRにアップロードファイルを移動
				move_uploaded_file($tmp_name, WWW_ROOT . 'files' . DS . $resource_ids . DS . 'pdf' . DS . $file_name);
			}
		}
		if (is_array($request_data['mp3'])) {
			// MP3のアップロードファイルも同じく展開
			foreach($request_data['mp3'] as $appentFile){
				// アップロードファイル(MP3)の一時ファイル名・ファイル名の取得
				$tmp_name = $appentFile['resource_file_name']['tmp_name'];
				$file_name = $appentFile['resource_file_name']['name'];
				// 作業DIRにアップロードファイルを移動
				move_uploaded_file($tmp_name, WWW_ROOT  . 'files' . DS . $resource_ids . DS . 'mp3' . DS . $file_name);
			}
		}

		// xmlファイルの生成
		$config_dir = WWW_ROOT  . 'files' . DS . $resource_ids;
		$config_file = $config_dir . DS . CONFIG_XML;
		if (!empty($request_data['MResource']['id'])) {
			// 更新時は古いxmlを削除 ※xmlを上書きしていたため
			$file = new File($config_file);
			$file->delete();
		}
		$xml_data = $this->ConfigXml->getDirXML($config_dir, $resource_ids, $resource_name, $request_data['MResourceFile']);
		if ($xml_data) {
			$file = new File($config_file, true, 0644);
			$file->open('wb');
			$file->write($xml_data);
			$file->close();
		}

		// zip形式で圧縮
		$base_dir = WWW_ROOT . 'files' . DS . $resource_ids;
		$base_zip = $base_dir . '.zip';
		if (!empty($request_data['MResource']['id'])) {
			// 更新時は古いzipを削除
			$file = new File($base_zip);
			$file->delete();
		}
		if (Configure::read('database') === 'development' && DIRECTORY_SEPARATOR == '\\') {
			exec('"C:\Program Files\7-Zip\7z.exe"' . " a $base_zip $base_dir");
			//$this->log('"C:\Program Files\7-Zip\7z.exe"' . " a -ptest ${base_zip} ${base_dir}", LOG_DEBUG);
		} else {
			chdir(WWW_ROOT . 'files');
			exec("zip -r $base_zip $resource_ids");
		}

		// zipファイルをS3にアップロード
		require_once(APP . "Vendor" . DS . "aws.phar");
		try {
			$file_dir = S3_DIR_RESOURCE_FILE . "/0/" . $resource_ids . '.zip';

			// S3にdeleteする（存在しなくても続行）
			$result = $this->S3Service->deleteFile($file_dir);
			// S3にputする
			$result = $this->S3Service->putFile($base_zip, $file_dir);
			if (!$result) {
				// $this->Session->setFlash("ファイルの保存に失敗しました");
			}
		} catch (Exception $e) {
			// $this->Session->setFlash($e->getMessage());
		}

		// 教材IDのフォルダ/ファイルを削除
		$dir = new Folder($base_dir);
		if (!$dir->delete()) {
			// $this->Session->setFlash("フォルダの削除に失敗しました");
		}
		$file = new File($base_zip);
		if (!$file->delete()) {
			// $this->Session->setFlash("ファイルの削除に失敗しました");
		}
	}
}
