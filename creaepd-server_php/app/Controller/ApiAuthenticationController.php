<?php
App::uses('ApiController', 'Controller');
/**
 * API Authentication Controller
 *
 * @property SessionComponent $Session
 * @property RequestHandlerComponent $RequestHandler
*/
class ApiAuthenticationController extends ApiController {

	/**
	 * index method
	 *
	 * @return void
	*/
	public function index() {
		try {
			if (empty($this->request->data['user_id'])) {
				throw new BadRequestException(MESSAGE_API_ERROR_001);
			}
			if (empty($this->request->data['password'])) {
				throw new BadRequestException(MESSAGE_API_ERROR_001);
			}
			if (empty($this->request->data['api_key'])) {
				throw new BadRequestException(MESSAGE_API_ERROR_001);
			}
			$user_ids = $this->request->data['user_id'];
			$password = $this->request->data['password'];
			// ***** api_keyの一致が必要 *****
			// POST送信なら
			if($this->request->is('post')) {
				$this->request->data['MUser']['user_ids'] = $user_ids;
				$this->request->data['MUser']['password'] = $password;
				// ログインOKなら
				if($this->Auth->login()) {
					if($this->Auth->User('delete_flg') != 1) {
						$this->log($user_ids . ' login success', 'access');
					} else { // ログインユーザ削除はNG
						$this->log($user_ids . ' login failed user deleted', 'access');
						throw new BadRequestException(MESSAGE_API_ERROR_011);
					}
				} else { // ログインNGなら
					$this->log($user_ids . ' login failed', 'access');
					throw new BadRequestException(MESSAGE_API_ERROR_011);
				}
			}
			// （一旦SESSIONキーを設定）
			$access_key = $this->Session->id();
			//$access_key = base64_encode($access_key);
			$muser = $this->Auth->user();
			unset($muser['Auth']);
			$muser['access_key'] = $access_key;
		} catch (BadRequestException $exc) {
			$error_id = $exc->getMessage();
			if (empty($GLOBALS['message']->getMessage($error_id))) {
				throw new BadRequestException($error_id);
			}
			$error_message = $GLOBALS['message']->getMessage($error_id);
			$error_code = Configure::read("message_error_code.{$error_id}");
			return $this->error($error_message, $error_code);
		}

		// ユーザ情報＋アクセスキー
		$result = array(
				'MUser' => $muser
		);

		return $this->success($result);
	}
}
