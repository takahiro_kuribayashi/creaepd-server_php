<?php
App::uses('AppController', 'Controller');
/**
 * Status Controller
 *
 * @property TSubmit $TSubmit
 */
class StatusController extends AppController {
	public $uses = array('TSubmit', 'MGroup', 'MUser');

/**
 * Components
 *
 * @var array
 */
	public $components = array('PagingSupport', 'Search.Prg', 'Paginator', 'Session');

	public function __construct($request, $response) {
		parent::__construct($request, $response);
		// 進捗確認画面用の定数ファイルを読み込み
		config('const/constStatus');
	}

/**
 * beforeFilter method
 *
 * @return void
 */
	public function beforeFilter() {
		// 親クラスのbeforeFilter()を呼び出す
		parent::beforeFilter();
		// 検索対象のフィールド設定代入
		$this->presetVars = $this->TSubmit->presetVars;

		// ページャ設定
		$pager_numbers = array(
				'before' => ' - ',
				'after'=>' - ',
				'modulus'=> 10,
				'separator'=> ' ',
				'class'=>'pagenumbers'
		);
		$this->set('pager_numbers', $pager_numbers);
	}

/**
 * isAuthorized method
 *
 * @return void
 */
	public function isAuthorized() {
		if ($this->Auth->User('user_type_id') == '3' || $this->Auth->User('user_type_id') == '9') {
			//admin権限を持つユーザは全てのページにアクセスできる
			return true;
		} elseif ($this->Auth->User('user_type_id') == '2') {
			//教師権限を持つユーザも全てのページにアクセスできる
			return true;
		}
		return false;
	}

	public function index() {
	}

/**
 * user method(Unused)
 *
 * @return void
 */
	public function user() {
		// 検索条件設定
		$this->Prg->commonProcess();
		// 検索条件取得
		$conditions = $this->TSubmit->parseCriteria($this->passedArgs);
		// 検索条件取得（管理者・教師のみ）
		$conditions += $this->setTSubmitUserConditions();

		$this->paginate = array(
				'conditions' => $conditions,
				'limit' => 20,
		);

		$this->TSubmit->recursive = 0;
		$this->set('tSubmits', $this->Paginator->paginate());

		//（管理者・教師－所属ユーザ絞込み）
		$conditions = $this->slctUserConditions();
		$options = array('fields' => array('user_ids','user_name'), 'conditions' => $conditions);
		$mUsers = $this->MUser->find('list', $options);
		$this->set(compact('mUsers'));
	}

/**
 * submit method
 *
 * @return void
 */
	public function submit() {
		// 検索条件設定
		$this->Prg->commonProcess();
		$this->PagingSupport->inheritPostData(); //$this->dataの値の引き継ぎが行われる

		// 検索条件取得
		$conditions = $this->TSubmit->parseCriteria($this->passedArgs);
		$conditions += array('TSubmit.user_id <>' => 0); // グループ配信は除く
		$conditions += array('TSubmit.unsubscribe' => 0); // 削除済みは除く
		$conditions += array('mResource.resource_type' => 1); // 教科書は除く

		$this->TSubmit->virtualFields = array(
				'resource_name' => 'mResource.resource_name',
				'user_name' => 'mUser.user_name',
				'group_name' => 'mGroup.group_name'
		);
		
		// 検索条件取得（管理者・教師のみ）
		$conditions += $this->setTSubmitUserConditions();
		// 結合条件
		$joins = array(
				array(
						'table'		 => 't_sections',
						'alias'		 => 'tSection',
						'type'		 => 'LEFT',
						'conditions' => array('TSubmit.user_id = tSection.user_id')
				),
		);

		$this->paginate = array(
				'conditions' => $conditions,
				'joins' => $joins,
				'limit' => 20,
				'order' => array('id' => 'desc'),
		);

		$this->TSubmit->recursive = 0;
		$this->set('tSubmits', $this->Paginator->paginate());

		//（管理者・教師－所属ユーザ絞込み）
		$conditions = $this->slctUserConditions();
		$conditions += array('user_type_id =' => 1);
		$conditions += array('delete_flg =' => 0);
		$options = array('fields' => array('id','user_name'), 'conditions' => $conditions);
		$allUsers = array(0 => $GLOBALS['message']->getMessage(MESSAGE_COMMON_ITEM_090));
		$allUsers += $this->MUser->find('list', $options);
		$this->set(compact('allUsers'));

		//（管理者－所属グループ絞込み）
		$conditions = $this->slctGroupConditions();
		$conditions += array($this->MGroup->alias . '.delete_flg = 0');
		$allGroups = array(0 => $GLOBALS['message']->getMessage(MESSAGE_COMMON_ITEM_090));
		$allGroups += $this->MGroup->find('list', array('fields' => array('id','group_name'), 'conditions' => $conditions));
		$this->set(compact('allGroups'));
	}

/**
 * check method
 *
 * @return void
 */
	public function check() {
		// 検索条件設定
		$this->Prg->commonProcess();
		$this->PagingSupport->inheritPostData(); //$this->dataの値の引き継ぎが行われる

		// 検索条件取得
		$conditions = $this->TSubmit->parseCriteria($this->passedArgs);
		$conditions += array('TSubmit.user_id <>' => 0); // グループ配信は除く
		$conditions += array('TSubmit.unsubscribe' => 0); // 削除済みは除く
		$conditions += array('mResource.resource_type' => 1); // 教科書は除く
		// 検索条件取得（管理者・教師のみ）
		$conditions += $this->setTSubmitUserConditions();
		
		$this->TSubmit->virtualFields = array(
				'resource_name' => 'mResource.resource_name',
				'user_name' => 'mUser.user_name',
				'group_name' => 'mGroup.group_name'
		);
		
		// 結合条件
		$joins = array(
				array(
						'table'		 => 't_sections',
						'alias'		 => 'tSection',
						'type'		 => 'LEFT',
						'conditions' => array('TSubmit.user_id = tSection.user_id')
				),
		);

		$this->paginate = array(
				'conditions' => $conditions,
				'joins' => $joins,
				'limit' => 20,
				'order' => array('id' => 'desc'),
		);

		$this->TSubmit->recursive = 0;
		$this->set('tSubmits', $this->Paginator->paginate());


		//（管理者・教師－所属ユーザ絞込み）
		$conditions = $this->slctUserConditions();
		$conditions += array('user_type_id =' => 1);
		$conditions += array('delete_flg =' => 0);
		$options = array('fields' => array('id','user_name'), 'conditions' => $conditions);
		$allUsers = array(0 => $GLOBALS['message']->getMessage(MESSAGE_COMMON_ITEM_090));
		$allUsers += $this->MUser->find('list', $options);
		$this->set(compact('allUsers'));

		//（管理者－所属グループ絞込み）
		$conditions = $this->slctGroupConditions();
		$conditions += array($this->MGroup->alias . '.delete_flg = 0');
		$allGroups = array(0 => $GLOBALS['message']->getMessage(MESSAGE_COMMON_ITEM_090));
		$allGroups += $this->MGroup->find('list', array('fields' => array('id','group_name'), 'conditions' => $conditions));
		$this->set(compact('allGroups'));

		//（管理者or教師－採点ユーザ絞込み）
		$conditions = $this->slctUserConditions($this->MUser->alias);
		$conditions += array('user_type_id >=' => 2);
		$options = array('fields' => array('id','user_name','user_ids'), 'conditions' => $conditions);
		$mBaseMUsers = $this->MUser->find('all', $options);
		$mUsers = array(0 => $GLOBALS['message']->getMessage(MESSAGE_COMMON_ITEM_090));
		foreach($mBaseMUsers as $key => $musers) {
			if (isset($musers['mUser']['id'])){
				$id = $musers['mUser']['id'];
				$user_name = $musers['mUser']['user_name'] . '['. $musers['mUser']['user_ids'] . ']';
				$mUsers[$id] = $user_name;
			}
		}
		$this->set(compact('mUsers'));
	}

/**
 * submit_state reset method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function submit_reset($id = null) {
		if (!$this->TSubmit->exists($id)) {
			throw new NotFoundException(__('Invalid m resource'));
		}
		if ($this->request->is(array('post','get'))) {
			$options = array('conditions' => array('TSubmit.' . $this->TSubmit->primaryKey => $id));
			$this->request->data = $this->TSubmit->find('first', $options);
			// 提出状態を未提出にリセット
			$this->request->data['TSubmit']['submit_state'] = 0;
			$this->request->data['TSubmit']['check_state'] = 0;
			if ($this->TSubmit->save($this->request->data)) {
				// $this->Session->setFlash(__('The t submit state has been reset.'));
			} else {
				// $this->Session->setFlash(__('The t submit state could not be reset. Please, try again.'));
			}
			return $this->redirect(array('action' => 'submit'));
		}
	}

/**
 * setTSubmitUserConditions method
 *
 * @return void
*/
	private function setTSubmitUserConditions() {
		// 検索条件取得（管理者・教師のみ）
		$conditions = array();
		if ($this->Auth->User('user_type_id') <= '3') {
			// 自身の上位グループIDを取得
			$options = array('conditions' => array('tSection.user_id' => $this->Auth->User('id')), 'fields' => array('parent_id'));
			$tsectionAncestor = $this->MGroup->tSection->find('first', $options);
			$parent_id = isset($tsectionAncestor['tSection']['parent_id']) ? $tsectionAncestor['tSection']['parent_id']: 0;
			$ary_tsections = $this->MGroup->tSection->children($parent_id);

			// 自身の所属配下のユーザ ※自身は含まない
			$valueUsers = array();
			foreach ($ary_tsections as $tsection) {
				if ($tsection['tSection']['user_id'] !== '0' && $tsection['tSection']['user_id'] !== $this->Auth->User('id')) {
					$valueUsers[] = $tsection['tSection']['user_id'];
				}
			}
			$conditions += array('TSubmit.user_id' => $valueUsers);
		}
		return $conditions;
	}
}
