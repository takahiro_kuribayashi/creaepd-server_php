<?php

App::uses('AppController', 'Controller');

class TopController extends AppController {
	public $uses = array('S3Service', 'Post', 'Attachment', 'PdfPost','TSection','VSchedule','VSubmit');
	public function __construct($request, $response) {
		parent::__construct($request, $response);
		// トップ画面用の定数ファイルを読み込み
		config('const/constTop');
		config('const/constSchedule');
		config('const/constDeliver');
		config('const/constRecture');

		// datepicker
		$this->helpers[] = "DatePicker";

		// 選択済みファイル情報を設定
		$newest_post = $this->Post->getNewestRecord();
    	$newest_pdf = $this->PdfPost->getNewestRecord();
		$this->set('selected_file_post', $newest_post['Image'][0]['attachment']);
		$this->set('attachment_id_post', $newest_post['Image'][0]['id']);
    	$this->set('post_id', $newest_post['Post']['id']);
    	$this->set('selected_file_pdf', $newest_pdf['Pdf'][0]['attachment']);
		$this->set('attachment_id_pdf', $newest_pdf['Pdf'][0]['id']);
    	$this->set('pdf_id', $newest_pdf['PdfPost']['id']);

		// 共通変数の初期化
		$this->set('selected_date', "");
	}

    public function index() {
    	$user = $this->Auth->User();
		$user_id = $user['id'];
		$user_type = $user['user_type_id'];

		$options = array('conditions' => array('TSection.user_id' => $user_id));
		$mgroup = $this->TSection->find('first', $options);
		$group_id = (!empty($mgroup)) ? $mgroup['TSection']['group_id'] : 0; // システム管理者の場合、グループ未所属

		// 検索条件(所属グループID or ユーザIDで一致)
		$start = date("Y-m-d H:i:s"); // ～当日
		$end = date("Y-m-d H:i:s", strtotime("-1 month")); //（期限日時が）1ヶ月前～
		$conditions = array(
				array(
						'VSchedule.start_datetime <= CAST(? AS DATETIME)'=>array($start),
						'VSchedule.end_datetime >= CAST(? AS DATETIME)'=>array($end),
						'VSchedule.submit_id' => 0,
						'VSchedule.sync_id' => 0,
						'VSchedule.status' => 1,
						'OR' => array(
							array(
								'VSchedule.group_id' => $group_id,
								'VSchedule.user_id' => 0
							),
							array(
								'VSchedule.group_id' => 0,
								'VSchedule.user_id' => $user_id
							)
						)
				)
		);
		// スケジュールVIEWを取得
		$options = array('conditions' => $conditions, 'order' => 'VSchedule.start_datetime desc', 'limit' => 5);
		$vschedule_data = $this->VSchedule->find('all', $options);
		$this->set('vSchedules', $vschedule_data);
//$this->log($vschedule_data, LOG_DEBUG);

		// 検索条件(所属グループID一致、提出済みで集計)
		$groups = array('VSubmit.resource_id', 'VSubmit.check_user_id');
		if ($user_type == '3'|| $user_type == '9') { // 管理者は全dataを取得
			$conditions = array(
					array(
							'VSubmit.check_user_id' => $user_id,
							'VSubmit.unsubscribe' => 0
					)
			);
		} else {
			$conditions = array(
					array(
							'VSubmit.check_user_id' => $user_id,
							'VSubmit.unsubscribe' => 0
					)
			);
		}

		$fields = array('VSubmit.submit_no','VSubmit.resource_ids','VSubmit.resource_name','VSubmit.start_datetime','VSubmit.end_datetime',
				'(SELECT COUNT(`id`) FROM `v_submits` WHERE `VSubmit`.`submit_no`=`v_submits`.`submit_no` AND `VSubmit`.`check_user_id`=`v_submits`.`check_user_id` AND `v_submits`.`submit_state` >= 2) as  VSubmit__submit_cnt',
				'(SELECT COUNT(`id`) FROM `v_submits` WHERE `VSubmit`.`submit_no`=`v_submits`.`submit_no` AND `VSubmit`.`check_user_id`=`v_submits`.`check_user_id`) as VSubmit__total_cnt' // GROUP BY `check_user_id`
		);
		$this->VSubmit->virtualFields = array(
				'submit_cnt' => 0,
				'total_cnt' => 0
		);

		// 提出テーブルVIEWを取得
		$options = array('group' => $groups, 'conditions' => $conditions, 'fields' => $fields, 'order' => 'VSubmit.end_datetime desc', 'limit' => 5);
		$vsubmit_data = $this->VSubmit->find('all', $options);
		$this->set('vSubmits', $vsubmit_data);
//$this->log($vsubmit_data, LOG_DEBUG);

		if ($user_type == '3' || $user_type == '9') { // 管理者・システム管理者のみ表示
			// 検索条件(所属グループID一致、採点済みで集計)
			$groups = array('VSubmit.resource_id', 'VSubmit.check_user_id');
			$conditions = array(
					array(
							'VSubmit.check_user_id' => $user_id,
							'VSubmit.unsubscribe' => 0
					)
			);
			$fields = array('VSubmit.submit_no','VSubmit.resource_ids','VSubmit.resource_name','VSubmit.start_datetime','VSubmit.end_datetime',
					'(SELECT COUNT(`id`) FROM `v_submits` WHERE `VSubmit`.`submit_no`=`v_submits`.`submit_no` AND `VSubmit`.`check_user_id`=`v_submits`.`check_user_id` AND `v_submits`.`check_state` >= 1) as  VSubmit__check_cnt',
					'(SELECT COUNT(`id`) FROM `v_submits` WHERE `VSubmit`.`submit_no`=`v_submits`.`submit_no` AND `VSubmit`.`check_user_id`=`v_submits`.`check_user_id`) as VSubmit__total_cnt' // GROUP BY `check_user_id`
			);
			$this->VSubmit->virtualFields = array(
					'check_cnt' => 0,
					'total_cnt' => 0
			);

			// 提出テーブルVIEWを取得
			$options = array('group' => $groups, 'conditions' => $conditions, 'fields' => $fields, 'order' => 'VSubmit.end_datetime desc', 'limit' => 5);
			$vsubmit_data = $this->VSubmit->find('all', $options);
			$this->set('vSubmitChecks', $vsubmit_data);
//$this->log($vsubmit_data, LOG_DEBUG);
		}
    }
}