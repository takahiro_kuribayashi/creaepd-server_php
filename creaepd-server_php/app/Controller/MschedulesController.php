<?php
App::uses('AppController', 'Controller');
/**
 * MSchedules Controller
 *
 * @property MSchedule $MSchedule
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class MSchedulesController extends AppController {
	public $name = 'mSchedules';
	public $uses = array('MSchedule', 'MGroup', 'MUser', 'MIndexNumber', 'TSchedule');

/**
 * Components
 *
 * @var array
 */
	public $components = array('PagingSupport', 'Search.Prg', 'Paginator', 'Session');

	public function __construct($request, $response) {
		parent::__construct($request, $response);
		// スケジュール管理画面用の定数ファイルを読み込み
		config('const/constSchedule');
	}

/**
 * beforeFilter method
 *
 * @return void
 */
	public function beforeFilter() {
		// 親クラスのbeforeFilter()を呼び出す
		parent::beforeFilter();
		// 検索対象のフィールド設定代入
		$this->presetVars = $this->MSchedule->presetVars;

		// ページャ設定
		$pager_numbers = array(
				'before' => ' - ',
				'after'=>' - ',
				'modulus'=> 10,
				'separator'=> ' ',
				'class'=>'pagenumbers'
		);
		$this->set('pager_numbers', $pager_numbers);
	}


/**
 * isAuthorized method
 *
 * @return void
 */
	public function isAuthorized() {
		if ($this->Auth->User('user_type_id') == '3' || $this->Auth->User('user_type_id') == '9') {
			//admin権限を持つユーザは全てのページにアクセスできる
			return true;
		} elseif ($this->Auth->User('user_type_id') == '2') {
			//教師権限を持つユーザも全てのページにアクセスできる
			return true;
		}
		return false;
	}

/**
 * index method
 *
 * @return void
 */
	public function index() {
		// 検索条件設定
		$this->Prg->commonProcess();
		$this->PagingSupport->inheritPostData(); //$this->dataの値の引き継ぎが行われる

		// 検索条件取得
		$conditions = $this->MSchedule->parseCriteria($this->passedArgs);
		//（デフォルトの検索条件）
		$conditions += array('MSchedule.delete_flg ' => 0);
		$conditions += array('TSchedule.submit_id ' => 0, 'TSchedule.sync_id ' => 0);
		// 検索条件取得（管理者のみ）
		if ($this->Auth->User('user_type_id') <= '3') {
			// 自身の上位グループIDを取得
			$options = array('conditions' => array('tSection.user_id' => $this->Auth->User('id')), 'fields' => array('parent_id'));
			$tsectionAncestor = $this->MGroup->tSection->find('first', $options);
			$parent_id = isset($tsectionAncestor['tSection']['parent_id']) ? $tsectionAncestor['tSection']['parent_id']: 0;
			$ary_tsections = $this->MGroup->tSection->children($parent_id);

			// 自身の所属配下のユーザ ※自身を含む
			$valueUsers = array();
			foreach ($ary_tsections as $tsection) {
				if ($tsection['tSection']['user_id'] !== '0' /*&& $tsection['tSection']['user_id'] !== $this->Auth->User('id')*/) {
					$valueUsers[] = $tsection['tSection']['user_id'];
				}
			}

			// 自身の所属配下のグループ ※自身は含まない
			$valueGroups = array();
			foreach ($ary_tsections as $tsection) {
				if ($tsection['tSection']['user_id'] == '0') {
					$valueGroups[] = $tsection['tSection']['group_id'];
				}
			}

			$conditions += array(
				'AND' => array(
						'OR' => array(
								array(
										'TSchedule.user_id' => $valueUsers
								),
								array(
										'TSchedule.group_id' => $valueGroups
								)
						)
				)
			);
		}
		// 結合条件
		$joins = array(
				array(
						'table'		 => 't_schedules',
						'alias'		 => 'TSchedule',
						'type'		 => 'LEFT',
						'conditions' => array('TSchedule.schedule_id = MSchedule.id')
				)
		);
		$groups = array('TSchedule.schedule_id');

		$this->paginate = array(
				'conditions' => $conditions,
				'group' => $groups,
				'joins' => $joins,
				'limit' => 20,
				'order' => array('id' => 'desc')
		);

		$this->MSchedule->recursive = 0;
		$this->set('mSchedules', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->MSchedule->exists($id)) {
			throw new NotFoundException(__('Invalid m schedule'));
		}
		$options = array('conditions' => array('MSchedule.' . $this->MSchedule->primaryKey => $id));
		$this->set('mSchedule', $this->MSchedule->find('first', $options));

		$options = array('conditions' => array('TSchedule.schedule_id' => $id), 'fields' => array('group_id'));
		$tschedule_data = $this->TSchedule->find('first', $options);
		$group_id = isset($tschedule_data['TSchedule']['group_id']) ? $tschedule_data['TSchedule']['group_id']: 0;
		if ($group_id != 0) {
			// 配信グループ一覧を取得
			$options = array('conditions' => array($this->MGroup->alias . '.id' => $group_id), 'fields' => array('id','group_name'));
			$scheduleTGroups = $this->MGroup->find('list', $options);
			$this->set('tScheduleGroups', $scheduleTGroups);
		} else {
			// 配信ユーザ一覧を取得
			$options = array('conditions' => array('TSchedule.schedule_id' => $id), 'fields' => array('user_id'));
			$ary_users = $this->TSchedule->find('list', $options);

			$belongsTo = array(
					'mUser' => array(
							'className' => 'MUser',
							'foreignKey' => 'user_id'
					));
			$this->TSchedule->bindModel(array('belongsTo' => $belongsTo), false);
			$options = array('conditions' => array($this->TSchedule->mUser->alias . '.id' => $ary_users), 'fields' => array('id','user_name'));
			$scheduleTUsers = $this->TSchedule->mUser->find('list', $options);
			$this->set('tScheduleUsers', $scheduleTUsers);
		}
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			// シーケンスNOが更新されていた場合、メッセージを表示
			$add_index_number = $this->get_index_number(COLUMN_SCHEDULE_IDX);
			$add_index_number = PREFIX_SCHEDULE_IDS.$add_index_number;
			if ($add_index_number !== $this->request->data['MSchedule']['schedule_ids']) {
				// $this->Session->setFlash(__('The m schedule ID overlaps.'));
			} else {
				// 配信対象を個人・グループで分けて初期化
				if ($this->request->data['MSchedule']['subject_flg'] === 'user') {
					$this->request->data['MSchedule']['group_id'] = 0;
				} else {
					$this->request->data['MSchedule']['user_id'] = 0;
				}
				// Version・作成者に初期値を設定
				$this->request->data['MSchedule']['version'] = 1;
				// リクエストdataを保存用に複写
				$request_data = $this->request->data;
				// ユーザID配列でバリデーションがかかるため、クリア
				$request_data['MSchedule']['user_id'] = 0;
				$this->MSchedule->create();
				if ($this->MSchedule->save($request_data)) {
					if (strstr($this->request->data['MSchedule']['schedule_ids'], PREFIX_SCHEDULE_IDS)) {
						$mschedule_last_id = $this->MSchedule->getLastInsertID();

						// ***スケジュールテーブルに対象者を登録***
						$this->TSchedule->create();
						if (is_array($this->request->data['MSchedule']['user_id'])) {
							// 複数の場合は、指定ユーザ分登録
							$tmpdata_ary = array();
							foreach ($this->request->data['MSchedule']['user_id'] as $user_id) {
								$tmpdata['TSchedule'] = array();
								$tmpdata['TSchedule']['id'] = NULL;
								$tmpdata['TSchedule']['schedule_id'] = (int)($mschedule_last_id);
								$tmpdata['TSchedule']['group_id'] = $this->request->data['MSchedule']['group_id'];
								$tmpdata['TSchedule']['user_id'] = $user_id;
								$tmpdata['TSchedule']['submit_id'] = 0;
								$tmpdata['TSchedule']['status'] = 1;
								$tmpdata_ary[] = $tmpdata;
							}
							$this->TSchedule->saveAll($tmpdata_ary); // 複数データ
						} else {
							$tmpdata['TSchedule'] = array();
							$tmpdata['TSchedule']['id'] = NULL;
							$tmpdata['TSchedule']['schedule_id'] = (int)($mschedule_last_id);
							$tmpdata['TSchedule']['group_id'] = $this->request->data['MSchedule']['group_id'];
							$tmpdata['TSchedule']['user_id'] = $this->request->data['MSchedule']['user_id'];
							$tmpdata['TSchedule']['submit_id'] = 0;
							$tmpdata['TSchedule']['status'] = 1;
							$this->TSchedule->save($tmpdata['TSchedule']); // 単一データ
						}
					}

					// シーケンスNOを更新する　※採番テーブルからIndexNoを更新する
					$next_index_number = $this->set_index_number(COLUMN_SCHEDULE_IDX);

					// $this->Session->setFlash(__('The m schedule has been saved.'));
					return $this->redirect(array('action' => 'index'));
				} else {
					// $this->Session->setFlash(__('The m schedule could not be saved. Please, try again.'));
				}
			}
		}

		$this->set('valueMUser', 0); // デフォルトは未選択

		//（管理者ユーザ－所属ユーザ絞込み）
		$conditions = $this->slctUserConditions();
		$conditions += array($this->MUser->alias.'.delete_flg = 0');
		$setMUsers = $this->MUser->find('list', array('fields' => array('id','user_name'), 'conditions' => $conditions));
		$this->set(compact('setMUsers'));

		//（管理者ユーザ－所属グループ絞込み） ※スケジュールのみ、自身のグループを含む
		$conditions = $this->slctGroupConditions($this->MGroup->alias, $this->Auth->User('id'));
		$conditions += array('MGroup.delete_flg = 0');
		$groupObjs = $this->MGroup->find('list', array('fields' => array('id','group_name'), 'conditions' => $conditions));
		$this->set(compact('groupObjs'));

		$options = array('conditions' => array('user_type_id >=' => 2), 'fields' => array('id','user_name'));
		$mUsers = $this->MUser->find('list', $options);
		$this->set(compact('mUsers'));

		// スケジュールID初期値生成　※採番テーブルからIndexNoを取得する
		$new_index_number = $this->get_index_number(COLUMN_SCHEDULE_IDX);
		$this->request->data['MSchedule']['schedule_ids'] = PREFIX_SCHEDULE_IDS.$new_index_number;
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->MSchedule->exists($id)) {
			throw new NotFoundException(__('Invalid m schedule'));
		}

		if ($this->request->is(array('post', 'put'))) {
			// 配信対象を個人・グループで分けて初期化
			if ($this->request->data['MSchedule']['subject_flg'] === 'user') {
				$this->request->data['MSchedule']['group_id'] = 0;
			} else {
				$this->request->data['MSchedule']['user_id'] = 0;
			}
			// Versionを更新
			$this->request->data['MSchedule']['version'] = $this->request->data['MSchedule']['version'] + 1;
			// リクエストdataを保存用に複写
			$request_data = $this->request->data;
			// ユーザID配列でバリデーションがかかるため、クリア
			$request_data['MSchedule']['user_id'] = 0;
			if ($this->MSchedule->save($request_data)) {
				if (strstr($this->request->data['MSchedule']['schedule_ids'], PREFIX_SCHEDULE_IDS)) {
					$mschedule_last_id = $this->request->data['MSchedule']['id'];

					// ***スケジュールテーブルに対象者を登録***
					$this->TSchedule->create();
					if (is_array($this->request->data['MSchedule']['user_id'])) {
						// 複数の場合は、指定ユーザ分登録
						$tmpdata_ary = array();
						//$this->log('複数', LOG_DEBUG);

						foreach ($this->request->data['MSchedule']['user_id'] as $user_id) {
							$tmpdata['TSchedule'] = array();
							$tmpdata['TSchedule']['id'] = NULL;
							$tmpdata['TSchedule']['schedule_id'] = (int)($mschedule_last_id);
							$tmpdata['TSchedule']['group_id'] = $this->request->data['MSchedule']['group_id'];
							$tmpdata['TSchedule']['user_id'] = $user_id;
							$tmpdata['TSchedule']['submit_id'] = 0;
							$tmpdata['TSchedule']['status'] = 1;
							$tmpdata_ary[] = $tmpdata;
						}
						$this->TSchedule->saveAll($tmpdata_ary); // 複数データ
					} else {
						$tmpdata['TSchedule'] = array();
						$tmpdata['TSchedule']['id'] = NULL;
						$tmpdata['TSchedule']['schedule_id'] = (int)($mschedule_last_id);
						$tmpdata['TSchedule']['group_id'] = $this->request->data['MSchedule']['group_id'];
						$tmpdata['TSchedule']['user_id'] = $this->request->data['MSchedule']['user_id'];
						$tmpdata['TSchedule']['submit_id'] = 0;
						$tmpdata['TSchedule']['status'] = 1;
						$this->TSchedule->save($tmpdata['TSchedule']); // 単一データ

						//$this->log($tmpdata['TSchedule'], LOG_DEBUG);

						//$this->log('単発', LOG_DEBUG);

						if( $this->request->data['MSchedule']['user_id'] == 0 ) {
							//$this->log('グループ', LOG_DEBUG);
							//$this->log($this->request->data, LOG_DEBUG);
							//$this->log($mschedule_last_id, LOG_DEBUG);

							// グループ登録の場合
							$options = array('conditions' => array('schedule_id' => ($mschedule_last_id)));
							$t_schedule_data = $this->TSchedule->find('all', $options); // 複数データ

							//$this->log($t_schedule_data, LOG_DEBUG);

							foreach( $t_schedule_data as $data ) {
								$data['TSchedule']['status'] = 1;
								//$this->log($data, LOG_DEBUG);
								$this->TSchedule->save($data['TSchedule']);
							}
						}
					}
				}
				// $this->Session->setFlash(__('The m schedule has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				// $this->Session->setFlash(__('The m schedule could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('MSchedule.' . $this->MSchedule->primaryKey => $id));
			$this->request->data = $this->MSchedule->find('first', $options);

			// 検索条件
			$conditions = array(
					'AND' => array(
							'TSchedule.schedule_id'=> $id,
							'OR' => array(
									array(
											'TSchedule.user_id' => 0
									),
									array(
											'TSchedule.group_id' => 0
									)
							)
					)
			);
			// スケジュールテーブル配信対象の取得
			$options = array('conditions' => $conditions);
			$tschedule_data = $this->TSchedule->find('all', $options);
			$valueTUsers = array();
			if (!empty($tschedule_data)) {
				if ($tschedule_data[0]['TSchedule']['user_id'] === '0') {
					$this->request->data['MSchedule']['subject_flg'] = 'group';
					$this->request->data['MSchedule']['group_id'] = $tschedule_data[0]['TSchedule']['group_id'];
					$this->request->data['MSchedule']['user_id'] = 0;
				} else {
					$this->request->data['MSchedule']['subject_flg'] = 'user';
					$this->request->data['MSchedule']['user_id'] = $tschedule_data[0]['TSchedule']['user_id'];
					$this->request->data['MSchedule']['group_id'] = 0;
				}
				// 登録ユーザの選択
				$valueTUsers = array();
				foreach ($tschedule_data as $tSchedule) {
					$tschedule_user_id = $tSchedule['TSchedule']['user_id'];
					$valueTUsers[] = $tschedule_user_id;
				}
			}
			$this->set('valueMUser', $valueTUsers);
		}

		//（管理者ユーザ－所属ユーザ絞込み）
		$conditions = $this->slctUserConditions();
		$conditions += array($this->MUser->alias.'.delete_flg = 0');
		$setMUsers = $this->MUser->find('list', array('fields' => array('id','user_name'), 'conditions' => $conditions));
		$this->set(compact('setMUsers'));

		//（管理者ユーザ－所属グループ絞込み） ※スケジュールのみ、自身のグループを含む
		$conditions = $this->slctGroupConditions($this->MGroup->alias, $this->Auth->User('id'));
		$conditions += array('MGroup.delete_flg = 0');
		$groupObjs = $this->MGroup->find('list', array('fields' => array('id','group_name'), 'conditions' => $conditions));
		$this->set(compact('groupObjs'));

		$options = array('conditions' => array('user_type_id >=' => 2), 'fields' => array('id','user_name'));
		$mUsers = $this->MUser->find('list', $options);
		$this->set(compact('mUsers'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		if (!$this->MSchedule->exists($id)) {
			throw new NotFoundException(__('Invalid m schedule'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->MSchedule->save($this->request->data)) {
				// スケジュールの配信状態全てを未配信に戻す
				$options = array('conditions' => array('TSchedule.schedule_id' => $id));
				$tschedule_data = $this->TSchedule->find('all', $options);
				if (!empty($tschedule_data)) {
					foreach($tschedule_data as $key => $data){
						$tschedule_data[$key]['TSchedule']['status'] = 1;
					}
					$this->TSchedule->saveAll($tschedule_data);
				}
				// $this->Session->setFlash(__('The m schedule has been deleted.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				// $this->Session->setFlash(__('The m schedule could not be deleted. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('MSchedule.' . $this->MSchedule->primaryKey => $id));
			$this->set('mSchedule', $this->MSchedule->find('first', $options));
		}
	}
}
