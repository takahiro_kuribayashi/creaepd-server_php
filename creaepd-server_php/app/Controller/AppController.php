<?php
/**
 * Application level Controller
 *
 * This file is application-wide controller file. You can put all
 * application-wide controller-related methods here.
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

App::uses('Controller', 'Controller');

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @package		app.Controller
 * @link		http://book.cakephp.org/2.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller {
	protected $message;
	protected $menu_array;
	public $uses = array('MSetting');

	public $components = array('Paginator', 'Session', 'PageTitle',
		'Auth' => array(
				//ログイン後の移動先
				'loginRedirect' => array('controller' => 'top', 'action' => 'index'),
				//ログアウ後の移動先
				'logoutRedirect' => array('controller' => 'musers', 'action' => 'login'),
				//ログインページのパス
				'loginAction' => array('controller' => 'musers', 'action' => 'login'),
				//未ログイン時のメッセージ
				'authError' => '',
				'authorize' => array('Controller'),
				'authenticate' => array('Form' => array('passwordHasher' => 'Original'))
		)
	);

	/** 共通処理
	 *
	 */
	function beforeFilter(){
		if (!isset($GLOBALS['message'])) {
			// メッセージクラスの取得
			//App::import('Vendor', 'Message');
			App::uses('Message', 'Vendor');
			global $message;
			$message = new Message();
		}
		// 共通定数ファイル読み込み
		config('const/constCommon');
		// 共通ヘルパー追加
		$this->helpers[] = 'Common';
		$this->helpers[] = 'Form';
		$this->helpers[] = 'Tree';

		$this->Session->write('ActiveURL', Router::url());
		// サイドメニュー用情報取得
		App::import('Vendor', 'Menu/SideMenu');
		$muser = $this->Auth->User();
		$this->set('menu_array', SideMenu::getMenuArray($muser));

		// 設定情報取得
		if (!isset($GLOBALS['msetting'])) {
			global $msetting;
			$msetting = $this->MSetting->find('first', array());
			$msetting['MSetting']['logo_file'] = Router::url( '/', true) . 'img/' . $msetting['MSetting']['logo_file'];
		}

		// 認証不要のページの指定
		$this->Auth->allow('login', 'logout');
		$this->Auth->authError = $GLOBALS['message']->getMessage(MESSAGE_LOGIN_ERROR_001);

		// アクセス情報をビューに渡す
		if( $this->Auth->User('user_type_id') !== '1') {
			// 生徒ユーザでログインした場合は表示しない
			// ログインはできないが、ユーザ情報が表示されるため、回避する
			$this->set('userinfo', $this->Auth->User());
		}
		// ページタイトルをビューに渡す
		$this->set('title_for_layout', $this->PageTitle->getPageTitle($this));
	}

	public function isAuthorized($muser) {
//$this->log($muser, LOG_DEBUG);
		// 削除済みのユーザは、ログイン不可
		if ($muser['delete_flg'] !== '1') {
			if ($muser['user_type_id'] == '3' || $muser['user_type_id'] == '9') {
				return true;
			} elseif ($muser['user_type_id'] == '2') {
				return true;
			}
		}
		return false;
	}

	/**
	 * get_index_number method
	 *
	 * @return void
	 */
	public function get_index_number($col_name = null) {
		$master_data = $this->MIndexNumber->find('first'); // 条件なしで先頭行のみ
		// 採番マスタから最新のIndexNoを取得
		$index_number = $master_data['MIndexNumber'][$col_name];
		return sprintf("%04d", $index_number);
	}

	/**
	 * set_index_number method
	 *
	 * @return void
	 */
	public function set_index_number($col_name = null) {
		$master_data = $this->MIndexNumber->find('first'); // 条件なしで先頭行のみ
		// 採番マスタから最新のIndexNoを取得
		$index_number = $master_data['MIndexNumber'][$col_name];
		// IndexNoをインクリメントし、更新
		$master_data['MIndexNumber'][$col_name] = $index_number + 1;
		$this->MIndexNumber->save($master_data['MIndexNumber']);
		return sprintf("%04d", $index_number);
	}

/**
 * slctUserConditions method
 *
 * @return array
 */
	public function slctUserConditions($alias='') {
		// 検索条件取得（管理者のみ）
		$conditions = array();
		if ($this->Auth->User('user_type_id') <= '3') {
			// 自身の上位グループIDを取得
			$options = array('conditions' => array('tSection.user_id' => $this->Auth->User('id')), 'fields' => array('parent_id'));
			$tsectionAncestor = $this->MGroup->tSection->find('first', $options);
			$parent_id = isset($tsectionAncestor['tSection']['parent_id']) ? $tsectionAncestor['tSection']['parent_id']: 0;

			// 自身の所属配下のユーザ
			$ary_tsections = $this->MGroup->tSection->children($parent_id);
			$valueUsers = array();
			foreach ($ary_tsections as $tsection) {
				if ($tsection['tSection']['user_id'] !== '0') {
					$valueUsers[] = $tsection['tSection']['user_id'];
				}
			}
			if ($alias === '') {
				//$conditions += array('id' => $valueUsers);
				$conditions += array(
					'AND' => array(
							'OR' => array(
									array(
											'id' => $valueUsers
									),
									array( //（登録時のユーザで絞込む）
											'create_user_id' => $this->Auth->User('id')
									)
							)
					)
				);
			} else {
				//$conditions += array($alias.'.id' => $valueUsers);
				$conditions += array(
					'AND' => array(
							'OR' => array(
									array(
											$alias.'.id' => $valueUsers
									),
									array( //（登録時のユーザで絞込む）
											$alias.'.create_user_id' => $this->Auth->User('id')
									)
							)
					)
				);
			}
		}
		return $conditions;
	}

/**
 * slctGroupConditions method
 * @param string $alias
 * @param intger $add_user_id
 *
 * @return array
 */
	public function slctGroupConditions($alias='', $auth_user_id=0) {
		// 検索条件取得（管理者のみ）
		$conditions = array();
		if ($this->Auth->User('user_type_id') <= '3') {
			// 自身の上位グループIDを取得
			$options = array('conditions' => array('tSection.user_id' => $this->Auth->User('id')), 'fields' => array('parent_id'));
			$tsectionAncestor = $this->MGroup->tSection->find('first', $options);
			$parent_id = isset($tsectionAncestor['tSection']['parent_id']) ? $tsectionAncestor['tSection']['parent_id']: 0;

			// 自身の所属配下のグループ（ユーザ指定時に自身のグループを含む）
			$ary_tsections = $this->MGroup->tSection->children($parent_id);
			$valueGroups = array();
			foreach ($ary_tsections as $tsection) {
				if (empty($auth_user_id)) {
					if ($tsection['tSection']['user_id'] == '0') {
						$valueGroups[] = $tsection['tSection']['group_id'];
					}
				} else {
					// ユーザ指定のグループを追加する
					if ($tsection['tSection']['user_id'] == '0' || $tsection['tSection']['user_id'] == $auth_user_id) {
						$valueGroups[] = $tsection['tSection']['group_id'];
					}
				}
			}
			if ($alias === '') {
				$conditions += array('id' => $valueGroups);
			} else {
				$conditions += array($alias.'.id' => $valueGroups);
			}
		}
		return $conditions;
	}

/**
 * slctResourceUserConditions method
 *
 * @return array
 */
	public function slctResourceUserConditions() {
		// 検索条件取得（管理者or教師）
		$conditions = array('delete_flg <>' => 1); // デフォルト削除済みを除く
		if ($this->Auth->User('user_type_id') == '3') {
			// 自身の上位グループIDを取得
			$options = array('conditions' => array('tSection.user_id' => $this->Auth->User('id')), 'fields' => array('parent_id'));
			$tsectionAncestor = $this->MGroup->tSection->find('first', $options);
			$parent_id = isset($tsectionAncestor['tSection']['parent_id']) ? $tsectionAncestor['tSection']['parent_id']: 0;
			$ary_tsections = $this->MGroup->tSection->children($parent_id);

			// 自身の所属配下のユーザ
			$valueUsers = array();
			foreach ($ary_tsections as $tsection) {
				if ($tsection['tSection']['user_id'] !== '0') {
					$valueUsers[] = $tsection['tSection']['user_id'];
				}
			}
			$conditions += array('create_user_id' => $valueUsers);
		} elseif ($this->Auth->User('user_type_id') == '2') {
			// 自身の上位グループIDを取得
			$options = array('conditions' => array('tSection.user_id' => $this->Auth->User('id')), 'fields' => array('parent_id'));
			$tsectionAncestor = $this->MGroup->tSection->find('first', $options);
			$parent_id = isset($tsectionAncestor['tSection']['parent_id']) ? $tsectionAncestor['tSection']['parent_id']: 0;
			// 自身の親グループIDを取得
			$options = array('conditions' => array('tSection.' . $this->MGroup->tSection->primaryKey => $parent_id), 'fields' => array('parent_id'));
			$tsectionParent = $this->MGroup->tSection->find('first', $options);
			$parent_group_id = isset($tsectionParent['tSection']['parent_id']) ? $tsectionParent['tSection']['parent_id']: 0;
			$ary_tsections = $this->MGroup->tSection->children($parent_group_id);

			// 親グループの所属配下のユーザ
			$valueUsers = array();
			foreach ($ary_tsections as $tsection) {
				if ($tsection['tSection']['user_id'] !== '0') {
					$valueUsers[] = $tsection['tSection']['user_id'];
				}
			}
			$conditions += array('create_user_id' => $valueUsers);
		}
		return $conditions;
	}
}
