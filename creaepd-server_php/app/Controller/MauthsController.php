<?php
App::uses('AppController', 'Controller');
/**
 * MAuths Controller
 *
 * @property MAuth $MAuth
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class MAuthsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Session');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->MAuth->recursive = 0;
		$this->set('mAuths', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->MAuth->exists($id)) {
			throw new NotFoundException(__('Invalid m auth'));
		}
		$options = array('conditions' => array('MAuth.' . $this->MAuth->primaryKey => $id));
		$this->set('mAuth', $this->MAuth->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->MAuth->create();
			if ($this->MAuth->save($this->request->data)) {
				// $this->Session->setFlash(__('The m auth has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				// $this->Session->setFlash(__('The m auth could not be saved. Please, try again.'));
			}
		}
		$mUsers = $this->MAuth->MUser->find('list');
		$this->set(compact('mUsers'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->MAuth->exists($id)) {
			throw new NotFoundException(__('Invalid m auth'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->MAuth->save($this->request->data)) {
				// $this->Session->setFlash(__('The m auth has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				// $this->Session->setFlash(__('The m auth could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('MAuth.' . $this->MAuth->primaryKey => $id));
			$this->request->data = $this->MAuth->find('first', $options);
		}
		$mUsers = $this->MAuth->MUser->find('list');
		$this->set(compact('mUsers'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->MAuth->id = $id;
		if (!$this->MAuth->exists()) {
			throw new NotFoundException(__('Invalid m auth'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->MAuth->delete()) {
			// $this->Session->setFlash(__('The m auth has been deleted.'));
		} else {
			// $this->Session->setFlash(__('The m auth could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
