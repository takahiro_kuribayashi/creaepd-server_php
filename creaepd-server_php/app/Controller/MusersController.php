<?php
App::uses('AppController', 'Controller');
/**
 * MUsers Controller
 *
 * @property MUser $MUser
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class MUsersController extends AppController {
	public $name = 'mUsers';
	public $uses = array('MUser', 'MUserType', 'MGroup', 'MIndexNumber', 'TSubmit');

/**
 * Components
 *
 * @var array
 */
	public $components = array('PagingSupport', 'RandomPassword', 'Search.Prg', 'Paginator', 'Session');
	public $presetVars = array();

	public function __construct($request, $response) {
		parent::__construct($request, $response);
		// ユーザ管理画面用の定数ファイルを読み込み
		config('const/constUser');
	}


/**
 * beforeFilter method
 *
 * @return void
 */
	public function beforeFilter() {
		// 親クラスのbeforeFilter()を呼び出す
		parent::beforeFilter();
		// 検索対象のフィールド設定代入
		$this->presetVars = $this->MUser->presetVars;

		// ページャ設定
		$pager_numbers = array(
				'before' => ' - ',
				'after'=>' - ',
				'modulus'=> 10,
				'separator'=> ' ',
				'class'=>'pagenumbers'
		);
		$this->set('pager_numbers', $pager_numbers);
	}

/**
 * isAuthorized method
 *
 * @return void
 */
	public function isAuthorized() {
		if ($this->Auth->User('user_type_id') == '3' || $this->Auth->User('user_type_id') == '9') {
			//admin権限を持つユーザは全てのページにアクセスできる
			return true;
		} elseif ($this->Auth->User('user_type_id') == '2') {
			if (in_array($this->action, array('index', 'view'))) {
				//教師権限を持つユーザは検索・閲覧のページにアクセスできる
				return true;
			}
		}
		return false;
	}

	/**
	 * login method
	 *
	 * @return void
	 */
	function login(){
		// POST送信なら
		if($this->request->is('post')) {
//$this->log($this->request->data, LOG_DEBUG);
			$user_ids = $this->request->data['MUser']['user_ids'];
			// ログインOKなら
			if($this->Auth->login()) {
				if($this->Auth->User('delete_flg') != 1) {
					$this->log($user_ids . ' login success', 'access');
					// Auth指定のログインページへ移動
					$url = $this->Auth->redirect();
					if($url == DS || $url == '/') {
						return $this->redirect('/top/');
					} else {
						return $this->redirect($url);
					}
				} else { // ログインユーザ削除はNG
					$this->log($user_ids . ' login failed user deleted', 'access');
					$message_login_error = $GLOBALS['message']->getMessage(MESSAGE_LOGIN_ERROR_002);
					$this->Session->setFlash($message_login_error, 'default', array(), 'auth');
				}
			} else { // ログインNGなら
				$this->log($user_ids . ' login failed', 'access');
				$message_login_error = $GLOBALS['message']->getMessage(MESSAGE_LOGIN_ERROR_002);
				$this->Session->setFlash($message_login_error, 'default', array(), 'auth');
			}
		}
	}

	/**
	 * logout method
	 *
	 * @return void
	 */
	function logout(){
		$options = array('conditions' => array('MUser.' . $this->MUser->primaryKey => $this->Auth->User('id')));
		$data = $this->MUser->find('first', $options);
		$data['MUser']['logout_time'] = date('Y-m-d H:i:s');
		$this->MUser->save($data);
		$this->redirect($this->Auth->logout()); // ログアウト画面を表示せずにログイン画面へ
	}


/**
 * index method
 *
 * @return void
 */
	public function index() {
		// 検索条件設定
		$this->Prg->commonProcess();
		$this->PagingSupport->inheritPostData(); //$this->dataの値の引き継ぎが行われる

		// 検索条件取得
		$conditions = $this->MUser->parseCriteria($this->passedArgs);
		//（デフォルトの検索条件）
		$conditions += array($this->MUser->alias . '.delete_flg ' => 0);
		//（管理者ユーザ－所属ユーザ絞込み）
		$conditions += $this->slctUserConditions($this->MUser->alias);

		$this->paginate = array(
		  'conditions' => $conditions,
		  'limit' => 20,
		  'order' => array('id' => 'desc')
		);

		$this->MUser->recursive = 0;
		$this->set('mUsers', $this->Paginator->paginate());

		$userTypes = array('' => $GLOBALS['message']->getMessage(MESSAGE_COMMON_ITEM_090));
		$userTypes += Configure::read('m_user_types');
		if ($this->Auth->User('user_type_id') == '3') {
			//（管理者権限のみ、管理者・教師・生徒に絞る）
			unset($userTypes['9']);
		}
		$this->set(compact('userTypes'));
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->MUser->exists($id)) {
			throw new NotFoundException(__('Invalid m user'));
		}
		$options = array('conditions' => array('MUser.' . $this->MUser->primaryKey => $id));

		$mUser = $this->MUser->find('first', $options);
		$this->set('mUser', $mUser);

		// 保護者メールアドレスをカンマ区切りで読み込み
		if (!strstr($mUser['MUser']['parent_address'], ',')) {
			$tparent_address[0] = $mUser['MUser']['parent_address'];
			$parent1 = $tparent_address[0];
			$parent2 = '';
		} else {
			$tparent_address = split( ',', $mUser['MUser']['parent_address']);
			$parent1 = $tparent_address[0];
			$parent2 = $tparent_address[1];
		}

		$this->set('parent1', $parent1);
		$this->set('parent2', $parent2);
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			// シーケンスNOが更新されていた場合、メッセージを表示
			$add_index_number = $this->get_index_number(COLUMN_USER_IDX);
			$add_index_number = PREFIX_USER_IDS.$add_index_number;
			// ユーザIDはバリデーションで重複チェックのため、コメントアウト
			//if ($add_index_number !== $this->request->data['MUser']['user_ids']) {
			// $this->Session->setFlash(__('The m user ID overlaps.'));
			//} else {
				// 保護者メールアドレスをカンマ区切りで保存
				$this->request->data['MUser']['parent_address'] =
					$this->request->data['MUser']['parent_address1'] . ',' . $this->request->data['MUser']['parent_address2'];
				// ソルトの生成
				$strSalt = $this->RandomPassword->createSalt();
				// パスワードのハッシュ値変換
				$strPasswd = $this->request->data['MUser']['auth_pass'];
				$this->request->data['MUser']['hash_pass'] = AuthComponent::password($strPasswd, $strSalt);
				$this->request->data['MUser']['ins_date'] = date('Y-m-d H:i:s');
				// 登録時にユーザIDを登録 ※グループ登録時に使用
				$this->request->data['MUser']['create_user_id'] = $this->Auth->User('id');
				$this->MUser->create();

				$this->MUser->set( $this->request->data );
				if ( $this->MUser->validates() ) {
//$this->log($muser, LOG_DEBUG);

					try {
						App::import("Controller", "Emails");
						// （テスト登録メール送信）
						$sent_email = new EmailsController();
						// ユーザ登録完了メールの内容設定
						$param = array(
								'user_name' => $this->request->data['MUser']['user_name'],
								'user_ids' => $this->request->data['MUser']['user_ids'],
								'password' => $strPasswd,
								'to' => $this->request->data['MUser']['admin_address'],
						);
						$sent_email->send(EmailsController::MAIL_KEY_USER_ENTRY, $param);

						$muser =$this->MUser->save($this->request->data);

						if (!empty($muser)) {
							$this->request->data['MAuth']['user_id'] = $this->MUser->id;
							$this->request->data['MAuth']['salt'] = $strSalt;
							$this->request->data['MAuth']['password'] = $this->request->data['MUser']['hash_pass'];

							// Authもあわせて保存
							$this->MUser->Auth->save($this->request->data['MAuth']);
						}

						$ancestor_group_id = $this->request->data['MUser']['ancestor_group_id'];
						$options = array('conditions' => array('tSection.group_id' => $ancestor_group_id), 'fields' => array('id'));
						$tsectionParent = $this->MGroup->tSection->find('first', $options);
						$parent_id = isset($tsectionParent['tSection']['id']) ? $tsectionParent['tSection']['id']: 0;
						// TSectionもあわせて保存
						$tsection_data['TSection']['id'] = NULL;
						$tsection_data['TSection']['parent_id'] = $parent_id;
						$tsection_data['TSection']['group_id'] = $ancestor_group_id;
						$tsection_data['TSection']['user_id'] = $this->MUser->id;
						$this->MGroup->tSection->save($tsection_data['TSection']);

						// シーケンスNOを更新する　※採番テーブルからIndexNoを更新する
						$next_index_number = $this->set_index_number(COLUMN_USER_IDX);

						// $this->Session->setFlash(__('The m user has been saved.'));

						return $this->redirect(array('action' => 'index'));
					} catch (Exception $e) {
						$this->MUser->invalidate('admin_address', $GLOBALS['message']->getMessage(MESSAGE_LOGIN_ERROR_022));
					}
				} else {
					// $this->Session->setFlash(__('The m user could not be saved. Please, try again.'));
				}
			//}
		}

		// ユーザID初期値生成　※採番テーブルからLAST_IDを取得する
		$new_index_number = $this->get_index_number(COLUMN_USER_IDX);
		//  ユーザ登録時のIDのデフォルト表示を以下で行うが不要であるため削除した
		//$this->request->data['MUser']['user_ids'] = PREFIX_USER_IDS.$new_index_number;

		// ランダムな文字列を生成する
		$auth_pass = $this->RandomPassword->createPassword(8, 'alnum');
		$this->set('auth_pass', $auth_pass);

		$userTypes = Configure::read('m_user_types');
		if ($this->Auth->User('user_type_id') == '3') {
			//（管理者権限のみ、管理者・教師・生徒に絞る）
			unset($userTypes['9']);
		}
		$this->set(compact('userTypes'));

		//（管理者－所属グループ絞込み）
		$conditions = $this->slctGroupConditions();
		$conditions += array('MGroup.delete_flg = 0');
		$allGroups = $this->MGroup->find('list', array('fields' => array('id','group_name'), 'conditions' => $conditions));
		$this->set(compact('allGroups'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->MUser->exists($id)) {
			throw new NotFoundException(__('Invalid m user'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if (isset($this->request->data['pass_reset'])) {
				// リセットフラグを立てる
				$this->request->data['MUser']['reset_flg'] = '1';
			} else {
				// 保護者メールアドレスをカンマ区切りで保存
				$this->request->data['MUser']['parent_address'] =
					$this->request->data['MUser']['parent_address1'] . ',' . $this->request->data['MUser']['parent_address2'];
				// ソルトの生成
				$strSalt = $this->RandomPassword->createSalt();
				// パスワードのハッシュ値変換
				$strPasswd = $this->request->data['MUser']['auth_pass'];
				$this->request->data['MUser']['hash_pass'] = AuthComponent::password($strPasswd, $strSalt);
				if ($this->MUser->save($this->request->data)) {
//$this->log($this->request->data, LOG_DEBUG);
					$options = array('conditions' => array('Auth.user_id' => $id));
					$mauth_data = $this->MUser->Auth->find('first', $options);
					// リセット時にAuthをあわせて更新
					if ($this->request->data['MUser']['reset_flg'] === '1') {
						$this->request->data['MAuth']['id'] = $mauth_data['Auth']['id'];
						$this->request->data['MAuth']['user_id'] = $this->MUser->id;
						$this->request->data['MAuth']['salt'] = $strSalt;
						$this->request->data['MAuth']['password'] = $this->request->data['MUser']['hash_pass'];

						// Authもあわせて保存
						$this->MUser->Auth->save($this->request->data['MAuth']);
					}

					$ancestor_group_id = $this->request->data['MUser']['ancestor_group_id'];
					$options = array('conditions' => array('tSection.group_id' => $ancestor_group_id), 'fields' => array('id'));
					$tsectionParent = $this->MGroup->tSection->find('first', $options);
					$parent_id = isset($tsectionParent['tSection']['id']) ? $tsectionParent['tSection']['id']: 0;

					$options = array('conditions' => array('tSection.user_id' => $id), 'fields' => array('id'));
					$tsectionParent = $this->MGroup->tSection->find('first', $options);
					$tsection_id = !empty($tsectionParent['tSection']['id']) ? $tsectionParent['tSection']['id']: NULL;
					// TSectionもあわせて保存
					$tsection_data['TSection']['id'] = $tsection_id;
					$tsection_data['TSection']['parent_id'] = $parent_id;
					$tsection_data['TSection']['group_id'] = $ancestor_group_id;
					$tsection_data['TSection']['user_id'] = $this->MUser->id;
					$this->MGroup->tSection->save($tsection_data['TSection']);

					// リセット時に登録完了メール送信
					if ($this->request->data['MUser']['reset_flg'] === '1') {
						App::import("Controller", "Emails");
						// （テスト登録メール送信）
						$sent_email = new EmailsController();
						// ユーザ登録完了メールの内容設定
						$param = array(
								'user_name' => $this->request->data['MUser']['user_name'],
								'user_ids' => $this->request->data['MUser']['user_ids'],
								'password' => $strPasswd,
								'to' => $this->request->data['MUser']['admin_address'],
						);
						$sent_email->send(EmailsController::MAIL_KEY_USER_ENTRY, $param);
					}

					// $this->Session->setFlash(__('The m user has been saved.'));
					return $this->redirect(array('action' => 'index'));
				} else {
					// $this->Session->setFlash(__('The m user could not be saved. Please, try again.'));
				}
			}
		} else {
			$options = array('conditions' => array('MUser.' . $this->MUser->primaryKey => $id));
			$this->request->data = $this->MUser->find('first', $options);
			// 保護者メールアドレスをカンマ区切りで読み込み
			if (!strstr($this->request->data['MUser']['parent_address'], ',')) {
				$tparent_address = $this->request->data['MUser']['parent_address'];
				$this->request->data['MUser']['parent_address1'] = $tparent_address;
				$this->request->data['MUser']['parent_address2'] = '';
			} else {
				$tparent_address = split( ',', $this->request->data['MUser']['parent_address']);
				$this->request->data['MUser']['parent_address1'] = $tparent_address[0];
				$this->request->data['MUser']['parent_address2'] = $tparent_address[1];
			}
			// 上位グループIDを取得（先頭）
			$options = array('conditions' => array('tSection.user_id' => $id), 'fields' => array('group_id'));
			$tsection = $this->MGroup->tSection->find('first', $options);
			$ancestor_group_id = isset($tsection['tSection']['group_id']) ? $tsection['tSection']['group_id']: 0;
			$this->request->data['MUser']['ancestor_group_id'] = $ancestor_group_id;
		}
		$userTypes = Configure::read('m_user_types');
		if ($this->Auth->User('user_type_id') == '3') {
			//（管理者権限のみ、管理者・教師・生徒に絞る）
			unset($userTypes['9']);
		}
		$this->set(compact('userTypes'));

		//（管理者－所属グループ絞込み）
		$conditions = $this->slctGroupConditions();
		$conditions += array('MGroup.delete_flg = 0');
		$allGroups = $this->MGroup->find('list', array('fields' => array('id','group_name'), 'conditions' => $conditions));
		$this->set(compact('allGroups'));

		// ランダムな文字列を生成する（リセット時に再設定）
		$auth_pass = $this->RandomPassword->createPassword(8, 'alnum');
		$this->set('auth_pass', $auth_pass);
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		if (!$this->MUser->exists($id)) {
			throw new NotFoundException(__('Invalid m user'));
		}
		if ($this->request->is(array('post', 'put'))) {
			$delete_exc = true;
			// 同じユーザIDの配信対象全てを配信停止に設定する
			$options = array('conditions' => array('TSubmit.user_id'=> $id));
			$tsubmit_data = $this->TSubmit->find('all', $options);
			if (!empty($tsubmit_data)) {
				foreach($tsubmit_data as $key => $data){
					$tsubmit_data[$key]['TSubmit']['unsubscribe'] = 1;
				}
				$delete_exc = $this->TSubmit->saveAll($tsubmit_data);
			}
			// エラーがなければ、削除
			if ($delete_exc && $this->MUser->save($this->request->data)) {
				// $this->Session->setFlash(__('The m user has been deleted.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$options = array('conditions' => array('MUser.' . $this->MUser->primaryKey => $id));
				$this->set('mUser', $this->MUser->find('first', $options));
				// $this->Session->setFlash(__('The m user could not be deleted. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('MUser.' . $this->MUser->primaryKey => $id));
			$mUser = $this->MUser->find('first', $options);
			$this->set('mUser', $mUser);

			// 保護者メールアドレスをカンマ区切りで読み込み
			if (!strstr($mUser['MUser']['parent_address'], ',')) {
				$tparent_address[0] = $mUser['MUser']['parent_address'];
				$parent1 = $tparent_address[0];
				$parent2 = '';
			} else {
				$tparent_address = split( ',', $mUser['MUser']['parent_address']);
				$parent1 = $tparent_address[0];
				$parent2 = $tparent_address[1];
			}

			$this->set('parent1', $parent1);
			$this->set('parent2', $parent2);

		}
		$userTypes = $this->MUser->UserType->find('list', array('fields' => array('id','user_type_name')));
		$this->set(compact('userTypes'));
	}


/**
 * multi_add method
 *
 * @return void
 */
	public function multi_add() {
		// ****ファイルサイズ(50MB)エラー****
		if ($this->request->is('put')) {
			if ($_SERVER['CONTENT_LENGTH'] > (10*1024)) {
				// $this->Session->setFlash(__('Oh.. too match big'));
				$this->Session->write('tmp_data', $this->request->data);
				$this->redirect($this->referer());
			}
		} elseif ($this->Session->read('tmp_data')) {
			$this->request->data = $this->Session->read('tmp_data');
			$this->Session->delete('tmp_data');
		}

		if (!empty($this->data)) {
			$tmp_file = $this->data['csv']['attachment']['tmp_name'];
			$filename = DIR_CSVFILE_UPLOAD.DS.$_FILES['data']['name']['csv']['attachment'];

			// Uploadファイルのフルパス
			$filename = dirname(APP) . $filename;

			// ファイル拡張子(csv)をチェック
			$error_ext = false;
			$info = new SplFileInfo($filename);
			$ext = $info->getExtension();
			if( $ext !== "csv" ) {
				$error_ext = true;
				$ext_errors['csv']['attachment'] =
					sprintf($GLOBALS['message']->getMessage(MESSAGE_LOGIN_ERROR_005), $GLOBALS['message']->getMessage(MESSAGE_USER_ITEM_007));
				$this->set('validationErrors', $ext_errors);
			}

			if (!$error_ext && is_uploaded_file($tmp_file) === true){
				copy($tmp_file, $filename); // アップロードされたファイルを新しい位置に移動

				// ロケール設定してCSV(SJIS)を読み込み
				setlocale(LC_ALL, 'ja_JP.UTF-8');
				$data = file_get_contents($filename);
				$data = mb_convert_encoding($data, 'UTF-8', 'sjis-win');
				$list = explode("\r\n",$data);
				// 一時ファイルディレクトリにファイルを作成します
				$read_file = tempnam(sys_get_temp_dir(), 'Tux');

				$fp = fopen($read_file, "w");
				foreach ($list as $idx => $line) {
					if (empty($line)) {
						continue;
					}
					if ($idx ==0) {
						// カラム名のヘッダを設定
						$fields = array("user_ids", "user_name", "user_type_id", "parent_address", "admin_address", "create_user_id");
					} else {

						$fields = explode(',', $line);
						// ユーザ種別をコードに変換
						if (Configure::read('m_user_types.1') == $fields[2]) {
							$fields[2] = 1;
						} elseif (Configure::read('m_user_types.2') == $fields[2]) {
							$fields[2] = 2;
						} elseif (Configure::read('m_user_types.3') == $fields[2]) {
							$fields[2] = 3;
						} elseif (Configure::read('m_user_types.9') == $fields[2]) {
							$fields[2] = 9;
						} else {
							$fields[2] = 1;
						}
						// 保護者メールアドレスをまとめる
						$fields[3] = $fields[3].','.$fields[4];
						array_splice($fields, 4, 1);

						// 作成者ユーザIDを追加
						$fields[5] = $this->Auth->User('id');

						// ユーザIDを更新する
						$next_index_number = $this->set_index_number(COLUMN_USER_IDX);
					}
					mb_convert_variables('sjis-win', 'UTF-8', $fields);
					fputcsv($fp, $fields);
				}
				fclose($fp);

				// MUser、MAuthの登録トランザクション
				$db = $this->MUser->getDataSource();
				$db->begin($this->MUser);
				$db2 = $this->MGroup->getDataSource();
				$db2->begin($this->MGroup);

				// （テストCSVインポート）
				//$this->Property->importCsv($filename);
				$result = $this->import($read_file);
//$this->log($result, LOG_DEBUG);
				if (!$result) {
					// MUserのcommitをrollback
					$db->rollback($this->MUser);
					// MGroupのcommitをrollback
					$db2->rollback($this->MGroup);
					$modelClass = $this->modelClass;
					$this->set('validationErrors', $this->$modelClass->getImportErrors());
				} else {
					$result_send = true;
					foreach($result as $key => $muser){
						$user_ids = $muser['MUser']['user_ids'];
						// ランダムな文字列を生成する
						$strPasswd = $this->RandomPassword->createPassword(8, 'alnum');
						// ソルトの生成
						$strSalt = $this->RandomPassword->createSalt();
						// パスワードのハッシュ値変換
						$authPasswd = AuthComponent::password($strPasswd, $strSalt);

						$options = array('conditions' => array('user_ids' => $user_ids));
						$this->MUser->recursive = -1;
						$muser_data = $this->MUser->find('first', $options);
						if (!empty($muser_data)) {
							$this->MUser->Auth->create();
							$mauth_data['MAuth']['user_id'] = $muser_data['MUser']['id'];
							$mauth_data['MAuth']['salt'] = $strSalt;
							$mauth_data['MAuth']['password'] = $authPasswd;
							// Authもあわせて保存
							$this->MUser->Auth->save($mauth_data['MAuth']);

							$group_id = $this->data['Post']['group_id'];
							$options = array('conditions' => array('tSection.group_id' => $group_id), 'fields' => array('id'));
							$tsectionParent = $this->MGroup->tSection->find('first', $options);
							$parent_id = isset($tsectionParent['tSection']['id']) ? $tsectionParent['tSection']['id']: 0;
							// TSectionもあわせて保存
							$tsection_data['TSection']['id'] = NULL;
							$tsection_data['TSection']['parent_id'] = $parent_id;
							$tsection_data['TSection']['group_id'] = $group_id;
							$tsection_data['TSection']['user_id'] = $muser_data['MUser']['id'];
							$this->MGroup->tSection->save($tsection_data['TSection']);

							try {
								App::import("Controller", "Emails");
								// （テスト登録メール送信）
								$sent_email = new EmailsController();
								// ユーザ登録完了メールの内容設定
								$param = array(
										'user_name' => $muser_data['MUser']['user_name'],
										'user_ids' => $muser_data['MUser']['user_ids'],
										'password' => $strPasswd,
										'to' => $muser_data['MUser']['admin_address'],
								);
								$sent_email->send(EmailsController::MAIL_KEY_USER_ENTRY, $param);
							} catch (Exception $e) {
								// メール送信エラー時は、中断しrollback
								$result_send = false;
								$mail_errors[]['validation']['admin_address'][$key] = $GLOBALS['message']->getMessage(MESSAGE_LOGIN_ERROR_022);
								$this->set('validationErrors', $mail_errors);
								// 登録を中断
								break;
							}
						}
					}

					if (!$result_send) {
						// MUser、MAuthのcommitをrollback
						$db->rollback($this->MUser);
						// MGroupのcommitをrollback
						$db2->rollback($this->MGroup);
					} else {
						// MUser、MAuthのcommit
						$db->commit($this->MUser);
						// MGroupのcommit
						$db2->commit($this->MGroup);

						return $this->redirect(array('action' => 'index'));
					}
				}
			}
		}
		//（管理者ユーザ－所属グループ絞込み）
		$conditions = $this->slctGroupConditions();
		$conditions += array('MGroup.delete_flg = 0');
		$this->set('mGroups', $this->MGroup->find('list', array('fields' => array('id','group_name'), 'conditions' => $conditions)));
	}

/**
 * export method
 *
 * @return void
*/
	private function export() {
		$this->autoRender = false;
		$modelClass = $this->modelClass;
		$this->response->type('Content-Type: text/csv');
		$this->response->download('hoge.csv');
		$this->response->body($this->$modelClass->exportCSV());
	}

/**
 * import method
 *
 * @return void
 */
	private function import($file_name = null) {
		$modelClass = $this->modelClass;
		$result = $this->$modelClass->importCSV($file_name ,array(), true);
		if($errors = $this->$modelClass->getImportErrors()) {
			$this->log($errors, LOG_DEBUG);
			return false;
		}
		return $result;
	}
}
