<?php
App::uses('AppController', 'Controller');
/**
 * File Controller
 *
*/
class FileController extends AppController {

	/**
	 * view method
	 *
	 * @throws NotFoundException
	 * @param string $id
	 * @return void
	 */
	public function download($file_name = null) {
		// viewを使用しない
		$this->autoRender = false;

		// ファイル名の区切りを置換（ユーザID@教材ID.zip）
		$file_name = str_replace("@", DS, $file_name);
		// ファイルがcake/app/webroot/file以下にあるとき
		$file_path = WWW_ROOT . 'files' . DS . $file_name;
		// response->file()でダウンロードもしくは表示するファイルをセット
		$this->response->file($file_path);

		// 単にダウンロードさせる
		$this->response->download($file_name);
	}
}
