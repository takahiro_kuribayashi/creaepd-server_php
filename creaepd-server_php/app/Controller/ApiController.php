<?php

App::uses('AppController', 'Controller');

class ApiController extends AppController {
	//CakeのJsonViewとXmlViewを使用するので、RequestHandler必須。
	public $components = array('Session', 'RequestHandler');

	// JSONやXMLにして返す値を格納するための配列です。
	protected $result = array();

	public function beforeFilter() {
		parent::beforeFilter();

		// Ajaxでないアクセスは禁止。直アクセスを許すとXSSとか起きかねないので。
//		if (!$this->request->is('ajax')) throw new BadRequestException('Ajax以外でのアクセスは許可していません。');

		//meta情報とかを返すといいですね。とりあえずいまアクセスしているurlとhttp methodでも含めておきましょう
		$this->result['meta'] = array(
			'url' => $this->request->here,
			'method' => $this->request->method(),
		);

		// ***APIユーザ認証が出来るまで全アクションのアクセスを許可***
		$this->Auth->allow();
		// CakePHPの場合
		$this->header('Access-Control-Allow-Origin: *');
		$this->header('Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept');

		// nosniffつけるべし。じゃないとIEでXSS起きる可能性あり。
		$this->response->header('X-Content-Type-Options', 'nosniff');

		App::uses('Folder', 'Utility');
		App::uses('File', 'Utility');
	}

	public function beforeRender() {
		// jsonp対応。JsonpViewクラスを自作(後述)
		if ($this->viewClass === 'Json' && isset($this->request->query['callback'])) {
			$this->viewClass = 'Jsonp';
		}
	}

	// 成功系処理。$this->resultに値いれる
	protected function success($response = array()) {
		$this->result['response'] = $response;

		$this->set('meta', $this->result['meta']);
		$this->set('response', $this->result['response']);
		$this->set('_serialize', array('meta', 'response'));
	}

	// エラー系処理。$this->resultに値いれる
	protected function error($message = '', $code) {
		$this->result['error']['message'] = $message;
		$this->result['error']['code'] = $code;

		//ちゃんと400ステータスコードにするの大事。後述
		$this->response->statusCode(400);
		$this->set('meta', $this->result['meta']);
		$this->set('error', $this->result['error']);
		$this->set('_serialize', array('meta', 'error'));
	}

	// バリデーションエラー系処理。$this->resultに値いれる
	protected function validationError($modelName, $validationError = array()) {
		$this->result['error']['message'] = 'Validation Error';
		$this->result['error']['code'] = '422'; //エラーコードはプロジェクトごとに定義すべし
		$this->result['error']['validation'][$modelName] = array();
		foreach ($validationError as $key => $value) {
			$this->result['error']['validation'][$modelName][$key] = $value[0];
		}

		//ちゃんと400ステータスコードにするの大事。後述
		$this->response->statusCode(400);
		$this->set('meta', $this->result['meta']);
		$this->set('error', $this->result['error']);
		$this->set('_serialize', array('meta', 'error'));
	}

	//（注意）同メソッド呼び出し側で、TSessionの使用モデルが必要
	public function accessAuth($user_ids, $session_id) {
		// 認証情報の取得
		$session = $this->TSession->getData($session_id);
		if ($session) {
			$auth_len = strlen('Auth|');
			$auth_pos = strpos($session[0]['cake_sessions']['data'] ,'Auth|');
			$auth_data = substr($session[0]['cake_sessions']['data'], ($auth_pos + $auth_len));
			$auth_data = unserialize($auth_data);
//$this->log($auth_data, LOG_DEBUG);
		}

		// 結合条件（認証マスタ）
		$joins = array(
				array(
						'table'		 => 'm_auths',
						'alias'		 => 'MAuth',
						'type'		 => 'INNER',
						'conditions' => array('MUser.id = MAuth.user_id')
				)
		);
		// 検索条件
		$conditions = array(
				array(
						'MUser.user_ids' => $user_ids,
						'MUser.delete_flg' => 0 // 削除ユーザはログイン不可
				)
		);
		// ユーザ情報の取得
		$options = array('conditions' => $conditions, 'joins' => $joins);
		$muser_data = $this->MUser->find('first', $options);
		// ユーザ情報と認証情報の一致をチェック
		if ($muser_data) {
			if ($auth_data['User']['user_ids'] !== $muser_data['MUser']['user_ids']) {
				throw new BadRequestException(MESSAGE_API_ERROR_004);
			}
			if ($auth_data['User']['Auth']['salt'] !== $muser_data['Auth']['salt'] ||
			$auth_data['User']['Auth']['password'] !== $muser_data['Auth']['password']) {
				throw new BadRequestException(MESSAGE_API_ERROR_004);
			}
		} else {
			throw new BadRequestException(MESSAGE_API_ERROR_003);
		}
	}
}
