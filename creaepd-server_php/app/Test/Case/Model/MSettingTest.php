<?php
App::uses('MSetting', 'Model');

/**
 * MSetting Test Case
 *
 */
class MSettingTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.m_setting'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->MSetting = ClassRegistry::init('MSetting');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->MSetting);

		parent::tearDown();
	}

}
