<?php
App::uses('MUserType', 'Model');

/**
 * MUserType Test Case
 *
 */
class MUserTypeTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.m_user_type'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->MUserType = ClassRegistry::init('MUserType');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->MUserType);

		parent::tearDown();
	}

}
