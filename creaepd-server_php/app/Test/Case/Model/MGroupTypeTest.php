<?php
App::uses('MGroupType', 'Model');

/**
 * MGroupType Test Case
 *
 */
class MGroupTypeTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.m_group_type'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->MGroupType = ClassRegistry::init('MGroupType');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->MGroupType);

		parent::tearDown();
	}

}
