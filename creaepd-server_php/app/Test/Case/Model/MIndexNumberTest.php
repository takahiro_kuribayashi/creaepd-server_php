<?php
App::uses('MIndexNumber', 'Model');

/**
 * MIndexNumber Test Case
 *
 */
class MIndexNumberTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.m_index_number'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->MIndexNumber = ClassRegistry::init('MIndexNumber');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->MIndexNumber);

		parent::tearDown();
	}

}
