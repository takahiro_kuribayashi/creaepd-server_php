<?php
App::uses('TSchedule', 'Model');

/**
 * TSchedule Test Case
 *
 */
class TScheduleTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.t_schedule'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->TSchedule = ClassRegistry::init('TSchedule');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->TSchedule);

		parent::tearDown();
	}

}
