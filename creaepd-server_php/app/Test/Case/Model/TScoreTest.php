<?php
App::uses('TScore', 'Model');

/**
 * TScore Test Case
 *
 */
class TScoreTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.t_score',
		'app.submit',
		'app.subtitle'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->TScore = ClassRegistry::init('TScore');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->TScore);

		parent::tearDown();
	}

}
