<?php
App::uses('TSubmit', 'Model');

/**
 * TSubmit Test Case
 *
 */
class TSubmitTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.t_submit'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->TSubmit = ClassRegistry::init('TSubmit');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->TSubmit);

		parent::tearDown();
	}

}
