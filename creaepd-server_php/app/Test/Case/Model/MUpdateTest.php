<?php
App::uses('MUpdate', 'Model');

/**
 * MUpdate Test Case
 *
 */
class MUpdateTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.m_update'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->MUpdate = ClassRegistry::init('MUpdate');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->MUpdate);

		parent::tearDown();
	}

}
