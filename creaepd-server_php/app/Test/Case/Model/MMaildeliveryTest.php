<?php
App::uses('MMaildelivery', 'Model');

/**
 * MMaildelivery Test Case
 *
 */
class MMaildeliveryTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.m_maildelivery'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->MMaildelivery = ClassRegistry::init('MMaildelivery');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->MMaildelivery);

		parent::tearDown();
	}

}
