<?php
App::uses('MAuth', 'Model');

/**
 * MAuth Test Case
 *
 */
class MAuthTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.m_auth',
		'app.y'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->MAuth = ClassRegistry::init('MAuth');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->MAuth);

		parent::tearDown();
	}

}
