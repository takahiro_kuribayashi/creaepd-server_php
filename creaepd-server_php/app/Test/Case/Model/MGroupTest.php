<?php
App::uses('MGroup', 'Model');

/**
 * MGroup Test Case
 *
 */
class MGroupTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.m_group',
		'app.m_group_type'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->MGroup = ClassRegistry::init('MGroup');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->MGroup);

		parent::tearDown();
	}

}
