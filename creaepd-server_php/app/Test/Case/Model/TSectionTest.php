<?php
App::uses('TSection', 'Model');

/**
 * TSection Test Case
 *
 */
class TSectionTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.t_section'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->TSection = ClassRegistry::init('TSection');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->TSection);

		parent::tearDown();
	}

}
