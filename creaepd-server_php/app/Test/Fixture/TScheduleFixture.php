<?php
/**
 * TScheduleFixture
 *
 */
class TScheduleFixture extends CakeTestFixture {

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'key' => 'primary', 'comment' => 'スケジュールID	 スケジュールID'),
		'schedule_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'comment' => 'スケジュールID	 スケジュールID'),
		'group_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'comment' => '配信グループID	 配信グループID'),
		'user_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'comment' => '配信ユーザID	 配信ユーザID'),
		'submit_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'comment' => '提出ID	 提出ID'),
		'status' => array('type' => 'integer', 'null' => false, 'default' => '0', 'length' => 1, 'comment' => '配信状態	 配信状態'),
		'indexes' => array(
			'PRIMARY' => array('column' => 'id', 'unique' => 1)
		),
		'tableParameters' => array('charset' => 'utf8', 'collate' => 'utf8_unicode_ci', 'engine' => 'InnoDB')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'id' => 1,
			'schedule_id' => 1,
			'group_id' => 1,
			'user_id' => 1,
			'submit_id' => 1,
			'status' => 1
		),
	);

}
