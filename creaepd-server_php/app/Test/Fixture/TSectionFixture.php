<?php
/**
 * TSectionFixture
 *
 */
class TSectionFixture extends CakeTestFixture {

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'key' => 'primary', 'comment' => '所属ID	 所属ID'),
		'group_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'comment' => 'グループID	 グループID'),
		'user_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'comment' => 'ユーザID	 ユーザID'),
		'indexes' => array(
			'PRIMARY' => array('column' => 'id', 'unique' => 1)
		),
		'tableParameters' => array('charset' => 'utf8', 'collate' => 'utf8_unicode_ci', 'engine' => 'InnoDB')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'id' => 1,
			'group_id' => 1,
			'user_id' => 1
		),
	);

}
