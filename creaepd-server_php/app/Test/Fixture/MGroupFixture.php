<?php
/**
 * MGroupFixture
 *
 */
class MGroupFixture extends CakeTestFixture {

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'key' => 'primary', 'comment' => 'グループID	 インデックス'),
		'group_id' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 20, 'collate' => 'utf8_unicode_ci', 'comment' => 'グループID	 グループID', 'charset' => 'utf8'),
		'group_name' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 50, 'collate' => 'utf8_unicode_ci', 'comment' => 'グループ名	 グループ名', 'charset' => 'utf8'),
		'group_type_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'comment' => 'グループ種別ID	 グループ種別'),
		'delete_flg' => array('type' => 'string', 'null' => false, 'default' => '1', 'length' => 1, 'collate' => 'utf8_unicode_ci', 'comment' => '\'削除フラグ	 削除フラグ', 'charset' => 'utf8'),
		'ins_date' => array('type' => 'datetime', 'null' => false, 'default' => null, 'comment' => '登録日	 ユーザの追加日時'),
		'indexes' => array(
			'PRIMARY' => array('column' => 'id', 'unique' => 1)
		),
		'tableParameters' => array('charset' => 'utf8', 'collate' => 'utf8_unicode_ci', 'engine' => 'InnoDB')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'id' => 1,
			'group_id' => 'Lorem ipsum dolor ',
			'group_name' => 'Lorem ipsum dolor sit amet',
			'group_type_id' => 1,
			'delete_flg' => 'Lorem ipsum dolor sit ame',
			'ins_date' => '2014-10-23 18:24:11'
		),
	);

}
