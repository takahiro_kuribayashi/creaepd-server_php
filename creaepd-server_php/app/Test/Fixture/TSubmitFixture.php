<?php
/**
 * TSubmitFixture
 *
 */
class TSubmitFixture extends CakeTestFixture {

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'key' => 'primary', 'comment' => '提出ID	 提出ID'),
		'submit_no' => array('type' => 'integer', 'null' => false, 'default' => null, 'comment' => '提出番号	 提出番号'),
		'resource_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'comment' => '教材ID	 教材のID'),
		'user_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'comment' => '提出者ID	 提出者のユーザID'),
		'group_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'comment' => 'グループID'),
		'submit_state' => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => 2, 'comment' => '提出状態	 提出状態'),
		'check_state' => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => 2, 'comment' => '採点状態'),
		'submit_date' => array('type' => 'datetime', 'null' => true, 'default' => null, 'comment' => '提出日	 提出日時'),
		'check_date' => array('type' => 'datetime', 'null' => true, 'default' => null, 'comment' => '採点日	 採点日時'),
		'result' => array('type' => 'integer', 'null' => true, 'default' => null, 'length' => 3, 'comment' => '採点結果	 採点結果'),
		'unsubscribe' => array('type' => 'integer', 'null' => false, 'default' => '0', 'length' => 1, 'comment' => '配信停止	 配信停止'),
		'start_datetime' => array('type' => 'datetime', 'null' => false, 'default' => null, 'comment' => '配信開始日時	 配信開始日時'),
		'end_datetime' => array('type' => 'datetime', 'null' => false, 'default' => null, 'comment' => '配信終了日時	 配信終了日時'),
		'indexes' => array(
			'PRIMARY' => array('column' => 'id', 'unique' => 1)
		),
		'tableParameters' => array('charset' => 'utf8', 'collate' => 'utf8_unicode_ci', 'engine' => 'InnoDB')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'id' => 1,
			'submit_no' => 1,
			'resource_id' => 1,
			'user_id' => 1,
			'group_id' => 1,
			'submit_state' => 1,
			'check_state' => 1,
			'submit_date' => '2014-11-17 15:35:53',
			'check_date' => '2014-11-17 15:35:53',
			'result' => 1,
			'unsubscribe' => 1,
			'start_datetime' => '2014-11-17 15:35:53',
			'end_datetime' => '2014-11-17 15:35:53'
		),
	);

}
