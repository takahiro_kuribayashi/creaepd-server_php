<?php
/**
 * MMaildeliveryFixture
 *
 */
class MMaildeliveryFixture extends CakeTestFixture {

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'key' => 'primary', 'comment' => 'ID	 メール配信ID	 メール配信ID'),
		'mail_key' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 20, 'collate' => 'utf8_unicode_ci', 'comment' => 'メールKEY', 'charset' => 'utf8'),
		'delivery_flg' => array('type' => 'boolean', 'null' => false, 'default' => null, 'comment' => 'メール配信有無'),
		'template_name' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 100, 'collate' => 'utf8_unicode_ci', 'comment' => 'テンプレート名', 'charset' => 'utf8'),
		'indexes' => array(
			'PRIMARY' => array('column' => 'id', 'unique' => 1)
		),
		'tableParameters' => array('charset' => 'utf8', 'collate' => 'utf8_unicode_ci', 'engine' => 'InnoDB')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'id' => 1,
			'mail_key' => 'Lorem ipsum dolor ',
			'delivery_flg' => 1,
			'template_name' => 'Lorem ipsum dolor sit amet'
		),
	);

}
