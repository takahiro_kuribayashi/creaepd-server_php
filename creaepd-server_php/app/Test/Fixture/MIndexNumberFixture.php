<?php
/**
 * MIndexNumberFixture
 *
 */
class MIndexNumberFixture extends CakeTestFixture {

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'key' => 'primary', 'comment' => 'ID	 インデックス'),
		'resource_idx' => array('type' => 'integer', 'null' => false, 'default' => null, 'comment' => '教材ID	 教材ID'),
		'submit_idx' => array('type' => 'integer', 'null' => false, 'default' => null, 'comment' => '配信ID	 配信ID'),
		'schedule_idx' => array('type' => 'integer', 'null' => false, 'default' => null, 'comment' => 'スケジュールID	 スケジュールID'),
		'user_idx' => array('type' => 'integer', 'null' => false, 'default' => null, 'comment' => 'ユーザID	 ユーザID'),
		'group_idx' => array('type' => 'integer', 'null' => false, 'default' => null, 'comment' => 'グループID	 グループID'),
		'indexes' => array(
			'PRIMARY' => array('column' => 'id', 'unique' => 1)
		),
		'tableParameters' => array('charset' => 'utf8', 'collate' => 'utf8_unicode_ci', 'engine' => 'InnoDB')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'id' => 1,
			'resource_idx' => 1,
			'submit_idx' => 1,
			'schedule_idx' => 1,
			'user_idx' => 1,
			'group_idx' => 1
		),
	);

}
