<?php
/**
 * MSettingFixture
 *
 */
class MSettingFixture extends CakeTestFixture {

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'key' => 'primary', 'comment' => '設定ID	 設定ID'),
		'language_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'comment' => '言語ID	 言語ID'),
		'logo_file' => array('type' => 'string', 'null' => true, 'default' => null, 'collate' => 'utf8_unicode_ci', 'comment' => 'ロゴファイルパス	 ロゴファイルパス', 'charset' => 'utf8'),
		'color' => array('type' => 'integer', 'null' => true, 'default' => null, 'length' => 2, 'comment' => 'ベースカラー設定	 ベースカラー'),
		'indexes' => array(
			'PRIMARY' => array('column' => 'id', 'unique' => 1)
		),
		'tableParameters' => array('charset' => 'utf8', 'collate' => 'utf8_unicode_ci', 'engine' => 'InnoDB')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'id' => 1,
			'language_id' => 1,
			'logo_file' => 'Lorem ipsum dolor sit amet',
			'color' => 1
		),
	);

}
