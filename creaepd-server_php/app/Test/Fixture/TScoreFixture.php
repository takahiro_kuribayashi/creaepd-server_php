<?php
/**
 * TScoreFixture
 *
 */
class TScoreFixture extends CakeTestFixture {

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'key' => 'primary', 'comment' => 'ID	 採点ID	 採点ID'),
		'submit_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'comment' => '提出ID	 提出ID	 提出ID'),
		'subtitle_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'comment' => '子ジャンルID	 子ジャンルID	 子ジャンルID'),
		'allot' => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => 3, 'comment' => '配点	 配点	 配点'),
		'result' => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => 3, 'comment' => '採点結果	 採点結果	 採点結果'),
		'indexes' => array(
			'PRIMARY' => array('column' => 'id', 'unique' => 1)
		),
		'tableParameters' => array('charset' => 'utf8', 'collate' => 'utf8_unicode_ci', 'engine' => 'InnoDB')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'id' => 1,
			'submit_id' => 1,
			'subtitle_id' => 1,
			'allot' => 1,
			'result' => 1
		),
	);

}
