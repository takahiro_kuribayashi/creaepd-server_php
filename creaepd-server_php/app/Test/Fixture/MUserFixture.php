<?php
/**
 * MUserFixture
 *
 */
class MUserFixture extends CakeTestFixture {

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'key' => 'primary', 'comment' => 'ユーザID	 インデックス'),
		'user_id' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 10, 'collate' => 'utf8_unicode_ci', 'comment' => 'ユーザID	 ユーザID', 'charset' => 'utf8'),
		'user_name' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 50, 'collate' => 'utf8_unicode_ci', 'comment' => 'ユーザ名	 ユーザ名', 'charset' => 'utf8'),
		'user_type_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'comment' => 'ユーザ種別ID	 ユーザ種別'),
		'parent_address' => array('type' => 'string', 'null' => true, 'default' => null, 'collate' => 'utf8_unicode_ci', 'comment' => '保護者メールアドレス	 進捗連絡用メールアドレス', 'charset' => 'utf8'),
		'admin_address' => array('type' => 'string', 'null' => true, 'default' => null, 'collate' => 'utf8_unicode_ci', 'comment' => '管理者メールアドレス	登録完了用メールアドレス', 'charset' => 'utf8'),
		'delete_flg' => array('type' => 'string', 'null' => false, 'default' => '0', 'length' => 1, 'collate' => 'utf8_unicode_ci', 'comment' => '削除フラグ	 削除フラグ', 'charset' => 'utf8'),
		'ins_date' => array('type' => 'datetime', 'null' => false, 'default' => null, 'comment' => '登録日	 ユーザの追加日時'),
		'indexes' => array(
			'PRIMARY' => array('column' => 'id', 'unique' => 1)
		),
		'tableParameters' => array('charset' => 'utf8', 'collate' => 'utf8_unicode_ci', 'engine' => 'InnoDB')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'id' => 1,
			'user_id' => 'Lorem ip',
			'user_name' => 'Lorem ipsum dolor sit amet',
			'user_type_id' => 1,
			'parent_address' => 'Lorem ipsum dolor sit amet',
			'admin_address' => 'Lorem ipsum dolor sit amet',
			'delete_flg' => 'Lorem ipsum dolor sit ame',
			'ins_date' => '2014-10-20 03:30:03'
		),
	);

}
