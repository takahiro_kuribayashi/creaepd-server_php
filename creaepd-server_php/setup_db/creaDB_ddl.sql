-- phpMyAdmin SQL Dump
-- version 2.11.11.3
-- http://www.phpmyadmin.net
--
-- ホスト: dev-crea.cb9cbnnzjufl.ap-northeast-1.rds.amazonaws.com:3306
-- 生成時間: 2015 年 2 月 23 日 10:42
-- サーバのバージョン: 5.6.21
-- PHP のバージョン: 5.5.21

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";

--
-- データベース: `creaDB`
--

-- --------------------------------------------------------

--
-- テーブルの構造 `attachments`
--

CREATE TABLE IF NOT EXISTS `attachments` (
  `id` int(10) unsigned NOT NULL,
  `model` varchar(20) NOT NULL,
  `foreign_key` int(11) NOT NULL,
  `name` varchar(32) NOT NULL,
  `attachment` varchar(255) NOT NULL,
  `dir` varchar(255) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  `size` int(11) DEFAULT '0',
  `active` tinyint(1) DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- テーブルの構造 `cake_sessions`
--

CREATE TABLE IF NOT EXISTS `cake_sessions` (
  `id` varchar(255) NOT NULL DEFAULT '',
  `data` text NOT NULL,
  `expires` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- テーブルの構造 `m_auths`
--

CREATE TABLE IF NOT EXISTS `m_auths` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '認証ID	 認証ID',
  `user_id` int(11) NOT NULL COMMENT 'ユーザID	 ユーザID',
  `salt` varchar(50) COLLATE utf8_unicode_ci NOT NULL COMMENT 'ソルト	 パスワードソルト',
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL COMMENT '認証ハッシュ	 ハッシュ',
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='認証マスタ' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- テーブルの構造 `m_base_colors`
--

CREATE TABLE IF NOT EXISTS `m_base_colors` (
  `id` int(11) NOT NULL COMMENT 'カラーID	 カラーID',
  `color_name` varchar(50) COLLATE utf8_unicode_ci NOT NULL COMMENT 'カラー名	 カラー名',
  `class_name` varchar(10) COLLATE utf8_unicode_ci NOT NULL COMMENT 'クラス名 クラス名'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='カラーマスタ';

-- --------------------------------------------------------

--
-- テーブルの構造 `m_groups`
--

CREATE TABLE IF NOT EXISTS `m_groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'グループID	 インデックス',
  `group_ids` varchar(20) COLLATE utf8_unicode_ci NOT NULL COMMENT 'グループID	 グループID',
  `group_name` varchar(50) COLLATE utf8_unicode_ci NOT NULL COMMENT 'グループ名	 グループ名',
  `group_type_id` int(11) NOT NULL COMMENT 'グループ種別ID	 グループ種別',
  `delete_flg` varchar(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0' COMMENT '''削除フラグ	 削除フラグ',
  `ins_date` datetime NOT NULL COMMENT '登録日	 ユーザの追加日時',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='グループマスタ' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- テーブルの構造 `m_group_types`
--

CREATE TABLE IF NOT EXISTS `m_group_types` (
  `id` int(11) NOT NULL COMMENT 'グループ種別ID	 グループ種別ID',
  `group_type_name` varchar(50) COLLATE utf8_unicode_ci NOT NULL COMMENT 'グループ種別名	 グループ種別名',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='グループ種別マスタ';

-- --------------------------------------------------------

--
-- テーブルの構造 `m_index_numbers`
--

CREATE TABLE IF NOT EXISTS `m_index_numbers` (
  `id` int(11) NOT NULL COMMENT 'ID	 インデックス',
  `resource_idx` int(11) NOT NULL COMMENT '教材ID	 教材ID',
  `submit_idx` int(11) NOT NULL COMMENT '配信ID	 配信ID',
  `schedule_idx` int(11) NOT NULL COMMENT 'スケジュールID	 スケジュールID',
  `user_idx` int(11) NOT NULL COMMENT 'ユーザID	 ユーザID',
  `group_idx` int(11) NOT NULL COMMENT 'グループID	 グループID',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='採番マスタ	 採番マスタ';

-- --------------------------------------------------------

--
-- テーブルの構造 `m_languages`
--

CREATE TABLE IF NOT EXISTS `m_languages` (
  `id` int(11) NOT NULL COMMENT '言語ID	 言語ID',
  `language_name` varchar(50) COLLATE utf8_unicode_ci NOT NULL COMMENT '言語名	 言語名',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='言語マスタ';

-- --------------------------------------------------------

--
-- テーブルの構造 `m_maildeliveries`
--

CREATE TABLE IF NOT EXISTS `m_maildeliveries` (
  `id` int(11) NOT NULL COMMENT 'ID	 メール配信ID	 メール配信ID',
  `mail_key` varchar(30) COLLATE utf8_unicode_ci NOT NULL COMMENT 'メールKEY',
  `delivery_flg` tinyint(1) NOT NULL COMMENT 'メール配信有無',
  `from_addr` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `from_name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `subject` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `template_name` varchar(100) COLLATE utf8_unicode_ci NOT NULL COMMENT 'テンプレート名',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='メール配信マスタ	 メール配信マスタ';

-- --------------------------------------------------------

--
-- テーブルの構造 `m_resources`
--

CREATE TABLE IF NOT EXISTS `m_resources` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '教材ID 教材ID',
  `resource_ids` varchar(20) COLLATE utf8_unicode_ci NOT NULL COMMENT '教材ID	教材ID',
  `resource_name` varchar(50) COLLATE utf8_unicode_ci NOT NULL COMMENT '教材名	 教材名',
  `resource_type` int(2) NOT NULL COMMENT '教材タイプ	 教材タイプ',
  `repeat_flg` tinyint(1) NOT NULL COMMENT '繰り返し学習フラグ 繰り返し学習フラグ',
  `version` int(5) NOT NULL COMMENT 'バージョン	 バージョン',
  `create_user_id` int(11) NOT NULL COMMENT '作成者	 作成者のユーザID',
  `subject_id` int(11) NOT NULL COMMENT 'ジャンル	 ジャンル',
  `style_id` int(11) NOT NULL COMMENT 'スタイル	 スタイル',
  `pdf_file_1` varchar(255) COLLATE utf8_unicode_ci NOT NULL COMMENT 'pdfファイル1',
  `pdf_file_2` varchar(255) COLLATE utf8_unicode_ci NOT NULL COMMENT 'pdfファイル2',
  `mp3_file` varchar(255) COLLATE utf8_unicode_ci NOT NULL COMMENT 'mp3ファイル',
  `delete_flg` tinyint(1) NOT NULL COMMENT '削除フラグ',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='教材マスタ' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- テーブルの構造 `m_resource_files`
--

CREATE TABLE IF NOT EXISTS `m_resource_files` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ID	 ファイルID	  インデックス',
  `resource_id` int(11) NOT NULL COMMENT '教材ID	 教材ID	  教材ID',
  `resource_file_type` int(2) NOT NULL COMMENT 'ファイル種別	 ファイル種別	  ファイル種別',
  `resource_file_name` varchar(50) COLLATE utf8_unicode_ci NOT NULL COMMENT 'ファイル名	 ファイル名	 ファイル名',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='教材ファイルマスタ	 教材ファイルマスタ' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- テーブルの構造 `m_resource_styles`
--

CREATE TABLE IF NOT EXISTS `m_resource_styles` (
  `id` int(11) NOT NULL COMMENT '教材スタイルID	 教材スタイルID',
  `style_name` varchar(50) COLLATE utf8_unicode_ci NOT NULL COMMENT '教材スタイル名	 教材のスタイル名',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='教材スタイルマスタ';

-- --------------------------------------------------------

--
-- テーブルの構造 `m_resource_subjects`
--

CREATE TABLE IF NOT EXISTS `m_resource_subjects` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ジャンルID	 ジャンルID',
  `subject_name` varchar(50) COLLATE utf8_unicode_ci NOT NULL COMMENT 'ジャンル名	 ジャンル名',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='教材ジャンルマスタ' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- テーブルの構造 `m_resource_subtitles`
--

CREATE TABLE IF NOT EXISTS `m_resource_subtitles` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '子ジャンルID	 子ジャンルID	 子ジャンルID',
  `resource_id` int(11) NOT NULL COMMENT '教材ID	 教材ID	 教材ID',
  `subtitle_name` varchar(50) COLLATE utf8_unicode_ci NOT NULL COMMENT '子ジャンル名	 子ジャンル名	 子ジャンル名',
  `question_num` int(3) NOT NULL,
  `allot` int(3) NOT NULL,
  `result` int(3) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='教材子ジャンルマスタ	 教材子ジャンルマスタ' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- テーブルの構造 `m_schedules`
--

CREATE TABLE IF NOT EXISTS `m_schedules` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'スケジュールID	 スケジュールID',
  `schedule_ids` varchar(20) COLLATE utf8_unicode_ci NOT NULL COMMENT 'スケジュールID	 スケジュールID',
  `title` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'タイトル	 タイトル',
  `detail` text COLLATE utf8_unicode_ci COMMENT '詳細	 詳細',
  `version` int(5) NOT NULL COMMENT 'バージョン	 バージョン',
  `start_datetime` datetime DEFAULT NULL COMMENT '開始日時	 開始日付',
  `end_datetime` datetime DEFAULT NULL COMMENT '終了日時	 終了日時',
  `delete_flg` tinyint(1) NOT NULL COMMENT '削除フラグ	削除フラグ',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='スケジュールマスタ' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- テーブルの構造 `m_settings`
--

CREATE TABLE IF NOT EXISTS `m_settings` (
  `id` int(11) NOT NULL COMMENT '設定ID	 設定ID',
  `language_id` int(11) NOT NULL COMMENT '言語ID	 言語ID',
  `logo_file` varchar(255) COLLATE utf8_unicode_ci NOT NULL COMMENT 'ロゴファイル	 ロゴファイル',
  `basecolor_id` int(2) NOT NULL COMMENT 'ベースカラー設定	 ベースカラー',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='設定マスタ';

-- --------------------------------------------------------

--
-- テーブルの構造 `m_updates`
--

CREATE TABLE IF NOT EXISTS `m_updates` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'アップデートID	 アップデートID',
  `version` varchar(20) COLLATE utf8_unicode_ci NOT NULL COMMENT 'バージョン	 バージョン',
  `apk_file` varchar(255) COLLATE utf8_unicode_ci NOT NULL COMMENT 'apkファイル	 apkファイル',
  `description` text COLLATE utf8_unicode_ci NOT NULL COMMENT '概要	 概要',
  `created` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='アップデート' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- テーブルの構造 `m_users`
--

CREATE TABLE IF NOT EXISTS `m_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ユーザID	 インデックス',
  `user_ids` varchar(10) COLLATE utf8_bin NOT NULL COMMENT 'ユーザID	 ユーザID',
  `user_name` varchar(50) COLLATE utf8_unicode_ci NOT NULL COMMENT 'ユーザ名	 ユーザ名',
  `user_type_id` int(11) NOT NULL COMMENT 'ユーザ種別ID	 ユーザ種別',
  `parent_address` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '保護者メールアドレス	 進捗連絡用メールアドレス',
  `admin_address` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '管理者メールアドレス	登録完了用メールアドレス',
  `superuser_flg` tinyint(1) NOT NULL COMMENT 'Superユーザフラグ Superユーザフラグ',
  `create_user_id` int(11) NOT NULL COMMENT '作成者 作成者のユーザID',
  `delete_flg` varchar(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0' COMMENT '削除フラグ	 削除フラグ',
  `ins_date` datetime NOT NULL COMMENT '登録日	 ユーザの追加日時',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='ユーザマスタ' AUTO_INCREMENT=1 ;

ALTER TABLE `m_users` ADD `logout_time` DATETIME NOT NULL COMMENT 'ログアウト日時 前回ログアウトの日時' AFTER `ins_date`;

-- --------------------------------------------------------

--
-- テーブルの構造 `m_user_types`
--

CREATE TABLE IF NOT EXISTS `m_user_types` (
  `id` int(11) NOT NULL COMMENT 'ユーザ種別ID	 ユーザ種別ID',
  `user_type_name` varchar(50) COLLATE utf8_unicode_ci NOT NULL COMMENT 'ユーザ種別名	 ユーザ種別名',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='ユーザ種別マスタ';

-- --------------------------------------------------------

--
-- テーブルの構造 `m_words`
--

CREATE TABLE IF NOT EXISTS `m_words` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '文言ID	 文言ID',
  `language_id` int(11) NOT NULL COMMENT '言語ID	 言語ID',
  `word_key` varchar(10) COLLATE utf8_unicode_ci NOT NULL COMMENT '文言KEY	 文言KEY',
  `word` varchar(100) COLLATE utf8_unicode_ci NOT NULL COMMENT '日本語文言	 日本語の文言',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='文言マスタ' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- テーブルの構造 `pdf_posts`
--

CREATE TABLE IF NOT EXISTS `pdf_posts` (
  `id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- テーブルの構造 `posts`
--

CREATE TABLE IF NOT EXISTS `posts` (
  `id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- テーブルの構造 `t_logs`
--

CREATE TABLE IF NOT EXISTS `t_logs` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ログID	 ログID',
  `uuid` varchar(36) COLLATE utf8_unicode_ci NOT NULL COMMENT 'UUID	 ログ作成時UUID',
  `user_id` varchar(10) COLLATE utf8_unicode_ci NOT NULL COMMENT 'ユーザID	 ログインユーザID',
  `submit_id` varchar(20) COLLATE utf8_unicode_ci NOT NULL COMMENT '提出ID	 提出ID',
  `create_datetime` datetime DEFAULT NULL COMMENT '発生日時	 発生日時',
  `event_id` int(1) NOT NULL COMMENT 'イベントID	 イベントID',
  `page_no` int(7) NOT NULL COMMENT 'ページ番号	 ページ番号',
  `option_data` varchar(1000) COLLATE utf8_unicode_ci NOT NULL COMMENT 'オプション	 オプション',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='ログテーブル' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- テーブルの構造 `t_schedules`
--

CREATE TABLE IF NOT EXISTS `t_schedules` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'スケジュールID	 スケジュールID',
  `schedule_id` int(11) NOT NULL COMMENT 'スケジュールID	 スケジュールID',
  `group_id` int(11) NOT NULL COMMENT '配信グループID	 配信グループID',
  `user_id` int(11) NOT NULL COMMENT '配信ユーザID	 配信ユーザID',
  `submit_id` int(11) NOT NULL COMMENT '提出ID	 提出ID',
  `sync_id` int(11) NOT NULL COMMENT '同期ID 同期ID',
  `status` int(1) NOT NULL DEFAULT '0' COMMENT '配信状態	 配信状態',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='スケジュールテーブル' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- テーブルの構造 `t_scores`
--

CREATE TABLE IF NOT EXISTS `t_scores` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID	 採点ID	 採点ID',
  `submit_id` int(11) NOT NULL COMMENT '提出ID	 提出ID	 提出ID',
  `subtitle_id` int(11) NOT NULL COMMENT '子ジャンルID	 子ジャンルID	 子ジャンルID',
  `allot` int(3) NOT NULL COMMENT '配点	 配点	 配点',
  `result` int(3) NOT NULL COMMENT '採点結果	 採点結果	 採点結果',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='採点テーブル（設問）	 採点テーブル（設問）' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- テーブルの構造 `t_screensyncs`
--

CREATE TABLE IF NOT EXISTS `t_screensyncs` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '同期ID	 同期ID	 同期ID',
  `document_id` varchar(20) COLLATE utf8_unicode_ci NOT NULL COMMENT '文書ID	 文書ID	 文書ID',
  `sync_mode` int(1) NOT NULL COMMENT '同期モード	 同期モード	 同期モード',
  `sync_state` int(2) NOT NULL COMMENT '同期状態	 同期状態	 同期状態',
  `sync_user_id` int(11) NOT NULL COMMENT '開始ユーザID',
  `start_datetime` datetime NOT NULL COMMENT '配信開始日時',
  `sync_datetime` datetime NOT NULL COMMENT '同期開始日時',
  `end_datetime` datetime NOT NULL COMMENT '配信終了日時',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='画面同期テーブル	 画面同期テーブル' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- テーブルの構造 `t_screensync_files`
--

CREATE TABLE IF NOT EXISTS `t_screensync_files` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '同期ID	 同期ID',
  `submit_id` int(11) NOT NULL,
  `document_id` varchar(20) COLLATE utf8_unicode_ci NOT NULL COMMENT '文書ID	 文書ID',
  `order_no` int(5) NOT NULL DEFAULT '0' COMMENT '履歴NO	 履歴NO',
  `mode` int(2) NOT NULL COMMENT 'モード	 モード',
  `create_user_id` int(11) NOT NULL COMMENT ' 登録ユーザID	 登録ユーザID',
  `user_id` int(11) NOT NULL COMMENT '配信ユーザID	 配信ユーザID',
  `group_id` int(11) NOT NULL COMMENT '配信グループID	 配信グループID',
  `resource_id` int(11) NOT NULL COMMENT '教材ID 教材ID',
  `sync_file_name` varchar(50) COLLATE utf8_unicode_ci NOT NULL COMMENT '同期ファイル名	 同期ファイル名',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='画面同期ファイルテーブル' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- テーブルの構造 `t_screensync_users`
--

CREATE TABLE IF NOT EXISTS `t_screensync_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID	 ID	 ID',
  `sync_id` int(11) NOT NULL COMMENT '同期ID	 同期ID	 同期ID',
  `user_id` int(11) DEFAULT NULL COMMENT '配信ユーザID	 配信ユーザID	 配信ユーザID',
  `group_id` int(11) DEFAULT NULL COMMENT '配信グループID	 配信グループID	 配信グループID',
  PRIMARY KEY (`id`,`sync_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='画面同期ユーザテーブル	 画面同期ユーザテーブル' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- テーブルの構造 `t_sections`
--

CREATE TABLE IF NOT EXISTS `t_sections` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '所属ID	 所属ID',
  `parent_id` int(11) NOT NULL DEFAULT '0' COMMENT '上位グループID	 上位グループID',
  `lft` int(11) NOT NULL DEFAULT '0',
  `rght` int(11) NOT NULL DEFAULT '0',
  `group_id` int(11) NOT NULL COMMENT 'グループID	 グループID',
  `user_id` int(11) NOT NULL COMMENT 'ユーザID	 ユーザID',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='所属テーブル' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- テーブルの構造 `t_submits`
--

CREATE TABLE IF NOT EXISTS `t_submits` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '提出ID	 提出ID',
  `submit_no` varchar(20) COLLATE utf8_unicode_ci NOT NULL COMMENT '提出番号	 提出番号',
  `resource_id` int(11) NOT NULL COMMENT '教材ID	 教材のID',
  `user_id` int(11) NOT NULL COMMENT '提出者ID	 提出者のユーザID',
  `group_id` int(11) NOT NULL COMMENT 'グループID',
  `check_user_id` int(11) NOT NULL COMMENT '採点者ID	 採点者のユーザID',
  `submit_state` int(2) NOT NULL COMMENT '提出状態	 提出状態',
  `check_state` int(2) NOT NULL COMMENT '採点状態',
  `submit_date` datetime NOT NULL DEFAULT '1999-12-31 00:00:00' COMMENT '提出日	 提出日時',
  `check_date` datetime NOT NULL DEFAULT '1999-12-31 00:00:00' COMMENT '採点日	 採点日時',
  `result` int(3) NOT NULL DEFAULT '0' COMMENT '採点結果	 採点結果',
  `unsubscribe` int(1) NOT NULL DEFAULT '0' COMMENT '配信停止	 配信停止',
  `start_datetime` datetime NOT NULL COMMENT '配信開始日時	 配信開始日時',
  `end_datetime` datetime NOT NULL COMMENT '配信終了日時	 配信終了日時',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='提出テーブル' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- ビュー用の代替構造 `v_schedules`
--
CREATE TABLE IF NOT EXISTS `v_schedules` (
`id` int(11)
,`schedule_id` int(11)
,`group_id` int(11)
,`user_id` int(11)
,`submit_id` int(11)
,`status` int(1)
,`schedule_ids` varchar(20)
,`title` varchar(50)
,`detail` text
,`version` int(5)
,`start_datetime` datetime
,`end_datetime` datetime
,`group_ids` varchar(20)
,`group_name` varchar(50)
,`group_type_id` int(11)
,`user_ids` varchar(10)
,`user_name` varchar(50)
,`user_type_id` int(11)
,`parent_address` varchar(255)
,`admin_address` varchar(255)
);
-- --------------------------------------------------------

--
-- ビュー用の代替構造 `v_submits`
--
CREATE TABLE IF NOT EXISTS `v_submits` (
`id` int(11)
,`submit_no` varchar(20)
,`resource_id` int(11)
,`user_id` int(11)
,`group_id` int(11)
,`check_user_id` int(11)
,`submit_state` int(2)
,`check_state` int(2)
,`submit_date` datetime
,`check_date` datetime
,`result` int(3)
,`unsubscribe` int(1)
,`start_datetime` datetime
,`end_datetime` datetime
,`resource_ids` varchar(20)
,`resource_name` varchar(50)
,`group_ids` varchar(20)
,`group_name` varchar(50)
,`group_type_id` int(11)
,`user_ids` varchar(10)
,`user_name` varchar(50)
,`user_type_id` int(11)
,`parent_address` varchar(255)
,`admin_address` varchar(255)
);
-- --------------------------------------------------------

--
-- ビュー用の構造 `v_schedules`
--
DROP TABLE IF EXISTS `v_schedules`;

CREATE ALGORITHM=UNDEFINED DEFINER=`admin`@`%` SQL SECURITY DEFINER VIEW `creaDB`.`v_schedules` AS select `creaDB`.`t_schedules`.`id` AS `id`,`creaDB`.`t_schedules`.`schedule_id` AS `schedule_id`,`creaDB`.`t_schedules`.`group_id` AS `group_id`,`creaDB`.`t_schedules`.`user_id` AS `user_id`,`creaDB`.`t_schedules`.`submit_id` AS `submit_id`,`creaDB`.`t_schedules`.`status` AS `status`,`creaDB`.`m_schedules`.`schedule_ids` AS `schedule_ids`,`creaDB`.`m_schedules`.`title` AS `title`,`creaDB`.`m_schedules`.`detail` AS `detail`,`creaDB`.`m_schedules`.`version` AS `version`,`creaDB`.`m_schedules`.`start_datetime` AS `start_datetime`,`creaDB`.`m_schedules`.`end_datetime` AS `end_datetime`,`creaDB`.`m_groups`.`group_ids` AS `group_ids`,`creaDB`.`m_groups`.`group_name` AS `group_name`,`creaDB`.`m_groups`.`group_type_id` AS `group_type_id`,`creaDB`.`m_users`.`user_ids` AS `user_ids`,`creaDB`.`m_users`.`user_name` AS `user_name`,`creaDB`.`m_users`.`user_type_id` AS `user_type_id`,`creaDB`.`m_users`.`parent_address` AS `parent_address`,`creaDB`.`m_users`.`admin_address` AS `admin_address` from (((`creaDB`.`t_schedules` left join `creaDB`.`m_schedules` on((`creaDB`.`t_schedules`.`schedule_id` = `creaDB`.`m_schedules`.`id`))) left join `creaDB`.`m_groups` on((`creaDB`.`t_schedules`.`group_id` = `creaDB`.`m_groups`.`id`))) left join `creaDB`.`m_users` on((`creaDB`.`t_schedules`.`user_id` = `creaDB`.`m_users`.`id`))) where (`creaDB`.`m_schedules`.`delete_flg` <> 1);
ALTER ALGORITHM=UNDEFINED DEFINER=`admin`@`%` SQL SECURITY DEFINER VIEW `creaDB`.`v_schedules` AS select `creaDB`.`t_schedules`.`id` AS `id`,`creaDB`.`t_schedules`.`schedule_id` AS `schedule_id`,`creaDB`.`t_schedules`.`group_id` AS `group_id`,`creaDB`.`t_schedules`.`user_id` AS `user_id`,`creaDB`.`t_schedules`.`submit_id` AS `submit_id`,`creaDB`.`t_schedules`.`sync_id` AS `sync_id`,`creaDB`.`t_schedules`.`status` AS `status`,`creaDB`.`m_schedules`.`schedule_ids` AS `schedule_ids`,`creaDB`.`m_schedules`.`title` AS `title`,`creaDB`.`m_schedules`.`detail` AS `detail`,`creaDB`.`m_schedules`.`version` AS `version`,`creaDB`.`m_schedules`.`start_datetime` AS `start_datetime`,`creaDB`.`m_schedules`.`end_datetime` AS `end_datetime`,`creaDB`.`m_groups`.`group_ids` AS `group_ids`,`creaDB`.`m_groups`.`group_name` AS `group_name`,`creaDB`.`m_groups`.`group_type_id` AS `group_type_id`,`creaDB`.`m_users`.`user_ids` AS `user_ids`,`creaDB`.`m_users`.`user_name` AS `user_name`,`creaDB`.`m_users`.`user_type_id` AS `user_type_id`,`creaDB`.`m_users`.`parent_address` AS `parent_address`,`creaDB`.`m_users`.`admin_address` AS `admin_address` from (((`creaDB`.`t_schedules` left join `creaDB`.`m_schedules` on((`creaDB`.`t_schedules`.`schedule_id` = `creaDB`.`m_schedules`.`id`))) left join `creaDB`.`m_groups` on((`creaDB`.`t_schedules`.`group_id` = `creaDB`.`m_groups`.`id`))) left join `creaDB`.`m_users` on((`creaDB`.`t_schedules`.`user_id` = `creaDB`.`m_users`.`id`))) where (`creaDB`.`m_schedules`.`delete_flg` <> 1);

-- --------------------------------------------------------

--
-- ビュー用の構造 `v_submits`
--
DROP TABLE IF EXISTS `v_submits`;

CREATE ALGORITHM=UNDEFINED DEFINER=`admin`@`%` SQL SECURITY DEFINER VIEW `creaDB`.`v_submits` AS select `creaDB`.`t_submits`.`id` AS `id`,`creaDB`.`t_submits`.`submit_no` AS `submit_no`,`creaDB`.`t_submits`.`resource_id` AS `resource_id`,`creaDB`.`t_submits`.`user_id` AS `user_id`,`creaDB`.`t_submits`.`group_id` AS `group_id`,`creaDB`.`t_submits`.`check_user_id` AS `check_user_id`,`creaDB`.`t_submits`.`submit_state` AS `submit_state`,`creaDB`.`t_submits`.`check_state` AS `check_state`,`creaDB`.`t_submits`.`submit_date` AS `submit_date`,`creaDB`.`t_submits`.`check_date` AS `check_date`,`creaDB`.`t_submits`.`result` AS `result`,`creaDB`.`t_submits`.`unsubscribe` AS `unsubscribe`,`creaDB`.`t_submits`.`start_datetime` AS `start_datetime`,`creaDB`.`t_submits`.`end_datetime` AS `end_datetime`,`creaDB`.`m_resources`.`resource_ids` AS `resource_ids`,`creaDB`.`m_resources`.`resource_name` AS `resource_name`,`creaDB`.`m_groups`.`group_ids` AS `group_ids`,`creaDB`.`m_groups`.`group_name` AS `group_name`,`creaDB`.`m_groups`.`group_type_id` AS `group_type_id`,`creaDB`.`m_users`.`user_ids` AS `user_ids`,`creaDB`.`m_users`.`user_name` AS `user_name`,`creaDB`.`m_users`.`user_type_id` AS `user_type_id`,`creaDB`.`m_users`.`parent_address` AS `parent_address`,`creaDB`.`m_users`.`admin_address` AS `admin_address` from (((`creaDB`.`t_submits` left join `creaDB`.`m_resources` on((`creaDB`.`t_submits`.`resource_id` = `creaDB`.`m_resources`.`id`))) left join `creaDB`.`m_groups` on((`creaDB`.`t_submits`.`group_id` = `creaDB`.`m_groups`.`id`))) left join `creaDB`.`m_users` on((`creaDB`.`t_submits`.`user_id` = `creaDB`.`m_users`.`id`))) where (`creaDB`.`t_submits`.`check_user_id` <> 0);
