--
-- データベース: `creaDB`
--

--
-- テーブルのデータをダンプしています `attachments`
--


--
-- テーブルのデータをダンプしています `cake_sessions`
--


--
-- テーブルのデータをダンプしています `m_auths`
--

INSERT INTO `m_auths` (`id`, `user_id`, `salt`, `password`) VALUES
(1, 1, '$2a$04$DGwed6g786zvfIH8UAyak3', '$2a$04$DGwed6g786zvfIH8UAyakuNmgJWTdAd6AM5uizQehLnhS3uren8C6');

--
-- テーブルのデータをダンプしています `m_base_colors`
--

INSERT INTO `m_base_colors` (`id`, `color_name`, `class_name`) VALUES
(1, 'レッド', 'red'),
(2, 'ブルー', 'blue'),
(3, 'グレー', 'gray'),
(4, 'オレンジ', 'orange'),
(5, 'グリーン', 'green');

--
-- テーブルのデータをダンプしています `m_groups`
--

INSERT INTO `m_groups` (`id`, `group_ids`, `group_name`, `group_type_id`, `delete_flg`, `ins_date`) VALUES
(1, 'GROUP_0001', 'システム管理', 3, '0', '0000-00-00 00:00:00');

--
-- テーブルのデータをダンプしています `m_group_types`
--

INSERT INTO `m_group_types` (`id`, `group_type_name`) VALUES
(1, '生徒G'),
(2, '教師G'),
(3, '管理G');

--
-- テーブルのデータをダンプしています `m_index_numbers`
--

INSERT INTO `m_index_numbers` (`id`, `resource_idx`, `submit_idx`, `schedule_idx`, `user_idx`, `group_idx`) VALUES
(1, 1, 1, 1, 2, 2);

--
-- テーブルのデータをダンプしています `m_languages`
--

INSERT INTO `m_languages` (`id`, `language_name`) VALUES
(1, '日本語'),
(2, '英語');

--
-- テーブルのデータをダンプしています `m_maildeliveries`
--

INSERT INTO `m_maildeliveries` (`id`, `mail_key`, `delivery_flg`, `from_addr`, `from_name`, `subject`, `template_name`) VALUES
(1, 'user_entry_mail', 1, 'info@crea-toy.co.jp', '株式会社クレア', 'ユーザ登録完了のお知らせ', 'user_entry_mail'),
(2, 'resource_deliver_mail', 1, 'info@crea-toy.co.jp', '株式会社クレア', '教材配信開始のお知らせ', 'resource_deliver_mail'),
(3, 'resource_submit_mail', 1, 'info@crea-toy.co.jp', '株式会社クレア', '教材提出完了のお知らせ', 'resource_submit_mail'),
(4, 'socore_check_mail', 1, 'info@crea-toy.co.jp', '株式会社クレア', '採点完了のお知らせ', 'socore_check_mail'),
(5, 'socore_confirm_mail', 1, 'info@crea-toy.co.jp', '株式会社クレア', '採点確認のお知らせ', 'socore_confirm_mail');

--
-- テーブルのデータをダンプしています `m_resources`
--


--
-- テーブルのデータをダンプしています `m_resource_files`
--


--
-- テーブルのデータをダンプしています `m_resource_styles`
--

INSERT INTO `m_resource_styles` (`id`, `style_name`) VALUES
(1, '1画面表示'),
(3, '1画面表示(リスニング問題)'),
(2, '2画面表示(長文問題)');

--
-- テーブルのデータをダンプしています `m_resource_subjects`
--

INSERT INTO `m_resource_subjects` (`id`, `subject_name`) VALUES
(1, '算数'),
(2, '国語'),
(3, '社会'),
(4, '理科'),
(5, '英語');

--
-- テーブルのデータをダンプしています `m_resource_subtitles`
--


--
-- テーブルのデータをダンプしています `m_schedules`
--


--
-- テーブルのデータをダンプしています `m_settings`
--

INSERT INTO `m_settings` (`id`, `language_id`, `logo_file`, `basecolor_id`) VALUES
(1, 1, 'CREA_logo_web_sw2.png', 1);

--
-- テーブルのデータをダンプしています `m_updates`
--


--
-- テーブルのデータをダンプしています `m_users`
--

INSERT INTO `m_users` (`id`, `user_ids`, `user_name`, `user_type_id`, `parent_address`, `admin_address`, `superuser_flg`, `create_user_id`, `delete_flg`, `ins_date`, `logout_time`) VALUES
(1, 'creaAdmin', 'システム管理者', 9, '', 'info@crea-toy.co.jp', 1, 1, '0', '2014-10-20 11:00:00', '0000-00-00 00:00:00');

--
-- テーブルのデータをダンプしています `m_user_types`
--

INSERT INTO `m_user_types` (`id`, `user_type_name`) VALUES
(1, '生徒'),
(2, '教師'),
(3, '管理者'),
(9, 'システム管理者');

--
-- テーブルのデータをダンプしています `m_words`
--

INSERT INTO `m_words` (`id`, `language_id`, `word_key`, `word`) VALUES
(NULL, 1, 'item00001', '登録'),
(NULL, 1, 'item00002', '検索'),
(NULL, 1, 'item00003', '設定'),
(NULL, 1, 'item00004', 'キャンセル'),
(NULL, 1, 'item00005', 'ログインID'),
(NULL, 1, 'item00006', 'パスワード'),
(NULL, 1, 'item00007', 'ログイン'),
(NULL, 1, 'item00008', '閲覧'),
(NULL, 1, 'item00009', '更新'),
(NULL, 1, 'item00010', '削除'),
(NULL, 1, 'item00011', '一括登録'),
(NULL, 1, 'item00012', '詳細'),
(NULL, 1, 'item00013', '追加'),
(NULL, 1, 'item00014', '子設定'),
(NULL, 1, 'item00015', 'リセット'),
(NULL, 1, 'item00016', '選択'),
(NULL, 1, 'item00017', '一覧選択'),
(NULL, 1, 'item00018', 'ログアウト'),
(NULL, 1, 'item00019', '前回ログアウト'),
(NULL, 1, 'item00020', '登録済みのファイル'),
(NULL, 1, 'item00021', '確認'),
(NULL, 1, 'item00022', '警告'),
(NULL, 1, 'item00023', 'ファイルを選択'),
(NULL, 1, 'code00001', '教科書'),
(NULL, 1, 'code00002', '課題'),
(NULL, 1, 'code00003', 'PDF'),
(NULL, 1, 'code00004', 'MP3'),
(NULL, 1, 'code00005', '未着手'),
(NULL, 1, 'code00006', '着手済み'),
(NULL, 1, 'code00007', '提出済み'),
(NULL, 1, 'code00008', '確認済み'),
(NULL, 1, 'code00009', '未着手'),
(NULL, 1, 'code00010', '着手済み'),
(NULL, 1, 'code00011', '採点済み'),
(NULL, 1, 'code00012', '配信中'),
(NULL, 1, 'code00013', '配信停止'),
(NULL, 1, 'code00014', '配信停止'),
(NULL, 1, 'code00015', '配信待ち'),
(NULL, 1, 'code00016', '配信済み'),
(NULL, 1, 'code00017', '教材別'),
(NULL, 1, 'code00018', 'ユーザ別'),
(NULL, 1, 'code00019', '削除済み'),
(NULL, 1, 'code00020', '一対多'),
(NULL, 1, 'code00021', '一対一'),
(NULL, 1, 'code00022', '停止中'),
(NULL, 1, 'code00023', '同期中'),
(NULL, 1, 'code00024', '同期終了'),
(NULL, 1, 'code00025', 'レッド'),
(NULL, 1, 'code00026', 'ブルー'),
(NULL, 1, 'code00027', 'グレー'),
(NULL, 1, 'code00028', 'オレンジ'),
(NULL, 1, 'code00029', 'グリーン'),
(NULL, 1, 'code00030', '日本語'),
(NULL, 1, 'code00031', '英語'),
(NULL, 1, 'code00032', '通常問題(1画面表示)'),
(NULL, 1, 'code00033', '長文問題(2画面表示)'),
(NULL, 1, 'code00034', 'リスニング問題(1画面表示)'),
(NULL, 1, 'code00035', '生徒G'),
(NULL, 1, 'code00036', '教師G'),
(NULL, 1, 'code00037', '管理者G'),
(NULL, 1, 'code00038', '生徒'),
(NULL, 1, 'code00039', '教師'),
(NULL, 1, 'code00040', '管理者'),
(NULL, 1, 'code00041', 'システム管理者'),
(NULL, 1, 'item00051', '教材登録'),
(NULL, 1, 'item00052', '教材検索'),
(NULL, 1, 'item00053', '教材更新'),
(NULL, 1, 'item00054', '教材削除'),
(NULL, 1, 'item00055', '教材閲覧'),
(NULL, 1, 'item00056', '配信管理'),
(NULL, 1, 'item00057', '配信検索'),
(NULL, 1, 'item00058', 'ユーザ進捗確認'),
(NULL, 1, 'item00059', '課題進捗確認'),
(NULL, 1, 'item00060', '採点状況確認'),
(NULL, 1, 'item00061', 'トップ'),
(NULL, 1, 'item00062', 'スケジュール登録'),
(NULL, 1, 'item00063', 'スケジュール検索'),
(NULL, 1, 'item00064', 'スケジュール更新'),
(NULL, 1, 'item00065', 'スケジュール削除'),
(NULL, 1, 'item00066', 'スケジュール閲覧'),
(NULL, 1, 'item00067', 'ユーザ登録'),
(NULL, 1, 'item00068', 'ユーザ検索'),
(NULL, 1, 'item00069', 'ユーザ更新'),
(NULL, 1, 'item00070', 'ユーザ削除'),
(NULL, 1, 'item00071', 'ユーザ閲覧'),
(NULL, 1, 'item00072', 'グループ登録'),
(NULL, 1, 'item00073', 'グループ検索'),
(NULL, 1, 'item00074', 'グループ更新'),
(NULL, 1, 'item00075', 'グループ削除'),
(NULL, 1, 'item00076', 'グループ閲覧'),
(NULL, 1, 'item00077', 'ログイン認証'),
(NULL, 1, 'item00078', 'ユーザ一括登録'),
(NULL, 1, 'item00079', 'デザイン変更'),
(NULL, 1, 'item00080', '言語設定'),
(NULL, 1, 'item00081', 'ジャンル設定'),
(NULL, 1, 'item00082', '採点設定'),
(NULL, 1, 'item00083', 'アップデート管理'),
(NULL, 1, 'item00084', '成績ランキング'),
(NULL, 1, 'item00085', 'グループ成績一覧'),
(NULL, 1, 'item00086', '教材・配信成績一覧'),
(NULL, 1, 'item00087', '配信対象'),
(NULL, 1, 'item00088', '生徒を指定'),
(NULL, 1, 'item00089', 'グループを指定'),
(NULL, 1, 'item00090', '指定しない'),
(NULL, 1, 'item00091', '画面同期検索'),
(NULL, 1, 'item00092', '画面同期管理'),
(NULL, 1, 'item00093', '成績一覧'),
(NULL, 1, 'item00094', 'ジャンル検索'),
(NULL, 1, 'item00100', '教材'),
(NULL, 1, 'item00101', '進捗確認'),
(NULL, 1, 'item00102', '生徒'),
(NULL, 1, 'item00103', '課題'),
(NULL, 1, 'item00104', '採点状況'),
(NULL, 1, 'item00105', 'スケジュール'),
(NULL, 1, 'item00106', 'ユーザ'),
(NULL, 1, 'item00107', 'グループ'),
(NULL, 1, 'item00108', 'システム'),
(NULL, 1, 'item00109', 'サーバ'),
(NULL, 1, 'item00110', 'ホーム'),
(NULL, 1, 'item00111', 'データ分析'),
(NULL, 1, 'item00112', '管理メニュー'),
(NULL, 1, 'item00200', '最新のお知らせ情報'),
(NULL, 1, 'item00201', '課題の提出状況'),
(NULL, 1, 'item00202', '採点の進捗状況'),
(NULL, 1, 'item00203', '提出数/総数'),
(NULL, 1, 'item00204', '採点数/総数'),
(NULL, 1, 'item00301', 'ユーザID'),
(NULL, 1, 'item00302', 'ユーザ名称'),
(NULL, 1, 'item00303', '保護者メールアドレス'),
(NULL, 1, 'item00304', 'ユーザ区分'),
(NULL, 1, 'item00305', '管理者メールアドレス'),
(NULL, 1, 'item00306', '所属グループ'),
(NULL, 1, 'item00307', '登録ファイル'),
(NULL, 1, 'item00401', 'グループID'),
(NULL, 1, 'item00402', 'グループ名'),
(NULL, 1, 'item00403', 'グループ種別'),
(NULL, 1, 'item00404', '上位グループ'),
(NULL, 1, 'item00405', '所属ユーザ'),
(NULL, 1, 'item00406', '(上位グループなし)'),
(NULL, 1, 'item00501', '教材ID'),
(NULL, 1, 'item00502', '教材名'),
(NULL, 1, 'item00503', '教材タイプ'),
(NULL, 1, 'item00504', 'ジャンル'),
(NULL, 1, 'item00505', 'スタイル'),
(NULL, 1, 'item00506', 'pdfファイル１'),
(NULL, 1, 'item00507', 'pdfファイル２'),
(NULL, 1, 'item00508', 'mp3ファイル'),
(NULL, 1, 'item00509', '配信ID'),
(NULL, 1, 'item00510', '配信日時'),
(NULL, 1, 'item00511', '提出日時'),
(NULL, 1, 'item00512', '採点者'),
(NULL, 1, 'item00513', '配信停止'),
(NULL, 1, 'item00514', '配信対象'),
(NULL, 1, 'item00515', '名称'),
(NULL, 1, 'item00516', '設問数'),
(NULL, 1, 'item00517', '配点'),
(NULL, 1, 'item00518', '繰り返し学習'),
(NULL, 1, 'item00601', 'スケジュールID'),
(NULL, 1, 'item00602', 'タイトル'),
(NULL, 1, 'item00603', '開始日時'),
(NULL, 1, 'item00604', '終了日時'),
(NULL, 1, 'item00605', '詳細'),
(NULL, 1, 'item00701', '配信ID'),
(NULL, 1, 'item00702', '教材名'),
(NULL, 1, 'item00703', '配信日時'),
(NULL, 1, 'item00704', '提出日時'),
(NULL, 1, 'item00705', '採点者'),
(NULL, 1, 'item00706', '配信停止'),
(NULL, 1, 'item00707', '指定なし'),
(NULL, 1, 'item00708', '停止する'),
(NULL, 1, 'item00709', '配信日'),
(NULL, 1, 'item00710', '提出日'),
(NULL, 1, 'item00801', 'ユーザID'),
(NULL, 1, 'item00802', 'ユーザ名'),
(NULL, 1, 'item00803', '教材ID'),
(NULL, 1, 'item00804', '教材名'),
(NULL, 1, 'item00805', '配信ID'),
(NULL, 1, 'item00806', '配信開始日'),
(NULL, 1, 'item00807', '配信期限'),
(NULL, 1, 'item00808', '状態'),
(NULL, 1, 'item00809', '生徒'),
(NULL, 1, 'item00810', 'グループ'),
(NULL, 1, 'item00811', '提出期間'),
(NULL, 1, 'item00812', '未提出'),
(NULL, 1, 'item00813', '採点者'),
(NULL, 1, 'item00901', 'ロゴファイル'),
(NULL, 1, 'item00902', 'ベースカラー'),
(NULL, 1, 'item00903', '言語'),
(NULL, 1, 'item00904', 'ジャンル名'),
(NULL, 1, 'item00905', '子ジャンル名'),
(NULL, 1, 'item00906', 'バージョン'),
(NULL, 1, 'item00907', 'APKファイル'),
(NULL, 1, 'item00908', '概要'),
(NULL, 1, 'item00909', '登録日時'),
(NULL, 1, 'item01001', 'グループ'),
(NULL, 1, 'item01002', 'ユーザ'),
(NULL, 1, 'item01003', '配信期間'),
(NULL, 1, 'item01004', 'ジャンル'),
(NULL, 1, 'item01005', '教材'),
(NULL, 1, 'item01006', 'ユーザID'),
(NULL, 1, 'item01007', 'ユーザ名称'),
(NULL, 1, 'item01008', '教材名'),
(NULL, 1, 'item01009', '最高点'),
(NULL, 1, 'item01010', '最低点'),
(NULL, 1, 'item01011', '平均点'),
(NULL, 1, 'item01012', '回数'),
(NULL, 1, 'item01013', '設問名'),
(NULL, 1, 'item01014', '配点'),
(NULL, 1, 'item01015', '点数'),
(NULL, 1, 'item01016', '順位'),
(NULL, 1, 'item01017', '採点日時'),
(NULL, 1, 'item01101', '同期ファイル'),
(NULL, 1, 'item01102', '同期モード'),
(NULL, 1, 'item01103', '同期状態'),
(NULL, 1, 'item01104', '同期開始日時'),
(NULL, 1, 'item01105', '配信開始日時'),
(NULL, 1, 'item01106', '配信終了日時'),
(NULL, 1, 'item01107', '開始ユーザ'),
(NULL, 1, 'item01108', '配信対象'),
(NULL, 1, 'item01109', '配信期間');
INSERT INTO `m_words` (`id`, `language_id`, `word_key`, `word`) VALUES
(NULL, 2, 'item00001', 'Register'),
(NULL, 2, 'item00002', 'Search'),
(NULL, 2, 'item00003', 'Setting'),
(NULL, 2, 'item00004', 'Cancel'),
(NULL, 2, 'item00005', 'Login ID'),
(NULL, 2, 'item00006', 'Password'),
(NULL, 2, 'item00007', 'Login'),
(NULL, 2, 'item00008', 'View'),
(NULL, 2, 'item00009', 'Update'),
(NULL, 2, 'item00010', 'Delete'),
(NULL, 2, 'item00011', 'Bulk Registration'),
(NULL, 2, 'item00012', 'Details'),
(NULL, 2, 'item00013', 'Addition'),
(NULL, 2, 'item00014', 'Child Setting'),
(NULL, 2, 'item00015', 'Reset'),
(NULL, 2, 'item00016', 'Select'),
(NULL, 2, 'item00017', 'List Choice'),
(NULL, 2, 'item00018', 'Logout'),
(NULL, 2, 'item00019', 'Last Logout'),
(NULL, 2, 'item00020', 'Registered File'),
(NULL, 2, 'item00021', 'Confirm'),
(NULL, 2, 'item00022', 'Alert'),
(NULL, 2, 'item00023', 'Select File'),
(NULL, 2, 'code00001', 'Text Book'),
(NULL, 2, 'code00002', 'Challenge'),
(NULL, 2, 'code00003', 'PDF'),
(NULL, 2, 'code00004', 'MP3'),
(NULL, 2, 'code00005', 'Not Started'),
(NULL, 2, 'code00006', 'Started'),
(NULL, 2, 'code00007', 'Submitted.'),
(NULL, 2, 'code00008', 'Confirmed'),
(NULL, 2, 'code00009', 'Not Started'),
(NULL, 2, 'code00010', 'Started'),
(NULL, 2, 'code00011', 'Marked'),
(NULL, 2, 'code00012', 'Under Distribution'),
(NULL, 2, 'code00013', 'Distribution Cancelled'),
(NULL, 2, 'code00014', 'Distribution Cancelled'),
(NULL, 2, 'code00015', 'Waiting Distribution'),
(NULL, 2, 'code00016', 'Distributed'),
(NULL, 2, 'code00017', 'Material'),
(NULL, 2, 'code00018', 'User'),
(NULL, 2, 'code00019', 'Deleted'),
(NULL, 2, 'code00020', 'Multiple'),
(NULL, 2, 'code00021', 'Single'),
(NULL, 2, 'code00022', 'Under Stop'),
(NULL, 2, 'code00023', 'Under Sync'),
(NULL, 2, 'code00024', 'Finished Sync'),
(NULL, 2, 'code00025', 'Red'),
(NULL, 2, 'code00026', 'Blue'),
(NULL, 2, 'code00027', 'Gray'),
(NULL, 2, 'code00028', 'Orange'),
(NULL, 2, 'code00029', 'Green'),
(NULL, 2, 'code00030', 'Japanese'),
(NULL, 2, 'code00031', 'English'),
(NULL, 2, 'code00032', 'Normal (1 Screen)'),
(NULL, 2, 'code00033', 'Long Sentence (2 screen)'),
(NULL, 2, 'code00034', 'Listening (1 screen)'),
(NULL, 2, 'code00035', 'Student Group'),
(NULL, 2, 'code00036', 'Teacher Group'),
(NULL, 2, 'code00037', 'Administrator Group'),
(NULL, 2, 'code00038', 'Student'),
(NULL, 2, 'code00039', 'Teacher'),
(NULL, 2, 'code00040', 'Admin'),
(NULL, 2, 'code00041', 'System Admin'),
(NULL, 2, 'item00051', 'Register Materials'),
(NULL, 2, 'item00052', 'Search Materials'),
(NULL, 2, 'item00053', 'Update Materials'),
(NULL, 2, 'item00054', 'Delete Materials'),
(NULL, 2, 'item00055', 'Read Materials'),
(NULL, 2, 'item00056', 'Manage Distribution'),
(NULL, 2, 'item00057', 'Search Distribution'),
(NULL, 2, 'item00058', 'User Progress Confirmation'),
(NULL, 2, 'item00059', 'Challenge Progress Confirmation'),
(NULL, 2, 'item00060', 'Mark Progress Confirmation'),
(NULL, 2, 'item00061', 'Top'),
(NULL, 2, 'item00062', 'Register Schedule'),
(NULL, 2, 'item00063', 'Search Schedule'),
(NULL, 2, 'item00064', 'Update Schedule'),
(NULL, 2, 'item00065', 'Delete Schedule'),
(NULL, 2, 'item00066', 'Read Schedule '),
(NULL, 2, 'item00067', 'Register User'),
(NULL, 2, 'item00068', 'Search User'),
(NULL, 2, 'item00069', 'Update User'),
(NULL, 2, 'item00070', 'Delete User'),
(NULL, 2, 'item00071', 'Read User'),
(NULL, 2, 'item00072', 'Register Group'),
(NULL, 2, 'item00073', 'Search Group'),
(NULL, 2, 'item00074', 'Update Group'),
(NULL, 2, 'item00075', 'Delete Group'),
(NULL, 2, 'item00076', 'Read Group'),
(NULL, 2, 'item00077', 'Login'),
(NULL, 2, 'item00078', 'Bulk Registration'),
(NULL, 2, 'item00079', 'Design'),
(NULL, 2, 'item00080', 'Language'),
(NULL, 2, 'item00081', 'Genre'),
(NULL, 2, 'item00082', 'Mark'),
(NULL, 2, 'item00083', 'Update'),
(NULL, 2, 'item00084', 'Ranking'),
(NULL, 2, 'item00085', 'Group Record List'),
(NULL, 2, 'item00086', 'Material Results'),
(NULL, 2, 'item00087', 'Distribution Target'),
(NULL, 2, 'item00088', 'Student'),
(NULL, 2, 'item00089', 'Group'),
(NULL, 2, 'item00090', 'Not Specified'),
(NULL, 2, 'item00091', 'Search Sync'),
(NULL, 2, 'item00092', 'Manage sync'),
(NULL, 2, 'item00093', 'Record List'),
(NULL, 2, 'item00094', 'Search Genre'),
(NULL, 2, 'item00100', 'Materials'),
(NULL, 2, 'item00101', 'Progress Confirm'),
(NULL, 2, 'item00102', 'Student'),
(NULL, 2, 'item00103', 'Challenge'),
(NULL, 2, 'item00104', 'Marking Status'),
(NULL, 2, 'item00105', 'Schedule'),
(NULL, 2, 'item00106', 'User'),
(NULL, 2, 'item00107', 'Group'),
(NULL, 2, 'item00108', 'System'),
(NULL, 2, 'item00109', 'Server'),
(NULL, 2, 'item00110', 'Home'),
(NULL, 2, 'item00111', 'Records Analysis'),
(NULL, 2, 'item00112', 'Administration Menu'),
(NULL, 2, 'item00200', 'The latest information'),
(NULL, 2, 'item00201', 'The submission situation of the problem'),
(NULL, 2, 'item00202', 'Marking Progress'),
(NULL, 2, 'item00203', 'Num/User'),
(NULL, 2, 'item00204', 'Num/User'),
(NULL, 2, 'item00301', 'User ID'),
(NULL, 2, 'item00302', 'User Name'),
(NULL, 2, 'item00303', 'Family\'s E-mail'),
(NULL, 2, 'item00304', 'User Type'),
(NULL, 2, 'item00305', 'Admin E-mail'),
(NULL, 2, 'item00306', 'Group'),
(NULL, 2, 'item00307', 'RegistrationFile'),
(NULL, 2, 'item00401', 'Group ID'),
(NULL, 2, 'item00402', 'Group Name'),
(NULL, 2, 'item00403', 'Group Type'),
(NULL, 2, 'item00404', 'Upper Group'),
(NULL, 2, 'item00405', 'Belonging User'),
(NULL, 2, 'item00406', '(Without Upper Group)'),
(NULL, 2, 'item00501', 'Material ID'),
(NULL, 2, 'item00502', 'Material Name'),
(NULL, 2, 'item00503', 'Type'),
(NULL, 2, 'item00504', 'Genre'),
(NULL, 2, 'item00505', 'Style'),
(NULL, 2, 'item00506', 'PDF File 1'),
(NULL, 2, 'item00507', 'PDF File 2'),
(NULL, 2, 'item00508', 'MP3 File'),
(NULL, 2, 'item00509', 'Distribution ID'),
(NULL, 2, 'item00510', 'Distribution Date'),
(NULL, 2, 'item00511', 'Submission Date'),
(NULL, 2, 'item00512', 'Marker'),
(NULL, 2, 'item00513', 'Stop Distribution'),
(NULL, 2, 'item00514', 'Distribution Target'),
(NULL, 2, 'item00515', 'Name'),
(NULL, 2, 'item00516', 'The Number of Questions'),
(NULL, 2, 'item00517', 'Allotment'),
(NULL, 2, 'item00518', 'Repeat Learning'),
(NULL, 2, 'item00601', 'Schedule ID'),
(NULL, 2, 'item00602', 'Schedule Title'),
(NULL, 2, 'item00603', 'Start Date'),
(NULL, 2, 'item00604', 'End Date'),
(NULL, 2, 'item00605', 'Details'),
(NULL, 2, 'item00701', 'Distribution ID'),
(NULL, 2, 'item00702', 'Material Name'),
(NULL, 2, 'item00703', 'Distribution Date'),
(NULL, 2, 'item00704', 'Submission Date'),
(NULL, 2, 'item00705', 'Marker'),
(NULL, 2, 'item00706', 'Stop Distribution'),
(NULL, 2, 'item00707', 'Not Specified'),
(NULL, 2, 'item00708', 'Stop'),
(NULL, 2, 'item00709', 'Start Date'),
(NULL, 2, 'item00710', 'End Date'),
(NULL, 2, 'item00801', 'User ID'),
(NULL, 2, 'item00802', 'User Name'),
(NULL, 2, 'item00803', 'Material ID'),
(NULL, 2, 'item00804', 'Material Name'),
(NULL, 2, 'item00805', 'Distribution ID'),
(NULL, 2, 'item00806', 'Distribution Start Date'),
(NULL, 2, 'item00807', 'Distribution Time Limit'),
(NULL, 2, 'item00808', 'Status'),
(NULL, 2, 'item00809', 'Student'),
(NULL, 2, 'item00810', 'Group'),
(NULL, 2, 'item00811', 'Period for Submission'),
(NULL, 2, 'item00812', 'Unsubmitted'),
(NULL, 2, 'item00813', 'Marker'),
(NULL, 2, 'item00901', 'Logo File'),
(NULL, 2, 'item00902', 'Base Color'),
(NULL, 2, 'item00903', 'Language'),
(NULL, 2, 'item00904', 'Genre'),
(NULL, 2, 'item00905', 'Child Genre'),
(NULL, 2, 'item00906', 'Version'),
(NULL, 2, 'item00907', 'APK File'),
(NULL, 2, 'item00908', 'Outline'),
(NULL, 2, 'item00909', 'Registration Date'),
(NULL, 2, 'item01001', 'Group'),
(NULL, 2, 'item01002', 'User'),
(NULL, 2, 'item01003', 'Period for Distribution'),
(NULL, 2, 'item01004', 'Genre'),
(NULL, 2, 'item01005', 'Material'),
(NULL, 2, 'item01006', 'User ID'),
(NULL, 2, 'item01007', 'User Name'),
(NULL, 2, 'item01008', 'Material Name'),
(NULL, 2, 'item01009', 'Highest Record'),
(NULL, 2, 'item01010', 'Lowest Record'),
(NULL, 2, 'item01011', 'Average Record'),
(NULL, 2, 'item01012', 'Frequency'),
(NULL, 2, 'item01013', 'Question Name'),
(NULL, 2, 'item01014', 'Allotment'),
(NULL, 2, 'item01015', 'Point'),
(NULL, 2, 'item01016', 'Ranking'),
(NULL, 2, 'item01017', 'Marked Date'),
(NULL, 2, 'item01101', 'Sync File'),
(NULL, 2, 'item01102', 'Sync Mode'),
(NULL, 2, 'item01103', 'Sync Status'),
(NULL, 2, 'item01104', 'Sync Start Date'),
(NULL, 2, 'item01105', 'Distribution Start Date'),
(NULL, 2, 'item01106', 'Distribution End Date'),
(NULL, 2, 'item01107', 'Start User'),
(NULL, 2, 'item01108', 'Distribution Target'),
(NULL, 2, 'item01109', 'Period for Distribution');
INSERT INTO `m_words` (`id`, `language_id`, `word_key`, `word`) VALUES
(NULL, 1, 'error00001', 'ユーザＩＤとパスワードを入力して下さい。'),
(NULL, 1, 'error00002', 'ユーザＩＤかパスワードが違います。'),
(NULL, 1, 'error00003', '%sが入力されていません。'),
(NULL, 1, 'error00004', '%sの入力形式が正しくありません。'),
(NULL, 1, 'error00005', '%sの値が有効ではありません。'),
(NULL, 1, 'error00006', '%sの値が既に登録されています。'),
(NULL, 1, 'info00001', '入力の内容を登録しますか。'),
(NULL, 1, 'info00002', '入力の内容で更新しますか。'),
(NULL, 1, 'warm00001', '※このユーザを更新することはできません'),
(NULL, 1, 'info00003', '表示の内容で削除しますか。'),
(NULL, 1, 'warm00002', '※このユーザを削除することはできません'),
(NULL, 1, 'info00005', 'パスワードの値をリセットしますか。'),
(NULL, 1, 'error00007', '全角・半角%s文字以内で入力して下さい。'),
(NULL, 1, 'error00008', '半角%s文字以内で入力して下さい。'),
(NULL, 1, 'error00009', '%sを後に設定してください。'),
(NULL, 1, 'error00011', '%sと異なる所属グループに設定できません。'),
(NULL, 1, 'error00012', '%sより下のグループ種別を設定できません。'),
(NULL, 1, 'error00022', 'メールの送信に失敗しました。'),
(NULL, 1, 'error00023', '%sより前の日時を設定できません。'),
(NULL, 1, 'warm00003', '※所属ユーザの設定は%sからおこなってください。'),
(NULL, 1, 'error00014', '関連する所属グループ・ユーザが登録されているため、削除できません。'),
(NULL, 1, 'error00015', '関連するスケジュールが登録されているため、削除できません。'),
(NULL, 1, 'error00016', '関連する教材配信が登録されているため、削除できません。'),
(NULL, 1, 'error00017', '関連する同期配信が登録されているため、削除できません。'),
(NULL, 1, 'error00018', '選択の同期モードでは、複数のユーザを選択することはできません。'),
(NULL, 1, 'warn00004', '採点設定でエラーの項目が存在しています。'),
(NULL, 1, 'info00004', '入力の内容で一度更新する必要があります。よろしいですか。'),
(NULL, 1, 'error00013', '%sは必須となります。'),
(NULL, 1, 'error00021', '%sから%sの値を入力してください'),
(NULL, 1, 'error00010', '有効な日付を入力してください。'),
(NULL, 1, 'info00006', '対象課題の提出状態をリセットしますか。'),
(NULL, 1, 'error0019', 'APKファイルではありません。'),
(NULL, 1, 'error0020', 'ファイルサイズが%sMBをこえています。'),
(NULL, 1, 'error00201', 'AJAX処理に必要な項目が設定されていません。'),
(NULL, 1, 'error00202', 'フォルダの作成に失敗しました'),
(NULL, 1, 'error00203', 'ユーザ情報が見つかりません'),
(NULL, 1, 'error00204', 'ユーザ情報が一致しません'),
(NULL, 1, 'error00205', 'フォルダの削除に失敗しました'),
(NULL, 1, 'error00206', 'フォルダの読み込みに失敗しました'),
(NULL, 1, 'error00207', 'ファイルのコピーに失敗しました'),
(NULL, 1, 'error00208', 'ファイルの読み込みに失敗しました'),
(NULL, 1, 'error00209', 'ファイルの削除に失敗しました'),
(NULL, 1, 'error00210', 'ファイルの保存に失敗しました'),
(NULL, 1, 'error00211', 'ログイン認証に失敗しました'),
(NULL, 1, 'error00212', '教材マスタが見つかりません'),
(NULL, 1, 'error00213', 'スケジュールマスタが見つかりません'),
(NULL, 1, 'error00214', 'スケジュール情報の保存に失敗しました。システム管理者にご連絡ください'),
(NULL, 1, 'error00215', 'スケジュール情報が見つかりません'),
(NULL, 1, 'error00216', '画面同期情報が見つかりません'),
(NULL, 1, 'error00217', '指定の画面同期を開始できません。'),
(NULL, 1, 'error00218', '指定の画面同期は終了しました。'),
(NULL, 1, 'error00219', '指定のユーザで開始・終了操作をおこなって下さい'),
(NULL, 1, 'error00220', '提出テーブル情報の保存に失敗しました。システム管理者にご連絡ください'),
(NULL, 1, 'error00221', '採点テーブル情報の保存に失敗しました。システム管理者にご連絡ください'),
(NULL, 1, 'error00222', '提出テーブル情報が見つかりません');
INSERT INTO `m_words` (`id`, `language_id`, `word_key`, `word`) VALUES
(NULL, 2, 'error00001', 'Please input an user identification and a password.'),
(NULL, 2, 'error00002', 'An user identification or a password is different.'),
(NULL, 2, 'error00003', '%s but it is not input.'),
(NULL, 2, 'error00004', 'The input form of %s is not right.'),
(NULL, 2, 'error00005', 'The price of %s is not effective.'),
(NULL, 2, 'error00006', 'The price of %s is registered already.'),
(NULL, 2, 'info00001', 'Are the contents of input registered?'),
(NULL, 2, 'info00002', 'Is it renewed by the contents of input?'),
(NULL, 2, 'warm00001', '※ It is not possible to renew this user.'),
(NULL, 2, 'info00003', 'Is it eliminated by the contents of indication?'),
(NULL, 2, 'warm00002', '※ It is not possible to eliminate this user.'),
(NULL, 2, 'info00005', 'Is the price of the password reset?'),
(NULL, 2, 'error00007', 'Please input %s characters or less.'),
(NULL, 2, 'error00008', 'Please input %s characters or less.'),
(NULL, 2, 'error00009', 'Please set after %s.'),
(NULL, 2, 'error00011', 'Can not set %s different group membership.'),
(NULL, 2, 'error00012', 'Can not set the group type below %s.'),
(NULL, 2, 'error00022', 'Failed in a send mail.'),
(NULL, 2, 'error00023', 'Can not set the previous date and time than %s.'),
(NULL, 2, 'warm00003', '※ Affiliation user settings please be done from %s.'),
(NULL, 2, 'error00014', 'For relevant group user is registered , it can not be deleted .'),
(NULL, 2, 'error00015', 'For the relevant schedule has been registered , it can not be deleted .'),
(NULL, 2, 'error00016', 'For relevant teaching materials delivery has been registered , it can not be deleted .'),
(NULL, 2, 'error00017', 'For related synchronization delivery has been registered , it can not be deleted .'),
(NULL, 2, 'error00018', 'In sync mode of selection , it is not possible to select multiple users .'),
(NULL, 2, 'warn00004', 'Error item exists in scoring settings .'),
(NULL, 2, 'info00004', 'It is necessary to renew once by the contents of input. Would that be OK?'),
(NULL, 2, 'error00013', '%s is required .'),
(NULL, 2, 'error00021', 'Please enter between from %s to %s'),
(NULL, 2, 'error00010', 'Please input valid date.'),
(NULL, 2, 'info00006', 'Is the submission state of the target problem reset?'),
(NULL, 2, 'error0019', 'This is not APK file'),
(NULL, 2, 'error0020', 'A File size exceeds %sMB.'),
(NULL, 2, 'error00201', 'The item necessary to AJAX processing is not established.'),
(NULL, 2, 'error00202', 'I failed in making of a folder.'),
(NULL, 2, 'error00203', 'The user information is not found.'),
(NULL, 2, 'error00204', 'The user information is not identical.'),
(NULL, 2, 'error00205', 'I failed in elimination of a folder.'),
(NULL, 2, 'error00206', 'I failed in reading of a folder.'),
(NULL, 2, 'error00207', 'I failed in a copy of a file.'),
(NULL, 2, 'error00208', 'I failed in reading of a file.'),
(NULL, 2, 'error00209', 'I failed in elimination of a file.'),
(NULL, 2, 'error00210', 'I failed in preservation of a file.'),
(NULL, 2, 'error00211', 'I failed in login authentication.'),
(NULL, 2, 'error00212', 'Resouce master is not found.'),
(NULL, 2, 'error00213', 'Schedule master is not found.'),
(NULL, 2, 'error00214', 'I failed in preservation of scheduling information. Please contact a system administrator.'),
(NULL, 2, 'error00215', 'Scheduling information is not found.'),
(NULL, 2, 'error00216', 'Screen synchronization information is not found.'),
(NULL, 2, 'error00217', 'A screen same year of designation has not been begun.'),
(NULL, 2, 'error00218', 'A delivery period in a screen same year of designation has passed.'),
(NULL, 2, 'error00219', 'Please do starting and end operation by the user of designation.'),
(NULL, 2, 'error00220', 'I failed to save the submission information. Please contact a system administrator.'),
(NULL, 2, 'error00221', 'I failed to save the scoring information. Please contact a system administrator.'),
(NULL, 2, 'error00222', 'Submission information is not found.');

--
-- テーブルのデータをダンプしています `pdf_posts`
--


--
-- テーブルのデータをダンプしています `posts`
--


--
-- テーブルのデータをダンプしています `t_logs`
--


--
-- テーブルのデータをダンプしています `t_schedules`
--


--
-- テーブルのデータをダンプしています `t_scores`
--


--
-- テーブルのデータをダンプしています `t_screensyncs`
--


--
-- テーブルのデータをダンプしています `t_screensync_files`
--


--
-- テーブルのデータをダンプしています `t_screensync_users`
--


--
-- テーブルのデータをダンプしています `t_sections`
--

INSERT INTO `t_sections` (`id`, `parent_id`, `lft`, `rght`, `group_id`, `user_id`) VALUES
(1, 0, 1, 4, 1, 0),
(2, 1, 2, 3, 1, 1);

--
-- テーブルのデータをダンプしています `t_submits`
--
