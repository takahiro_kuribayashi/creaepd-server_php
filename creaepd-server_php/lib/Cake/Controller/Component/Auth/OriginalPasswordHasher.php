<?php
App::uses('AbstractPasswordHasher', 'Controller/Component/Auth');

/**
 * Original password hashing class.
 *
 * @package       Cake.Controller.Component.Auth
 */
class OriginalPasswordHasher extends AbstractPasswordHasher {

/**
 * Config for this object.
 *
 * @var array
 */
	protected $_config = array('salt' => null);

	public function setSalt($salt=null) {
		$this->_config['salt'] = $salt;
	}

	public function getSalt() {
		return $this->_config['salt'];
	}

/**
 * Generates password hash.
 *
 * @param string $password Plain text password to hash.
 * @return string Password hash
 * @link http://book.cakephp.org/2.0/en/core-libraries/components/authentication.html#using-bcrypt-for-passwords
 */
	public function hash($password) {
		return crypt($password, $this->_config['salt']);
	}

/**
 * Check hash. Generate hash for user provided password and check against existing hash.
 *
 * @param string $password Plain text password to hash.
 * @param string $hashedPassword Existing hashed password.
 * @return bool True if hashes match else false.
 */
	public function check($password, $hashedPassword) {
		return $hashedPassword === $this->hash($password);
	}

}
